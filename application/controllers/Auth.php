<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Auth extends CI_Controller {

    // Public Variable
    public $custom_log, $session, $response, $fileUpload;

    public function __construct() {
        parent::__construct();

        // Load Model
        $this->load->model("MUsers");
        $this->load->model("MMedias");
        $this->load->model("UUser");

        // Load Helper
        $this->session = new Session_helper();
        $this->response = new Response_helper();
        $this->fileUpload = new Upload_file_helper(
            array(
                "file_type" => array(
                    "png",
                    "jpg",
                    "jpeg"
                ),
                "max_size"  => 200000000
            )
        );

        // Check is already authenticate
        if ($this->session->check_session("auth")) {
            redirect(base_url("index.php/dashboard"));
        }
    }

    // Login
    public function login() {
        // Setup Meta
        $this->meta["title"] = "Authentication | Login";
        $this->meta["description"] = "Jaket BUS is the apps for booking bus everytime";
        $this->meta["robots"] = "JAKET BUS, Shuttle, Modernesia";

        // Load Sign in
        $this->load->view('auth/login', array(
            "meta" => $this->meta
        ));
    }

    // Do Login
    public function doLogin() {
        $email = $this->input->post("email", TRUE) ?: "";
        $password = $this->input->post("password", TRUE) ?: "";

        if (empty($email) || empty($password)) 
            echo ("
                <p>Gagal login, email dan password tidak boleh kosong</p>
                <a href='".base_url('index.php/auth/login')."'>Kembali login</a>
            ");
        else {
            $checkAlready = $this->MUsers->getWhere(
                array(
                    "email" => $email,
                    "password" => md5($password)
                )
            );

            if (count($checkAlready) > 0) {
                $user = $checkAlready[0];
                $checkLevel = $this->UUser->getWhere(
                    array("id_m_users" => $user["id"])
                );
                if (count($checkLevel) > 0) {
                    $utility = $checkLevel[0];
                    if ($utility["level"] == 2) {
                        $face = $this->MMedias->getWhere(
                            array("id" => $utility["id_face"])
                        );
                        if (count($face) > 0) {
                            $face = $face[0];
                            $utility["face"] = $face;
                            $user["utility"] = $utility;
                            $data = $user;
                            $this->session->add_session("auth", $data);
                            redirect(base_url("index.php/auth/login"));
                        } else {
                            echo ("
                                <p>Gagal login, terjadi masalah</p>
                                <a href='".base_url('index.php/auth/login')."'>Kembali login</a>
                            ");
                        }
                    } else {
                        echo ("
                            <p>Gagal login, akses dilarang</p>
                            <a href='".base_url('index.php/auth/login')."'>Kembali login</a>
                        ");
                    }
                } else {
                    echo ("
                        <p>Gagal login, email dan password tidak ditemukan</p>
                        <a href='".base_url('index.php/auth/login')."'>Kembali login</a>
                    ");
                }
            } else {
                echo ("
                    <p>Gagal login, email dan password tidak ditemukan</p>
                    <a href='".base_url('index.php/auth/login')."'>Kembali login</a>
                ");
            }
        }
    }
}