<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Welcome extends CI_Controller {

    public function __construct() {
		parent::__construct();

        $this->load->model("MPages");
	}

	public function index()
	{
		redirect(base_url('index.php/auth/login'));
	}

	public function page($id) {
        // Setup Meta
        $this->meta["title"] = "Dashboard | Pulo Gebang";
        $this->meta["description"] = "Jaket BUS is the apps for booking bus everytime";
		$this->meta["robots"] = "JAKET BUS, Shuttle, Modernesia";
		
		$data = $this->MPages->getWhere(array("id" => $id), 0, "id", "DESC");
		if (count($data) > 0)
			$data = $data[0];
		// Load Sign in
        $this->load->view('page', array(
            "meta" => $this->meta,
            "data" => $data
        ));
	}
}
