<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard extends CI_Controller {

    // Public Variable
    public $custom_curl, $session, $response, $fileUpload;
    public $menuSidebar;
    public $auth;

    public function __construct() {
        parent::__construct();

        // Load Model
        $this->load->model("MUsers");
        $this->load->model("MTronWallet");
        $this->load->model("MPuloGebang");
        $this->load->model("MNotification");
        $this->load->model("MMedias");
        $this->load->model("MPages");
        $this->load->model("MFeatures");
        $this->load->model("MExperiences");
        $this->load->model("UUserOrderTicket");
        $this->load->model("UUser");

        // Load Helper
        $this->session = new Session_helper();
        $this->response = new Response_helper();
        $this->custom_curl = new Mycurl_helper("");
        $this->fileUpload = new Upload_file_helper(
            array(
                "file_type" => array(
                    "png",
                    "jpg",
                    "jpeg"
                ),
                "max_size"  => 200000000
            )
        );

        // Check is already authenticate
        if (!$this->session->check_session("auth")) {
            redirect(base_url("index.php/auth/login"));
        } else {
            $this->auth = $this->session->get_session("auth");
        }

        // Setup Sidebar
        $this->load->model("statics/Sidebar");
        $this->menuSidebar = $this->Sidebar->getMenus();
    }

    // Do Logout
    public function doLogout() {
        if ($this->session->check_session("auth")) {
            $this->session->remove_session("auth");
            $this->session->destroy_session();
            redirect(base_url("index.php/auth/login"));
        }
    }

    // Home
    public function index() {
        // Setup Meta
        $this->meta["title"] = "Dashboard | Home";
        $this->meta["description"] = "Jaket BUS is the apps for booking bus everytime";
        $this->meta["robots"] = "JAKET BUS, Shuttle, Modernesia";

        $sidebar = $this->menuSidebar;
        $sidebar[0]["items"][0]["is_active"] = true;

        $data = array();
        $data["transaction"] = $this->getTransactionsPagination();
        $data["notification"] = $this->getNotificationPagination();
        $data["user"] = $this->getUserPagination();
        $data["counter"] = $this->getPriceTransaction();

        // Load Sign in
        $this->load->view('dashboard/home', array(
            "meta" => $this->meta,
            "sidebar" => $sidebar,
            "data" => $data,
            "auth" => $this->auth
        ));
    }

    // Transaction
    public function transaction() {
        // Setup Meta
        $this->meta["title"] = "Dashboard | Transaction";
        $this->meta["description"] = "Jaket BUS is the apps for booking bus everytime";
        $this->meta["robots"] = "JAKET BUS, Shuttle, Modernesia";

        $sidebar = $this->menuSidebar;
        $sidebar[0]["items"][1]["is_active"] = true;

        $data = array();
        $data["transaction"] = $this->getTransactionsPagination();
        $data["count_transaction"] = $this->UUserOrderTicket->getCount();
        $data["detail_transaction"] = $this->getDetailTransaction();

        // Load Sign in
        $this->load->view('dashboard/transaction', array(
            "meta" => $this->meta,
            "sidebar" => $sidebar,
            "data" => $data,
            "auth" => $this->auth,
        ));
    }

    // Do Download Transaction
    public function downloadTransaction($type="1") {
        $date_to = $this->input->get("date_to", TRUE) ?: "";
        $date_from = $this->input->get("date_from", TRUE) ?: "";

        $where = array();

        if (!empty($date_from)) $where["created_at >="] = $date_from;
        if (!empty($date_to)) $where["created_at <="] = $date_to;

        $title = "";
        if ($type == 1 || $type == "1") {
            $where["payment_status"] = "paid";
            $title = "Semua Transaksi Terbayar";
        }
        else if ($type == 2 || $type == "2") {
            $where["payment_status"] = "unpaid";
            $title = "Semua Transaksi Belum Terbayar";
        }
        else if ($type == 3 || $type == "3") {
            $where["payment_status"] = "paid";
            $where["payment_method"] = "balance";
            $title = "Transaksi DompetJak Terbayar";
        }
        else if ($type == 4 || $type == "4") {
            $where["payment_status"] = "unpaid";
            $where["payment_method"] = "balance";
            $title = "Transaksi DompetJak Belum Terbayar";
        }
        else if ($type == 5 || $type == "5") {
            $where["payment_status"] = "paid";
            $where["payment_method"] = "indomaret";
            $title = "Transaksi Indomaret Terbayar";
        }
        else if ($type == 6 || $type == "6") {
            $where["payment_status"] = "unpaid";
            $where["payment_method"] = "indomaret";
            $title = "Transaksi Indomaret Belum Terbayar";
        }
        else if ($type == 7 || $type == "7") {
            $where["payment_status"] = "paid";
            $where["payment_method"] = "alfamart";
            $title = "Transaksi Alfamart Terbayar";
        }
        else if ($type == 8 || $type == "8") {
            $where["payment_status"] = "unpaid";
            $where["payment_method"] = "alfamart";
            $title = "Transaksi Alfamart Belum Terbayar";
        }
        $data = $this->UUserOrderTicket->getPaginationWhere(
            $where,
            "id",
            "DESC"
        );
        // die(json_encode($data));
        $this->downloadToExcel($data, array(
            "creator" => "JaketBus",
            "modify_by" => "Administrator JaketBus",
            "title" => $title,
            "subject" => "TRANSAKSI",
            "description" => "Menampilkan daftar transaksi yang sukses dari yang terbaru hingga ke awal",
            "keywords" => "Transaksi, Sukses, Terbaru",
            "heading" => $title,
            "heading_merge" => "A1:AH1",
            "file_name" => $title." - ".date("YmdHis"),
            "cols" => array(
                array(
                    "col" => array(
                        "index" => "A",
                        "label" => "NO",
                        "length" => 10
                    )
                ),
                array(
                    "col" => array(
                        "index" => "B",
                        "label" => "ID",
                        "length" => 10
                    )
                ),
                array(
                    "col" => array(
                        "index" => "C",
                        "label" => "ID USER ORDER",
                        "length" => 32
                    )
                ),
                array(
                    "col" => array(
                        "index" => "D",
                        "label" => "ID USER",
                        "length" => 32
                    )
                ),
                array(
                    "col" => array(
                        "index" => "E",
                        "label" => "ID TIKET",
                        "length" => 10
                    )
                ),
                array(
                    "col" => array(
                        "index" => "F",
                        "label" => "NAMA PENUMPANG",
                        "length" => 32
                    )
                ),
                array(
                    "col" => array(
                        "index" => "G",
                        "label" => "GENDER",
                        "length" => 32
                    )
                ),
                array(
                    "col" => array(
                        "index" => "H",
                        "label" => "NO TELEPON",
                        "length" => 32
                    )
                ),array(
                    "col" => array(
                        "index" => "I",
                        "label" => "PNR",
                        "length" => 32
                    )
                ),
                array(
                    "col" => array(
                        "index" => "J",
                        "label" => "ID KURSI",
                        "length" => 10
                    )
                ),
                array(
                    "col" => array(
                        "index" => "K",
                        "label" => "BOARDING?",
                        "length" => 32
                    )
                ),
                array(
                    "col" => array(
                        "index" => "L",
                        "label" => "TANGGAL BOARDING",
                        "length" => 32
                    )
                ),
                array(
                    "col" => array(
                        "index" => "M",
                        "label" => "TANGGAL KEBERANGKATAN",
                        "length" => 32
                    )
                ),array(
                    "col" => array(
                        "index" => "N",
                        "label" => "WAKTU KEBERANGKATAN",
                        "length" => 32
                    )
                ),
                array(
                    "col" => array(
                        "index" => "O",
                        "label" => "HARGA",
                        "length" => 32
                    )
                ),
                array(
                    "col" => array(
                        "index" => "P",
                        "label" => "ID SCHEDULE",
                        "length" => 10
                    )
                ),
                array(
                    "col" => array(
                        "index" => "Q",
                        "label" => "ID ASAL",
                        "length" => 10
                    )
                ),
                array(
                    "col" => array(
                        "index" => "R",
                        "label" => "ID TUJUAN",
                        "length" => 10
                    )
                ),array(
                    "col" => array(
                        "index" => "S",
                        "label" => "KATEGORI",
                        "length" => 32
                    )
                ),
                array(
                    "col" => array(
                        "index" => "T",
                        "label" => "NAMA PO",
                        "length" => 32
                    )
                ),
                array(
                    "col" => array(
                        "index" => "U",
                        "label" => "ID PO",
                        "length" => 10
                    )
                ),
                array(
                    "col" => array(
                        "index" => "V",
                        "label" => "TUJUAN",
                        "length" => 32
                    )
                ),
                array(
                    "col" => array(
                        "index" => "W",
                        "label" => "ASAL",
                        "length" => 32
                    )
                ),array(
                    "col" => array(
                        "index" => "X",
                        "label" => "NOMOR KURSI",
                        "length" => 32
                    )
                ),
                array(
                    "col" => array(
                        "index" => "Y",
                        "label" => "TANGGAL BELI",
                        "length" => 32
                    )
                ),
                array(
                    "col" => array(
                        "index" => "Z",
                        "label" => "TANGGAL UPDATE",
                        "length" => 32
                    )
                ),
                array(
                    "col" => array(
                        "index" => "AA",
                        "label" => "STATUS PEMBAYARAN",
                        "length" => 32
                    )
                ),
                array(
                    "col" => array(
                        "index" => "AB",
                        "label" => "JENIS PEMBAYARAN",
                        "length" => 32
                    )
                ),array(
                    "col" => array(
                        "index" => "AC",
                        "label" => "DATA PEMBAYARAN",
                        "length" => 32
                    )
                ),
                array(
                    "col" => array(
                        "index" => "AD",
                        "label" => "PAYMENT FEE",
                        "length" => 32
                    )
                ),
                array(
                    "col" => array(
                        "index" => "AE",
                        "label" => "TANGGAL EXPIRED PEMBAYARAN",
                        "length" => 32
                    )
                ),
                array(
                    "col" => array(
                        "index" => "AF",
                        "label" => "KODE PEMBAYARAN",
                        "length" => 32
                    )
                ),
                array(
                    "col" => array(
                        "index" => "AG",
                        "label" => "TANGGAL PEMBAYARAN",
                        "length" => 32
                    )
                ),
                array(
                    "col" => array(
                        "index" => "AH",
                        "label" => "REF PEMBAYARAN",
                        "length" => 32
                    )
                )
            )
        ));
    }

    private function downloadToExcel($data=array(), $components=array()) {
        // Load plugin PHPExcel nya
        include APPPATH.'third_party/PHPExcel/PHPExcel.php';

        // Panggil class PHPExcel nya
        $excel = new PHPExcel();

        // Settingan awal fil excel
        $excel->getProperties()->setCreator($components["creator"])
        ->setLastModifiedBy($components["modify_by"])
        ->setTitle($components["title"])
        ->setSubject($components["subject"])
        ->setDescription($components["description"])
        ->setKeywords($components["keywords"]);

        // Buat sebuah variabel untuk menampung pengaturan style dari header tabel
        $style_col = array(
            'font' => array('bold' => true), // Set font nya jadi bold
            'alignment' => array(
                'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER, // Set text jadi ditengah secara horizontal (center)
                'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER // Set text jadi di tengah secara vertical (middle)
            ),
            'borders' => array(
                'top' => array('style'  => PHPExcel_Style_Border::BORDER_THIN), // Set border top dengan garis tipis
                'right' => array('style'  => PHPExcel_Style_Border::BORDER_THIN),  // Set border right dengan garis tipis
                'bottom' => array('style'  => PHPExcel_Style_Border::BORDER_THIN), // Set border bottom dengan garis tipis
                'left' => array('style'  => PHPExcel_Style_Border::BORDER_THIN) // Set border left dengan garis tipis
            )
        );

        // Buat sebuah variabel untuk menampung pengaturan style dari isi tabel
        $style_row = array(
            'alignment' => array(
                'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER // Set text jadi di tengah secara vertical (middle)
            ),
            'borders' => array(
                'top' => array('style'  => PHPExcel_Style_Border::BORDER_THIN), // Set border top dengan garis tipis
                'right' => array('style'  => PHPExcel_Style_Border::BORDER_THIN),  // Set border right dengan garis tipis
                'bottom' => array('style'  => PHPExcel_Style_Border::BORDER_THIN), // Set border bottom dengan garis tipis
                'left' => array('style'  => PHPExcel_Style_Border::BORDER_THIN) // Set border left dengan garis tipis
            )
        );

        $excel->setActiveSheetIndex(0)->setCellValue('A1', $components["heading"]); // Set kolom A1 dengan tulisan "DATA SISWA"
        $excel->getActiveSheet()->mergeCells($components["heading_merge"]); // Set Merge Cell pada kolom A1 sampai E1
        $excel->getActiveSheet()->getStyle('A1')->getFont()->setBold(TRUE); // Set bold kolom A1
        $excel->getActiveSheet()->getStyle('A1')->getFont()->setSize(15); // Set font size 15 untuk kolom A1
        $excel->getActiveSheet()->getStyle('A1')->getAlignment()
            ->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER); // Set text center untuk kolom A1

        foreach ($components["cols"] as $item) {
            $excel->setActiveSheetIndex(0)->setCellValue($item["col"]["index"]."3", $item["col"]["label"]); // Set kolom A3 dengan tulisan "NO"
            $excel->getActiveSheet()->getStyle($item["col"]["index"]."3")->applyFromArray($style_col);
            $excel->getActiveSheet()->getColumnDimension($item["col"]["index"])->setWidth($item["col"]["length"]); // Set width kolom A
        }

        $no = 1; // Untuk penomoran tabel, di awal set dengan 1
        $numrow = 4; // Set baris pertama untuk isi tabel adalah baris ke 4
        foreach($data as $item){ // Lakukan looping pada variabel siswa
            $excel->setActiveSheetIndex(0)->setCellValue('A'.$numrow, $no);
            $excel->getActiveSheet()->getStyle('A'.$numrow)->applyFromArray($style_row);

            $keyItem = array_keys($item);
            $index = 0;
            foreach ($components["cols"] as $key => $value) {
                if ($key == 0) continue;
                else {
                    $excel->setActiveSheetIndex(0)->setCellValue($value["col"]["index"].$numrow, $item[$keyItem[$index]]);
                    $excel->getActiveSheet()->getStyle($value["col"]["index"].$numrow)->applyFromArray($style_row);
                    $index += 1;
                }
            }
            
            $no++; // Tambah 1 setiap kali looping
            $numrow++; // Tambah 1 setiap kali looping
        }

        // Set height semua kolom menjadi auto (mengikuti height isi dari kolommnya, jadi otomatis)
        $excel->getActiveSheet()->getDefaultRowDimension()->setRowHeight(-1);
        // Set orientasi kertas jadi LANDSCAPE
        $excel->getActiveSheet()->getPageSetup()
        ->setOrientation(PHPExcel_Worksheet_PageSetup::ORIENTATION_LANDSCAPE);

        // Set judul file excel nya
        $excel->getActiveSheet(0)->setTitle($components["title"]);
        $excel->setActiveSheetIndex(0);
        // Proses file excel
        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment; filename="'.$components["file_name"].'.xlsx"'); // Set nama file excel nya
        header('Cache-Control: max-age=0');
        $write = PHPExcel_IOFactory::createWriter($excel, 'Excel2007');
        $write->save('php://output');
    }

    // Load Pagination Transactions
    private function getTransactionsPagination() {
        $page = $this->input->get("page") ?: 0;
        $m_pulo_gebang = $this->UUserOrderTicket->getPagination($page, "id", "DESC");

        // print_r($m_pulo_gebang);
        // die();
        return $m_pulo_gebang;
    }

    public function getDetailTransaction() {
        $id = $this->input->get("id") ?: 0;
        if ($id == 0) return null;
        $m_pulo_gebang = $this->UUserOrderTicket->getWhere(array(
            "id" => $id
        ), "id", "DESC");
        if (count($m_pulo_gebang) > 0) {
            $m_pulo_gebang = $m_pulo_gebang[0];

            $m_pulo_gebang["passanger_gender"] = $this->getRealGender($m_pulo_gebang["passanger_gender"]);
            $m_pulo_gebang["date_of_departure"] = $this->tgl_indo($m_pulo_gebang["date_of_departure"]);
            if (!empty($m_pulo_gebang["created_at"])) {
                $m_pulo_gebang["created_at"] = $this->tgl_indo(explode(" ", $m_pulo_gebang["created_at"])[0]);
            }
            if (!empty($m_pulo_gebang["payment_expired"])) {
                $paymentEx = strtotime($m_pulo_gebang["payment_expired"]);
                $currentDate = strtotime(date("Y-m-d H:i:s"));

                if ($paymentEx < $currentDate) {
                    if ($m_pulo_gebang["payment_status"] == "unpaid") {
                        $m_pulo_gebang["payment_status"] = "expired";
                    }
                }

                $expired = explode(" ", $m_pulo_gebang["payment_expired"]);

                $m_pulo_gebang["payment_expired"] = $this->tgl_indo($expired[0]) . " " . $expired[1];
            }

            if (isset($m_pulo_gebang["payment_status"])) {
                switch ($m_pulo_gebang["payment_status"]) {
                    case "paid": $m_pulo_gebang["payment_status"] = "Lunas"; break;
                    case "unpaid": $m_pulo_gebang["payment_status"] = "Belum Terbayar"; break;
                    default: $m_pulo_gebang["payment_status"] = "Expired"; break;
                }
            }
        }
        // print_r($m_pulo_gebang);
        // die();
        return $m_pulo_gebang;
    }

    private function tgl_indo($tanggal){
        $bulan = array (
            1 =>   'Januari',
            'Februari',
            'Maret',
            'April',
            'Mei',
            'Juni',
            'Juli',
            'Agustus',
            'September',
            'Oktober',
            'November',
            'Desember'
        );
        $pecahkan = explode('-', $tanggal);
        
        // variabel pecahkan 0 = tanggal
        // variabel pecahkan 1 = bulan
        // variabel pecahkan 2 = tahun
     
        return $pecahkan[2] . ' ' . $bulan[ (int)$pecahkan[1] ] . ' ' . $pecahkan[0];
    }

    private function getPriceTransaction() {
        // Get Count
        $getTotalTransactionActive = $this->UUserOrderTicket->getCountByStatus(
            array(
                "ticket_id !=" => NULL
            )
        );

        $getTotalTransactionActiveToday = $this->UUserOrderTicket->getCountByStatus(
            array(
                "ticket_id !=" => NULL,
                "created_at >=" => date("Y-m-d")
            )
        );

        $getTotalTransactionNonActive = $this->UUserOrderTicket->getCountByStatus(
            array(
                "ticket_id" => NULL
            )
        );

        $getTotalTransactionNonActiveToday = $this->UUserOrderTicket->getCountByStatus(
            array(
                "ticket_id" => NULL,
                "created_at >=" => date("Y-m-d")
            )
        );

        // Get Sum
        $getTotalTransactionActiveSUM = $this->UUserOrderTicket->getSumByStatus(
            array(
                "ticket_id !=" => NULL
            )
        );

        $getTotalTransactionActiveTodaySUM = $this->UUserOrderTicket->getSumByStatus(
            array(
                "ticket_id !=" => NULL,
                "created_at >=" => date("Y-m-d")
            )
        );

        $getTotalTransactionNonActiveSUM = $this->UUserOrderTicket->getSumByStatus(
            array(
                "ticket_id" => NULL
            )
        );

        $getTotalTransactionNonActiveTodaySUM = $this->UUserOrderTicket->getSumByStatus(
            array(
                "ticket_id" => NULL,
                "created_at >=" => date("Y-m-d")
            )
        );

        // Get Count
        $getTotalTransactionActiveAlfa = $this->UUserOrderTicket->getCountByStatus(
            array(
                "ticket_id !=" => NULL,
                "payment_method" => "alfamart"
            )
        );

        $getTotalTransactionActiveTodayAlfa = $this->UUserOrderTicket->getCountByStatus(
            array(
                "ticket_id !=" => NULL,
                "payment_method" => "alfamart",
                "created_at >=" => date("Y-m-d")
            )
        );

        $getTotalTransactionNonActiveAlfa = $this->UUserOrderTicket->getCountByStatus(
            array(
                "ticket_id" => NULL,
                "payment_method" => "alfamart"
            )
        );

        $getTotalTransactionNonActiveTodayAlfa = $this->UUserOrderTicket->getCountByStatus(
            array(
                "ticket_id" => NULL,
                "created_at >=" => date("Y-m-d"),
                "payment_method" => "alfamart"
            )
        );

        // Get Sum
        $getTotalTransactionActiveSUMAlfa = $this->UUserOrderTicket->getSumByStatus(
            array(
                "ticket_id !=" => NULL,
                "payment_method" => "alfamart"
            )
        );

        $getTotalTransactionActiveTodaySUMAlfa = $this->UUserOrderTicket->getSumByStatus(
            array(
                "ticket_id !=" => NULL,
                "created_at >=" => date("Y-m-d"),
                "payment_method" => "alfamart"
            )
        );

        $getTotalTransactionNonActiveSUMAlfa = $this->UUserOrderTicket->getSumByStatus(
            array(
                "ticket_id" => NULL,
                "payment_method" => "alfamart"
            )
        );

        $getTotalTransactionNonActiveTodaySUMAlfa = $this->UUserOrderTicket->getSumByStatus(
            array(
                "ticket_id" => NULL,
                "created_at >=" => date("Y-m-d"),
                "payment_method" => "alfamart"
            )
        );

        // Get Count
        $getTotalTransactionActiveIndo = $this->UUserOrderTicket->getCountByStatus(
            array(
                "ticket_id !=" => NULL,
                "payment_method" => "indomaret"
            )
        );

        $getTotalTransactionActiveTodayIndo = $this->UUserOrderTicket->getCountByStatus(
            array(
                "ticket_id !=" => NULL,
                "payment_method" => "indomaret",
                "created_at >=" => date("Y-m-d")
            )
        );

        $getTotalTransactionNonActiveIndo = $this->UUserOrderTicket->getCountByStatus(
            array(
                "ticket_id" => NULL,
                "payment_method" => "indomaret"
            )
        );

        $getTotalTransactionNonActiveTodayIndo = $this->UUserOrderTicket->getCountByStatus(
            array(
                "ticket_id" => NULL,
                "created_at >=" => date("Y-m-d"),
                "payment_method" => "indomaret"
            )
        );

        // Get Sum
        $getTotalTransactionActiveSUMIndo = $this->UUserOrderTicket->getSumByStatus(
            array(
                "ticket_id !=" => NULL,
                "payment_method" => "indomaret"
            )
        );

        $getTotalTransactionActiveTodaySUMIndo = $this->UUserOrderTicket->getSumByStatus(
            array(
                "ticket_id !=" => NULL,
                "created_at >=" => date("Y-m-d"),
                "payment_method" => "indomaret"
            )
        );

        $getTotalTransactionNonActiveSUMIndo = $this->UUserOrderTicket->getSumByStatus(
            array(
                "ticket_id" => NULL,
                "payment_method" => "indomaret"
            )
        );

        $getTotalTransactionNonActiveTodaySUMIndo = $this->UUserOrderTicket->getSumByStatus(
            array(
                "ticket_id" => NULL,
                "created_at >=" => date("Y-m-d"),
                "payment_method" => "indomaret"
            )
        );

        return array(
            "total_active" => $getTotalTransactionActive,
            "total_active_today" => $getTotalTransactionActiveToday,
            "total_progress" => $getTotalTransactionNonActive,
            "total_progress_today" => $getTotalTransactionNonActiveToday,

            "total_active_sum" => $getTotalTransactionActiveSUM,
            "total_active_today_sum" => $getTotalTransactionActiveTodaySUM,
            "total_progress_sum" => $getTotalTransactionNonActiveSUM,
            "total_progress_today_sum" => $getTotalTransactionNonActiveTodaySUM,

            "total_active_alfa" => $getTotalTransactionActiveAlfa,
            "total_active_today_alfa" => $getTotalTransactionActiveTodayAlfa,
            "total_progress_alfa" => $getTotalTransactionNonActiveAlfa,
            "total_progress_today_alfa" => $getTotalTransactionNonActiveTodayAlfa,

            "total_active_sum_alfa" => $getTotalTransactionActiveSUMAlfa,
            "total_active_today_sum_alfa" => $getTotalTransactionActiveTodaySUMAlfa,
            "total_progress_sum_alfa" => $getTotalTransactionNonActiveSUMAlfa,
            "total_progress_today_sum_alfa" => $getTotalTransactionNonActiveTodaySUMAlfa,

            "total_active_indo" => $getTotalTransactionActiveIndo,
            "total_active_today_indo" => $getTotalTransactionActiveTodayIndo,
            "total_progress_indo" => $getTotalTransactionNonActiveIndo,
            "total_progress_today_indo" => $getTotalTransactionNonActiveTodayIndo,

            "total_active_sum_indo" => $getTotalTransactionActiveSUMIndo,
            "total_active_today_sum_indo" => $getTotalTransactionActiveTodaySUMIndo,
            "total_progress_sum_indo" => $getTotalTransactionNonActiveSUMIndo,
            "total_progress_today_sum_indo" => $getTotalTransactionNonActiveTodaySUMIndo
        );
    }

    private function getRealGender($char) {
        $newGender = "-";
        switch ($char) {
            case "m": 
            case "l": $newGender = "Laki-laki";
            break;
            case "f":
            case "w":
            case "p": $newGender = "Perempuan";
            break;
        }
        return $newGender;
    }

    // Experiences
    public function experiences() {
        // Setup Meta
        $this->meta["title"] = "Dashboard | Experiences";
        $this->meta["description"] = "Jaket BUS is the apps for booking bus everytime";
        $this->meta["robots"] = "JAKET BUS, Shuttle, Modernesia";

        $sidebar = $this->menuSidebar;
        $sidebar[1]["items"][0]["is_active"] = true;

        $data = array();
        $data["experiences"] = $this->getExperiencesPagination();
        $data["count_experiences"] = $this->MExperiences->getCount();

        // Load Sign in
        $this->load->view('dashboard/experiences', array(
            "meta" => $this->meta,
            "sidebar" => $sidebar,
            "data" => $data,
            "auth" => $this->auth
        ));
    }

    // Load Pagination Experiences
    private function getExperiencesPagination() {
        $page = $this->input->get("page") ?: 0;
        $m_pulo_gebang = $this->MExperiences->getPagination($page, "id", "DESC");
        // print_r($m_pulo_gebang);
        // die();
        return $m_pulo_gebang;
    }

    public function doCreateExperiences() {
        if (isset($_FILES["cover"])) {
            $photo = $this->fileUpload->do_upload("cover");
            if ($photo["status"]) {
                // Upload File
                $id_photo = $this->MMedias->create(array(
                    "uri" => base_url("assets/dist/img/") . $photo["file_name"],
                    "label" => $photo["file_name"],
                    "created_at" => date("Y-m-d H:i:s"),
                    "updated_at" => date("Y-m-d H:i:s")
                ));

                if ($id_photo != -1) {
                    $title = $this->input->post("title") ?: "";
                    $content = $this->input->post("content") ?: "";
                    $rating = $this->input->post("rating") ?: "";
                    $id_m_users = $this->auth["id"];

                    if (empty($title) || empty($content) || empty($rating)) {
                        echo "
                        <p>Gagal harap mengisi seluruh form</p>
                        <a href='".base_url('index.php/dashboard/experiences')."'>Kembali</a>
                        ";
                    } else {
                        $id_experiences = $this->MExperiences->create(
                            array(
                                "title" => $title,
                                "content" => $content,
                                "rating" => $rating,
                                "id_m_users" => $id_m_users,
                                "id_m_medias" => $id_photo,
                                "created_at" => date("Y-m-d H:i:s"),
                                "updated_at" => date("Y-m-d H:i:s")
                            )
                        );

                        if ($id_experiences != -1) {
                            echo "
                            <p>Berhasil membuat experiences</p>
                            <a href='".base_url('index.php/dashboard/experiences')."'>Kembali</a>
                            ";
                        } else {
                            echo "
                            <p>Gagal membuat experiences</p>
                            <a href='".base_url('index.php/dashboard/experiences')."'>Kembali</a>
                            ";
                        }
                    }
                } else {
                    echo "
                    <p>Gagal mengupload cover</p>
                    <a href='".base_url('index.php/dashboard/experiences')."'>Kembali</a>
                    ";
                }
            }
        } else {
            echo "
            <p>Gagal harap mengisi seluruh form</p>
            <a href='".base_url('index.php/dashboard/experiences')."'>Kembali</a>
            ";
        }
    }

    public function doDeleteExperiences() {
        $id_m_users = $this->input->get("id") ?: "";
        if (empty($id_m_users)) {
            echo "
            <p>Gagal harap mengisi seluruh form</p>
            <a href='".base_url('index.php/dashboard/features')."'>Kembali</a>
            ";
        } else {
            $id_user = $this->MExperiences->delete(array(
                "id" => $id_m_users
            ));
            if ($id_user != -1) {
                echo "
                <p>Berhasil menghapus experiences</p>
                <a href='".base_url('index.php/dashboard/experiences')."'>Kembali</a>
                ";  
            } else {
                echo "
                <p>Gagal menghapus experiences</p>
                <a href='".base_url('index.php/dashboard/experiences')."'>Kembali</a>
                ";  
            }
        }
    }

    // Features
    public function features() {
        // Setup Meta
        $this->meta["title"] = "Dashboard | Features";
        $this->meta["description"] = "Jaket BUS is the apps for booking bus everytime";
        $this->meta["robots"] = "JAKET BUS, Shuttle, Modernesia";

        $sidebar = $this->menuSidebar;
        $sidebar[1]["items"][1]["is_active"] = true;

        $data = array();
        $data["features"] = $this->getFeaturesPagination();
        $data["count_features"] = $this->MFeatures->getCount();

        // Load Sign in
        $this->load->view('dashboard/features', array(
            "meta" => $this->meta,
            "sidebar" => $sidebar,
            "data" => $data,
            "auth" => $this->auth
        ));
    }

    // Load Pagination Features
    private function getFeaturesPagination() {
        $page = $this->input->get("page") ?: 0;
        $m_pulo_gebang = $this->MFeatures->getPagination($page, "id", "DESC");
        // print_r($m_pulo_gebang);
        // die();
        return $m_pulo_gebang;
    }

    public function doCreateFeatures() {
        if (isset($_FILES["cover"])) {
            $photo = $this->fileUpload->do_upload("cover");
            if ($photo["status"]) {
                // Upload File
                $id_photo = $this->MMedias->create(array(
                    "uri" => base_url("assets/dist/img/") . $photo["file_name"],
                    "label" => $photo["file_name"],
                    "created_at" => date("Y-m-d H:i:s"),
                    "updated_at" => date("Y-m-d H:i:s")
                ));

                if ($id_photo != -1) {
                    $title = $this->input->post("title") ?: "";
                    $content = $this->input->post("content") ?: "";
                    $id_m_users = $this->auth["id"];

                    if (empty($title) || empty($content)) {
                        echo "
                        <p>Gagal harap mengisi seluruh form</p>
                        <a href='".base_url('index.php/dashboard/features')."'>Kembali</a>
                        ";
                    } else {
                        $id_features = $this->MFeatures->create(
                            array(
                                "title" => $title,
                                "content" => $content,
                                "id_m_users" => $id_m_users,
                                "id_m_medias" => $id_photo,
                                "created_at" => date("Y-m-d H:i:s"),
                                "updated_at" => date("Y-m-d H:i:s")
                            )
                        );

                        if ($id_features != -1) {
                            echo "
                            <p>Berhasil membuat features</p>
                            <a href='".base_url('index.php/dashboard/features')."'>Kembali</a>
                            ";
                        } else {
                            echo "
                            <p>Gagal membuat features</p>
                            <a href='".base_url('index.php/dashboard/features')."'>Kembali</a>
                            ";
                        }
                    }
                } else {
                    echo "
                    <p>Gagal mengupload cover</p>
                    <a href='".base_url('index.php/dashboard/features')."'>Kembali</a>
                    ";
                }
            }
        } else {
            echo "
            <p>Gagal harap mengisi seluruh form</p>
            <a href='".base_url('index.php/dashboard/features')."'>Kembali</a>
            ";
        }
    }

    public function doDeleteFeatures() {
        $id_m_users = $this->input->get("id") ?: "";
        if (empty($id_m_users)) {
            echo "
            <p>Gagal harap mengisi seluruh form</p>
            <a href='".base_url('index.php/dashboard/features')."'>Kembali</a>
            ";
        } else {
            $id_user = $this->MFeatures->delete(array(
                "id" => $id_m_users
            ));
            if ($id_user != -1) {
                echo "
                <p>Berhasil menghapus features</p>
                <a href='".base_url('index.php/dashboard/features')."'>Kembali</a>
                ";  
            } else {
                echo "
                <p>Gagal menghapus features</p>
                <a href='".base_url('index.php/dashboard/features')."'>Kembali</a>
                ";  
            }
        }
    }

    // Medias
    public function medias() {
        // Setup Meta
        $this->meta["title"] = "Dashboard | Medias";
        $this->meta["description"] = "Jaket BUS is the apps for booking bus everytime";
        $this->meta["robots"] = "JAKET BUS, Shuttle, Modernesia";

        $sidebar = $this->menuSidebar;
        $sidebar[1]["items"][2]["is_active"] = true;

        $data = array();
        $data["medias"] = $this->getMediasPagination();
        $data["count_medias"] = $this->MMedias->getCount();

        // Load Sign in
        $this->load->view('dashboard/medias', array(
            "meta" => $this->meta,
            "sidebar" => $sidebar,
            "data" => $data,
            "auth" => $this->auth
        ));
    }

    // Load Pagination Medias
    private function getMediasPagination() {
        $page = $this->input->get("page") ?: 0;
        $m_pulo_gebang = $this->MMedias->getPagination($page, "id", "DESC");
        // print_r($m_pulo_gebang);
        // die();
        return $m_pulo_gebang;
    }

    // Notification
    public function notification() {
        // Setup Meta
        $this->meta["title"] = "Dashboard | Notification";
        $this->meta["description"] = "Jaket BUS is the apps for booking bus everytime";
        $this->meta["robots"] = "JAKET BUS, Shuttle, Modernesia";

        $sidebar = $this->menuSidebar;
        $sidebar[1]["items"][3]["is_active"] = true;

        $data = array();
        $data["notification"] = $this->getNotificationPagination();
        $data["count_notification"] = $this->MNotification->getCount();

        // Load Sign in
        $this->load->view('dashboard/notification', array(
            "meta" => $this->meta,
            "sidebar" => $sidebar,
            "data" => $data,
            "auth" => $this->auth
        ));
    }

    // Load Pagination Notification
    private function getNotificationPagination() {
        $page = $this->input->get("page") ?: 0;
        $m_pulo_gebang = $this->MNotification->getPagination1($page, "id", "DESC");
        // print_r($m_pulo_gebang);
        // die();
        return $m_pulo_gebang;
    }

    // Pulo Gebang
    public function puloGebang() {
        // Setup Meta
        $this->meta["title"] = "Dashboard | Pulo Gebang";
        $this->meta["description"] = "Jaket BUS is the apps for booking bus everytime";
        $this->meta["robots"] = "JAKET BUS, Shuttle, Modernesia";

        $sidebar = $this->menuSidebar;
        $sidebar[1]["items"][4]["is_active"] = true;

        $data = array();
        $data["pulo_gebang"] = $this->getPuloGebangPagination();
        $data["count_pulo_gebang"] = $this->MPuloGebang->getCount();

        // Load Sign in
        $this->load->view('dashboard/pulo_gebang', array(
            "meta" => $this->meta,
            "sidebar" => $sidebar,
            "data" => $data,
            "auth" => $this->auth
        ));
    }

    // Load Pagination Pulo Gebang
    private function getPuloGebangPagination() {
        $page = $this->input->get("page") ?: 0;
        $m_pulo_gebang = $this->MPuloGebang->getPagination($page, "id", "DESC");
        return $m_pulo_gebang;
    }

    public function doSyncPuloGebang() {
        $username = $this->input->post("username") ?: "";
        $password = $this->input->post("password") ?: "";
        if (empty($username) || empty($password)) {
            echo "
            <p>Gagal harap mengisi seluruh form</p>
            <a href='".base_url('index.php/dashboard/puloGebang')."'>Kembali</a>
            ";
        } else {
            // Todo Authentication
            $this->custom_curl->setHeader(array(
                "Authorization:Basic " . base64_encode(
                    $username.":".$password
                )
            ));
            $this->custom_curl->createCurl(API_URI_PULO_GEBANG . "authentication");

            $data = json_decode($this->custom_curl->__tostring());
            try {
                $data = $data->data;
                $id_sync = $this->MPuloGebang->create(array(
                    "username" => $username,
                    "password" => $password,
                    "api_token" => $data->development_key,
                    "created_at" => date("Y-m-d H:i:s"),
                    "updated_at" => date("Y-m-d H:i:s")
                ));
                if ($id_sync != -1) {
                    echo "
                    <p>Berhasil melakukan sync</p>
                    <a href='".base_url('index.php/dashboard/puloGebang')."'>Kembali</a>
                    ";
                } else {
                    echo "
                    <p>Gagal melakukan sync</p>
                    <a href='".base_url('index.php/dashboard/puloGebang')."'>Kembali</a>
                    ";
                }
            } catch(Exception $e) {
                echo "
                <p>".$e->getMessage()."</p>
                <a href='".base_url('index.php/dashboard/puloGebang')."'>Kembali</a>
                ";
            }
        }
    }

    // Wallet
    public function wallets() {
        // Setup Meta
        $this->meta["title"] = "Dashboard | Wallets";
        $this->meta["description"] = "Jaket BUS is the apps for booking bus everytime";
        $this->meta["robots"] = "JAKET BUS, Shuttle, Modernesia";

        $sidebar = $this->menuSidebar;
        $sidebar[1]["items"][5]["is_active"] = true;

        $data = array();
        $data["wallet"] = $this->getWalletPagination();
        $data["count_wallet"] = $this->MTronWallet->getCount();

        // Load Sign in
        $this->load->view('dashboard/wallets', array(
            "meta" => $this->meta,
            "sidebar" => $sidebar,
            "data" => $data,
            "auth" => $this->auth
        ));
    }

    // Load Pagination Wallet
    private function getWalletPagination() {
        $page = $this->input->get("page") ?: 0;
        $m_wallet = $this->MTronWallet->getPagination($page, "id", "DESC");
        return $m_wallet;
    }

    public function doSyncWallet() {
        $username = $this->input->post("username") ?: "";
        $password = $this->input->post("password") ?: "";
        $api_token = $this->input->post("api_token") ?: "";
        if (empty($username) || empty($password) || empty($api_token)) {
            echo "
            <p>Gagal harap mengisi seluruh form</p>
            <a href='".base_url('index.php/dashboard/wallets')."'>Kembali</a>
            ";
        } else {
            // Todo Authentication
            $data = array(
                "username" => $username,
                "password" => $password,
                "api_token" => $api_token,
                "created_at" => date("Y-m-d H:i:s"),
                "updated_at" => date("Y-m-d H:i:s")
            );
            $id = $this->MTronWallet->create($data);
            if ($id != -1) {
                echo "
                <p>Berhasil melakukan sync</p>
                <a href='".base_url('index.php/dashboard/wallets')."'>Kembali</a>
                ";
            } else {
                echo "
                <p>Gagal melakukan sync</p>
                <a href='".base_url('index.php/dashboard/wallets')."'>Kembali</a>
                ";
            }
        }
    }

    // Users
    public function users() {
        // Setup Meta
        $this->meta["title"] = "Dashboard | Users";
        $this->meta["description"] = "Jaket BUS is the apps for booking bus everytime";
        $this->meta["robots"] = "JAKET BUS, Shuttle, Modernesia";

        $sidebar = $this->menuSidebar;
        $sidebar[1]["items"][6]["is_active"] = true;

        $data = array();
        $data["user"] = $this->getUserPagination();
        $data["count_user"] = $this->MUsers->getCount();

        // Load Sign in
        $this->load->view('dashboard/users', array(
            "meta" => $this->meta,
            "sidebar" => $sidebar,
            "data" => $data,
            "auth" => $this->auth
        ));
    }

    // Load Pagination User
    private function getUserPagination() {
        $page = $this->input->get("page") ?: 0;
        $m_users = $this->MUsers->getPagination($page, "id", "DESC");
        $temp = array();
        foreach ($m_users as $item) {
            $u_user = $this->UUser->getWhere(array(
                "id_m_users" => $item["id"]
            ));
            if (count($u_user) > 0) {
                $item["utility"] = $u_user[0];
                $temp[] = $item;
            }
        }
        $m_users = $temp;
        return $m_users;
    }

    public function doCreateUsers() {
        $first_name = $this->input->post("first_name") ?: "";
        $last_name = $this->input->post("last_name") ?: "";
        $email = $this->input->post("email") ?: "";
        $password = $this->input->post("password") ?: "";
        $phone_number = $this->input->post("phone_number") ?: "";
        $level = $this->input->post("level") ?: "";
        if (empty($first_name) || empty($last_name) || empty($email) ||
            empty($password) || empty($phone_number) || empty($level)) {
            echo "
            <p>Gagal harap mengisi seluruh form</p>
            <a href='".base_url('index.php/dashboard/users')."'>Kembali</a>
            ";
        } else {
            // Todo Authentication
            $id_m_users = $this->MUsers->create(
                array(
                    "first_name" => $first_name,
                    "last_name" => $last_name,
                    "email" => $email,
                    "password" => md5($password),
                    "phone_number" => $phone_number,
                    "created_at" => date("Y-m-d H:i:s"),
                    "updated_at" => date("Y-m-d H:i:s")
                )
            );
            // Check Is Created User?
            if ($id_m_users != -1) {
                // Create Utility User
                $token = md5(date("Y-m-d H:i:s") . $id_m_users . $password);
                $id_u_user = $this->UUser->create(
                    array(
                        "id_face" => 1,
                        "id_m_users" => $id_m_users,
                        "level" => $level,
                        "token" => $token,
                        "created_at" => date("Y-m-d H:i:s"),
                        "updated_at" => date("Y-m-d H:i:s")
                    )
                );
                echo "
                <p>Berhasil membuat user</p>
                <a href='".base_url('index.php/dashboard/users')."'>Kembali</a>
                ";
            } else {
                echo "
                <p>Gagal membuat user</p>
                <a href='".base_url('index.php/dashboard/users')."'>Kembali</a>
                ";
            }
        }
    }

    public function doEditUsers() {
        $first_name = $this->input->post("edit_first_name") ?: "";
        $last_name = $this->input->post("edit_last_name") ?: "";
        $phone_number = $this->input->post("edit_phone_number") ?: "";
        $level = $this->input->post("edit_level") ?: "";
        $id = $this->input->post("edit_id") ?: "";
        if (empty($first_name) || empty($last_name) || 
            empty($phone_number) || empty($level) || empty($id)) {
            echo "
            <p>Gagal harap mengisi seluruh form</p>
            <a href='".base_url('index.php/dashboard/users')."'>Kembali</a>
            ";
        } else {
            // Todo Authentication
            $id_m_users = $this->MUsers->update(
                array("id" => $id),
                array(
                    "first_name" => $first_name,
                    "last_name" => $last_name,
                    "phone_number" => $phone_number,
                    "updated_at" => date("Y-m-d H:i:s")
                )
            );
            // Check Is Created User?
            if ($id_m_users != -1) {
                // Create Utility User
                $id_u_user = $this->UUser->update(
                    array(
                        "id_m_users" => $id
                    ),
                    array(
                        "level" => $level,
                        "updated_at" => date("Y-m-d H:i:s")
                    )
                );
                echo "
                <p>Berhasil mengubah user</p>
                <a href='".base_url('index.php/dashboard/users')."'>Kembali</a>
                ";
            } else {
                echo "
                <p>Gagal mengubah user</p>
                <a href='".base_url('index.php/dashboard/users')."'>Kembali</a>
                ";
            }
        }
    }

    public function doDeleteUsers() {
        $id_m_users = $this->input->get("id") ?: "";
        if (empty($id_m_users)) {
            echo "
            <p>Gagal harap mengisi seluruh form</p>
            <a href='".base_url('index.php/dashboard/users')."'>Kembali</a>
            ";
        } else {
            $id_user = $this->UUser->update(array(
                "id_m_users" => $id_m_users
            ), array(
                "level" => 0
            ));
            if ($id_user != -1) {
                echo "
                <p>Berhasil menghapus pengguna</p>
                <a href='".base_url('index.php/dashboard/users')."'>Kembali</a>
                ";  
            } else {
                echo "
                <p>Gagal menghapus pengguna</p>
                <a href='".base_url('index.php/dashboard/users')."'>Kembali</a>
                ";  
            }
        }
    }

    public function pages() {
        // Setup Meta
        $this->meta["title"] = "Dashboard | Pages";
        $this->meta["description"] = "Jaket BUS is the apps for booking bus everytime";
        $this->meta["robots"] = "JAKET BUS, Shuttle, Modernesia";

        $sidebar = $this->menuSidebar;
        $sidebar[0]["items"][2]["is_active"] = true;

        $data = array();
        $data["pages"] = $this->getPages();

        // Load Sign in
        $this->load->view('dashboard/pages', array(
            "meta" => $this->meta,
            "sidebar" => $sidebar,
            "data" => $data,
            "auth" => $this->auth
        ));
    }

    public function page($id) {
        // Setup Meta
        $this->meta["title"] = "Dashboard | Pages Edit";
        $this->meta["description"] = "Jaket BUS is the apps for booking bus everytime";
        $this->meta["robots"] = "JAKET BUS, Shuttle, Modernesia";

        $sidebar = $this->menuSidebar;
        $sidebar[0]["items"][2]["is_active"] = true;

        $data = array();
        $data["page"] = $this->getPage($id);
        if (count($data["page"]) > 0)
            $data["page"] = $data["page"][0];

        // Load Sign in
        $this->load->view('dashboard/page-edit', array(
            "meta" => $this->meta,
            "sidebar" => $sidebar,
            "data" => $data["page"],
            "auth" => $this->auth
        ));
    }

    private function getPages() {
        $m_users = $this->MPages->getWhere(array(), 0, "id", "DESC");
        return $m_users;
    }

    private function getPage($id) {
        $m_users = $this->MPages->getWhere(array('id' => $id), 0, "id", "DESC");
        return $m_users;
    }

    public function doEditPage($id) {
        $title = $this->input->post("title", TRUE) ?: "";
        $content = $this->input->post("content", TRUE) ?: "";
        $updated_at = date("Y-m-d H:i:s");
        $this->MPages->update(array(
            "id" => $id
        ), array(
            "title" => $title,
            "content" => $content,
            "updated_at" => $updated_at
        ));

        redirect(base_url("index.php/dashboard/pages"));
    }
}