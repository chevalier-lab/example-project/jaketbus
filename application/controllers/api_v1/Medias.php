<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Medias extends CI_Controller {

    // Public Variable
    public $custom_log, $session, $response, $fileUpload, $userType;

    public function __construct() {
        parent::__construct();

        // Load Model
        $this->load->model("MMedias");
        $this->load->model("UUser");

        // Load Helper
        $this->session = new Session_helper();
        $this->response = new Response_helper();
        $this->fileUpload = new Upload_file_helper(
            array(
                "file_type" => array(
                    "png",
                    "jpg",
                    "jpeg"
                ),
                "max_size"  => 200000000
            )
        );
    }

    // Get Medias
    public function get_medias() {

        // Initial Variable
        $res = array();
        $code = 200;
        $error = array();
        $message = "";

        // Get Token
        $token = $this->input->get_request_header('Authorization', TRUE) ?: "";

        // Check Token Is Not Null
        if (isset($token) || !empty($token)) {
            $medias = $this->MMedias->getAll();
            $res = $medias;
            $message = "Success to fetch photos";
        } else {
            $code = 403;
            $error[] = "Token not found";
            $message = "Token not found";
        }

        print_r ($this->response->json($res, $code, $error, $message));

    }

    // Upload Media
    public function upload() {

        // Initial Variable
        $res = null;
        $code = 200;
        $error = array();
        $message = "";

        // Get Token
        $token = $this->input->get_request_header('Authorization', TRUE) ?: "";

        // Check Token Is Not Null
        if (isset($token) || !empty($token)) {
            if (isset($_FILES["photo"]["name"])) {
                $photo = $this->fileUpload->do_upload("photo");
                if ($photo["status"]) {
                    // Upload File
                    $id_photo = $this->MMedias->create(array(
                        "uri" => base_url("assets/dist/img/") . $photo["file_name"],
                        "label" => $photo["file_name"],
                        "created_at" => date("Y-m-d H:i:s"),
                        "updated_at" => date("Y-m-d H:i:s")
                    ));

                    // Check If Upload Success
                    if ($id_photo != -1) {
                        $res = $photo;
                        $message = "Success to update photo";
                    } else {
                        $code = 500;
                        $error[] = "Failed to upload photo";
                        $error[] = $photo;
                        $message = "Failed to upload photo, cause status is not ready";
                    }
                } else {
                    $code = 402;
                    $error[] = "Failed to upload photo";
                    $error[] = $photo;
                    $message = "Failed to upload photo, cause status is not ready";
                }
            } else {
                $code = 403;
                $error[] = "File photo is required";
                $message = "File photo is required";
            }
        } else {
            $code = 403;
            $error[] = "Token not found";
            $message = "Token not found";
        }

        print_r ($this->response->json($res, $code, $error, $message));

    }

}