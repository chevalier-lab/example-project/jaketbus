<?php
date_default_timezone_set('Asia/Jakarta');
defined('BASEPATH') OR exit('No direct script access allowed');

class Mst_webhook_alfamart extends CI_Controller {

    // Public Variable
    public $custom_curl, $session, $response, $userType;

    public function __construct() {
        parent::__construct();
        $this->load->model("UUserOrderTicket");
        $this->load->model("MPuloGebang");
        $this->load->model("MAlfamaret");

        $this->response = new Response_helper();
        $this->custom_curl = new Mycurl_helper("");
    }

  public function createAccount() {
    $res = null;
    $code = 200;
    $message = "";
    $req = json_decode($this->input->raw_input_stream, TRUE);

    $checkIsValid = $this->response->checkIsNotNull(
      array(
        "username",
        "password"
      ),
      $req
    );

    if (count($checkIsValid) == 0) {
      $token = date("YmdHis") . strtotime('now');
      $id_m_alfamart = $this->MAlfamaret->create(
        array(
          "username" => $req["username"],
          "password" => $req["password"],
          "token" => $token,
          "created_at" => date("Y-m-d H:i:s"),
          "updated_at" => date("Y-m-d H:i:s")
        )
      );

      if ($id_m_alfamart != -1) {
        $res = array(
          "token" => $token,
          "created_at" => date("Y-m-d H:i:s"),
          "updated_at" => date("Y-m-d H:i:s")
        );
        $message = "Success";
      } else {
        $code = 402;
        $message = "Failed to create account";
      }

    } else {
      $code = 403;
      $message = "Please fill username and password";
    }

    print_r($this->response->json2($res, $code, $message));
  }

  public function getValidToken() {
    $res = null;
    $code = 200;
    $message = "";
    $req = json_decode($this->input->raw_input_stream, TRUE);

    $checkIsValid = $this->response->checkIsNotNull(
      array(
        "username",
        "password"
      ),
      $req
    );

    if (count($checkIsValid) == 0) {
      $alfamart = $this->MAlfamaret->getWhere(
        array(
          "username" => $req["username"],
          "password" => $req["password"]
        ),
        "id",
        "DESC"
      );

      if (count($alfamart) > 0) {
        $alfamart = $alfamart[0];
        $res = array(
          "token" => $alfamart["token"],
          "created_at" => $alfamart["created_at"],
          "updated_at" => $alfamart["updated_at"]
        );
        $message = "Success";
      } else {
        $code = 404;
        $message = "Username and password not found";
      }
    } else {
      $code = 403;
      $message = "Please fill username and password";
    }

    print_r($this->response->json2($res, $code, $message));
  }

  public function inquiry()
  {
    $res = null;
    $code = 200;
    $message = "";
    $req = json_decode($this->input->raw_input_stream, TRUE);
    $checkIsValid = $this->response->checkIsNotNull(
      array(
        "payment_code",
        "reff_id",
        "trx_date",
        "merchant_code",
        "token",
      ),
      $req
    );
    $message = $checkIsValid;
    if (count($message) == 0) {

      $checkToken = $this->MAlfamaret->getWhere(
        array(
          "token" => $req["token"]
        ),
        "id",
        "DESC"
      );

      if (count($checkToken) == 0) {
        $code = 403;
        $message = "Invalid credential";
        print_r($this->response->json2($res, $code, $message));
        die();
      }

      $userOrder = $this->UUserOrderTicket->getWhere(
        array(
          "payment_method" => "alfamart",
          "payment_status" => "unpaid",
          "payment_code" => $req['payment_code']
        ),
        "id",
        "DESC"
      );
      if (count($userOrder) > 0) {
        $res = array(
          "payment_code" => (int)$req['payment_code'],
          "customer" => $userOrder[0]['passanger_name'],
          "description" => $userOrder[0]['origin'] . ' - ' . $userOrder[0]['destionation'],
          "admin_fee" => (int)$userOrder[0]['payment_fee'],
          "total_amount" => $userOrder[0]['payment_fee'] + $userOrder[0]['price'],
          "expired" => 1440,
          "trx_date" => strtotime($userOrder[0]['created_at']),
          "created_at" => $userOrder[0]['created_at'],
          "payment_expired" => $userOrder[0]['payment_expired'],
          "info_text" => 'STRUK INI SEBAGAI BUKTI PEMBAYARAN YANG SAH. Call Center 6281314630883.',
        );
        $message = "Success";
      } else {
        $code = 403;
        $message = "Payment Code not available/already paid off";
      }
    } else {
      $code = 403;
      $message = "Request not complate";
    }
    print_r($this->response->json2($res, $code, $message));
  }

  public function payment()
  {
    $res = null;
    $code = 200;
    $message = "";
    $req = json_decode($this->input->raw_input_stream, TRUE);
    $checkIsValid = $this->response->checkIsNotNull(
      array(
        "payment_code",
        "reff_id",
        "trx_date",
        "total_amount",
        "token",
      ),
      $req
    );
    $message = $checkIsValid;
    if (count($message) == 0) {

      $checkToken = $this->MAlfamaret->getWhere(
        array(
          "token" => $req["token"]
        ),
        "id",
        "DESC"
      );

      if (count($checkToken) == 0) {
        $code = 403;
        $message = "Invalid credential";
        print_r($this->response->json2($res, $code, $message));
        die();
      }

      $userOrder = $this->UUserOrderTicket->getWhere(
        array(
          "payment_method" => "alfamart",
          "payment_code" => $req['payment_code']
        ),
        "id",
        "DESC"
      );
      if (count($userOrder) > 0) {
        if ($userOrder[0]["payment_status"] == "unpaid") {
          $ref = strtotime('now');

          $paymentEx = strtotime($userOrder[0]["payment_expired"]);
          $currentDate = strtotime(date("Y-m-d H:i:s"));

          if ($paymentEx < $currentDate) {
            $code = 400;
            $message = "Payment Code is expired";
          } else {

            if (($userOrder[0]['price'] + $userOrder[0]['payment_fee']) == $req['total_amount']) {
              $this->UUserOrderTicket->update(
                array(
                  "payment_code" => $req['payment_code']
                ),
                array(
                  "payment_status" => "paid",
                  "payment_data" => $req['reff_id'],
                  "payment_date" => date("Y-m-d H:i:s", strtotime('now')),
                  "payment_ref" => $ref,
                ),
              );
              $res = array(
                "refference_no" => $ref
              );
              $message = "Success";

              $userOrder = $userOrder[0];
              $token = $this->getToken();

              // Do Update Ticket ID
              // Todo Authentication
              $this->custom_curl->setHeader(array(
                "Content-Type: application/json",
                "X-TTPG-KEY: " . $token
              ));
              $this->custom_curl->setPost(json_encode(array(
                array(
                  "id_schedule" => $userOrder["id_schedule"],
                  "pnr" => $userOrder["pnr"],
                  "pnp_name" => $userOrder["passanger_name"],
                  "pnp_no_telp" => $userOrder["passanger_telp"],
                  "pnp_genre" => $userOrder["passanger_gender"],
                  "id_seat" => $userOrder["seat_id"]
                )
              )));
              $this->custom_curl->createCurl(API_URI_PULO_GEBANG . "ticket/booking");
              try {
                $data = json_decode($this->custom_curl->__tostring());
                if ($data->status) {
                  foreach ($data->data as $item) {
                    $this->UUserOrderTicket->update(
                      array(
                        "payment_code" => $req['payment_code']
                      ),
                      array(
                        "ticket_id" => $item->ticket_id
                      )
                    );
                  }
                }
              } catch(Exception $e) {}

            } else {
              $code = 403;
              $message = "Wrong amount payment";
            }
          }

        } else {
          $code = 402;
          $message = "Already paid off";
        }
      } else {
        $code = 401;
        $message = "Payment Code not available";
      }
        
    } else {
      $code = 403;
      $message = "Request not complate";
    }

    print_r($this->response->json2($res, $code, $message));
  }

  // Get Token
  private function getToken() {

      // Todo Authentication
      $res = $this->MPuloGebang->query("
      SELECT `api_token` FROM `m_pulo_gebang` ORDER BY `id` DESC LIMIT 1");
      if (count($res) == 1) {
          $res = $res[0];
          return $res["api_token"];
      } else {
          return "";
      }

  }

}