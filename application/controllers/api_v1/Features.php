<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Features extends CI_Controller {

    // Public Variable
    public $custom_log, $session, $response, $fileUpload, $userType;

    public function __construct() {
        parent::__construct();

        // Load Model
        $this->load->model("MUsers");
        $this->load->model("MMedias");
        $this->load->model("MFeatures");
        $this->load->model("UUser");

        // Load Helper
        $this->session = new Session_helper();
        $this->response = new Response_helper();
        $this->fileUpload = new Upload_file_helper(
            array(
                "file_type" => array(
                    "png",
                    "jpg",
                    "jpeg"
                ),
                "max_size"  => 200000000
            )
        );
    }

    // Create Feature
    public function createFeature() {

        // Initial Variable
        $res = null;
        $code = 200;
        $error = array();
        $message = "";

        // Get Token
        $token = $this->input->get_request_header('Authorization', TRUE) ?: "";

        // Check Token Is Not Null
        if (isset($token) || !empty($token)) {

            $title = $this->input->post("title", TRUE) ?: "";
            $content = $this->input->post("content", TRUE) ?: "";
            if (isset($_FILES["cover"]["name"]) &&
                !empty($title) && !empty($content)) {
                // Get Utility User
                $u_user = $this->UUser->getWhere(array(
                    "token" => $token
                ));

                // Check User Is Exists
                if (count($u_user) > 0) {
                    $u_user = $u_user[0];
                    // Get User
                    $m_users = $this->MUsers->getWhere(array(
                        "id" => $u_user["id_m_users"]
                    ));
                    
                    // Check Is Exists
                    if (count($m_users) > 0) {
                        $m_users = $m_users[0];
                        // Insert Media
                        $face = $this->fileUpload->do_upload("cover");
                        if ($face["status"]) {
                            // Upload File
                            $id_face = $this->MMedias->create(array(
                                "uri" => base_url("assets/dist/img/") . $face["file_name"],
                                "label" => $face["file_name"],
                                "created_at" => date("Y-m-d H:i:s"),
                                "updated_at" => date("Y-m-d H:i:s")
                            ));

                            // Check If Upload Success
                            if ($id_face != -1) {

                                $id_features = $this->MFeatures->create(array(
                                    "title" => $title,
                                    "content" => $content,
                                    "id_m_users" => $m_users["id"],
                                    "id_m_medias" => $id_face,
                                    "created_at" => date("Y-m-d H:i:s"),
                                    "updated_at" => date("Y-m-d H:i:s")
                                ));

                                if ($id_features != -1) {
                                    $res = $face;
                                    $message = "Success to create features";
                                } else {
                                    $code = 500;
                                    $error[] = "Failed to create features";
                                    $message = "Failed to create features";
                                }

                            } else {
                                $code = 500;
                                $error[] = "Failed to upload cover";
                                $message = "Failed to upload cover";
                            }
                        } else {
                            $code = 500;
                            $error[] = "Failed to upload cover";
                            $message = "Failed to upload cover";
                        }
                    } else {
                        $code = 500;
                        $error[] = "Failed to fetch user";
                        $message = "Failed to fetch user";
                    }
                } else {
                    $code = 500;
                    $error[] = "Failed to fetch utility user, cause token not valid";
                    $message = "Failed to fetch utility user, cause token not valid";
                }
            } else {
                $code = 403;
                $error[] = "Please fill title, content, and cover";
                $message = "Please fill title, content, and cover";
            }
        } else {
            $code = 403;
            $error[] = "Token not found";
            $message = "Token not found";
        }

        print_r ($this->response->json($res, $code, $error, $message));

    }

    // Get Features
    public function getFeatures() {
        // Initial Variable
        $res = array();
        $code = 200;
        $error = array();
        $message = "";

        // Get Token
        $token = $this->input->get_request_header('Authorization', TRUE) ?: "";
        $page = $this->input->post_get("page") ?: 0;

        // Check Token Is Not Null
        if (isset($token) || !empty($token)) {
            // Get Utility User
            $u_user = $this->UUser->getWhere(array(
                "token" => $token
            ));

            // Check User Is Exists
            if (count($u_user) > 0) {
                $u_user = $u_user[0];
                // Get User
                $m_users = $this->MUsers->getWhere(array(
                    "id" => $u_user["id_m_users"]
                ));
                
                // Check Is Exists
                if (count($m_users) > 0) {
                    $m_users = $m_users[0];
                    $data = array();
                    $features = $this->MFeatures->getPagination($page, "id", "DESC");
                    foreach ($features as $item) {
                        $media = $this->MMedias->getWhere(array(
                            "id" => $item["id_m_medias"]
                        ));
                        if (count($media) > 0) {
                            $media = $media[0];
                            $item["cover"] = $media;
                            $data[] = $item;
                        }
                    }

                    $res = $data;
                    $message = "Success to fetch features";
                } else {
                    $code = 500;
                    $error[] = "Failed to fetch user";
                    $message = "Failed to fetch user";
                }
            } else {
                $code = 500;
                $error[] = "Failed to fetch utility user, cause token not valid";
                $message = "Failed to fetch utility user, cause token not valid";
            }
        } else {
            $code = 403;
            $error[] = "Token not found";
            $message = "Token not found";
        }

        print_r ($this->response->json($res, $code, $error, $message));
    }

    // Get Detail Feature
    public function getDetailFeature() {
        // Initial Variable
        $res = null;
        $code = 200;
        $error = array();
        $message = "";

        // Get Token
        $token = $this->input->get_request_header('Authorization', TRUE) ?: "";
        $id_feature = $this->input->post_get("id_feature") ?: 0;

        // Check Token Is Not Null
        if (isset($token) || !empty($token)) {
            if (!isset($id_feature) && $id_feature == 0) {
                $code = 403;
                $error[] = "ID Feature is required";
                $message = "Failed, ID Feature is required";
            } else {
                // Get Utility User
                $u_user = $this->UUser->getWhere(array(
                    "token" => $token
                ));

                // Check User Is Exists
                if (count($u_user) > 0) {
                    $u_user = $u_user[0];
                    // Get User
                    $m_users = $this->MUsers->getWhere(array(
                        "id" => $u_user["id_m_users"]
                    ));
                    
                    // Check Is Exists
                    if (count($m_users) > 0) {
                        $m_users = $m_users[0];
                        $features = $this->MFeatures->getWhere(array(
                            "id" => $id_feature
                        ));
                        if (count($features) > 0) {
                            $features = $features[0];
                            $media = $this->MMedias->getWhere(array(
                                "id" => $features["id_m_medias"]
                            ));
                            if (count($media) > 0) {
                                $media = $media[0];
                                $features["cover"] = $media;
                                $res = $features;
                            }
                        }
                        $message = "Success to fetch features";
                    } else {
                        $code = 500;
                        $error[] = "Failed to fetch user";
                        $message = "Failed to fetch user";
                    }
                } else {
                    $code = 500;
                    $error[] = "Failed to fetch utility user, cause token not valid";
                    $message = "Failed to fetch utility user, cause token not valid";
                }
            }
        } else {
            $code = 403;
            $error[] = "Token not found";
            $message = "Token not found";
        }

        print_r ($this->response->json($res, $code, $error, $message));
    }

    // Update Feature
    public function updateFeature() {

        // Initial Variable
        $res = null;
        $code = 200;
        $error = array();
        $message = "";

        // Get Token
        $token = $this->input->get_request_header('Authorization', TRUE) ?: "";

        // Check Token Is Not Null
        if (isset($token) || !empty($token)) {

            $title = $this->input->post("title", TRUE) ?: "";
            $content = $this->input->post("content", TRUE) ?: "";
            $id_feature = $this->input->post("id_feature", TRUE) ?: "";
            if (!empty($title) && !empty($content) && !empty($id_feature)) {
                $current_feature = $this->MFeatures->getWhere(array(
                    "id" => $id_feature
                ));
                if (count($current_feature) > 0) {
                    $current_feature = $current_feature[0];
                    $status_update = $this->MFeatures->update(array(
                        "id" => $id_feature
                    ), array(
                        "title" => $title,
                        "content" => $content
                    ));

                    if ($status_update != -1) {
                        $res = $current_feature;
                        $res["title"] = $title;
                        $res["content"] = $content;
                        $message = "Success, to update feature";
                    } else {
                        $code = 500;
                        $error[] = "Failed, to update feature";
                        $message = "Failed, to update feature";
                    }
                } else {
                    $code = 500;
                    $error[] = "Failed, feature tidak ditemukan";
                    $message = "Failed, feature tidak ditemukan";
                }
            } else {
                $code = 403;
                $error[] = "Please fill title, content, and cover";
                $message = "Please fill title, content, and cover";
            }
        } else {
            $code = 403;
            $error[] = "Token not found";
            $message = "Token not found";
        }

        print_r ($this->response->json($res, $code, $error, $message));

    }

    // Update Cover Feature
    public function updateCoverFeature() {

        // Initial Variable
        $res = null;
        $code = 200;
        $error = array();
        $message = "";

        // Get Token
        $token = $this->input->get_request_header('Authorization', TRUE) ?: "";

        // Check Token Is Not Null
        if (isset($token) || !empty($token)) {

            $id_feature = $this->input->post("id_feature", TRUE) ?: "";
            if (isset($_FILES["cover"]["name"]) && !empty($id_feature)) {
                $current_feature = $this->MFeatures->getWhere(array(
                    "id" => $id_feature
                ));
                if (count($current_feature) > 0) {
                    $current_feature = $current_feature[0];

                    $face = $this->fileUpload->do_upload("cover");
                    if ($face["status"]) {
                        // Upload File
                        $id_face = $this->MMedias->create(array(
                            "uri" => base_url("assets/dist/img/") . $face["file_name"],
                            "label" => $face["file_name"],
                            "created_at" => date("Y-m-d H:i:s"),
                            "updated_at" => date("Y-m-d H:i:s")
                        ));

                        // Check If Upload Success
                        if ($id_face != -1) {
                            $status_update = $this->MFeatures->update(array(
                                "id" => $id_feature
                            ), array(
                                "id_m_medias" => $id_face
                            ));
        
                            if ($status_update != -1) {
                                $res = $current_feature;
                                $message = "Success, to update feature";
                            } else {
                                $code = 500;
                                $error[] = "Failed, to update feature";
                                $message = "Failed, to update feature";
                            }
                        } else {
                            $code = 500;
                            $error[] = "Failed, to update cover";
                            $message = "Failed, to update cover";
                        }
                    } else {
                        $code = 500;
                        $error[] = "Failed, to update cover";
                        $message = "Failed, to update cover";
                    }
                } else {
                    $code = 500;
                    $error[] = "Failed, feature tidak ditemukan";
                    $message = "Failed, feature tidak ditemukan";
                }
            } else {
                $code = 403;
                $error[] = "Please fill title, content, and cover";
                $message = "Please fill title, content, and cover";
            }
        } else {
            $code = 403;
            $error[] = "Token not found";
            $message = "Token not found";
        }

        print_r ($this->response->json($res, $code, $error, $message));

    }

    // Delete Feature
    public function deleteFeature() {

        // Initial Variable
        $res = null;
        $code = 200;
        $error = array();
        $message = "";

        // Get Token
        $token = $this->input->get_request_header('Authorization', TRUE) ?: "";

        // Check Token Is Not Null
        if (isset($token) || !empty($token)) {
            $id_feature = $this->input->post("id_feature", TRUE) ?: "";
            if (!empty($id_feature)) {
                $current_feature = $this->MFeatures->getWhere(array(
                    "id" => $id_feature
                ));
                if (count($current_feature) > 0) {
                    $current_feature = $current_feature[0];
                    $status_update = $this->MFeatures->delete(array(
                        "id" => $id_feature
                    ));

                    if ($status_update != -1) {
                        $res = $current_feature;
                        $message = "Success, to delete feature";
                    } else {
                        $code = 500;
                        $error[] = "Failed, to delete feature";
                        $message = "Failed, to delete feature";
                    }
                } else {
                    $code = 500;
                    $error[] = "Failed, feature tidak ditemukan";
                    $message = "Failed, feature tidak ditemukan";
                }
            } else {
                $code = 403;
                $error[] = "Please fill ID Feature";
                $message = "Please fill ID Feature";
            }
        } else {
            $code = 403;
            $error[] = "Token not found";
            $message = "Token not found";
        }

        print_r ($this->response->json($res, $code, $error, $message));

    }

}