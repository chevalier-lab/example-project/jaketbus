<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class User extends CI_Controller {

    // Public Variable
    public $custom_curl, $session, $response, $fileUpload, $userType;

    public function __construct() {
        parent::__construct();

        // Load Model
        $this->load->model("MUsers");
        $this->load->model("MMedias");
        $this->load->model("MNotification");
        $this->load->model("UUser");
        $this->load->model("MTronWallet");

        // Load Helper
        $this->session = new Session_helper();
        $this->response = new Response_helper();
        $this->custom_curl = new Mycurl_helper("");
        $this->fileUpload = new Upload_file_helper(
            array(
                "file_type" => array(
                    "png",
                    "jpg",
                    "jpeg",
                    "JPG",
                    "JPEG"
                ),
                "max_size"  => 500000000
            )
        );
    }

    // Get TOKEN
    private function getWalletToken() {

        // Initial Variable
        $res = array();
        $error = array();

        if (count($error) == 0) {
            // Todo Authentication
            $res = $this->MTronWallet->query("
            SELECT `api_token` FROM `m_tron_wallet` ORDER BY `id` DESC LIMIT 1");
            if (count($res) == 1) {
                $res = $res[0];
                return $res["api_token"];
            } else {
                return "";
            }
        } else {
            return "";
        }

    }

    // Registration
    public function registration() {

        // Initial Variable
        $res = null;
        $code = 200;
        $error = array();
        $message = "";

        // Ger Request
        $req = json_decode($this->input->raw_input_stream, TRUE);

        // Check Is Not Null
        $checkIsValid = $this->response->checkIsNotNull(
            array(
                "first_name",
                "last_name",
                "email",
                "password",
                "phone_number",
                "level"
            ),
            $req
        );

        // Setup Error If Have
        $error = $checkIsValid;

        if (count($error) == 0) {

            // Check Is Not Duplicated
            $checkIsUnique = $this->MUsers->getWhere(
                array(
                    "email" => $req["email"]
                )
            );

            if (count($checkIsUnique) > 0) {
                $code = 403;
                $error[] = "Email already exists";
            }
    
            // Check Is Not Duplicated
            $checkIsUnique = $this->MUsers->getWhere(
                array(
                    "phone_number" => $req["phone_number"]
                )
            );
    
            if (count($checkIsUnique) > 0) {
                $code = 403;
                $error[] = "Phone Number already exists";
            }
        }

        // Check Is Have Error or not?
        if (count($error) == 0) {
            // Todo: Create User
            $id_m_users = $this->MUsers->create(
                array(
                    "first_name" => $req["first_name"],
                    "last_name" => $req["last_name"],
                    "email" => $req["email"],
                    "password" => md5($req["password"]),
                    "phone_number" => $req["phone_number"],
                    "created_at" => date("Y-m-d H:i:s"),
                    "updated_at" => date("Y-m-d H:i:s")
                )
            );
            // Check Is Created User?
            if ($id_m_users != -1) {
                // Create Utility User
                $token = md5(date("Y-m-d H:i:s") . $id_m_users . $req["password"]);
                $id_u_user = $this->UUser->create(
                    array(
                        "id_face" => 1,
                        "id_m_users" => $id_m_users,
                        "level" => $req["level"],
                        "token" => $token,
                        "created_at" => date("Y-m-d H:i:s"),
                        "updated_at" => date("Y-m-d H:i:s")
                    )
                );
                // Check Is Utility Created?
                if ($id_u_user != -1) {
                    // Create Notification Success
                    $this->MNotification->create(array(
                        "type" => "Authentication",
                        "title" => "Success to create user",
                        "content" => "Success to create user with phone number " . $req["phone_number"],
                        "id_m_users" => $id_m_users,
                        "created_at" => date("Y-m-d H:i:s"),
                        "updated_at" => date("Y-m-d H:i:s")
                    ));

                    // Create Wallet
                    $walletToken = $this->getWalletToken();
                    $this->custom_curl->setHeader(array(
                        "Authorization:" . $walletToken
                    ));
                    $this->custom_curl->setPost(http_build_query(
                        array(
                            "user_type" => "personal",
                            "phone" => $req["phone_number"],
                            "name" => $req["first_name"] . " " . $req["last_name"],
                            "email" => $req["email"]
                        )
                    ));
                    $this->custom_curl->createCurl(API_URI_TRON_WALLET);
        
                    try {
                        $data = json_decode($this->custom_curl->__tostring());
                        $req["wallet"] = $data;
                    } catch(Exception $e) {
                        $req["wallet"] = null;
                    }

                    // Setup Response
                    $res = $req;
                    $res["password"] = md5($req["password"]);
                    $res["token"] = $token;
                    $message = "Success to create user";
                } else {
                    $this->MUsers->delete(array(
                        "id" => $id_m_users
                    ));

                    // Setup Failed Response
                    $code = 500;
                    $error[] = "Failed to create utility users";
                    $message = "Faild for create user";
                }
            } else {
                $code = 500;
                $error[] = "Failed to create users";
                $message = "Faild for create user";
            }
        } else {
            $code = 402;
            $message = "Something went wrong on your form";
        }

        print_r ($this->response->json($res, $code, $error, $message));

    }

    // Login
    public function login() {

        // Initial Variable
        $res = null;
        $code = 200;
        $error = array();
        $message = "";

        // Ger Request
        $req = json_decode($this->input->raw_input_stream, TRUE);

        // Check Is Not Null
        $checkIsValid = $this->response->checkIsNotNull(
            array(
                "phone_number"
            ),
            $req
        );

        // Setup Error If Have
        $error = $checkIsValid;

        // Check Is Exists
        $checkIsExist = array();
        if (count($error) == 0) {
            $checkIsExist = $this->MUsers->getWhere(
                array(
                    "phone_number" => $req["phone_number"]
                )
            );
        }

        // Check Is Error And User Is Exists
        if (count($checkIsExist) > 0 && count($error) == 0) {
            // Todo: Get Data User
            $checkIsExist = $checkIsExist[0];
            $u_user = $this->UUser->getWhere(array(
                "id_m_users" => $checkIsExist["id"]
            ));
            if (count($u_user) > 0) {
                $u_user = $u_user[0];
                // Create Notification Success
                $this->MNotification->create(array(
                    "type" => "Authentication",
                    "title" => "Success to login user",
                    "content" => "Success to login user with phone number " . $req["phone_number"],
                    "id_m_users" => $checkIsExist["id"],
                    "created_at" => date("Y-m-d H:i:s"),
                    "updated_at" => date("Y-m-d H:i:s")
                ));
                $res = array(
                    "first_name" => $checkIsExist["first_name"],
                    "last_name" => $checkIsExist["last_name"],
                    "email" => $checkIsExist["email"],
                    "password" => $checkIsExist["password"],
                    "phone_number" => $checkIsExist["phone_number"],
                    "level" => $u_user["level"],
                    "token" => $u_user["token"]
                );
                $message = "Success to fetch user";
            } else {
                $code = 500;
                $error[] = "Failed to login users";
                $message = "Faild for login user";
            }
        } else {
            $code = 403;
            $error[] = "User not found";
            $message = "User not found, please check your phone number";
        }

        print_r ($this->response->json($res, $code, $error, $message));

    }

    // Profile
    public function profile() {

        // Initial Variable
        $res = null;
        $code = 200;
        $error = array();
        $message = "";

        // Get Token
        $token = $this->input->get_request_header('Authorization', TRUE) ?: "";

        // Check Token Is Not Null
        if (isset($token) || !empty($token)) {
            // Get Utility User
            $u_user = $this->UUser->getWhere(array(
                "token" => $token
            ));

            // Check User Is Exists
            if (count($u_user) > 0) {
                $u_user = $u_user[0];
                // Get User
                $m_users = $this->MUsers->getWhere(array(
                    "id" => $u_user["id_m_users"]
                ));
                
                // Check Is Exists
                if (count($m_users) > 0) {
                    $m_users = $m_users[0];
                    // Get Face
                    $m_medias = $this->MMedias->getWhere(array(
                        "id" => $u_user["id_face"]
                    ));
                    // Check Is Exists
                    if (count($m_medias) > 0) {
                        $m_medias = $m_medias[0];
                        // Create Notification Success
                        $this->MNotification->create(array(
                            "type" => "Authentication",
                            "title" => "Success to fetch profile",
                            "content" => "Success to fetch profile",
                            "id_m_users" => $m_users["id"],
                            "created_at" => date("Y-m-d H:i:s"),
                            "updated_at" => date("Y-m-d H:i:s")
                        ));
                        $res = array(
                            "first_name" => $m_users["first_name"],
                            "last_name" => $m_users["last_name"],
                            "email" => $m_users["email"],
                            "password" => $m_users["password"],
                            "phone_number" => $m_users["phone_number"],
                            "utility" => array(
                                "level" => $u_user["level"],
                                "token" => $u_user["token"],
                                "face" => array(
                                    "uri" => $m_medias["uri"],
                                    "label" => $m_medias["label"]
                                )
                            )
                        );
                        $message = "Success to fetch profile";
                    } else {
                        $code = 500;
                        $error[] = "Failed to fetch face";
                        $message = "Failed to fetch face";
                    }
                } else {
                    $code = 500;
                    $error[] = "Failed to fetch user";
                    $message = "Failed to fetch user";
                }
            } else {
                $code = 500;
                $error[] = "Failed to fetch utility user, cause token not valid";
                $message = "Failed to fetch utility user, cause token not valid";
            }
        } else {
            $code = 403;
            $error[] = "Token not found";
            $message = "Token not found";
        }

        print_r ($this->response->json($res, $code, $error, $message));

    }

    // Update Profile
    public function update_profile() {

        // Initial Variable
        $res = null;
        $code = 200;
        $error = array();
        $message = "";

        // Get Token
        $token = $this->input->get_request_header('Authorization', TRUE) ?: "";

        // Check Token Is Not Null
        if (isset($token) || !empty($token)) {
            // Ger Request
            $req = json_decode($this->input->raw_input_stream, TRUE);

            // Check Is Not Null
            $checkIsValid = $this->response->checkIsNotNull(
                array(
                    "first_name",
                    "last_name",
                    "email",
                    "phone_number",
                    "level"
                ),
                $req
            );

            // Setup Error If Have
            $error = $checkIsValid;

            // Check Is Have Error or not?
            if (count($error) == 0) {
                // Get Utility User
                $u_user = $this->UUser->getWhere(array(
                    "token" => $token
                ));
    
                // Check User Is Exists
                if (count($u_user) > 0) {
                    $u_user = $u_user[0];
                    // Update M User
                    $status_update_m_users = $this->MUsers->update(array(
                        "id" => $u_user["id_m_users"]
                    ), array(
                        "first_name" => $req["first_name"],
                        "last_name" => $req["last_name"],
                        "email" => $req["email"],
                        "phone_number" => $req["phone_number"],
                        "updated_at" => date("Y-m-d H:i:s")
                    ));

                    // Check is success updated
                    if ($status_update_m_users != -1) {
                        // Update U User
                        $status_update_u_user = $this->UUser->update(array(
                            "id" => $u_user["id"]
                        ), array(
                            "level" => $req["level"],
                            "updated_at" => date("Y-m-d H:i:s")
                        ));

                        // Create Notification Success
                        $this->MNotification->create(array(
                            "type" => "Authentication",
                            "title" => "Success to update profile",
                            "content" => "Success to update profile",
                            "id_m_users" => $u_user["id_m_users"],
                            "created_at" => date("Y-m-d H:i:s"),
                            "updated_at" => date("Y-m-d H:i:s")
                        ));

                        $res = $req;
                        $res["token"] = $token;
                        $message = "Success to update user";
                    } else {
                        $code = 500;
                        $error[] = "Failed to update user";
                        $message = "Failed to update user";
                    }
                } else {
                    $code = 500;
                    $error[] = "Failed to fetch utility user, cause token not valid";
                    $message = "Failed to fetch utility user, cause token not valid";
                }
            } else {
                $code = 402;
                $message = "Something went wrong on your form";
            }
        } else {
            $code = 403;
            $error[] = "Token not found";
            $message = "Token not found";
        }

        print_r ($this->response->json($res, $code, $error, $message));

    }

    // Update Profile Photo
    public function update_photo_profile() {

        // Initial Variable
        $res = null;
        $code = 200;
        $error = array();
        $message = "";

        // Get Token
        $token = $this->input->get_request_header('Authorization', TRUE) ?: "";

        // Check Token Is Not Null
        if (isset($token) || !empty($token)) {
            if (isset($_FILES["face"]["name"])) {
                $face = $this->fileUpload->do_upload("face");
                if ($face["status"]) {
                    // Upload File
                    $id_face = $this->MMedias->create(array(
                        "uri" => base_url("assets/dist/img/") . $face["file_name"],
                        "label" => $face["file_name"],
                        "created_at" => date("Y-m-d H:i:s"),
                        "updated_at" => date("Y-m-d H:i:s")
                    ));

                    // Check If Upload Success
                    if ($id_face != -1) {
                        // Update Id Face
                        $status_update_u_user = $this->UUser->update(array(
                            "token" => $token
                        ), array(
                            "id_face" => $id_face,
                            "updated_at" => date("Y-m-d H:i:s")
                        ));

                        if($status_update_u_user != -1) {
                            $res = $face;
                            $message = "Success to update face";
                        } else {
                            $code = 500;
                            $error[] = "Failed to update face";
                            $message = "Failed to update face";
                        }
                    } else {
                        $code = 500;
                        $error[] = "Failed to upload face";
                        $message = "Failed to upload face, cause status is not ready";
                    }
                } else {
                    $code = 402;
                    $error[] = "Failed to upload face";
                    $message = "Failed to upload face, cause status is not ready";
                }
            } else {
                $code = 403;
                $error[] = "File face is required";
                $message = "File face is required";
            }
        } else {
            $code = 403;
            $error[] = "Token not found";
            $message = "Token not found";
        }

        print_r ($this->response->json($res, $code, $error, $message));

    }

    // Get Users
    public function get_users() {

        // Initial Variable
        $res = array();
        $code = 200;
        $error = array();
        $message = "";

        // Get Token
        $token = $this->input->get_request_header('Authorization', TRUE) ?: "";

        // Check Token Is Not Null
        if (isset($token) || !empty($token)) {
            // Get Utility User
            $u_user = $this->UUser->getWhere(array(
                "token" => $token
            ));

            // Check User Is Exists
            if (count($u_user) > 0) {
                // Get User
                $m_users = $this->MUsers->getAll();
                
                // Check Is Exists
                if (count($m_users) > 0) {
                    $temp_m_users = array();
                    foreach ($m_users as $m_user) {
                        // Get Utility
                        $u_user = $this->UUser->getWhere(array(
                            "id_m_users" => $m_user["id"]
                        ));
                        if (count($u_user) > 0) {
                            $u_user = $u_user[0];
                            // Get Face
                            $m_medias = $this->MMedias->getWhere(array(
                                "id" => $u_user["id_face"]
                            ));
                            // Check Is Exists
                            if (count($m_medias) > 0) {
                                $m_medias = $m_medias[0];
                                $temp_m_users[] = array(
                                    "first_name" => $m_user["first_name"],
                                    "last_name" => $m_user["last_name"],
                                    "email" => $m_user["email"],
                                    "password" => $m_user["password"],
                                    "phone_number" => $m_user["phone_number"],
                                    "utility" => array(
                                        "level" => $u_user["level"],
                                        "token" => $u_user["token"],
                                        "face" => array(
                                            "uri" => $m_medias["uri"],
                                            "label" => $m_medias["label"]
                                        )
                                    )
                                );
                            }
                        }
                    }
                    $res = $temp_m_users;
                    $message = "Success to fetch users";
                } else {
                    $code = 500;
                    $error[] = "Failed to fetch users";
                    $message = "Failed to fetch users";
                }
            } else {
                $code = 500;
                $error[] = "Failed to fetch utility users, cause token not valid";
                $message = "Failed to fetch utility users, cause token not valid";
            }
        } else {
            $code = 403;
            $error[] = "Token not found";
            $message = "Token not found";
        }

        print_r ($this->response->json($res, $code, $error, $message));

    }
}