<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Notification extends CI_Controller {

    // Public Variable
    public $custom_log, $session, $response, $fileUpload, $userType;

    public function __construct() {
        parent::__construct();

        // Load Model
        $this->load->model("MMedias");
        $this->load->model("MNotification");
        $this->load->model("UUser");

        // Load Helper
        $this->session = new Session_helper();
        $this->response = new Response_helper();
        $this->fileUpload = new Upload_file_helper(
            array(
                "file_type" => array(
                    "png",
                    "jpg",
                    "jpeg"
                ),
                "max_size"  => 200000000
            )
        );
    }

    public function getNotification() {
        // Initial Variable
        $res = null;
        $code = 200;
        $error = array();
        $message = "";

        // Get Token
        $token = $this->input->get_request_header('Authorization', TRUE) ?: "";
        $page = $this->input->post_get("page") ?: 0;

        // Check Token Is Not Null
        if (isset($token) || !empty($token)) {

            // Get Utility User
            $u_user = $this->UUser->getWhere(array(
                "token" => $token
            ));

            // Check User Is Exists
            if (count($u_user) > 0) {
                $u_user = $u_user[0];
                
                // Check Is Exists
                $notification = $this->MNotification->getPagination(
                    array(
                        "id_m_users" => $u_user["id_m_users"]
                    ), $page, "id", "DESC"
                );
                $res = $notification;
                $message = "Success to fetch notification";
            } else {
                $code = 500;
                $error[] = "Failed to fetch utility user, cause token not valid";
                $message = "Failed to fetch utility user, cause token not valid";
            }

        } else {
            $code = 403;
            $error[] = "Token not found";
            $message = "Token not found";
        }

        print_r ($this->response->json($res, $code, $error, $message));
    }

}