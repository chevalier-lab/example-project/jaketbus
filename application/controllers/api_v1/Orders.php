<?php
date_default_timezone_set('Asia/Jakarta');
defined('BASEPATH') OR exit('No direct script access allowed');

class Orders extends CI_Controller {

    // Public Variable
    public $custom_curl, $session, $response, $fileUpload, $userType;

    public function __construct() {
        parent::__construct();

        // Load Model
        $this->load->model("MMedias");
        $this->load->model("MUsers");
        $this->load->model("MTronWallet");
        $this->load->model("MPuloGebang");
        $this->load->model("UUser");
        $this->load->model("UUserOrder");
        $this->load->model("UUserOrderRf");
        $this->load->model("UUserOrderRd");
        $this->load->model("UUserOrderSg");
        $this->load->model("UUserOrderTicket");
        $this->load->model("UUserOrderTicketF");
        $this->load->model("UUserOrderTicketS");
        $this->load->model("UUserOrderTicketSP");
        $this->load->model("MNotification");

        // Load Helper
        $this->session = new Session_helper();
        $this->response = new Response_helper();
        $this->custom_curl = new Mycurl_helper("");
        $this->fileUpload = new Upload_file_helper(
            array(
                "file_type" => array(
                    "png",
                    "jpg",
                    "jpeg"
                ),
                "max_size"  => 200000000
            )
        );
    }

    // Create Order
    private function c_order($data=array()) {
        $id_u_user_order = $this->UUserOrder->create(
            array(
                "count_of_passenger" => $data["count_of_passenger"],
                "id_m_users" => $data["id_m_users"]
            )
        );
        if ($id_u_user_order != -1) {
            $data["id"] = $id_u_user_order;
            $data = $this->c_order_rf($data);
            return $data;
        } else {
            return null;
        }
    }

    // Create Order Route From
    private function c_order_rf($data=array()) {
        $id_u_user_order_rf = $this->UUserOrderRf->create(
            array(
                "id_u_user_order" => $data["id"],
                "id_route" => $data["rf"]["id_route"],
                "province" => $data["rf"]["province"],
                "city" => $data["rf"]["city"],
                "region" => $data["rf"]["region"]
            )
        );
        if ($id_u_user_order_rf != -1) {
            $data["rf"]["id"] = $id_u_user_order_rf;
            $data = $this->c_order_rd($data);
            return $data;
        } else {
            return null;
        }
    }

    // Create Order Route Destination
    private function c_order_rd($data=array()) {
        $id_u_user_order_rd = $this->UUserOrderRd->create(
            array(
                "id_u_user_order" => $data["id"],
                "id_route" => $data["rd"]["id_route"],
                "province" => $data["rd"]["province"],
                "city" => $data["rd"]["city"],
                "region" => $data["rd"]["region"]
            )
        );
        if ($id_u_user_order_rd != -1) {
            $data["rd"]["id"] = $id_u_user_order_rd;
            $data = $this->c_order_sg($data);
            return $data;
        } else {
            return null;
        }
    }

    // Craete Order Schedule Go
    private function c_order_sg($data=array()) {
        $id_u_user_order_sg = $this->UUserOrderSg->create(
            array(
                "id_u_user_order" => $data["id"],
                "date" => $data["sg"]["date"]
            )
        );
        if ($id_u_user_order_sg != -1) {
            $data["sg"]["id"] = $id_u_user_order_sg;
            return $data;
        } else {
            return null;
        }
    }

    // Create Order
    public function createOrder() {
        
        // Initial Variable
        $res = null;
        $code = 200;
        $error = array();
        $message = "";

        // Get Token
        $token = $this->input->get_request_header('Authorization', TRUE) ?: "";

        // Check Token Is Not Null
        if (isset($token) || !empty($token)) {

            // Ger Request
            $req = json_decode($this->input->raw_input_stream, TRUE);

            // Check Is Not Null
            $checkIsValid = $this->response->checkIsNotNull(
                array(
                    "route_from",
                    "route_to",
                    "count_of_passenger",
                    "schedule_go"
                ),
                $req
            );

            // Setup Error If Have
            $error = $checkIsValid;

            if (count($error) == 0) {
                // Get Utility User
                $u_user = $this->UUser->getWhere(array(
                    "token" => $token
                ));

                // Check User Is Exists
                if (count($u_user) > 0) {
                    $u_user = $u_user[0];
                    // Get User
                    $m_users = $this->MUsers->getWhere(array(
                        "id" => $u_user["id_m_users"]
                    ));
                    
                    // Check Is Exists
                    if (count($m_users) > 0) {
                        $m_users = $m_users[0];
                        $data = $this->c_order(
                            array(
                                "id_m_users" => $m_users["id"],
                                "count_of_passenger" => $req["count_of_passenger"],
                                "rf" => array(
                                    "id_route" => $req["route_from"]["id_route"],
                                    "province" => $req["route_from"]["province"],
                                    "city" => $req["route_from"]["city"],
                                    "region" => $req["route_from"]["region"]
                                ),
                                "rd" => array(
                                    "id_route" => $req["route_to"]["id_route"],
                                    "province" => $req["route_to"]["province"],
                                    "city" => $req["route_to"]["city"],
                                    "region" => $req["route_to"]["region"]
                                ),
                                "sg" => array(
                                    "date" => $req["schedule_go"]
                                )
                            )
                        );

                        $res = $data;
                        $message = "Success to create order";
                    } else {
                        $code = 500;
                        $error[] = "Failed to fetch order";
                        $message = "Failed to fetch order";
                    }
                } else {
                    $code = 500;
                    $error[] = "Failed to fetch utility user, cause token not valid";
                    $message = "Failed to fetch utility user, cause token not valid";
                }
            } else {
                $code = 403;
                $message = "Something went, wrong";
            }

        } else {
            $code = 403;
            $error[] = "Token not found";
            $message = "Token not found";
        }

        print_r ($this->response->json($res, $code, $error, $message));
    }

    private function c_order_ticket($data=array()) {
        $payment = isset($data["payment_method"]) ? $data["payment_method"] : 'balance';
        $id_u_user_order_ticket = $this->UUserOrderTicket->create(
            array(
                "id_u_user_order" => $data["id_u_user_order"],
                "id_schedule" => $data["id_schedule"],
                "id_info" => $data["id_info"],
                "date_of_departure" => $data["date_of_departure"],
                "time_of_departure" => $data["time_of_departure"],
                "price" => $data["price"],
                "id_of_origin" => $data["id_of_origin"],
                "destionation_id" => $data["destionation_id"],
                "route" => $data["route"],
                "id_category" => $data["id_category"],
                "id_po" => $data["id_po"],
                "total_passenger" => $data["total_passenger"],
                "po_name" => $data["po_name"],
                "destionation" => $data["destionation"],
                "origin" => $data["origin"],
                "type_of_bus" => $data["type_of_bus"],
                "available" => $data["available"],
                "payment_method" => $payment,
                "payment_status" => $payment == 'balance' ? 'paid' : 'unpaid',
                "payment_fee" => 0,
                "payment_expired" => date("Y-m-d H:i:s", strtotime('+1 hour')),
                "payment_code" => rand(10000, 99999). strtotime('now')
            )
        );
        if ($id_u_user_order_ticket != -1) {
            $data["id"] = $id_u_user_order_ticket;
            $data = $this->c_order_ticket_f($data);
            return $data;
        } else {
            return null;
        }
    }

    // Get TOKEN
    private function getWalletToken() {

        // Initial Variable
        $res = array();
        $error = array();

        if (count($error) == 0) {
            // Todo Authentication
            $res = $this->MTronWallet->query("
            SELECT `api_token` FROM `m_tron_wallet` ORDER BY `id` DESC LIMIT 1");
            if (count($res) == 1) {
                $res = $res[0];
                return $res["api_token"];
            } else {
                return "";
            }
        } else {
            return "";
        }

    }

    // CURL Wallet
    private function curlWalletTron($req, $token, $path) {
        $this->custom_curl->setHeader(array(
            "Authorization:" . $token
        ));
        $this->custom_curl->setPost(http_build_query($req));
        $this->custom_curl->createCurl(API_URI_TRON_WALLET . $path);

        $kaspro = array();
        try {
            $kaspro = json_decode($this->custom_curl->__tostring());
        } catch(Exception $e) {
            $kaspro = $e->getMessage();
        }
        return $kaspro;
    }

    // Get Token
    private function getToken() {

        // Todo Authentication
        $res = $this->MPuloGebang->query("
        SELECT `api_token` FROM `m_pulo_gebang` ORDER BY `id` DESC LIMIT 1");
        if (count($res) == 1) {
            $res = $res[0];
            return $res["api_token"];
        } else {
            return "";
        }

    }

    // Create Order Ticket
    public function createOrderTicket() {
        
        // Initial Variable
        $res = null;
        $code = 200;
        $error = array();
        $message = "";

        // Get Token
        $token = $this->input->get_request_header('Authorization', TRUE) ?: "";

        // Check Token Is Not Null
        if (isset($token) || !empty($token)) {

            // Ger Request
            $req = json_decode($this->input->raw_input_stream, TRUE);

            // Check Is Not Null
            $checkIsValid = $this->response->checkIsNotNull(
                array(
                    "id_u_user_order",
                    "id_schedule",
                    "id_info",
                    "date_of_departure",
                    "time_of_departure",
                    "price",
                    "id_of_origin",
                    "destionation_id",
                    "route",
                    "id_category",
                    "id_po",
                    "total_passenger",
                    "po_name",
                    "destionation",
                    "origin",
                    "type_of_bus",
                    "available",
                    // "facilities",
                    "seats",
                    "payment_method",
                    "booking"
                ),
                $req
            );

            // Setup Error If Have
            $error = $checkIsValid;

            if (count($error) == 0) {
                // Get Utility User
                $u_user = $this->UUser->getWhere(array(
                    "token" => $token
                ));

                // Check User Is Exists
                if (count($u_user) > 0) {
                    $u_user = $u_user[0];
                    // Get User
                    $m_users = $this->MUsers->getWhere(array(
                        "id" => $u_user["id_m_users"]
                    ));
                    
                    // Check Is Exists
                    if (count($m_users) > 0) {
                        $m_users = $m_users[0];

                        $payment = isset($req["payment_method"]) ? $req["payment_method"] : 'balance';

                        if ($payment == 'balance') {
                            $this->paymentBalance($m_users, $req);
                        } else {
                            $this->paymentIndomaret($m_users, $req);
                        }

                    } else {
                        $code = 500;
                        $error[] = "Failed to fetch order ticket";
                        $message = "Failed to fetch order ticket";
                    }
                } else {
                    $code = 500;
                    $error[] = "Failed to fetch utility user, cause token not valid";
                    $message = "Failed to fetch utility user, cause token not valid";
                }
            } else {
                $code = 403;
                $message = "Something went, wrong";
            }

        } else {
            $code = 403;
            $error[] = "Token not found";
            $message = "Token not found";
        }

        print_r ($this->response->json($res, $code, $error, $message));
    }

    private function paymentBalance($m_users, $req) {
        $token = $this->getToken();
        $payment = isset($req["payment_method"]) ? $req["payment_method"] : 'balance';
        
        // Initial Variable
        $res = null;
        $code = 200;
        $error = array();
        $message = "";

        $walletToken = $this->getWalletToken();
        $balance = $this->curlWalletTron(array(
            "user_type" => "personal",
            "phone" => $m_users["phone_number"]
        ), $walletToken, "profile");
        if ($balance->success) {

            // Todo Authentication
            $this->custom_curl->setHeader(array(
                "Content-Type: application/json",
                "X-TTPG-KEY: " . $token
            ));
            $this->custom_curl->setPost(json_encode($req["booking"]));
            $this->custom_curl->createCurl(API_URI_PULO_GEBANG . "ticket/booking");

            try {
                $data = json_decode($this->custom_curl->__tostring());
                if ($data->status) {

                    $balance = (int)$balance->result->balance;
                    $price = ((int) $req["price"]) * count($req["booking"]);
                    if ($balance >= $price) {
                        // Do Cut Balance
                        $this->curlWalletTron(array(
                            "phone" => $m_users["phone_number"],
                            "amount" => $price,
                            "user_type" => "personal",
                            "trx_type" => "D",
                            "note" => "Pembelian tiket dengan rute: ".$req["route"]
                        ), $walletToken, "transaction");
                    } else {
                        $code = 500;
                        $error[] = "Failed, saldo tidak mencukupi";
                        $message = "Failed, saldo tidak mencukupi";
                        print_r ($this->response->json($res, $code, $error, $message));
                        die();
                    }

                    foreach ($data->data as $item) {
                        $id_u_user_order_ticket = $this->UUserOrderTicket->create(
                            array(
                                "id_u_user_order" => $req["id_u_user_order"],
                                "ticket_id" => $item->ticket_id,
                                "id_m_users" => $m_users["id"],
                                "passanger_name" => $item->passanger_name,
                                "passanger_gender" => $item->passanger_gender,
                                "passanger_telp" => $item->passanger_telp,
                                "pnr" => $item->pnr,
                                "seat_id" => $item->seat_id,
                                "is_boarding" => $item->is_boarding,
                                "date_of_boarding" => $item->date_of_boarding,
                                "date_of_departure" => $item->date_of_departure,
                                "time_of_departure" => $item->time_of_departure,
                                "price" => $item->price,
                                "id_schedule" => $item->id_schedule,
                                "id_of_origin" => $item->id_of_origin,
                                "destionation_id" => $item->destionation_id,
                                "category" => $item->category,
                                "po_name" => $item->po_name,
                                "id_po" => isset($item->id_po) ? $item->id_po : "",
                                "destionation" => $item->destionation,
                                "origin" => $item->origin,
                                "seat_number" => $item->seat_number,
                                "created_at" => date("Y-m-d H:i:s"),
                                "updated_at" => date("Y-m-d H:i:s"),
                                "payment_method" => $payment,
                                "payment_status" => $payment == 'balance' ? 'paid' : 'unpaid',
                                "payment_fee" => 0,
                                "payment_expired" => date("Y-m-d H:i:s"),
                                "payment_code" => rand(10000, 99999). strtotime('now')
                            )
                        );
                        if ($id_u_user_order_ticket != -1) {
                            foreach ($item->facility as $facility) {
                                $id_u_user_order_ticket_f = $this->UUserOrderTicketF->create(
                                    array(
                                        "id_u_user_order_ticket" => $id_u_user_order_ticket,
                                        "facility_name" => $facility->facility_name,
                                        "facility_icon" => $facility->facility_icon
                                    )
                                );
                            }

                            // Create Notification 
                            $this->MNotification->create(
                                array(
                                    "type" => "Order",
                                    "title" => "Berhasil memesan tiket dengan ID-" . $item->ticket_id,
                                    "content" => "Tiket dengan ID-" . $item->ticket_id . " berhasil dipesan dengan rute " . $item->origin . " - " . $item->destionation,
                                    "id_m_users" => $m_users["id"],
                                    "created_at" => date("Y-m-d H:i:s"),
                                    "updated_at" => date("Y-m-d H:i:s")
                                )
                            );
                        }
                    }
                }
                $res = $req;
            } catch(Exception $e) {
                $res = null;
                $code = 500;
                $error[] = "Failed, saldo belum dibuat";
                $message = "Failed, saldo belum dibuat";
            }
        } else {
            $code = 500;
            $error[] = "Failed, saldo belum dibuat";
            $message = "Failed, saldo belum dibuat";
        }
        print_r ($this->response->json($res, $code, $error, $message));
        die();
    }

    private function paymentIndomaret($m_users, $req) {
        
        // Initial Variable
        $res = null;
        $code = 200;
        $error = array();
        $message = "";

        $payment = isset($req["payment_method"]) ? $req["payment_method"] : 'balance';

        foreach ($req["booking"] as $item) {
            $id_u_user_order_ticket = $this->UUserOrderTicket->create(
                array(
                    "id_u_user_order" => $req["id_u_user_order"],
                    "ticket_id" => null,
                    "id_m_users" => $m_users["id"],
                    "passanger_name" => $item["pnp_name"],
                    "passanger_gender" => $item["pnp_genre"],
                    "passanger_telp" => $item["pnp_no_telp"],
                    "pnr" => $item["pnr"],
                    "seat_id" => $item["id_seat"],
                    "is_boarding" => 0,
                    "date_of_departure" => $req["date_of_departure"],
                    "time_of_departure" => $req["time_of_departure"],
                    "price" => $req["price"],
                    "id_schedule" => $req["id_schedule"],
                    "id_of_origin" => $req["id_of_origin"],
                    "destionation_id" => $req["destionation_id"],
                    "category" => $req["type_of_bus"],
                    "po_name" => $req["po_name"],
                    "id_po" => isset($req["id_po"]) ? $req["id_po"] : "",
                    "destionation" => $req["destionation"],
                    "origin" => $req["origin"],
                    "seat_number" => $item["seat"],
                    "created_at" => date("Y-m-d H:i:s"),
                    "updated_at" => date("Y-m-d H:i:s"),
                    "payment_method" => $payment,
                    "payment_status" => $payment == 'balance' ? 'paid' : 'unpaid',
                    "payment_fee" => 0,
                    "payment_expired" => date("Y-m-d H:i:s", strtotime('+1 hour')),
                    "payment_code" => rand(10000, 99999). strtotime('now')
                )
            );
            if ($id_u_user_order_ticket != -1) {
                foreach ($req["facilities"] as $facility) {
                    $id_u_user_order_ticket_f = $this->UUserOrderTicketF->create(
                        array(
                            "id_u_user_order_ticket" => $id_u_user_order_ticket,
                            "facility_name" => $facility["facility_name"],
                            "facility_icon" => $facility["facility_icon"]
                        )
                    );
                }

                // Create Notification 
                $this->MNotification->create(
                    array(
                        "type" => "Bill",
                        "title" => "Berhasil membuat tagihan",
                        "content" => "Tagihan berhasil dibuat dengan rute " . $req['origin'] . " - " . $req['destionation'],
                        "id_m_users" => $m_users["id"],
                        "created_at" =>date("Y-m-d H:i:s"),
                        "updated_at" => date("Y-m-d H:i:s")
                    )
                );
            } else {
                $code = 500;
                $error[] = "Failed, Something went wrong";
                $message = "Failed, Something went wrong";
            }
        }

        if ($code == 200)
            $res = $req;

        print_r ($this->response->json($res, $code, $error, $message));
        die();
    }

    // Get Tickets
    public function getTickets() {
        
        // Initial Variable
        $res = null;
        $code = 200;
        $error = array();
        $message = "";

        // Get Token
        $token = $this->input->get_request_header('Authorization', TRUE) ?: "";

        // Ger Request
        $req = json_decode($this->input->raw_input_stream, TRUE);

        // Check Is Not Null
        $checkIsValid = $this->response->checkIsNotNull(
            array(
                "page"
            ),
            $req
        );

        $page = 0;
        $isHistory = 0;

        if (count($checkIsValid) > 0) {
            $page = isset($req["page"]) ? $req["page"] : 0;
            $isHistory = isset($req["isHistory"]) ? $req["isHistory"] : 0;
        }

        // Check Token Is Not Null
        if (isset($token) || !empty($token)) {

            // Get Utility User
            $u_user = $this->UUser->getWhere(array(
                "token" => $token
            ));

            // Check User Is Exists
            if (count($u_user) > 0) {
                $u_user = $u_user[0];
                // Get User
                $m_users = $this->MUsers->getWhere(array(
                    "id" => $u_user["id_m_users"]
                ));
                
                // Check Is Exists
                if (count($m_users) > 0) {
                    $m_users = $m_users[0];
                    $data = array();
                    $where = array(
                        "id_m_users" => $m_users["id"],
                        "payment_status" => "paid"
                    );

                    if ($isHistory == 0) {
                        $where["date_of_departure >="] = date("Y-m-d");
                        $userOrder = $this->UUserOrderTicket->getPaginationWherePage(
                            $where, $page, "id", "DESC"
                        );
                    } else {
                        $where["date_of_departure <"] = date("Y-m-d");
                        $userOrder = $this->UUserOrderTicket->getWhere(
                            $where, "id", "DESC"
                        );
                    }

                    foreach ($userOrder as $item) {
                        $ticket = $this->UUserOrderTicketF->getWhere(
                            array(
                                "id_u_user_order_ticket" => $item["id"]
                            )
                        );

                        if (count($ticket) > 0) {
                            $item["facility"] = $ticket;
                        }
                        $data[] = $item;
                    }

                    $res = $data;
                    $message = "Success to fetch order";
                } else {
                    $code = 500;
                    $error[] = "Failed to fetch order";
                    $message = "Failed to fetch order";
                }
            } else {
                $code = 500;
                $error[] = "Failed to fetch utility user, cause token not valid";
                $message = "Failed to fetch utility user, cause token not valid";
            }

        } else {
            $code = 403;
            $error[] = "Token not found";
            $message = "Token not found";
        }

        print_r ($this->response->json($res, $code, $error, $message));
    }

    public function getOrder() {
        // Initial Variable
        $res = null;
        $code = 200;
        $error = array();
        $message = "";

        // Get Token
        $token = $this->input->get_request_header('Authorization', TRUE) ?: "";
        $page = $this->input->post_get("page") ?: 0;

        // Check Token Is Not Null
        if (isset($token) || !empty($token)) {

            // Get Utility User
            $u_user = $this->UUser->getWhere(array(
                "token" => $token
            ));

            // Check User Is Exists
            if (count($u_user) > 0) {
                $u_user = $u_user[0];
                // Get User
                $m_users = $this->MUsers->getWhere(array(
                    "id" => $u_user["id_m_users"]
                ));
                
                // Check Is Exists
                if (count($m_users) > 0) {
                    $m_users = $m_users[0];
                    $data = array();
                    $userOrder = $this->UUserOrder->getPagination(
                        array(
                            "id_m_users" => $m_users["id"]
                        ), $page, "id", "DESC"
                    );

                    foreach($userOrder as $item) {
                        $routeFrom = $this->UUserOrderRf->getWhere(
                            array("id_u_user_order" => $item["id"])
                        );
                        $routeTo = $this->UUserOrderRd->getWhere(
                            array("id_u_user_order" => $item["id"])
                        );
                        $scheduleGo = $this->UUserOrderSg->getWhere(
                            array("id_u_user_order" => $item["id"])
                        );

                        if (count($routeFrom) > 0) $routeFrom = $routeFrom[0];
                        if (count($routeTo) > 0) $routeTo = $routeTo[0];
                        if (count($scheduleGo) > 0) $scheduleGo = $scheduleGo[0];

                        $item["route_from"] = $routeFrom;
                        $item["route_to"] = $routeTo;
                        $item["schedule_go"] = $scheduleGo;
                        $data[] = $item;
                    }

                    $res = $data;
                    $message = "Success to fetch order";
                } else {
                    $code = 500;
                    $error[] = "Failed to fetch order";
                    $message = "Failed to fetch order";
                }
            } else {
                $code = 500;
                $error[] = "Failed to fetch utility user, cause token not valid";
                $message = "Failed to fetch utility user, cause token not valid";
            }

        } else {
            $code = 403;
            $error[] = "Token not found";
            $message = "Token not found";
        }

        print_r ($this->response->json($res, $code, $error, $message));
    }
	
    public function getTicket() {
        // Initial Variable
        $res = null;
        $code = 200;
        $error = array();
        $message = "";

        // Get Token
        $page = $this->input->post_get("page") ?: 0;

        // Check Token Is Not Null
        if (isset($token) || !empty($token)) {

            // Get Utility User
            $u_user = $this->UUser->getWhere(array(
                "token" => $token
            ));

            // Check User Is Exists
            if (count($u_user) > 0) {
                $u_user = $u_user[0];
                // Get User
                $m_users = $this->MUsers->getWhere(array(
                    "id" => $u_user["id_m_users"]
                ));
                
                // Check Is Exists
                if (count($m_users) > 0) {
                    $m_users = $m_users[0];
                    $data = array();
                    $userOrder = $this->UUserOrder->getPagination(
                        array(
                            "id_m_users" => $m_users["id"]
                        ), $page, "id", "DESC"
                    );

                    foreach($userOrder as $item) {
                        $routeFrom = $this->UUserOrderRf->getWhere(
                            array("id_u_user_order" => $item["id"])
                        );
                        $routeTo = $this->UUserOrderRd->getWhere(
                            array("id_u_user_order" => $item["id"])
                        );
                        $scheduleGo = $this->UUserOrderSg->getWhere(
                            array("id_u_user_order" => $item["id"])
                        );

                        if (count($routeFrom) > 0) $routeFrom = $routeFrom[0];
                        if (count($routeTo) > 0) $routeTo = $routeTo[0];
                        if (count($scheduleGo) > 0) $scheduleGo = $scheduleGo[0];

                        $item["route_from"] = $routeFrom;
                        $item["route_to"] = $routeTo;
                        $item["schedule_go"] = $scheduleGo;
                        $data[] = $item;
                    }

                    $res = $data;
                    $message = "Success to fetch order";
                } else {
                    $code = 500;
                    $error[] = "Failed to fetch order";
                    $message = "Failed to fetch order";
                }
            } else {
                $code = 500;
                $error[] = "Failed to fetch utility user, cause token not valid";
                $message = "Failed to fetch utility user, cause token not valid";
            }

        } else {
            $code = 403;
            $error[] = "Token not found";
            $message = "Token not found";
        }

        print_r ($this->response->json($res, $code, $error, $message));
    }

    public function ticket_unpaid()
    {
        // Initial Variable
        $res = null;
        $code = 200;
        $error = array();
        $message = "";

        // Get Token
        $data = array();
        $userOrder = $this->UUserOrderTicket->getPaginationWhere(
                array(
                    // "payment_method" => "indomaret",
                    "payment_status" => "unpaid"
                ),
                "id",
                "DESC"
            );

        foreach ($userOrder as $item) {
            $routeFrom = $this->UUserOrderRf->getWhere(
                array("id_u_user_order" => $item["id_u_user_order"])
            );
            $routeTo = $this->UUserOrderRd->getWhere(
                array("id_u_user_order" => $item["id_u_user_order"])
            );
            $scheduleGo = $this->UUserOrderSg->getWhere(
                array("id_u_user_order" => $item["id_u_user_order"])
            );

            if (count($routeFrom) > 0) $routeFrom = $routeFrom[0];
            if (count($routeTo) > 0) $routeTo = $routeTo[0];
            if (count($scheduleGo) > 0) $scheduleGo = $scheduleGo[0];

            $item["route_from"] = $routeFrom;
            $item["route_to"] = $routeTo;
            $item["schedule_go"] = $scheduleGo;
            $item["payment_code"] = (int)$item["payment_code"];
            $item["admin_fee"] = (int)$item["payment_fee"];
            $item["total_amount"] = $item["price"] + $item["payment_fee"];
            $item["trx_date"] = strtotime($item["created_at"]);
            $item["expired"] = 1440;

            if (!empty($item["payment_expired"])) {
                $paymentEx = strtotime($item["payment_expired"]);
                $currentDate = strtotime(date("Y-m-d H:i:s"));

                if ($paymentEx < $currentDate) {
                    if ($item["payment_status"] == "unpaid") {
                        $item["payment_status"] = "expired";
                    }
                }
            }

            $data[] = $item;
        }

        $res = $data;
        $message = "List Ticket Unpaid";


        print_r($this->response->json2($res, $code, $message));
    }

    public function ticket_unpaid_user() {
        // Initial Variable
        $res = array();
        $code = 200;
        $error = array();
        $message = "";

        // Get Token
        $token = $this->input->get_request_header('Authorization', TRUE) ?: "";

        // Check Token Is Not Null
        if (isset($token) || !empty($token)) {

            // Get Utility User
            $u_user = $this->UUser->getWhere(array(
                "token" => $token
            ));

            // Check User Is Exists
            if (count($u_user) > 0) {
                $u_user = $u_user[0];

                // Get Token
                $data = array();
                $userOrder = $this->UUserOrderTicket->getPaginationWhere(
                        array(
                            // "payment_method" => "indomaret",
                            "payment_status" => "unpaid",
                            "id_m_users" => $u_user["id_m_users"]
                        ),
                        "id",
                        "DESC"
                    );

                foreach ($userOrder as $item) {
                    $item["payment_code"] = (int)$item["payment_code"];
                    $item["admin_fee"] = (int)$item["payment_fee"];
                    $item["total_amount"] = $item["price"] + $item["payment_fee"];
                    $item["trx_date"] = strtotime($item["created_at"]);
                    $item["expired"] = 1440;

                    if (!empty($item["payment_expired"])) {
                        $paymentEx = strtotime($item["payment_expired"]);
                        $currentDate = strtotime(date("Y-m-d H:i:s"));
        
                        if ($paymentEx < $currentDate) {
                            if ($item["payment_status"] == "unpaid") {
                                $item["payment_status"] = "expired";
                            }
                        }
                    }

                    $data[] = $item;
                }

                $res = $data;
                $message = "List Order History";

            } else {
                $code = 500;
                $error[] = "Failed to fetch utility user, cause token not valid";
                $message = "Failed to fetch utility user, cause token not valid";
            }

        } else {
            $code = 403;
            $error[] = "Token not found";
            $message = "Token not found";
        }

        print_r ($this->response->json($res, $code, $error, $message));
    }

}