<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class TronWallet extends CI_Controller {

    // Public Variable
    public $custom_curl, $session, $response, $userType;

    public function __construct() {
        parent::__construct();

        // Load Model
        $this->load->model("MUsers");
        $this->load->model("MMedias");
        $this->load->model("UUser");
        $this->load->model("MTronWallet");

        // Load Helper
        $this->session = new Session_helper();
        $this->response = new Response_helper();
        $this->custom_curl = new Mycurl_helper("");
    }

    // Syncronize
    public function syncronize() {

        // Initial Variable
        $res = null;
        $code = 200;
        $error = array();
        $message = "";

        // Ger Request
        $req = json_decode($this->input->raw_input_stream, TRUE);

        // Check Is Not Null
        $checkIsValid = $this->response->checkIsNotNull(
            array(
                "username",
                "password",
                "api_token",
            ),
            $req
        );

        // Setup Error If Have
        $error = $checkIsValid;

        if (count($error) == 0) {
            // Todo Authentication
            $data = array(
                "username" => $req["username"],
                "password" => $req["password"],
                "api_token" => $req["api_token"],
                "created_at" => date("Y-m-d H:i:s"),
                "updated_at" => date("Y-m-d H:i:s")
            );
            $id = $this->MTronWallet->create($data);
            if ($id != -1) {
                $data["id"] = $id;
                $res = $data;
                $message = "Success to create data";
            } else {
                $code = 500;
                $message = "Failed, internal server error";
            }
        } else {
            $code = 402;
            $message = "Something went wrong on your form";
        }

        print_r ($this->response->json($res, $code, $error, $message));

    }

    // Get Last API TOKEN
    public function getAPIToken() {

        // Initial Variable
        $res = null;
        $code = 200;
        $error = array();
        $message = "";

        if (count($error) == 0) {
            // Todo Authentication
            $res = $this->MTronWallet->query("
            SELECT `api_token` FROM `m_tron_wallet` ORDER BY `id` DESC LIMIT 1");
            if (count($res) == 1) {
                $res = $res[0];
                $message = "Success to fetch data";
            } else {
                $code = 401;
                $message = "Failed, data not found";
            }
        } else {
            $code = 402;
            $message = "Something went wrong on your form";
        }

        print_r ($this->response->json($res, $code, $error, $message));

    }

    // Get TOKEN
    private function getWalletToken() {

        // Initial Variable
        $res = array();
        $error = array();

        if (count($error) == 0) {
            // Todo Authentication
            $res = $this->MTronWallet->query("
            SELECT `api_token` FROM `m_tron_wallet` ORDER BY `id` DESC LIMIT 1");
            if (count($res) == 1) {
                $res = $res[0];
                return $res["api_token"];
            } else {
                return "";
            }
        } else {
            return "";
        }

    }

    // CURL Wallet
    private function curlWalletTron($req, $token, $path) {
        $this->custom_curl->setHeader(array(
            "Authorization:" . $token
        ));
        $this->custom_curl->setPost(http_build_query($req));
        $this->custom_curl->createCurl(API_URI_TRON_WALLET . $path);

        $kaspro = array();
        try {
            $kaspro = json_decode($this->custom_curl->__tostring());
        } catch(Exception $e) {
            $kaspro = $e->getMessage();
        }
        return $kaspro;
    }

    // Get Balance
    public function getBalance() {
        
        // Initial Variable
        $res = null;
        $code = 200;
        $error = array();
        $message = "";

        // Get Token
        $token = $this->input->get_request_header('Authorization', TRUE) ?: "";

        // Check Token Is Not Null
        if (isset($token) || !empty($token)) {
            // Get Utility User
            $u_user = $this->UUser->getWhere(array(
                "token" => $token
            ));

            // Check User Is Exists
            if (count($u_user) > 0) {
                $u_user = $u_user[0];
                // Get User
                $m_users = $this->MUsers->getWhere(array(
                    "id" => $u_user["id_m_users"]
                ));
                
                // Check Is Exists
                if (count($m_users) > 0) {
                    $m_users = $m_users[0];
                    // Get Wallet
                    $walletToken = $this->getWalletToken();

                    $wallet = $this->curlWalletTron(array(
                        "user_type" => "personal",
                        "phone" => $m_users["phone_number"]
                    ), $walletToken, "profile");

                    // Get Face
                    $m_medias = $this->MMedias->getWhere(array(
                        "id" => $u_user["id_face"]
                    ));
                    // Check Is Exists
                    if (count($m_medias) > 0) {
                        $m_medias = $m_medias[0];
                        $u_user["face"] = $m_medias;
                    }

                    $m_users["utility"] = $u_user;
                    $m_users["wallet"] = $wallet;
                    $res = $m_users;
                    $message = "Success to fetch user wallet";
                } else {
                    $code = 500;
                    $error[] = "Failed to fetch user, cause token not valid";
                    $message = "Failed to fetch user, cause token not valid";
                }
            } else {
                $code = 500;
                $error[] = "Failed to fetch utility user, cause token not valid";
                $message = "Failed to fetch utility user, cause token not valid";
            }
        } else {
            $code = 403;
            $error[] = "Token not found";
            $message = "Token not found";
        }

        print_r ($this->response->json($res, $code, $error, $message));
    }

    // Deposito
    public function deposito() {
        // Initial Variable
        $res = null;
        $code = 200;
        $error = array();
        $message = "";

        // Get Token
        $token = $this->input->get_request_header('Authorization', TRUE) ?: "";

        // Check Token Is Not Null
        if (isset($token) || !empty($token)) {

            // Ger Request
            $req = json_decode($this->input->raw_input_stream, TRUE);
    
            // Check Is Not Null
            $checkIsValid = $this->response->checkIsNotNull(
                array(
                    "amount"
                ),
                $req
            );

            // Setup Error If Have
            $error = $checkIsValid;

            if (count($error) == 0) {
                // Get Utility User
                $u_user = $this->UUser->getWhere(array(
                    "token" => $token
                ));

                // Check User Is Exists
                if (count($u_user) > 0) {
                    $u_user = $u_user[0];
                    // Get User
                    $m_users = $this->MUsers->getWhere(array(
                        "id" => $u_user["id_m_users"]
                    ));
                    
                    // Check Is Exists
                    if (count($m_users) > 0) {
                        $m_users = $m_users[0];

                        $payment_id = 5;

                        if (isset($req["payment_id"])) $payment_id = $req["payment_id"];

                        // Get Wallet
                        $walletToken = $this->getWalletToken();
                        $res = $this->curlWalletTron(array(
                            "phone" => $m_users["phone_number"],
                            "amount" => $req["amount"],
                            "payment_id" => $payment_id
                        ), $walletToken, "deposit");
                        $message = "Success to deposito";

                    } else {
                        $code = 500;
                        $error[] = "Failed to fetch user, cause token not valid";
                        $message = "Failed to fetch user, cause token not valid";
                    }
                } else {
                    $code = 500;
                    $error[] = "Failed to fetch utility user, cause token not valid";
                    $message = "Failed to fetch utility user, cause token not valid";
                }
            } else {
                $code = 403;
                $message = "Something went wrong";
            }
        } else {
            $code = 403;
            $error[] = "Token not found";
            $message = "Token not found";
        }

        print_r ($this->response->json($res, $code, $error, $message));
    }

    // History Deposito
    public function historyDeposito() {

        // Initial Variable
        $res = array();
        $code = 200;
        $error = array();
        $message = "";

        // Get Token
        $token = $this->input->get_request_header('Authorization', TRUE) ?: "";
        $page = $this->input->post_get("page", TRUE) ?: 1;

        // Check Token Is Not Null
        if (isset($token) || !empty($token)) {
            // Get Utility User
            $u_user = $this->UUser->getWhere(array(
                "token" => $token
            ));

            // Check User Is Exists
            if (count($u_user) > 0) {
                $u_user = $u_user[0];
                // Get User
                $m_users = $this->MUsers->getWhere(array(
                    "id" => $u_user["id_m_users"]
                ));
                
                // Check Is Exists
                if (count($m_users) > 0) {
                    $m_users = $m_users[0];

                    // Get Wallet
                    $walletToken = $this->getWalletToken();

                    $res = $this->curlWalletTron(array(
                        "phone" => $m_users["phone_number"],
                        "page" => $page
                    ), $walletToken, "deposit/history");
                    $message = "Success to fetch history deposito";
                } else {
                    $code = 500;
                    $error[] = "Failed to fetch user, cause token not valid";
                    $message = "Failed to fetch user, cause token not valid";
                }
            } else {
                $code = 500;
                $error[] = "Failed to fetch utility user, cause token not valid";
                $message = "Failed to fetch utility user, cause token not valid";
            }
        } else {
            $code = 403;
            $error[] = "Token not found";
            $message = "Token not found";
        }

        print_r ($this->response->json($res, $code, $error, $message));
    }

    // Detail Deposito
    public function detailDeposito() {

        // Initial Variable
        $res = null;
        $code = 200;
        $error = array();
        $message = "";

        // Get Token
        $token = $this->input->get_request_header('Authorization', TRUE) ?: "";
        $deposit_id = $this->input->post_get("deposit_id", TRUE) ?: 1;

        // Check Token Is Not Null
        if (isset($token) || !empty($token)) {
            // Get Utility User
            $u_user = $this->UUser->getWhere(array(
                "token" => $token
            ));

            // Check User Is Exists
            if (count($u_user) > 0) {
                $u_user = $u_user[0];
                // Get User
                $m_users = $this->MUsers->getWhere(array(
                    "id" => $u_user["id_m_users"]
                ));
                
                // Check Is Exists
                if (count($m_users) > 0) {
                    $m_users = $m_users[0];

                    // Get Wallet
                    $walletToken = $this->getWalletToken();

                    $res = $this->curlWalletTron(array(
                        "phone" => $m_users["phone_number"],
                        "deposit_id" => $deposit_id
                    ), $walletToken, "deposit/detail");
                    $message = "Success to fetch Detail deposito";
                } else {
                    $code = 500;
                    $error[] = "Failed to fetch user, cause token not valid";
                    $message = "Failed to fetch user, cause token not valid";
                }
            } else {
                $code = 500;
                $error[] = "Failed to fetch utility user, cause token not valid";
                $message = "Failed to fetch utility user, cause token not valid";
            }
        } else {
            $code = 403;
            $error[] = "Token not found";
            $message = "Token not found";
        }

        print_r ($this->response->json($res, $code, $error, $message));
    }

    // Transfer
    public function transfer() {
        // Initial Variable
        $res = null;
        $code = 200;
        $error = array();
        $message = "";

        // Get Token
        $token = $this->input->get_request_header('Authorization', TRUE) ?: "";

        // Check Token Is Not Null
        if (isset($token) || !empty($token)) {

            // Ger Request
            $req = json_decode($this->input->raw_input_stream, TRUE);
    
            // Check Is Not Null
            $checkIsValid = $this->response->checkIsNotNull(
                array(
                    "amount",
                    "send_to",
                    "note"
                ),
                $req
            );

            // Setup Error If Have
            $error = $checkIsValid;

            if (count($error) == 0) {
                // Get Utility User
                $u_user = $this->UUser->getWhere(array(
                    "token" => $token
                ));

                // Check User Is Exists
                if (count($u_user) > 0) {
                    $u_user = $u_user[0];
                    // Get User
                    $m_users = $this->MUsers->getWhere(array(
                        "id" => $u_user["id_m_users"]
                    ));
                    
                    // Check Is Exists
                    if (count($m_users) > 0) {
                        $m_users = $m_users[0];

                        // Get Wallet
                        $walletToken = $this->getWalletToken();
                        $res = $this->curlWalletTron(array(
                            "phone" => $m_users["phone_number"],
                            "amount" => $req["amount"],
                            "send_to" => $req["send_to"],
                            "note" => $req["note"]
                        ), $walletToken, "transfer");
                        $message = "Success to transfer";

                    } else {
                        $code = 500;
                        $error[] = "Failed to fetch user, cause token not valid";
                        $message = "Failed to fetch user, cause token not valid";
                    }
                } else {
                    $code = 500;
                    $error[] = "Failed to fetch utility user, cause token not valid";
                    $message = "Failed to fetch utility user, cause token not valid";
                }
            } else {
                $code = 403;
                $message = "Something went wrong";
            }
        } else {
            $code = 403;
            $error[] = "Token not found";
            $message = "Token not found";
        }

        print_r ($this->response->json($res, $code, $error, $message));
    }

    // Transfer History
    public function historyTransfer() {

        // Initial Variable
        $res = array();
        $code = 200;
        $error = array();
        $message = "";

        // Get Token
        $token = $this->input->get_request_header('Authorization', TRUE) ?: "";
        $page = $this->input->post_get("page", TRUE) ?: 1;

        // Check Token Is Not Null
        if (isset($token) || !empty($token)) {
            // Get Utility User
            $u_user = $this->UUser->getWhere(array(
                "token" => $token
            ));

            // Check User Is Exists
            if (count($u_user) > 0) {
                $u_user = $u_user[0];
                // Get User
                $m_users = $this->MUsers->getWhere(array(
                    "id" => $u_user["id_m_users"]
                ));
                
                // Check Is Exists
                if (count($m_users) > 0) {
                    $m_users = $m_users[0];

                    // Get Wallet
                    $walletToken = $this->getWalletToken();

                    $res = $this->curlWalletTron(array(
                        "phone" => $m_users["phone_number"],
                        "page" => $page
                    ), $walletToken, "transfer/history");
                    $message = "Success to fetch history transfer";
                } else {
                    $code = 500;
                    $error[] = "Failed to fetch user, cause token not valid";
                    $message = "Failed to fetch user, cause token not valid";
                }
            } else {
                $code = 500;
                $error[] = "Failed to fetch utility user, cause token not valid";
                $message = "Failed to fetch utility user, cause token not valid";
            }
        } else {
            $code = 403;
            $error[] = "Token not found";
            $message = "Token not found";
        }

        print_r ($this->response->json($res, $code, $error, $message));
    }

    // Withdraw
    public function withdraw() {
        // Initial Variable
        $res = null;
        $code = 200;
        $error = array();
        $message = "";

        // Get Token
        $token = $this->input->get_request_header('Authorization', TRUE) ?: "";

        // Check Token Is Not Null
        if (isset($token) || !empty($token)) {

            // Ger Request
            $req = json_decode($this->input->raw_input_stream, TRUE);
    
            // Check Is Not Null
            $checkIsValid = $this->response->checkIsNotNull(
                array(
                    "amount"
                ),
                $req
            );

            // Setup Error If Have
            $error = $checkIsValid;

            if (count($error) == 0) {
                // Get Utility User
                $u_user = $this->UUser->getWhere(array(
                    "token" => $token
                ));

                // Check User Is Exists
                if (count($u_user) > 0) {
                    $u_user = $u_user[0];
                    // Get User
                    $m_users = $this->MUsers->getWhere(array(
                        "id" => $u_user["id_m_users"]
                    ));
                    
                    // Check Is Exists
                    if (count($m_users) > 0) {
                        $m_users = $m_users[0];

                        // Get Wallet
                        $walletToken = $this->getWalletToken();
                        $res = $this->curlWalletTron(array(
                            "phone" => $m_users["phone_number"],
                            "amount" => $req["amount"],
                            "user_type" => "personal"
                        ), $walletToken, "cashout");
                        $message = "Success to Withdraw";

                    } else {
                        $code = 500;
                        $error[] = "Failed to fetch user, cause token not valid";
                        $message = "Failed to fetch user, cause token not valid";
                    }
                } else {
                    $code = 500;
                    $error[] = "Failed to fetch utility user, cause token not valid";
                    $message = "Failed to fetch utility user, cause token not valid";
                }
            } else {
                $code = 403;
                $message = "Something went wrong";
            }
        } else {
            $code = 403;
            $error[] = "Token not found";
            $message = "Token not found";
        }

        print_r ($this->response->json($res, $code, $error, $message));
    }

    // Transfer History
    public function historyWithdraw() {

        // Initial Variable
        $res = array();
        $code = 200;
        $error = array();
        $message = "";

        // Get Token
        $token = $this->input->get_request_header('Authorization', TRUE) ?: "";
        $page = $this->input->post_get("page", TRUE) ?: 1;

        // Check Token Is Not Null
        if (isset($token) || !empty($token)) {
            // Get Utility User
            $u_user = $this->UUser->getWhere(array(
                "token" => $token
            ));

            // Check User Is Exists
            if (count($u_user) > 0) {
                $u_user = $u_user[0];
                // Get User
                $m_users = $this->MUsers->getWhere(array(
                    "id" => $u_user["id_m_users"]
                ));
                
                // Check Is Exists
                if (count($m_users) > 0) {
                    $m_users = $m_users[0];

                    // Get Wallet
                    $walletToken = $this->getWalletToken();

                    $res = $this->curlWalletTron(array(
                        "phone" => $m_users["phone_number"],
                        "page" => $page,
                        "user_type" => "personal"
                    ), $walletToken, "cashout/history");
                    $message = "Success to fetch history withdraw";
                } else {
                    $code = 500;
                    $error[] = "Failed to fetch user, cause token not valid";
                    $message = "Failed to fetch user, cause token not valid";
                }
            } else {
                $code = 500;
                $error[] = "Failed to fetch utility user, cause token not valid";
                $message = "Failed to fetch utility user, cause token not valid";
            }
        } else {
            $code = 403;
            $error[] = "Token not found";
            $message = "Token not found";
        }

        print_r ($this->response->json($res, $code, $error, $message));
    }

    // Debit
    public function debit() {
        // Initial Variable
        $res = null;
        $code = 200;
        $error = array();
        $message = "";

        // Get Token
        $token = $this->input->get_request_header('Authorization', TRUE) ?: "";

        // Check Token Is Not Null
        if (isset($token) || !empty($token)) {

            // Ger Request
            $req = json_decode($this->input->raw_input_stream, TRUE);
    
            // Check Is Not Null
            $checkIsValid = $this->response->checkIsNotNull(
                array(
                    "amount",
                    "note"
                ),
                $req
            );

            // Setup Error If Have
            $error = $checkIsValid;

            if (count($error) == 0) {
                // Get Utility User
                $u_user = $this->UUser->getWhere(array(
                    "token" => $token
                ));

                // Check User Is Exists
                if (count($u_user) > 0) {
                    $u_user = $u_user[0];
                    // Get User
                    $m_users = $this->MUsers->getWhere(array(
                        "id" => $u_user["id_m_users"]
                    ));
                    
                    // Check Is Exists
                    if (count($m_users) > 0) {
                        $m_users = $m_users[0];

                        // Get Wallet
                        $walletToken = $this->getWalletToken();
                        $res = $this->curlWalletTron(array(
                            "phone" => $m_users["phone_number"],
                            "amount" => $req["amount"],
                            "user_type" => "personal",
                            "trx_type" => "D",
                            "note" => $req["note"]
                        ), $walletToken, "transaction");
                        $message = "Success to Withdraw";

                    } else {
                        $code = 500;
                        $error[] = "Failed to fetch user, cause token not valid";
                        $message = "Failed to fetch user, cause token not valid";
                    }
                } else {
                    $code = 500;
                    $error[] = "Failed to fetch utility user, cause token not valid";
                    $message = "Failed to fetch utility user, cause token not valid";
                }
            } else {
                $code = 403;
                $message = "Something went wrong";
            }
        } else {
            $code = 403;
            $error[] = "Token not found";
            $message = "Token not found";
        }

        print_r ($this->response->json($res, $code, $error, $message));
    }

    // Credit
    public function credit() {
        // Initial Variable
        $res = null;
        $code = 200;
        $error = array();
        $message = "";

        // Get Token
        $token = $this->input->get_request_header('Authorization', TRUE) ?: "";

        // Check Token Is Not Null
        if (isset($token) || !empty($token)) {

            // Ger Request
            $req = json_decode($this->input->raw_input_stream, TRUE);
    
            // Check Is Not Null
            $checkIsValid = $this->response->checkIsNotNull(
                array(
                    "amount",
                    "note"
                ),
                $req
            );

            // Setup Error If Have
            $error = $checkIsValid;

            if (count($error) == 0) {
                // Get Utility User
                $u_user = $this->UUser->getWhere(array(
                    "token" => $token
                ));

                // Check User Is Exists
                if (count($u_user) > 0) {
                    $u_user = $u_user[0];
                    // Get User
                    $m_users = $this->MUsers->getWhere(array(
                        "id" => $u_user["id_m_users"]
                    ));
                    
                    // Check Is Exists
                    if (count($m_users) > 0) {
                        $m_users = $m_users[0];

                        // Get Wallet
                        $walletToken = $this->getWalletToken();
                        $res = $this->curlWalletTron(array(
                            "phone" => $m_users["phone_number"],
                            "amount" => $req["amount"],
                            "user_type" => "personal",
                            "trx_type" => "C",
                            "note" => $req["note"]
                        ), $walletToken, "transaction");
                        $message = "Success to Withdraw";

                    } else {
                        $code = 500;
                        $error[] = "Failed to fetch user, cause token not valid";
                        $message = "Failed to fetch user, cause token not valid";
                    }
                } else {
                    $code = 500;
                    $error[] = "Failed to fetch utility user, cause token not valid";
                    $message = "Failed to fetch utility user, cause token not valid";
                }
            } else {
                $code = 403;
                $message = "Something went wrong";
            }
        } else {
            $code = 403;
            $error[] = "Token not found";
            $message = "Token not found";
        }

        print_r ($this->response->json($res, $code, $error, $message));
    }

    // Transaction History
    public function historyTransaction() {

        // Initial Variable
        $res = array();
        $code = 200;
        $error = array();
        $message = "";

        // Get Token
        $token = $this->input->get_request_header('Authorization', TRUE) ?: "";
        $page = $this->input->post_get("page", TRUE) ?: 1;

        // Check Token Is Not Null
        if (isset($token) || !empty($token)) {
            // Get Utility User
            $u_user = $this->UUser->getWhere(array(
                "token" => $token
            ));

            // Check User Is Exists
            if (count($u_user) > 0) {
                $u_user = $u_user[0];
                // Get User
                $m_users = $this->MUsers->getWhere(array(
                    "id" => $u_user["id_m_users"]
                ));
                
                // Check Is Exists
                if (count($m_users) > 0) {
                    $m_users = $m_users[0];

                    // Get Wallet
                    $walletToken = $this->getWalletToken();

                    $res = $this->curlWalletTron(array(
                        "phone" => $m_users["phone_number"],
                        "page" => $page,
                        "user_type" => "personal"
                    ), $walletToken, "transaction/history");
                    $message = "Success to fetch history transaction";
                } else {
                    $code = 500;
                    $error[] = "Failed to fetch user, cause token not valid";
                    $message = "Failed to fetch user, cause token not valid";
                }
            } else {
                $code = 500;
                $error[] = "Failed to fetch utility user, cause token not valid";
                $message = "Failed to fetch utility user, cause token not valid";
            }
        } else {
            $code = 403;
            $error[] = "Token not found";
            $message = "Token not found";
        }

        print_r ($this->response->json($res, $code, $error, $message));
    }
}