<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Experiences extends CI_Controller {

    // Public Variable
    public $custom_log, $session, $response, $fileUpload, $userType;

    public function __construct() {
        parent::__construct();

        // Load Model
        $this->load->model("MMedias");
        $this->load->model("MUsers");
        $this->load->model("MExperiences");
        $this->load->model("UUser");

        // Load Helper
        $this->session = new Session_helper();
        $this->response = new Response_helper();
        $this->fileUpload = new Upload_file_helper(
            array(
                "file_type" => array(
                    "png",
                    "jpg",
                    "jpeg"
                ),
                "max_size"  => 200000000
            )
        );
    }

    // Create Experience
    public function createExperience() {

        // Initial Variable
        $res = null;
        $code = 200;
        $error = array();
        $message = "";

        // Get Token
        $token = $this->input->get_request_header('Authorization', TRUE) ?: "";

        // Check Token Is Not Null
        if (isset($token) || !empty($token)) {

            $title = $this->input->post("title", TRUE) ?: "";
            $content = $this->input->post("content", TRUE) ?: "";
            $rating = $this->input->post("rating", TRUE) ?: 1;
            if (isset($_FILES["cover"]["name"]) &&
                !empty($title) && !empty($content) && !empty($rating)) {
                // Get Utility User
                $u_user = $this->UUser->getWhere(array(
                    "token" => $token
                ));

                // Check User Is Exists
                if (count($u_user) > 0) {
                    $u_user = $u_user[0];
                    // Get User
                    $m_users = $this->MUsers->getWhere(array(
                        "id" => $u_user["id_m_users"]
                    ));
                    
                    // Check Is Exists
                    if (count($m_users) > 0) {
                        $m_users = $m_users[0];
                        // Insert Media
                        $face = $this->fileUpload->do_upload("cover");
                        if ($face["status"]) {
                            // Upload File
                            $id_face = $this->MMedias->create(array(
                                "uri" => base_url("assets/dist/img/") . $face["file_name"],
                                "label" => $face["file_name"],
                                "created_at" => date("Y-m-d H:i:s"),
                                "updated_at" => date("Y-m-d H:i:s")
                            ));

                            // Check If Upload Success
                            if ($id_face != -1) {

                                $id_Experiences = $this->MExperiences->create(array(
                                    "title" => $title,
                                    "content" => $content,
                                    "rating" => $rating,
                                    "id_m_users" => $m_users["id"],
                                    "id_m_medias" => $id_face,
                                    "created_at" => date("Y-m-d H:i:s"),
                                    "updated_at" => date("Y-m-d H:i:s")
                                ));

                                if ($id_Experiences != -1) {
                                    $res = $face;
                                    $message = "Success to create Experiences";
                                } else {
                                    $code = 500;
                                    $error[] = "Failed to create Experiences";
                                    $message = "Failed to create Experiences";
                                }

                            } else {
                                $code = 500;
                                $error[] = "Failed to upload cover";
                                $message = "Failed to upload cover";
                            }
                        } else {
                            $code = 500;
                            $error[] = "Failed to upload cover";
                            $message = "Failed to upload cover";
                        }
                    } else {
                        $code = 500;
                        $error[] = "Failed to fetch user";
                        $message = "Failed to fetch user";
                    }
                } else {
                    $code = 500;
                    $error[] = "Failed to fetch utility user, cause token not valid";
                    $message = "Failed to fetch utility user, cause token not valid";
                }
            } else {
                $code = 403;
                $error[] = "Please fill title, content, and cover";
                $message = "Please fill title, content, and cover";
            }
        } else {
            $code = 403;
            $error[] = "Token not found";
            $message = "Token not found";
        }

        print_r ($this->response->json($res, $code, $error, $message));

    }

    // Get Experiences
    public function getExperiences() {
        // Initial Variable
        $res = array();
        $code = 200;
        $error = array();
        $message = "";

        // Get Token
        $token = $this->input->get_request_header('Authorization', TRUE) ?: "";
        $page = $this->input->post_get("page") ?: 0;

        // Check Token Is Not Null
        if (isset($token) || !empty($token)) {
            // Get Utility User
            $u_user = $this->UUser->getWhere(array(
                "token" => $token
            ));

            // Check User Is Exists
            if (count($u_user) > 0) {
                $u_user = $u_user[0];
                // Get User
                $m_users = $this->MUsers->getWhere(array(
                    "id" => $u_user["id_m_users"]
                ));
                
                // Check Is Exists
                if (count($m_users) > 0) {
                    $m_users = $m_users[0];
                    $data = array();
                    $Experiences = $this->MExperiences->getPagination($page, "id", "DESC");
                    foreach ($Experiences as $item) {
                        $media = $this->MMedias->getWhere(array(
                            "id" => $item["id_m_medias"]
                        ));
                        if (count($media) > 0) {
                            $media = $media[0];
                            $item["cover"] = $media;
                            $data[] = $item;
                        }
                    }

                    $res = $data;
                    $message = "Success to fetch Experiences";
                } else {
                    $code = 500;
                    $error[] = "Failed to fetch user";
                    $message = "Failed to fetch user";
                }
            } else {
                $code = 500;
                $error[] = "Failed to fetch utility user, cause token not valid";
                $message = "Failed to fetch utility user, cause token not valid";
            }
        } else {
            $code = 403;
            $error[] = "Token not found";
            $message = "Token not found";
        }

        print_r ($this->response->json($res, $code, $error, $message));
    }

    // Get Detail Experience
    public function getDetailExperience() {
        // Initial Variable
        $res = null;
        $code = 200;
        $error = array();
        $message = "";

        // Get Token
        $token = $this->input->get_request_header('Authorization', TRUE) ?: "";
        $id_Experience = $this->input->post_get("id_experience") ?: 0;

        // Check Token Is Not Null
        if (isset($token) || !empty($token)) {
            if (!isset($id_Experience) && $id_Experience == 0) {
                $code = 403;
                $error[] = "ID Experience is required";
                $message = "Failed, ID Experience is required";
            } else {
                // Get Utility User
                $u_user = $this->UUser->getWhere(array(
                    "token" => $token
                ));

                // Check User Is Exists
                if (count($u_user) > 0) {
                    $u_user = $u_user[0];
                    // Get User
                    $m_users = $this->MUsers->getWhere(array(
                        "id" => $u_user["id_m_users"]
                    ));
                    
                    // Check Is Exists
                    if (count($m_users) > 0) {
                        $m_users = $m_users[0];
                        $Experiences = $this->MExperiences->getWhere(array(
                            "id" => $id_Experience
                        ));
                        if (count($Experiences) > 0) {
                            $Experiences = $Experiences[0];
                            $media = $this->MMedias->getWhere(array(
                                "id" => $Experiences["id_m_medias"]
                            ));
                            if (count($media) > 0) {
                                $media = $media[0];
                                $Experiences["cover"] = $media;
                                $res = $Experiences;
                            }
                        }
                        $message = "Success to fetch Experiences";
                    } else {
                        $code = 500;
                        $error[] = "Failed to fetch user";
                        $message = "Failed to fetch user";
                    }
                } else {
                    $code = 500;
                    $error[] = "Failed to fetch utility user, cause token not valid";
                    $message = "Failed to fetch utility user, cause token not valid";
                }
            }
        } else {
            $code = 403;
            $error[] = "Token not found";
            $message = "Token not found";
        }

        print_r ($this->response->json($res, $code, $error, $message));
    }

    // Update Experience
    public function updateExperience() {

        // Initial Variable
        $res = null;
        $code = 200;
        $error = array();
        $message = "";

        // Get Token
        $token = $this->input->get_request_header('Authorization', TRUE) ?: "";

        // Check Token Is Not Null
        if (isset($token) || !empty($token)) {

            $title = $this->input->post("title", TRUE) ?: "";
            $content = $this->input->post("content", TRUE) ?: "";
            $rating = $this->input->post("rating", TRUE) ?: 1;
            $id_Experience = $this->input->post("id_experience", TRUE) ?: "";
            if (!empty($title) && !empty($content) && !empty($rating) && !empty($id_Experience)) {
                $current_Experience = $this->MExperiences->getWhere(array(
                    "id" => $id_Experience
                ));
                if (count($current_Experience) > 0) {
                    $current_Experience = $current_Experience[0];
                    $status_update = $this->MExperiences->update(array(
                        "id" => $id_Experience
                    ), array(
                        "title" => $title,
                        "content" => $content,
                        "rating" => $rating
                    ));

                    if ($status_update != -1) {
                        $res = $current_Experience;
                        $res["title"] = $title;
                        $res["content"] = $content;
                        $res["rating"] = $rating;
                        $message = "Success, to update Experience";
                    } else {
                        $code = 500;
                        $error[] = "Failed, to update Experience";
                        $message = "Failed, to update Experience";
                    }
                } else {
                    $code = 500;
                    $error[] = "Failed, Experience tidak ditemukan";
                    $message = "Failed, Experience tidak ditemukan";
                }
            } else {
                $code = 403;
                $error[] = "Please fill title, content, and cover";
                $message = "Please fill title, content, and cover";
            }
        } else {
            $code = 403;
            $error[] = "Token not found";
            $message = "Token not found";
        }

        print_r ($this->response->json($res, $code, $error, $message));

    }

    // Update Cover Experience
    public function updateCoverExperience() {

        // Initial Variable
        $res = null;
        $code = 200;
        $error = array();
        $message = "";

        // Get Token
        $token = $this->input->get_request_header('Authorization', TRUE) ?: "";

        // Check Token Is Not Null
        if (isset($token) || !empty($token)) {

            $id_Experience = $this->input->post("id_experience", TRUE) ?: "";
            if (isset($_FILES["cover"]["name"]) && !empty($id_Experience)) {
                $current_Experience = $this->MExperiences->getWhere(array(
                    "id" => $id_Experience
                ));
                if (count($current_Experience) > 0) {
                    $current_Experience = $current_Experience[0];

                    $face = $this->fileUpload->do_upload("cover");
                    if ($face["status"]) {
                        // Upload File
                        $id_face = $this->MMedias->create(array(
                            "uri" => base_url("assets/dist/img/") . $face["file_name"],
                            "label" => $face["file_name"],
                            "created_at" => date("Y-m-d H:i:s"),
                            "updated_at" => date("Y-m-d H:i:s")
                        ));

                        // Check If Upload Success
                        if ($id_face != -1) {
                            $status_update = $this->MExperiences->update(array(
                                "id" => $id_Experience
                            ), array(
                                "id_m_medias" => $id_face
                            ));
        
                            if ($status_update != -1) {
                                $res = $current_Experience;
                                $message = "Success, to update Experience";
                            } else {
                                $code = 500;
                                $error[] = "Failed, to update Experience";
                                $message = "Failed, to update Experience";
                            }
                        } else {
                            $code = 500;
                            $error[] = "Failed, to update cover";
                            $message = "Failed, to update cover";
                        }
                    } else {
                        $code = 500;
                        $error[] = "Failed, to update cover";
                        $message = "Failed, to update cover";
                    }
                } else {
                    $code = 500;
                    $error[] = "Failed, Experience tidak ditemukan";
                    $message = "Failed, Experience tidak ditemukan";
                }
            } else {
                $code = 403;
                $error[] = "Please fill title, content, and cover";
                $message = "Please fill title, content, and cover";
            }
        } else {
            $code = 403;
            $error[] = "Token not found";
            $message = "Token not found";
        }

        print_r ($this->response->json($res, $code, $error, $message));

    }

    // Delete Experience
    public function deleteExperience() {

        // Initial Variable
        $res = null;
        $code = 200;
        $error = array();
        $message = "";

        // Get Token
        $token = $this->input->get_request_header('Authorization', TRUE) ?: "";

        // Check Token Is Not Null
        if (isset($token) || !empty($token)) {
            $id_Experience = $this->input->post("id_experience", TRUE) ?: "";
            if (!empty($id_Experience)) {
                $current_Experience = $this->MExperiences->getWhere(array(
                    "id" => $id_Experience
                ));
                if (count($current_Experience) > 0) {
                    $current_Experience = $current_Experience[0];
                    $status_update = $this->MExperiences->delete(array(
                        "id" => $id_Experience
                    ));

                    if ($status_update != -1) {
                        $res = $current_Experience;
                        $message = "Success, to delete Experience";
                    } else {
                        $code = 500;
                        $error[] = "Failed, to delete Experience";
                        $message = "Failed, to delete Experience";
                    }
                } else {
                    $code = 500;
                    $error[] = "Failed, Experience tidak ditemukan";
                    $message = "Failed, Experience tidak ditemukan";
                }
            } else {
                $code = 403;
                $error[] = "Please fill title, content, and cover";
                $message = "Please fill title, content, and cover";
            }
        } else {
            $code = 403;
            $error[] = "Token not found";
            $message = "Token not found";
        }

        print_r ($this->response->json($res, $code, $error, $message));

    }

}