<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class PuloGebang extends CI_Controller {

    // Public Variable
    public $custom_curl, $session, $response, $userType;

    public function __construct() {
        parent::__construct();

        // Load Model
        $this->load->model("MUsers");
        $this->load->model("UUser");
        $this->load->model("MPuloGebang");
        $this->load->model("MTronWallet");
        $this->load->model("UUserOrderTicket");
        $this->load->model("UUserOrderTicketF");

        // Load Helper
        $this->session = new Session_helper();
        $this->response = new Response_helper();
        $this->custom_curl = new Mycurl_helper("");
    }

    // Syncronize
    public function syncronize() {

        // Initial Variable
        $res = null;
        $code = 200;
        $error = array();
        $message = "";

        // Ger Request
        $req = json_decode($this->input->raw_input_stream, TRUE);

        // Check Is Not Null
        $checkIsValid = $this->response->checkIsNotNull(
            array(
                "username",
                "password",
            ),
            $req
        );

        // Setup Error If Have
        $error = $checkIsValid;

        if (count($error) == 0) {
            // Todo Authentication
            $this->custom_curl->setHeader(array(
                "Authorization:Basic " . base64_encode(
                    $req["username"].":".$req["password"]
                )
            ));
            $this->custom_curl->createCurl(API_URI_PULO_GEBANG . "authentication");

            $data = json_decode($this->custom_curl->__tostring());
            try {
                $data = $data->data;
                $this->MPuloGebang->create(array(
                    "username" => $req["username"],
                    "password" => $req["password"],
                    "api_token" => $data->production_key,
                    // "api_token" => $data->development_key,
                    "created_at" => date("Y-m-d H:i:s"),
                    "updated_at" => date("Y-m-d H:i:s")
                ));
                $res = $data;
            } catch(Exception $e) {
                $res = $e->getMessage();
            }
        } else {
            $code = 402;
            $message = "Something went wrong on your form";
        }

        print_r ($this->response->json($res, $code, $error, $message));

    }

    // Get Last API TOKEN
    public function getAPIToken() {

        // Initial Variable
        $res = null;
        $code = 200;
        $error = array();
        $message = "";

        if (count($error) == 0) {
            // Todo Authentication
            $res = $this->MPuloGebang->query("
            SELECT `api_token` FROM `m_pulo_gebang` ORDER BY `id` DESC LIMIT 1");
            if (count($res) == 1) {
                $res = $res[0];
                $message = "Success to fetch data";
            } else {
                $code = 401;
                $message = "Failed, data not found";
            }
        } else {
            $code = 402;
            $message = "Something went wrong on your form";
        }

        print_r ($this->response->json($res, $code, $error, $message));

    }

    // Get Token
    private function getToken() {

        // Todo Authentication
        $res = $this->MPuloGebang->query("
        SELECT `api_token` FROM `m_pulo_gebang` ORDER BY `id` DESC LIMIT 1");
        if (count($res) == 1) {
            $res = $res[0];
            return $res["api_token"];
        } else {
            return "";
        }

    }

    // Get Route
    public function getRoute() {
        // Initial Variable
        $res = null;
        $code = 200;
        $error = array();
        $message = "";

        $token = $this->getToken();
        if(!empty($token)) {

            // Todo Authentication
            $this->custom_curl->setHeader(array(
                "X-TTPG-KEY: " . $token
            ));
            // $this->custom_curl->createCurl(API_URI_PULO_GEBANG . "tujuan/list");
            $this->custom_curl->createCurl(API_URI_PULO_GEBANG . "tujuan/tersedia");

            $data = json_decode($this->custom_curl->__tostring());
            try {
                $res = $data;
            } catch(Exception $e) {
                $res = $e->getMessage();
            }

        } else {
            $code = 402;
            $error[] = "Token not found";
            $message = "Token not found, please sync on server first";
        }

        print_r ($this->response->json($res, $code, $error, $message));
    }

    // Get Schedule
    public function getSchedule() {
        // Initial Variable
        $res = null;
        $code = 200;
        $error = array();
        $message = "";

        $token = $this->getToken();
        if(!empty($token)) {

            // Ger Request
            $req = json_decode($this->input->raw_input_stream, TRUE);

            // Check Is Not Null
            $checkIsValid = $this->response->checkIsNotNull(
                array(
                    "destionation_id",
                    "id_of_origin",
                    "date_of_departure"
                ),
                $req
            );

            // Setup Error If Have
            $error = $checkIsValid;

            if (count($error) == 0) {

                // Todo Authentication
                $this->custom_curl->setHeader(array(
                    "X-TTPG-KEY: " . $token
                ));
                $this->custom_curl->createCurl(API_URI_PULO_GEBANG . "ticket?destionation_id=".$req['destionation_id']."&id_of_origin=".$req['id_of_origin']."&date_of_departure=".$req['date_of_departure']);

                $data = json_decode($this->custom_curl->__tostring());
                try {
                    $res = $data;
                } catch(Exception $e) {
                    $res = $e->getMessage();
                }

            } else {
                $message = "Something, went wrong";
                $code = 500;
            }

        } else {
            $code = 402;
            $error[] = "Token not found";
            $message = "Token not found, please sync on server first";
        }

        print_r ($this->response->json($res, $code, $error, $message));
    }

    // Get Seat
    public function getSeat() {
        // Initial Variable
        $res = null;
        $code = 200;
        $error = array();
        $message = "";

        $token = $this->getToken();
        if(!empty($token)) {

            // Ger Request
            $req = json_decode($this->input->raw_input_stream, TRUE);

            // Check Is Not Null
            $checkIsValid = $this->response->checkIsNotNull(
                array(
                    "id_schedule"
                ),
                $req
            );

            // Setup Error If Have
            $error = $checkIsValid;

            if (count($error) == 0) {

                // Todo Authentication
                $this->custom_curl->setHeader(array(
                    "X-TTPG-KEY: " . $token
                ));
                $this->custom_curl->createCurl(API_URI_PULO_GEBANG . "ticket/seat?id_schedule=" . $req['id_schedule']);

                $data = json_decode($this->custom_curl->__tostring());
                try {
                    $res = $data;
                } catch(Exception $e) {
                    $res = $e->getMessage();
                }

            } else {
                $message = "Something, went wrong";
                $code = 500;
            }

        } else {
            $code = 402;
            $error[] = "Token not found";
            $message = "Token not found, please sync on server first";
        }

        print_r ($this->response->json($res, $code, $error, $message));
    }

    // Get Ticket
    public function getTicket() {
        // Initial Variable
        $res = null;
        $code = 200;
        $error = array();
        $message = "";

        $token = $this->getToken();
        if(!empty($token)) {

            // Ger Request
            $req = json_decode($this->input->raw_input_stream, TRUE);

            // Check Is Not Null
            $checkIsValid = $this->response->checkIsNotNull(
                array(
                    "page"
                ),
                $req
            );

            // Setup Error If Have
            $error = $checkIsValid;

            if (count($error) == 0) {

                // Todo Authentication
                $this->custom_curl->setHeader(array(
                    "X-TTPG-KEY: " . $token
                ));
                $this->custom_curl->createCurl(API_URI_PULO_GEBANG . "ticket/booking?page=" . $req['page']);

                $data = json_decode($this->custom_curl->__tostring());
                try {
                    $res = $data;
                } catch(Exception $e) {
                    $res = $e->getMessage();
                }

            } else {
                $message = "Something, went wrong";
                $code = 500;
            }

        } else {
            $code = 402;
            $error[] = "Token not found";
            $message = "Token not found, please sync on server first";
        }

        print_r ($this->response->json($res, $code, $error, $message));
    }

    // Get TOKEN
    private function getWalletToken() {

        // Initial Variable
        $res = array();
        $error = array();

        if (count($error) == 0) {
            // Todo Authentication
            $res = $this->MTronWallet->query("
            SELECT `api_token` FROM `m_tron_wallet` ORDER BY `id` DESC LIMIT 1");
            if (count($res) == 1) {
                $res = $res[0];
                return $res["api_token"];
            } else {
                return "";
            }
        } else {
            return "";
        }

    }

    // CURL Wallet
    private function curlWalletTron($req, $token, $path) {
        $this->custom_curl->setHeader(array(
            "Authorization:" . $token
        ));
        $this->custom_curl->setPost(http_build_query($req));
        $this->custom_curl->createCurl(API_URI_TRON_WALLET . $path);

        $kaspro = array();
        try {
            $kaspro = json_decode($this->custom_curl->__tostring());
        } catch(Exception $e) {
            $kaspro = $e->getMessage();
        }
        return $kaspro;
    }

    // Booking
    public function booking() {
        // Initial Variable
        $res = null;
        $code = 200;
        $error = array();
        $message = "";

        $token = $this->getToken();
        if(!empty($token)) {

            // Ger Request
            $req = json_decode($this->input->raw_input_stream, TRUE);

            // Get Token
            $api_token = $this->input->get_request_header('Authorization', TRUE) ?: "";

            // Check Is Not Null
            $checkIsValid = $this->response->checkIsNotNull(
                array(
                    "booking",
                    "price",
                    "route"
                ),
                $req
            );

            // Setup Error If Have
            $error = $checkIsValid;

            if (count($error) == 0) {

                // print_r(json_encode($req["booking"]));
                // die();

                // Get Utility User
                $u_user = $this->UUser->getWhere(array(
                    "token" => $api_token
                ));

                // Check User Is Exists
                if (count($u_user) > 0) {
                    $u_user = $u_user[0];
                    // Get User
                    $m_users = $this->MUsers->getWhere(array(
                        "id" => $u_user["id_m_users"]
                    ));
                    
                    // Check Is Exists
                    if (count($m_users) > 0) {
                        $m_users = $m_users[0];

                        $walletToken = $this->getWalletToken();
                        $balance = $this->curlWalletTron(array(
                            "user_type" => "personal",
                            "phone" => $m_users["phone_number"]
                        ), $walletToken, "profile");

                        if ($balance->success) {

                                // Todo Authentication
                                $this->custom_curl->setHeader(array(
                                    "Content-Type: application/json",
                                    "X-TTPG-KEY: " . $token
                                ));
                                $this->custom_curl->setPost(json_encode($req["booking"]));
                                $this->custom_curl->createCurl(API_URI_PULO_GEBANG . "ticket/booking");

                                try {
                                    $data = json_decode($this->custom_curl->__tostring());
                                    if ($data->status) {

                                        $balance = (int)$balance->result->balance;
                                        $price = ((int) $req["price"]) * count($req["booking"]);
                                        if ($balance >= $price) {
                                            // Do Cut Balance
                                            $this->curlWalletTron(array(
                                                "phone" => $m_users["phone_number"],
                                                "amount" => $price,
                                                "user_type" => "personal",
                                                "trx_type" => "D",
                                                "note" => "Pembelian tiket dengan rute: ".$req["route"]
                                            ), $walletToken, "transaction");
                                        } else {
                                            $code = 500;
                                            $error[] = "Failed, saldo tidak mencukupi";
                                            $message = "Failed, saldo tidak mencukupi";
                                            print_r ($this->response->json($res, $code, $error, $message));
                                            die();
                                        }

                                        foreach ($data->data as $item) {
                                            $id_u_user_order_ticket = $this->UUserOrderTicket->create(
                                                array(
                                                    "ticket_id" => $item->ticket_id,
                                                    "id_m_users" => $m_users["id"],
                                                    "passanger_name" => $item->passanger_name,
                                                    "passanger_gender" => $item->passanger_gender,
                                                    "passanger_telp" => $item->passanger_telp,
                                                    "pnr" => $item->pnr,
                                                    "seat_id" => $item->seat_id,
                                                    "is_boarding" => $item->is_boarding,
                                                    "date_of_boarding" => $item->date_of_boarding,
                                                    "date_of_departure" => $item->date_of_departure,
                                                    "time_of_departure" => $item->time_of_departure,
                                                    "price" => $item->price,
                                                    "id_schedule" => $item->id_schedule,
                                                    "id_of_origin" => $item->id_of_origin,
                                                    "destionation_id" => $item->destionation_id,
                                                    "category" => $item->category,
                                                    "po_name" => $item->po_name,
                                                    "id_po" => isset($item->id_po) ? $item->id_po : "",
                                                    "destionation" => $item->destionation,
                                                    "origin" => $item->origin,
                                                    "seat_number" => $item->seat_number,
                                                    "created_at" => date("Y-m-d H:i:s"),
                                                    "updated_at" => date("Y-m-d H:i:s")
                                                )
                                            );
                                            if ($id_u_user_order_ticket != -1) {
                                                foreach ($item->facility as $facility) {
                                                    $id_u_user_order_ticket_f = $this->UUserOrderTicketF->create(
                                                        array(
                                                            "id_u_user_order_ticket" => $id_u_user_order_ticket,
                                                            "facility_name" => $facility->facility_name,
                                                            "facility_icon" => $facility->facility_icon
                                                        )
                                                    );
                                                }
                                            }
                                        }
                                    }
                                    $res = $data;
                                } catch(Exception $e) {
                                    $res = $e->getMessage();
                                }
                        } else {
                            $code = 500;
                            $error[] = "Failed, saldo belum dibuat";
                            $message = "Failed, saldo belum dibuat";
                        }
                    } else {
                        $code = 500;
                        $error[] = "Failed to fetch order ticket";
                        $message = "Failed to fetch order ticket";
                    }
                } else {
                    $code = 500;
                    $error[] = "Failed to fetch utility user, cause token not valid";
                    $message = "Failed to fetch utility user, cause token not valid";
                }

            } else {
                $message = "Something, went wrong";
                $code = 500;
            }

        } else {
            $code = 402;
            $error[] = "Token not found";
            $message = "Token not found, please sync on server first";
        }

        print_r ($this->response->json($res, $code, $error, $message));
    }
}