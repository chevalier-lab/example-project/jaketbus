<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Sidebar extends CI_Model {
    public function getMenus() {
        return array(
            $this->getGeleral(),
            $this->getMasterData()
        );
    }

    // General
    private function getGeleral() {
        return array(
            "heading" => "General",
            "items" => array(
                array(
                    "is_active" => false,
                    "label" => "Dashboard",
                    "icon" => "icon ni ni-dashboard",
                    "link" => base_url("index.php/dashboard")
                ),
                array(
                    "is_active" => false,
                    "label" => "Transaction",
                    "icon" => "icon ni ni-tranx",
                    "link" => base_url("index.php/dashboard/transaction")
                ),
                array(
                    "is_active" => false,
                    "label" => "Pages",
                    "icon" => "icon ni ni-files",
                    "link" => base_url("index.php/dashboard/pages")
                )
            )
        );
    }

    // Master Data
    private function getMasterData() {
        return array(
            "heading" => "Master Data",
            "items" => array(
                array(
                    "is_active" => false,
                    "label" => "Experiences",
                    "icon" => "icon ni ni-star-round",
                    "link" => base_url("index.php/dashboard/experiences")
                ),
                array(
                    "is_active" => false,
                    "label" => "Features",
                    "icon" => "icon ni ni-list-ol",
                    "link" => base_url("index.php/dashboard/features")
                ),
                array(
                    "is_active" => false,
                    "label" => "Medias",
                    "icon" => "icon ni ni-img-fill",
                    "link" => base_url("index.php/dashboard/medias")
                ),
                array(
                    "is_active" => false,
                    "label" => "Notification",
                    "icon" => "icon ni ni-bell",
                    "link" => base_url("index.php/dashboard/notification")
                ),
                array(
                    "is_active" => false,
                    "label" => "Pulo Gebang",
                    "icon" => "icon ni ni-briefcase",
                    "link" => base_url("index.php/dashboard/puloGebang")
                ),
                array(
                    "is_active" => false,
                    "label" => "Wallet",
                    "icon" => "icon ni ni-wallet",
                    "link" => base_url("index.php/dashboard/wallets")
                ),
                array(
                    "is_active" => false,
                    "label" => "Users",
                    "icon" => "icon ni ni-users",
                    "link" => base_url("index.php/dashboard/users")
                )
            )
        );
    }
}