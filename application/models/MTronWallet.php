<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class MTronWallet extends CI_Model {
    public $table = "m_tron_wallet";

    public function create($data=array()) {
        $this->db->insert($this->table, $data);
        if ($this->db->affected_rows() == 1) return $this->db->insert_id();
        return -1;
    }

    public function delete($where=array()) {
        $this->db->where($where);
        $this->db->delete($this->table);
        return ($this->db->affected_rows() == 1) ? 1 : -1;
    }

    public function update($where=array(), $data=array()) {
        $this->db->where($where);
        $this->db->update($this->table, $data);
        return ($this->db->affected_rows() == 1) ? 1 : -1;
    }

    public function query($sql="") {
        if (!empty($sql)) {
            return $this->db->query($sql)->result_array();
        } else return $this->db->get($this->table)->result_array();
    }

    public function getWhere($where=array(), $order_by="id", $order_direction="ASC") {
        $this->db->where($where);
        $this->db->order_by($order_by, $order_direction);
        return $this->db->get($this->table)->result_array();
    }

    public function getLike($like=array(), $order_by="id", $order_direction="ASC") {
        $this->db->like($like);
        $this->db->order_by($order_by, $order_direction);
        return $this->db->get($this->table)->result_array();
    }

    public function getAll($order_by="id", $order_direction="ASC") {
        $this->db->order_by($order_by, $order_direction);
        return $this->db->get($this->table)->result_array();
    }

    public function getPagination($page=0, $order_by="id", $order_direction="ASC") {
        $this->db->order_by($order_by, $order_direction);
        $this->db->limit(10);
        $this->db->offset((10 * $page));
        return $this->db->get($this->table)->result_array();
    }

    public function getCount() {
        $this->db->from($this->table);
        return $this->db->count_all_results();
    }

    public function getCountWhere($where=array()) {
        $this->db->where($where);
        $this->db->from($this->table);
        return $this->db->count_all_results();
    }

    public function getCountLike($like=array()) {
        $this->db->like($like);
        $this->db->from($this->table);
        return $this->db->count_all_results();
    }
}