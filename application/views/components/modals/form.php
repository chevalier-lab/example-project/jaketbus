<!-- Modal Zoom -->
<div class="modal fade zoom" 
    tabindex="-1" 
    id="<?= isset($formID) ? $formID : 'modal-form'; ?>">
    <div class="modal-dialog <?= isset($formLarge) ? 'modal-lg' : ''; ?>" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">
                    <?php if (isset($formTitle)) echo $formTitle; ?>
                </h5>
                <a href="#" class="close" data-dismiss="modal" aria-label="Close">
                    <em class="icon ni ni-cross"></em>
                </a>
            </div>
            <div class="modal-body">
                <?php
                    if (isset($formContent)) echo $formContent;
                ?>
            </div>
            <?php if (isset($formFooter)) : ?>
                <div class="modal-footer bg-light">
                    <?php echo $formFooter; ?>
                </div>
            <?php endif; ?>
        </div>
    </div>
</div>