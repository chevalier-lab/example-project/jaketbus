<?php
/**
 * HOW TO USE CONTAINER
 * $this->load->view("components/container", array(
 *  "containerTitle" => "Example title",
 *  "containerTools" => '<a href="html/subscription/tickets.html" class="link">View All</a>'
 *  "containerContent" => '
 *  <div class="card-inner">
 *      <p>Hello World</p>
 *  </div>
 *  '
 * ));
 */
?>
<div class="card card-bordered card-full">
    <div class="card-inner-group">
        <div class="card-inner">
            <div class="card-title-group">
                <div class="card-title">
                    <h6 class="title">
                        <?= isset($containerTitle) ? $containerTitle : "Container Title"; ?>
                    </h6>
                </div>
                <div class="card-tools">
                    <?php 
                        if (isset($containerTools)) echo $containerTools;
                    ?>
                </div>
            </div>
        </div>
        <?php
            if (isset($containerContent)) echo $containerContent;
        ?>
    </div>
</div>