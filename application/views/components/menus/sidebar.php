<!-- sidebar @s -->
<div class="nk-sidebar nk-sidebar-fixed is-dark " data-content="sidebarMenu">
    <div class="nk-sidebar-element nk-sidebar-head">
        <div class="nk-sidebar-brand">
            <a href="html/general/index.html" class="logo-link nk-sidebar-logo">
                <img class="logo-light logo-img" src="<?= base_url('assets/template/images/origin_logo_white.png'); ?>" srcset="<?= base_url('assets/template/images/origin_logo_white.png'); ?>" alt="logo">
                <img class="logo-dark logo-img" src="<?= base_url('assets/template/images/origin_logo_white.png'); ?>" srcset="<?= base_url('assets/template/images/origin_logo_white.png'); ?>" alt="logo-dark">
            </a>
        </div>
        <div class="nk-menu-trigger mr-n2">
            <a href="#" class="nk-nav-toggle nk-quick-nav-icon d-xl-none" data-target="sidebarMenu"><em class="icon ni ni-arrow-left"></em></a>
        </div>
    </div><!-- .nk-sidebar-element -->
    <div class="nk-sidebar-element">
        <div class="nk-sidebar-content">
            <div class="nk-sidebar-menu" data-simplebar>
                <ul class="nk-menu">

                <?php
                    if (isset($sidebar)) {
                        foreach ($sidebar as $content) {
                            ?>
                            <li class="nk-menu-heading">
                                <h6 class="overline-title text-primary-alt">
                                    <?= $content['heading']; ?>
                                </h6>
                            </li><!-- .nk-menu-item -->
                            <?php
                            foreach($content['items'] as $item) {
                                ?>
                                <li class="nk-menu-item">
                                    <a href="<?= $item['link']; ?>" class="nk-menu-link">
                                        <span class="nk-menu-icon"><em class="<?= $item['icon']; ?>"></em></span>
                                        <span class="nk-menu-text"><?= $item['label']; ?></span>
                                    </a>
                                </li><!-- .nk-menu-item -->
                                <?php
                            }
                        }
                    }
                ?>

                </ul><!-- .nk-menu -->
            </div><!-- .nk-sidebar-menu -->
        </div><!-- .nk-sidebar-content -->
    </div><!-- .nk-sidebar-element -->
</div>
<!-- sidebar @e -->