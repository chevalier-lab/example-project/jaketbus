<div class="nk-block-head nk-block-head-sm">
    <div class="nk-block-between">
        <div class="nk-block-head-content">
            <h3 class="nk-block-title page-title"><?= $meta['title']; ?></h3>
            <div class="nk-block-des text-soft">
                <p>Welcome to DashLite Dashboard Template.</p>
            </div>
        </div><!-- .nk-block-head-content -->

        <div class="nk-block-head-content">
            <div class="toggle-wrap nk-block-tools-toggle">
                <a href="#" class="btn btn-icon btn-trigger toggle-expand mr-n1" data-target="pageMenu"><em class="icon ni ni-more-v"></em></a>
                <div class="toggle-expand-content" data-content="pageMenu">
                    <ul class="nk-block-tools g-3">
                        <?php
                            if (isset($headerTools)) {
                                echo $headerTools;
                            }
                        ?>
                    </ul>
                </div>
            </div>
        </div><!-- .nk-block-head-content -->

    </div><!-- .nk-block-between -->
</div><!-- .nk-block-head -->