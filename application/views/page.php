<!DOCTYPE html>
<html lang="zxx" class="js">

<head>
    <meta charset="utf-8">
    <meta name="author" content="JAKET BUS">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="<?= $meta["description"]; ?>">
    <!-- Fav Icon  -->
    <link rel="shortcut icon" href="<?= base_url("assets/template/images/favicon.png"); ?>">
    <!-- Page Title  -->
    <title><?= $meta["title"]; ?></title>
    <!-- StyleSheets  -->
    <link rel="stylesheet" href="<?= base_url("assets/template/assets/css/dashlite.css?ver=1.4.0"); ?>">
    <link id="skin-default" rel="stylesheet" href="<?= base_url("assets/template/assets/css/theme.css?ver=1.4.0"); ?>">
</head>

<body class="nk-body bg-lighter npc-general has-sidebar ">
    <div class="nk-app-root">
        <!-- main @s -->
        <div class="nk-main ">
            <!-- wrap @s -->
            <div class="nk-wrap ">
                <!-- content @s -->
                <div class="nk-content ">
                    <div class="container-fluid">
                        <div class="nk-content-inner">
                            <div class="nk-content-body">
                                <div class="content-page wide-md m-auto">
                                    <div class="nk-block-head nk-block-head-lg wide-sm mx-auto">
                                        <div class="nk-block-head-content text-center">
                                            <h2 class="nk-block-title fw-normal"><?= $data['title']; ?></h2>
                                            <div class="nk-block-des">
                                                <p class="text-soft ff-italic">Last Update: <?= $data['updated_at']; ?></p>
                                            </div>
                                        </div>
                                    </div><!-- .nk-block-head -->
                                    <div class="nk-block">
                                        <div class="card card-bordered">
                                            <div class="card-inner card-inner-xl">
                                                <div class="entry">
                                                    <?= $data["content"]; ?>
                                                </div>
                                            </div><!-- .card-inner -->
                                        </div><!-- .card -->
                                    </div><!-- .nk-block -->
                                </div><!-- .content-page -->
                            </div>
                        </div>
                    </div>
                </div>
                <!-- content @e -->
                <?php $this->load->view("components/footer"); ?>
            </div>
            <!-- wrap @e -->
        </div>
        <!-- main @e -->
    </div>
    <!-- app-root @e -->

    <!-- JavaScript -->
    <script src="<?= base_url('assets/template/assets/js/bundle.js?ver=1.4.0'); ?>"></script>
    <script src="<?= base_url('assets/template/assets/js/scripts.js?ver=1.4.0'); ?>"></script>
    <script src="<?= base_url('assets/template/assets/js/charts/gd-general.js?ver=1.4.0'); ?>"></script>

    <script>
        // Base URL
        var base_url = "<?= base_url('index.php'); ?>";
    </script>
</body>

</html>