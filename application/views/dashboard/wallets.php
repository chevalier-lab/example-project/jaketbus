<!DOCTYPE html>
<html lang="zxx" class="js">

<head>
    <meta charset="utf-8">
    <meta name="author" content="JAKET BUS">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="<?= $meta["description"]; ?>">
    <!-- Fav Icon  -->
    <link rel="shortcut icon" href="<?= base_url("assets/template/images/favicon.png"); ?>">
    <!-- Page Title  -->
    <title><?= $meta["title"]; ?></title>
    <!-- StyleSheets  -->
    <link rel="stylesheet" href="<?= base_url("assets/template/assets/css/dashlite.css?ver=1.4.0"); ?>">
    <link id="skin-default" rel="stylesheet" href="<?= base_url("assets/template/assets/css/theme.css?ver=1.4.0"); ?>">
</head>

<body class="nk-body bg-lighter npc-general has-sidebar ">
    <div class="nk-app-root">
        <!-- main @s -->
        <div class="nk-main ">
            <?php $this->load->view("components/menus/sidebar"); ?>
            <!-- wrap @s -->
            <div class="nk-wrap ">
                <?php $this->load->view("components/menus/topbar"); ?>
                <!-- content @s -->
                <div class="nk-content ">
                    <div class="container-fluid">
                        <div class="nk-content-inner">
                            <div class="nk-content-body">
                                <?php 
                                    // Header
                                    $this->load->view("components/header", array(
                                        "headerTools" => '
                                        <li class="nk-block-tools-opt">
                                            <a href="#" class="btn btn-primary" data-toggle="modal" data-target="#newSync">
                                                <em class="icon ni ni-reload"></em>
                                                <span>Syncronize</span>
                                            </a>
                                        </li>
                                        '
                                    )); 
                                    
                                    // Modal New Experience Form
                                    $this->load->view("components/modals/form", array(
                                        "formID" => 'newSync',
                                        "formTitle" => 'New Syncronize',
                                        "formLarge" => true,
                                        "formContent" => '
                                        <form action="'.base_url("index.php/dashboard/doSyncWallet").'" 
                                            method="POST" class="row gy-4">
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label class="form-label" for="username">Username</label>
                                                    <input type="text" class="form-control form-control-lg" id="username" name="username" placeholder="Enter username">
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label class="form-label" for="password">Password</label>
                                                    <input type="password" class="form-control form-control-lg" id="password" name="password" placeholder="Enter password">
                                                </div>
                                            </div>
                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <label class="form-label" for="api_token">API Token</label>
                                                    <input type="text" class="form-control form-control-lg" id="api_token" name="api_token" placeholder="Enter api token">
                                                </div>
                                            </div>
                                            <div class="col-12">
                                                <ul class="align-center flex-wrap flex-sm-nowrap gx-4 gy-2">
                                                    <li>
                                                        <button type="submit" class="btn btn-lg btn-primary">
                                                        <em class="icon ni ni-save mr-1"></em> Save
                                                        </button>
                                                    </li>
                                                    <li>
                                                        <a href="#" data-dismiss="modal" class="link link-light">Cancel</a>
                                                    </li>
                                                </ul>
                                            </div>
                                        </form>
                                        '
                                    ));
                                ?>
                                <div class="nk-block">
                                    <div class="row g-gs">
                                    
                                    <!-- CONTENT -->
                                    <div class="col-xxl-12">
                                    <?php

                                        // Render Rows Of Data
                                        $renderWalletData = "";
                                        $no = 1;
                                        foreach ($data["wallet"] as $item) {
                                            $renderWalletData .= '
                                            <div class="nk-tb-item">
                                                <div class="nk-tb-col">
                                                    <span class="tb-lead"><a href="#">'.$no.'</a></span>
                                                </div>
                                                <div class="nk-tb-col tb-col-md">
                                                    <span class="tb-sub">'.$item["username"].'</span>
                                                </div>
                                                <div class="nk-tb-col tb-col-md">
                                                    <span class="tb-sub">'.$item["api_token"].'</span>
                                                </div>
                                                <div class="nk-tb-col tb-col-md">
                                                    <span class="tb-sub">'.$item["created_at"].'</span>
                                                </div>
                                            </div>
                                            ';
                                            $no++;
                                        }

                                        // Users (Show 10)
                                        $page = $this->input->get("page") ?: 0;
                                        $maxPage = ceil((int)$data["count_wallet"] / 10);
                                        $options = "";
                                        for ($i = 0; $i < $maxPage; $i++) {
                                            if ($i == $page)
                                                $options .= '<option value="'.$i.'" selected>'.($i + 1).'</option>';
                                            else 
                                                $options .= '<option value="'.$i.'">'.($i + 1).'</option>';
                                        }

                                        // Load Component Table
                                        $this->load->view("components/container", array(
                                            "containerTitle" => "Wallet Tron Syncronize",
                                            "containerTools" => '
                                                <div class="pagination-goto d-flex justify-content-center justify-content-md-start gx-3">
                                                    <div>Page</div>
                                                    <div>
                                                        <select class="form-select form-select-sm" 
                                                            data-search="on" 
                                                            data-dropdown="xs center"
                                                            onchange="loadPagination(this.value)">
                                                            '.$options.'
                                                        </select>
                                                    </div>
                                                    <div>OF '.$maxPage.'</div>
                                                </div>
                                                ',
                                            "containerContent" => '
                                                <div class="card-inner p-0 border-top">
                                                    <div class="nk-tb-list nk-tb-orders">
                                                        <div class="nk-tb-item nk-tb-head">
                                                            <div class="nk-tb-col"><span>No</span></div>
                                                            <div class="nk-tb-col tb-col-sm"><span>Username</span></div>
                                                            <div class="nk-tb-col tb-col-sm"><span>API Key</span></div>
                                                            <div class="nk-tb-col tb-col-md"><span>Tgl Syncronize</span></div>
                                                        </div>
                                                        '.$renderWalletData.'
                                                    </div>
                                                </div>
                                                '
                                        ));
                                    ?>
                                    </div>
                                    <!-- CONTENT -->

                                    </div><!-- .row -->
                                </div><!-- .nk-block -->
                            </div>
                        </div>
                    </div>
                </div>
                <!-- content @e -->
                <?php $this->load->view("components/footer"); ?>
            </div>
            <!-- wrap @e -->
        </div>
        <!-- main @e -->
    </div>
    <!-- app-root @e -->

    <!-- JavaScript -->
    <script src="<?= base_url('assets/template/assets/js/bundle.js?ver=1.4.0'); ?>"></script>
    <script src="<?= base_url('assets/template/assets/js/scripts.js?ver=1.4.0'); ?>"></script>
    <script src="<?= base_url('assets/template/assets/js/charts/gd-general.js?ver=1.4.0'); ?>"></script>

    <script>
        // Base URL
        var base_url = "<?= base_url('index.php'); ?>";
        // Load Pagination
        function loadPagination(page) {
            location.assign(base_url + "/dashboard/wallets?page=" + page);
        }
    </script>
</body>

</html>