<!DOCTYPE html>
<html lang="zxx" class="js">

<head>
    <meta charset="utf-8">
    <meta name="author" content="JAKET BUS">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="<?= $meta["description"]; ?>">
    <!-- Fav Icon  -->
    <link rel="shortcut icon" href="<?= base_url("assets/template/images/favicon.png"); ?>">
    <!-- Page Title  -->
    <title><?= $meta["title"]; ?></title>
    <!-- StyleSheets  -->
    <link rel="stylesheet" href="<?= base_url("assets/template/assets/css/dashlite.css?ver=1.4.0"); ?>">
    <link id="skin-default" rel="stylesheet" href="<?= base_url("assets/template/assets/css/theme.css?ver=1.4.0"); ?>">
</head>

<body class="nk-body bg-lighter npc-general has-sidebar ">
    <div class="nk-app-root">
        <!-- main @s -->
        <div class="nk-main ">
            <?php $this->load->view("components/menus/sidebar"); ?>
            <!-- wrap @s -->
            <div class="nk-wrap ">
                <?php $this->load->view("components/menus/topbar"); ?>
                <!-- content @s -->
                <div class="nk-content ">
                    <div class="container-fluid">
                        <div class="nk-content-inner">
                            <div class="nk-content-body">
                                <?php $this->load->view("components/header"); ?>
                                <div class="nk-block">
                                    <div class="row g-gs">

                                    <div class="col-xxl-12">
                                        <div class="card card-bordered h-100">
                                            <div class="card-inner">
                                                <div class="card-title-group align-start pb-3 g-2">
                                                    <div class="card-title card-title-sm">
                                                        <h6 class="title">Total Transaksi</h6>
                                                        <p>Total transaksi JaketBus.</p>
                                                    </div>
                                                </div>
                                                <div class="analytic-au">
                                                    <div class="analytic-data-group analytic-ov-group g-3">
                                                        <?php
                                                            $counterTransaction = $data["counter"];
                                                        ?>
                                                        <div class="analytic-data analytic-ov-data">
                                                            <div class="title">Total Transaksi Paid</div>
                                                            <div class="amount"><?= $counterTransaction["total_active"]; ?></div>
                                                            <div class="change up"><em class="icon ni ni-arrow-long-up"></em>Transaksi</div>
                                                        </div>
                                                        <div class="analytic-data analytic-ov-data">
                                                            <div class="title">Total Transaksi Unpaid</div>
                                                            <div class="amount"><?= $counterTransaction["total_progress"]; ?></div>
                                                            <div class="change down"><em class="icon ni ni-arrow-long-down"></em>Transaksi</div>
                                                        </div>

                                                        <div class="analytic-data analytic-ov-data">
                                                            <div class="title">Total Transaksi Paid Hari Ini</div>
                                                            <div class="amount"><?= $counterTransaction["total_active_today"]; ?></div>
                                                            <div class="change up"><em class="icon ni ni-arrow-long-up"></em>Transaksi</div>
                                                        </div>
                                                        <div class="analytic-data analytic-ov-data">
                                                            <div class="title">Total Transaksi Unpaid Hari Ini</div>
                                                            <div class="amount"><?= $counterTransaction["total_progress_today"]; ?></div>
                                                            <div class="change down"><em class="icon ni ni-arrow-long-down"></em>Transaksi</div>
                                                        </div>
                                                    </div>
                                                </div>

                                                <hr>

                                                <div class="card-title-group align-start pb-3 g-2">
                                                    <div class="card-title card-title-sm">
                                                        <h6 class="title">Total Summary Transaksi</h6>
                                                        <p>Total summary transaksi JaketBus.</p>
                                                    </div>
                                                </div>
                                                <div class="analytic-au">
                                                    <div class="analytic-data-group analytic-ov-group g-3">
                                                        <?php
                                                            $counterTransaction = $data["counter"];
                                                        ?>
                                                        <div class="analytic-data analytic-ov-data">
                                                            <div class="title">Total Summary Transaksi Paid</div>
                                                            <div class="amount"><?= ($counterTransaction["total_active_sum"]->price != null) ? number_format($counterTransaction["total_active_sum"]->price,2,",",".") : number_format("0",2,",","."); ?></div>
                                                            <div class="change up"><em class="icon ni ni-arrow-long-up"></em>Transaksi</div>
                                                        </div>
                                                        <div class="analytic-data analytic-ov-data">
                                                            <div class="title">Total Summary Transaksi Unpaid</div>
                                                            <div class="amount"><?= ($counterTransaction["total_progress_sum"]->price != null) ? number_format($counterTransaction["total_progress_sum"]->price,2,",",".") : number_format("0",2,",","."); ?></div>
                                                            <div class="change down"><em class="icon ni ni-arrow-long-down"></em>Transaksi</div>
                                                        </div>

                                                        <div class="analytic-data analytic-ov-data">
                                                            <div class="title">Total Summary Transaksi Paid Hari Ini</div>
                                                            <div class="amount"><?= ($counterTransaction["total_active_today_sum"]->price != null) ? number_format($counterTransaction["total_active_today_sum"]->price,2,",",".") : number_format("0",2,",","."); ?></div>
                                                            <div class="change up"><em class="icon ni ni-arrow-long-up"></em>Transaksi</div>
                                                        </div>
                                                        <div class="analytic-data analytic-ov-data">
                                                            <div class="title">Total Summary Transaksi Unpaid Hari Ini</div>
                                                            <div class="amount"><?= ($counterTransaction["total_progress_today_sum"]->price != null) ? number_format($counterTransaction["total_progress_today_sum"]->price,2,",",".") : number_format("0",2,",","."); ?></div>
                                                            <div class="change down"><em class="icon ni ni-arrow-long-down"></em>Transaksi</div>
                                                        </div>
                                                    </div>
                                                </div>

                                                <hr>

                                                <div class="card-title-group align-start pb-3 g-2">
                                                    <div class="card-title card-title-sm">
                                                        <h6 class="title">Total Transaksi dengan Alfamart</h6>
                                                        <p>Total transaksi JaketBus dengan metode pembayaran Alfamart.</p>
                                                    </div>
                                                </div>
                                                <div class="analytic-au">
                                                    <div class="analytic-data-group analytic-ov-group g-3">
                                                        <?php
                                                            $counterTransaction = $data["counter"];
                                                        ?>
                                                        <div class="analytic-data analytic-ov-data">
                                                            <div class="title">Total Transaksi Alfamart Paid</div>
                                                            <div class="amount"><?= $counterTransaction["total_active_alfa"]; ?></div>
                                                            <div class="change up"><em class="icon ni ni-arrow-long-up"></em>Transaksi</div>
                                                        </div>
                                                        <div class="analytic-data analytic-ov-data">
                                                            <div class="title">Total Transaksi Alfamart Unpaid</div>
                                                            <div class="amount"><?= $counterTransaction["total_progress_alfa"]; ?></div>
                                                            <div class="change down"><em class="icon ni ni-arrow-long-down"></em>Transaksi</div>
                                                        </div>

                                                        <div class="analytic-data analytic-ov-data">
                                                            <div class="title">Total Transaksi Alfamart Paid Hari Ini</div>
                                                            <div class="amount"><?= $counterTransaction["total_active_today_alfa"]; ?></div>
                                                            <div class="change up"><em class="icon ni ni-arrow-long-up"></em>Transaksi</div>
                                                        </div>
                                                        <div class="analytic-data analytic-ov-data">
                                                            <div class="title">Total Transaksi Alfamart Unpaid Hari Ini</div>
                                                            <div class="amount"><?= $counterTransaction["total_progress_today_alfa"]; ?></div>
                                                            <div class="change down"><em class="icon ni ni-arrow-long-down"></em>Transaksi</div>
                                                        </div>
                                                    </div>
                                                </div>

                                                <hr>

                                                <div class="card-title-group align-start pb-3 g-2">
                                                    <div class="card-title card-title-sm">
                                                        <h6 class="title">Total Summary Transaksi dengan Alfamart</h6>
                                                        <p>Total summary transaksi JaketBus dengan metode pembayaran Alfamart.</p>
                                                    </div>
                                                </div>
                                                <div class="analytic-au">
                                                    <div class="analytic-data-group analytic-ov-group g-3">
                                                        <?php
                                                            $counterTransaction = $data["counter"];
                                                        ?>
                                                        <div class="analytic-data analytic-ov-data">
                                                            <div class="title">Total Summary Transaksi Alfamart Paid</div>
                                                            <div class="amount"><?= ($counterTransaction["total_active_sum_alfa"]->price != null) ? number_format($counterTransaction["total_active_sum_alfa"]->price,2,",",".") : number_format("0",2,",","."); ?></div>
                                                            <div class="change up"><em class="icon ni ni-arrow-long-up"></em>Transaksi</div>
                                                        </div>
                                                        <div class="analytic-data analytic-ov-data">
                                                            <div class="title">Total Summary Transaksi Alfamart Unpaid</div>
                                                            <div class="amount"><?= ($counterTransaction["total_progress_sum_alfa"]->price != null) ? number_format($counterTransaction["total_progress_sum_alfa"]->price,2,",",".") : number_format("0",2,",","."); ?></div>
                                                            <div class="change down"><em class="icon ni ni-arrow-long-down"></em>Transaksi</div>
                                                        </div>

                                                        <div class="analytic-data analytic-ov-data">
                                                            <div class="title">Total Summary Transaksi Alfamart Paid Hari Ini</div>
                                                            <div class="amount"><?= ($counterTransaction["total_active_today_sum_alfa"]->price != null) ? number_format($counterTransaction["total_active_today_sum_alfa"]->price,2,",",".") : number_format("0",2,",","."); ?></div>
                                                            <div class="change up"><em class="icon ni ni-arrow-long-up"></em>Transaksi</div>
                                                        </div>
                                                        <div class="analytic-data analytic-ov-data">
                                                            <div class="title">Total Summary Transaksi Alfamart Unpaid Hari Ini</div>
                                                            <div class="amount"><?= ($counterTransaction["total_progress_today_sum_alfa"]->price != null) ? number_format($counterTransaction["total_progress_today_sum_alfa"]->price,2,",",".") : number_format("0",2,",","."); ?></div>
                                                            <div class="change down"><em class="icon ni ni-arrow-long-down"></em>Transaksi</div>
                                                        </div>
                                                    </div>
                                                </div>

                                                <hr>

                                                <div class="card-title-group align-start pb-3 g-2">
                                                    <div class="card-title card-title-sm">
                                                        <h6 class="title">Total Transaksi dengan Indomaret</h6>
                                                        <p>Total transaksi JaketBus dengan metode pembayaran Indomaret.</p>
                                                    </div>
                                                </div>
                                                <div class="analytic-au">
                                                    <div class="analytic-data-group analytic-ov-group g-3">
                                                        <?php
                                                            $counterTransaction = $data["counter"];
                                                        ?>
                                                        <div class="analytic-data analytic-ov-data">
                                                            <div class="title">Total Transaksi Indomaret Paid</div>
                                                            <div class="amount"><?= $counterTransaction["total_active_indo"]; ?></div>
                                                            <div class="change up"><em class="icon ni ni-arrow-long-up"></em>Transaksi</div>
                                                        </div>
                                                        <div class="analytic-data analytic-ov-data">
                                                            <div class="title">Total Transaksi Indomaret Unpaid</div>
                                                            <div class="amount"><?= $counterTransaction["total_progress_indo"]; ?></div>
                                                            <div class="change down"><em class="icon ni ni-arrow-long-down"></em>Transaksi</div>
                                                        </div>

                                                        <div class="analytic-data analytic-ov-data">
                                                            <div class="title">Total Transaksi Indomaret Paid Hari Ini</div>
                                                            <div class="amount"><?= $counterTransaction["total_active_today_indo"]; ?></div>
                                                            <div class="change up"><em class="icon ni ni-arrow-long-up"></em>Transaksi</div>
                                                        </div>
                                                        <div class="analytic-data analytic-ov-data">
                                                            <div class="title">Total Transaksi Indomaret Unpaid Hari Ini</div>
                                                            <div class="amount"><?= $counterTransaction["total_progress_today_indo"]; ?></div>
                                                            <div class="change down"><em class="icon ni ni-arrow-long-down"></em>Transaksi</div>
                                                        </div>
                                                    </div>
                                                </div>

                                                <hr>

                                                <div class="card-title-group align-start pb-3 g-2">
                                                    <div class="card-title card-title-sm">
                                                        <h6 class="title">Total Summary Transaksi dengan Indomaret</h6>
                                                        <p>Total summary transaksi JaketBus dengan metode pembayaran Indomaret.</p>
                                                    </div>
                                                </div>
                                                <div class="analytic-au">
                                                    <div class="analytic-data-group analytic-ov-group g-3">
                                                        <?php
                                                            $counterTransaction = $data["counter"];
                                                        ?>
                                                        <div class="analytic-data analytic-ov-data">
                                                            <div class="title">Total Summary Transaksi Indomaret Paid</div>
                                                            <div class="amount"><?= ($counterTransaction["total_active_sum_indo"]->price != null) ? number_format($counterTransaction["total_active_sum_indo"]->price,2,",",".") : number_format("0",2,",","."); ?></div>
                                                            <div class="change up"><em class="icon ni ni-arrow-long-up"></em>Transaksi</div>
                                                        </div>
                                                        <div class="analytic-data analytic-ov-data">
                                                            <div class="title">Total Summary Transaksi Indomaret Unpaid</div>
                                                            <div class="amount"><?= ($counterTransaction["total_progress_sum_indo"]->price != null) ? number_format($counterTransaction["total_progress_sum_indo"]->price,2,",",".") : number_format("0",2,",","."); ?></div>
                                                            <div class="change down"><em class="icon ni ni-arrow-long-down"></em>Transaksi</div>
                                                        </div>

                                                        <div class="analytic-data analytic-ov-data">
                                                            <div class="title">Total Summary Transaksi Indomaret Paid Hari Ini</div>
                                                            <div class="amount"><?= ($counterTransaction["total_active_today_sum_indo"]->price != null) ? number_format($counterTransaction["total_active_today_sum_indo"]->price,2,",",".") : number_format("0",2,",","."); ?></div>
                                                            <div class="change up"><em class="icon ni ni-arrow-long-up"></em>Transaksi</div>
                                                        </div>
                                                        <div class="analytic-data analytic-ov-data">
                                                            <div class="title">Total Summary Transaksi Indomaret Unpaid Hari Ini</div>
                                                            <div class="amount"><?= ($counterTransaction["total_progress_today_sum_indo"]->price != null) ? number_format($counterTransaction["total_progress_today_sum_indo"]->price,2,",",".") : number_format("0",2,",","."); ?></div>
                                                            <div class="change down"><em class="icon ni ni-arrow-long-down"></em>Transaksi</div>
                                                        </div>
                                                    </div>
                                                </div>

                                            </div>
                                        </div><!-- .card -->
                                    </div>
                                    
                                    <!-- CONTENT -->
                                    <div class="col-xxl-12">
                                    <?php
                                        // Render Rows Of Data
                                        $renderData = "";
                                        $no = 1;
                                        foreach ($data["transaction"] as $item) {
                                            $renderData .= '
                                            <div class="nk-tb-item">
                                                <div class="nk-tb-col">
                                                    <span class="tb-lead"><a href="#">'.$no.'</a></span>
                                                </div>
                                                <div class="nk-tb-col tb-col-md">
                                                    <span class="tb-sub">'.$item["ticket_id"].'</span>
                                                </div>
                                                <div class="nk-tb-col tb-col-md">
                                                    <span class="tb-sub">'.$item["passanger_name"].'</span>
                                                </div>
                                                <div class="nk-tb-col tb-col-md">
                                                    <span class="tb-sub">'.$item["passanger_telp"].'</span>
                                                </div>
                                                <div class="nk-tb-col tb-col-md">
                                                    <span class="tb-sub">'.$item["date_of_departure"].'</span>
                                                </div>
                                                <div class="nk-tb-col tb-col-md">
                                                    <span class="tb-sub">'.$item["time_of_departure"].'</span>
                                                </div>
                                                <div class="nk-tb-col tb-col-md">
                                                    <span class="tb-sub">Rp '.$item["price"].'</span>
                                                </div>
                                                <div class="nk-tb-col tb-col-md">
                                                    <span class="tb-sub">'.$item["created_at"].'</span>
                                                </div>
                                            </div>
                                            ';
                                            $no++;
                                        }
                                        // Transaction (Show 5)
                                        $this->load->view("components/container", array(
                                         "containerTitle" => "Transaksi hari ini",
                                         "containerTools" => '<a href="'.base_url("index.php/dashboard/transaction").'" class="link">View All</a>',
                                         "containerContent" => '
                                            <div class="card-inner p-0 border-top">
                                                <div class="nk-tb-list nk-tb-orders">
                                                    <div class="nk-tb-item nk-tb-head">
                                                        <div class="nk-tb-col"><span>No</span></div>
                                                        <div class="nk-tb-col tb-col-sm"><span>ID Ticket</span></div>
                                                        <div class="nk-tb-col tb-col-sm"><span>Penumpang</span></div>
                                                        <div class="nk-tb-col tb-col-md"><span>No Telp</span></div>
                                                        <div class="nk-tb-col tb-col-md"><span>Tanggal Keberangkatan</span></div>
                                                        <div class="nk-tb-col tb-col-md"><span>Waktu Keberangkatan</span></div>
                                                        <div class="nk-tb-col tb-col-md"><span>Amount</span></div>
                                                        <div class="nk-tb-col tb-col-md"><span>Tanggal Pembuatan</span></div>
                                                    </div>
                                                    '.$renderData.'
                                                </div>
                                            </div>
                                         '
                                        ));
                                    ?>
                                    </div>
                                    <div class="col-md-6 col-xxl-4">
                                    <?php
                                        // Render Rows Of Data
                                        $renderData = "";
                                        $no = 1;
                                        foreach ($data["notification"] as $item) {
                                            $renderData .= '
                                            <li class="nk-activity-item">
                                                <div class="nk-activity-data">
                                                    <div class="label">'.$item["title"].'</div>
                                                    <div class="time">'.$item["content"].'</div>
                                                    <span class="time">'.$item["created_at"].'</span>
                                                </div>
                                            </li>
                                            ';
                                            $no++;
                                        }
                                        // Recent Activity
                                        $this->load->view("components/container", array(
                                         "containerTitle" => "Aktivitas terbaru",
                                         "containerTools" => '<a href="'.base_url("index.php/dashboard/notification").'" class="link">View All</a>',
                                         "containerContent" => '
                                            <ul class="nk-activity">
                                                '.$renderData.'
                                            </ul>
                                         '
                                        ));
                                    ?>  
                                    </div>
                                    <div class="col-md-6 col-xxl-4">
                                    <?php

                                        // Render Rows Of Data
                                        $renderUserData = "";
                                        $no = 1;
                                        foreach ($data["user"] as $item) {
                                            $renderUserData .= '
                                            <div class="user-card">
                                                <div class="user-info">
                                                    <span class="lead-text">'.$item["first_name"].' '.$item["last_name"].'</span>
                                                    <span class="sub-text">'.$item["phone_number"].' - '.$item["email"].'</span>
                                                    <span class="sub-text">'.$item["created_at"].'</span>
                                                </div>
                                            </div>
                                            <hr>
                                            ';
                                            $no++;
                                        }
                                        // Recent Activity
                                        $this->load->view("components/container", array(
                                         "containerTitle" => "Pengguna terbaru",
                                         "containerTools" => '<a href="'.base_url("index.php/dashboard/users").'" class="link">View All</a>',
                                         "containerContent" => '  
                                        <div class="card-inner card-inner-md">
                                            '.$renderUserData.'
                                        </div>
                                         '
                                        ));
                                    ?>  
                                    </div>
                                    <!-- CONTENT -->

                                    </div><!-- .row -->
                                </div><!-- .nk-block -->
                            </div>
                        </div>
                    </div>
                </div>
                <!-- content @e -->
                <?php $this->load->view("components/footer"); ?>
            </div>
            <!-- wrap @e -->
        </div>
        <!-- main @e -->
    </div>
    <!-- app-root @e -->

    <!-- JavaScript -->
    <script src="<?= base_url('assets/template/assets/js/bundle.js?ver=1.4.0'); ?>"></script>
    <script src="<?= base_url('assets/template/assets/js/scripts.js?ver=1.4.0'); ?>"></script>
    <script src="<?= base_url('assets/template/assets/js/charts/gd-general.js?ver=1.4.0'); ?>"></script>
    <script>
    console.log("<?= json_encode($data['counter']); ?>");
    </script>
</body>

</html>