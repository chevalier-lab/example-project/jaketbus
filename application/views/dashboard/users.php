<!DOCTYPE html>
<html lang="zxx" class="js">

<head>
    <meta charset="utf-8">
    <meta name="author" content="JAKET BUS">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="<?= $meta["description"]; ?>">
    <!-- Fav Icon  -->
    <link rel="shortcut icon" href="<?= base_url("assets/template/images/favicon.png"); ?>">
    <!-- Page Title  -->
    <title><?= $meta["title"]; ?></title>
    <!-- StyleSheets  -->
    <link rel="stylesheet" href="<?= base_url("assets/template/assets/css/dashlite.css?ver=1.4.0"); ?>">
    <link id="skin-default" rel="stylesheet" href="<?= base_url("assets/template/assets/css/theme.css?ver=1.4.0"); ?>">
</head>

<body class="nk-body bg-lighter npc-general has-sidebar ">
    <div class="nk-app-root">
        <!-- main @s -->
        <div class="nk-main ">
            <?php $this->load->view("components/menus/sidebar"); ?>
            <!-- wrap @s -->
            <div class="nk-wrap ">
                <?php $this->load->view("components/menus/topbar"); ?>
                <!-- content @s -->
                <div class="nk-content ">
                    <div class="container-fluid">
                        <div class="nk-content-inner">
                            <div class="nk-content-body">
                                <?php 
                                    // Header
                                    $this->load->view("components/header", array(
                                        "headerTools" => '
                                        <li class="nk-block-tools-opt">
                                            <a href="#" class="btn btn-primary" data-toggle="modal" data-target="#newUsers">
                                                <em class="icon ni ni-plus"></em>
                                                <span>New User</span>
                                            </a>
                                        </li>
                                        '
                                    )); 
                                    
                                    // Modal New Experience Form
                                    $this->load->view("components/modals/form", array(
                                        "formID" => 'newUsers',
                                        "formTitle" => 'New Users',
                                        "formLarge" => true,
                                        "formContent" => '
                                        <form action="'.base_url("index.php/dashboard/doCreateUsers").'" 
                                            method="POST" class="row gy-4">
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label class="form-label" for="first_name">First Name</label>
                                                    <input type="text" class="form-control form-control-lg" id="first_name" name="first_name" placeholder="Enter first name">
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label class="form-label" for="last_name">Last Name</label>
                                                    <input type="text" class="form-control form-control-lg" id="last_name" name="last_name" placeholder="Enter last name">
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label class="form-label" for="email">Email</label>
                                                    <input type="email" class="form-control form-control-lg" id="email" name="email" placeholder="Enter email">
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label class="form-label" for="password">Password</label>
                                                    <input type="password" class="form-control form-control-lg" id="password" name="password" placeholder="Enter password">
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label class="form-label" for="phone_number">Phone Number</label>
                                                    <input type="number" class="form-control form-control-lg" id="phone_number" name="phone_number" placeholder="Enter phone number 62xxx">
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label class="form-label">Level</label>
                                                    <div class="form-control-wrap">
                                                        <select class="form-select" id="level" name="level">
                                                            <option value="1">User</option>
                                                            <option value="2">Administrator</option>
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-12">
                                                <ul class="align-center flex-wrap flex-sm-nowrap gx-4 gy-2">
                                                    <li>
                                                        <button type="submit" class="btn btn-lg btn-primary">
                                                        <em class="icon ni ni-save mr-1"></em> Save
                                                        </button>
                                                    </li>
                                                    <li>
                                                        <a href="#" data-dismiss="modal" class="link link-light">Cancel</a>
                                                    </li>
                                                </ul>
                                            </div>
                                        </form>
                                        '
                                    ));
                                    
                                    // Modal New Experience Form
                                    $this->load->view("components/modals/form", array(
                                        "formID" => 'editUsers',
                                        "formTitle" => 'Edit Users',
                                        "formLarge" => true,
                                        "formContent" => '
                                        <form action="'.base_url("index.php/dashboard/doEditUsers").'" 
                                            method="POST" class="row gy-4">
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label class="form-label" for="edit_first_name">First Name</label>
                                                    <input type="text" class="form-control form-control-lg" id="edit_first_name" name="edit_first_name" placeholder="Enter first name">
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label class="form-label" for="edit_last_name">Last Name</label>
                                                    <input type="text" class="form-control form-control-lg" id="edit_last_name" name="edit_last_name" placeholder="Enter last name">
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label class="form-label" for="edit_email">Email</label>
                                                    <input type="email" class="form-control form-control-lg" id="edit_email" name="edit_email" placeholder="Enter email" disabled>
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label class="form-label" for="edit_password">Password</label>
                                                    <input type="password" class="form-control form-control-lg" id="edit_password" name="edit_password" placeholder="Enter password" disabled>
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label class="form-label" for="edit_phone_number">Phone Number</label>
                                                    <input type="number" class="form-control form-control-lg" id="edit_phone_number" name="edit_phone_number" placeholder="Enter phone number 62xxx">
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label class="form-label">Level</label>
                                                    <div class="form-control-wrap">
                                                        <select class="form-select" id="edit_level" name="edit_level">
                                                            <option value="1">User</option>
                                                            <option value="2">Administrator</option>
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-12">
                                                <ul class="align-center flex-wrap flex-sm-nowrap gx-4 gy-2">
                                                    <input type="hidden" id="edit_id" name="edit_id">
                                                    <li>
                                                        <button type="submit" class="btn btn-lg btn-primary">
                                                        <em class="icon ni ni-save mr-1"></em> Save
                                                        </button>
                                                    </li>
                                                    <li>
                                                        <a href="#" data-dismiss="modal" class="link link-light">Cancel</a>
                                                    </li>
                                                </ul>
                                            </div>
                                        </form>
                                        '
                                    ));
                                ?>
                                <div class="nk-block">
                                    <div class="row g-gs">
                                    
                                    <!-- CONTENT -->
                                    <div class="col-xxl-12">
                                    <?php

                                        // Render Rows Of Data
                                        $renderUserData = "";
                                        $no = 1;
                                        foreach ($data["user"] as $item) {
                                            if ($item["utility"]["level"] == 0) continue;
                                            $selected = "";
                                            if ($item["utility"]["level"] == 1) $selected .= "<option value='1' selected>User</option>";
                                            else $selected .= "<option value='1'>User</option>";

                                            if ($item["utility"]["level"] == 2) $selected .= "<option value='2' selected>Administrator</option>";
                                            else $selected .= "<option value='2'>Administrator</option>";

                                            $renderUserData .= '
                                            <div class="nk-tb-item">
                                                <div class="nk-tb-col">
                                                    <span class="tb-lead"><a href="#">'.$no.'</a></span>
                                                </div>
                                                <div class="nk-tb-col tb-col-md">
                                                    <span class="tb-sub">'.$item["first_name"].'</span>
                                                </div>
                                                <div class="nk-tb-col tb-col-md">
                                                    <span class="tb-sub">'.$item["last_name"].'</span>
                                                </div>
                                                <div class="nk-tb-col tb-col-lg">
                                                    <span class="tb-sub">'.$item["email"].'</span>
                                                </div>
                                                <div class="nk-tb-col">
                                                    <span class="tb-sub">'.$item["phone_number"].'</span>
                                                </div>
                                                <div class="nk-tb-col">
                                                    <span class="badge badge-warning" style="cursor: pointer"
                                                        onclick="
                                                            $(`#edit_first_name`).val(`'.$item["first_name"].'`);
                                                            $(`#edit_last_name`).val(`'.$item["last_name"].'`);
                                                            $(`#edit_email`).val(`'.$item["email"].'`);
                                                            $(`#edit_password`).val(`'.$item["password"].'`);
                                                            $(`#edit_phone_number`).val(`'.$item["phone_number"].'`);
                                                            $(`#edit_level`).html(`'.$selected.'`);
                                                            $(`#edit_id`).val(`'.$item["id"].'`);
                                                        " data-toggle="modal" data-target="#editUsers">
                                                        <span class="tb-sub">Ubah</span>
                                                    </span>
                                                    <a href="'.base_url("index.php/dashboard/doDeleteUsers?id=" . $item["id"]).'" class="badge badge-danger" style="cursor: pointer">
                                                        <span class="tb-sub">Hapus</span>
                                                    </a>
                                                </div>
                                            </div>
                                            ';
                                            $no++;
                                        }

                                        // Users (Show 10)
                                        $page = $this->input->get("page") ?: 0;
                                        $maxPage = ceil((int)$data["count_user"] / 10);
                                        $options = "";
                                        for ($i = 0; $i < $maxPage; $i++) {
                                            if ($i == $page)
                                                $options .= '<option value="'.$i.'" selected>'.($i + 1).'</option>';
                                            else 
                                                $options .= '<option value="'.$i.'">'.($i + 1).'</option>';
                                        }

                                        // Load Component Table
                                        $this->load->view("components/container", array(
                                            "containerTitle" => "Users Lists",
                                            "containerTools" => '
                                                <div class="pagination-goto d-flex justify-content-center justify-content-md-start gx-3">
                                                    <div>Page</div>
                                                    <div>
                                                        <select class="form-select form-select-sm" 
                                                            data-search="on" 
                                                            data-dropdown="xs center"
                                                            onchange="loadPagination(this.value)">
                                                            '.$options.'
                                                        </select>
                                                    </div>
                                                    <div>OF '.$maxPage.'</div>
                                                </div>
                                                ',
                                            "containerContent" => '
                                                <div class="card-inner p-0 border-top">
                                                    <div class="nk-tb-list nk-tb-orders">
                                                        <div class="nk-tb-item nk-tb-head">
                                                            <div class="nk-tb-col"><span>No</span></div>
                                                            <div class="nk-tb-col tb-col-sm"><span>Nama Depan</span></div>
                                                            <div class="nk-tb-col tb-col-sm"><span>Nama Belakang</span></div>
                                                            <div class="nk-tb-col tb-col-md"><span>E-Mail</span></div>
                                                            <div class="nk-tb-col tb-col-lg"><span>Phone Number</span></div>
                                                            <div class="nk-tb-col"><span>Aksi</span></div>
                                                        </div>
                                                        '.$renderUserData.'
                                                    </div>
                                                </div>
                                                '
                                        ));
                                    ?>
                                    </div>
                                    <!-- CONTENT -->

                                    </div><!-- .row -->
                                </div><!-- .nk-block -->
                            </div>
                        </div>
                    </div>
                </div>
                <!-- content @e -->
                <?php $this->load->view("components/footer"); ?>
            </div>
            <!-- wrap @e -->
        </div>
        <!-- main @e -->
    </div>
    <!-- app-root @e -->

    <!-- JavaScript -->
    <script src="<?= base_url('assets/template/assets/js/bundle.js?ver=1.4.0'); ?>"></script>
    <script src="<?= base_url('assets/template/assets/js/scripts.js?ver=1.4.0'); ?>"></script>
    <script src="<?= base_url('assets/template/assets/js/charts/gd-general.js?ver=1.4.0'); ?>"></script>

    <script>
        // Base URL
        var base_url = "<?= base_url('index.php'); ?>";
        // Load Pagination
        function loadPagination(page) {
            location.assign(base_url + "/dashboard/users?page=" + page);
        }
    </script>
</body>

</html>