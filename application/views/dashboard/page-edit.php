<!DOCTYPE html>
<html lang="zxx" class="js">

<head>
    <meta charset="utf-8">
    <meta name="author" content="JAKET BUS">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="<?= $meta["description"]; ?>">
    <!-- Fav Icon  -->
    <link rel="shortcut icon" href="<?= base_url("assets/template/images/favicon.png"); ?>">
    <!-- Page Title  -->
    <title><?= $meta["title"]; ?></title>
    <!-- StyleSheets  -->
    <link rel="stylesheet" href="<?= base_url("assets/template/assets/css/dashlite.css?ver=1.4.0"); ?>">
    <link id="skin-default" rel="stylesheet" href="<?= base_url("assets/template/assets/css/theme.css?ver=1.4.0"); ?>">
    <script src="https://cdn.tiny.cloud/1/uztqw8olgep9jjri6g4y082d8ftw8ff878wexa349pk5km4l/tinymce/5/tinymce.min.js" referrerpolicy="origin"></script>
    <script>
      tinymce.init({
        selector: '#content'
      });
    </script>
</head>

<body class="nk-body bg-lighter npc-general has-sidebar ">
    <div class="nk-app-root">
        <!-- main @s -->
        <div class="nk-main ">
            <?php $this->load->view("components/menus/sidebar"); ?>
            <!-- wrap @s -->
            <div class="nk-wrap ">
                <?php $this->load->view("components/menus/topbar"); ?>
                <!-- content @s -->
                <div class="nk-content ">
                    <div class="container-fluid">
                        <div class="nk-content-inner">
                            <div class="nk-content-body">
                                <?php 
                                    // Header
                                    $this->load->view("components/header", array(
                                        "headerTools" => ''
                                    )); 
                                ?>
                                <div class="nk-block">
                                    <div class="row g-gs">
                                    
                                    <!-- CONTENT -->
                                    <div class="col-xxl-12">
                                        <div class="card">
                                            <div class="card-body">
                                                <form action="<?= base_url('index.php/dashboard/doEditPage/' . $data['id']); ?>" method="POST">
                                                    <div class="form-group">
                                                        <label class="form-label" for="title">Title</label>
                                                        <div class="form-control-wrap">
                                                            <input type="text" class="form-control" id="title" name="title" placeholder="Title" value="<?= $data["title"]; ?>">
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <label class="form-label" for="content">Konten</label>
                                                        <div class="form-control-wrap">
                                                            <textarea type="text" class="form-control" id="content" name="content" placeholder="content"
                                                            rows="20"><?= $data["content"]; ?></textarea>
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <div class="form-control-wrap">
                                                            <button class="btn btn-primary"
                                                            type="submit">Save</button>
                                                        </div>
                                                    </div>
                                                </form>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- CONTENT -->

                                    </div><!-- .row -->
                                </div><!-- .nk-block -->
                            </div>
                        </div>
                    </div>
                </div>
                <!-- content @e -->
                <?php $this->load->view("components/footer"); ?>
            </div>
            <!-- wrap @e -->
        </div>
        <!-- main @e -->
    </div>
    <!-- app-root @e -->

    <!-- JavaScript -->
    <script src="<?= base_url('assets/template/assets/js/bundle.js?ver=1.4.0'); ?>"></script>
    <script src="<?= base_url('assets/template/assets/js/scripts.js?ver=1.4.0'); ?>"></script>
    <script src="<?= base_url('assets/template/assets/js/charts/gd-general.js?ver=1.4.0'); ?>"></script>

    <script>
        // Base URL
        var base_url = "<?= base_url('index.php'); ?>";
    </script>
</body>

</html>