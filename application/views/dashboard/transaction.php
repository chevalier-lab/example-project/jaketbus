<!DOCTYPE html>
<html lang="zxx" class="js">

<head>
    <meta charset="utf-8">
    <meta name="author" content="JAKET BUS">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="<?= $meta["description"]; ?>">
    <!-- Fav Icon  -->
    <link rel="shortcut icon" href="<?= base_url("assets/template/images/favicon.png"); ?>">
    <!-- Page Title  -->
    <title><?= $meta["title"]; ?></title>
    <!-- StyleSheets  -->
    <link rel="stylesheet" href="<?= base_url("assets/template/assets/css/dashlite.css?ver=1.4.0"); ?>">
    <link id="skin-default" rel="stylesheet" href="<?= base_url("assets/template/assets/css/theme.css?ver=1.4.0"); ?>">
</head>

<body class="nk-body bg-lighter npc-general has-sidebar ">
    <div class="nk-app-root">
        <!-- main @s -->
        <div class="nk-main ">
            <?php $this->load->view("components/menus/sidebar"); ?>
            <!-- wrap @s -->
            <div class="nk-wrap ">
                <?php $this->load->view("components/menus/topbar"); ?>
                <!-- content @s -->
                <div class="nk-content ">
                    <div class="container-fluid">
                        <div class="nk-content-inner">
                            <div class="nk-content-body">
                                <?php 
                                    // Header
                                    $targetAction = base_url("index.php/dashboard/downloadTransaction");
                                    $actionTools = '
                                    <li>
                                        <button type="button" class="btn btn-success" data-toggle="modal" data-target="#modalDownload">Download Excel</button>
                                    </li>
                                    ';
                                    $this->load->view("components/header", array(
                                        "headerTools" => $actionTools
                                    )); 
                                ?>
                                <div class="nk-block">
                                    <div class="row g-gs">
                                    
                                    <!-- CONTENT -->
                                    <div class="col-xxl-12">
                                    <?php

                                        // Render Rows Of Data
                                        $page = $this->input->get("page") ?: 0;
                                        $renderData = "";
                                        $no = 1;
                                        foreach ($data["transaction"] as $item) {
                                            $gender = $item["passanger_gender"];

                                            if (!empty($gender)) {
                                                switch ($gender) {
                                                    case "m": 
                                                    case "l": $gender = "Laki-laki";
                                                    break;
                                                    case "f":
                                                    case "w":
                                                    case "p": $gender = "Perempuan";
                                                    break;
                                                }
                                            }

                                            $renderData .= '
                                            <div class="nk-tb-item">
                                                <div class="nk-tb-col">
                                                    <span class="tb-lead"><a href="#">'.$no.'</a></span>
                                                </div>
                                                <div class="nk-tb-col tb-col-md">
                                                    <span class="tb-sub">'.$item["ticket_id"].'</span>
                                                </div>
                                                <div class="nk-tb-col tb-col-md">
                                                    <span class="tb-sub">'.$item["passanger_name"].'</span>
                                                </div>
                                                <div class="nk-tb-col tb-col-md">
                                                    <span class="tb-sub">'.$gender.'</span>
                                                </div>
                                                <div class="nk-tb-col tb-col-md">
                                                    <span class="tb-sub">'.$item["passanger_telp"].'</span>
                                                </div>
                                                <div class="nk-tb-col tb-col-md">
                                                    <span class="tb-sub">'.$item["date_of_departure"].'</span>
                                                </div>
                                                <div class="nk-tb-col tb-col-md">
                                                    <span class="tb-sub">'.$item["time_of_departure"].'</span>
                                                </div>
                                                <div class="nk-tb-col tb-col-md">
                                                    <span class="tb-sub">'.$item["price"].'</span>
                                                </div>
                                                <div class="nk-tb-col tb-col-md">
                                                    <span class="tb-sub">'.$item["created_at"].'</span>
                                                </div>
                                                <div class="nk-tb-col tb-col-md">
                                                    <a href="javascript:void(0)" 
                                                    class="badge badge-primary" style="cursor: pointer"
                                                    onclick="loadDetail('.$page.', '.$item["id"].')">
                                                        <span class="tb-sub">Lihat</span>
                                                    </a>
                                                </div>
                                            </div>
                                            ';
                                            $no++;
                                        }

                                        // Users (Show 10)
                                        $maxPage = ceil((int)$data["count_transaction"] / 10);
                                        $options = "";
                                        for ($i = 0; $i < $maxPage; $i++) {
                                            if ($i == $page)
                                                $options .= '<option value="'.$i.'" selected>'.($i + 1).'</option>';
                                            else 
                                                $options .= '<option value="'.$i.'">'.($i + 1).'</option>';
                                        }

                                        // Load Component Table
                                        $this->load->view("components/container", array(
                                            "containerTitle" => "Transaction Lists",
                                            "containerTools" => '
                                                <div class="pagination-goto d-flex justify-content-center justify-content-md-start gx-3">
                                                    <div>Page</div>
                                                    <div>
                                                        <select class="form-select form-select-sm" 
                                                            data-search="on" 
                                                            data-dropdown="xs center"
                                                            onchange="loadPagination(this.value)">
                                                            '.$options.'
                                                        </select>
                                                    </div>
                                                    <div>OF '.$maxPage.'</div>
                                                </div>
                                                ',
                                            "containerContent" => '
                                                <div class="card-inner p-0 border-top">
                                                    <div class="nk-tb-list nk-tb-orders">
                                                        <div class="nk-tb-item nk-tb-head">
                                                            <div class="nk-tb-col"><span>No</span></div>
                                                            <div class="nk-tb-col tb-col-sm"><span>ID Ticket</span></div>
                                                            <div class="nk-tb-col tb-col-sm"><span>Penumpang</span></div>
                                                            <div class="nk-tb-col tb-col-md"><span>JK</span></div>
                                                            <div class="nk-tb-col tb-col-md"><span>No Telp</span></div>
                                                            <div class="nk-tb-col tb-col-md"><span>Tanggal Keberangkatan</span></div>
                                                            <div class="nk-tb-col tb-col-md"><span>Waktu Keberangkatan</span></div>
                                                            <div class="nk-tb-col tb-col-md"><span>Harga</span></div>
                                                            <div class="nk-tb-col tb-col-md"><span>Tanggal Pembuatan</span></div>
                                                            <div class="nk-tb-col tb-col-md"><span>Detail</span></div>
                                                        </div>
                                                        '.$renderData.'
                                                    </div>
                                                </div>
                                                '
                                        ));
                                    ?>
                                    </div>
                                    <!-- CONTENT -->

                                    </div><!-- .row -->
                                </div><!-- .nk-block -->
                            </div>
                        </div>
                    </div>
                </div>
                <!-- content @e -->
                <?php $this->load->view("components/footer"); ?>
            </div>
            <!-- wrap @e -->
        </div>
        <!-- main @e -->
    </div>
    <!-- app-root @e -->

    <!-- detail transaction -->
    <?php
    if (isset($data["detail_transaction"])) {
        $detail_transaction = $data["detail_transaction"];
        echo ('
        <div class="modal fade zoom" tabindex="-1" id="modalZoom" aria-modal="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title">Detail Transaksi</h5>
                        <a href="javascript:void(0)" class="close" data-dismiss="modal" aria-label="Close"
                        onclick="loadPagination('.$page.')">
                            <em class="icon ni ni-cross"></em>
                        </a>
                    </div>
                    <div class="modal-body">
                        <h4>Informasi Pengguna</h4>
                        <div class="row">
                            <div class="col-md-6 col-sm-12">
                                <div class="form-group">
                                    <label class="form-label">Nama Penumpang</label>
                                    <div>'.$detail_transaction["passanger_name"].'</div>
                                </div>
                            </div>
                            <div class="col-md-6 col-sm-12">
                                <div class="form-group">
                                    <label class="form-label">Jenis Kelamin</label>
                                    <div>'.$detail_transaction["passanger_gender"].'</div>
                                </div>
                            </div>
                            <div class="col-md-6 col-sm-12">
                                <div class="form-group">
                                    <label class="form-label">Nomor Telepon</label>
                                    <div>'.$detail_transaction["passanger_telp"].'</div>
                                </div>
                            </div>
                        </div>

                        <hr>

                        <h4>Informasi Tiket</h4>
                        <div class="row">
                            <div class="col-md-6 col-sm-12">
                                <div class="form-group">
                                    <label class="form-label">ID Tiket</label>
                                    <div>'.$detail_transaction["ticket_id"].'</div>
                                </div>
                            </div>
                            <div class="col-md-6 col-sm-12">
                                <div class="form-group">
                                    <label class="form-label">PNR</label>
                                    <div>'.$detail_transaction["pnr"].'</div>
                                </div>
                            </div>
                            <div class="col-md-6 col-sm-12">
                                <div class="form-group">
                                    <label class="form-label">Tanggal Keberangkatan</label>
                                    <div>'.$detail_transaction["date_of_departure"].'</div>
                                </div>
                            </div>
                            <div class="col-md-6 col-sm-12">
                                <div class="form-group">
                                    <label class="form-label">Jam Keberangkatan</label>
                                    <div>'.$detail_transaction["time_of_departure"].'</div>
                                </div>
                            </div>
                            <div class="col-md-6 col-sm-12">
                                <div class="form-group">
                                    <label class="form-label">Keberangkatan</label>
                                    <div>'.$detail_transaction["origin"].'</div>
                                </div>
                            </div>
                            <div class="col-md-6 col-sm-12">
                                <div class="form-group">
                                    <label class="form-label">Tujuan</label>
                                    <div>'.$detail_transaction["destionation"].'</div>
                                </div>
                            </div>
                            <div class="col-md-6 col-sm-12">
                                <div class="form-group">
                                    <label class="form-label">Nama PO</label>
                                    <div>'.$detail_transaction["po_name"].'</div>
                                </div>
                            </div>
                            <div class="col-md-6 col-sm-12">
                                <div class="form-group">
                                    <label class="form-label">Nomor Kursi</label>
                                    <div>'.$detail_transaction["seat_number"].'</div>
                                </div>
                            </div>
                            <div class="col-md-6 col-sm-12">
                                <div class="form-group">
                                    <label class="form-label">Kategori</label>
                                    <div>'.$detail_transaction["category"].'</div>
                                </div>
                            </div>
                            <div class="col-md-6 col-sm-12">
                                <div class="form-group">
                                    <label class="form-label">Tanggal Pemesanan</label>
                                    <div>'.$detail_transaction["created_at"].'</div>
                                </div>
                            </div>
                        </div>

                        <hr>
                        
                        <h4>Informasi Pembayaran</h4>
                        <div class="row">
                            <div class="col-md-6 col-sm-12">
                                <div class="form-group">
                                    <label class="form-label">Metode Pembayaran</label>
                                    <div>'.$detail_transaction["payment_method"].'</div>
                                </div>
                            </div>
                            <div class="col-md-6 col-sm-12">
                                <div class="form-group">
                                    <label class="form-label">Status Pembayaran</label>
                                    <div>'.$detail_transaction["payment_status"].'</div>
                                </div>
                            </div>
                            <div class="col-md-6 col-sm-12">
                                <div class="form-group">
                                    <label class="form-label">Tanggal Expired</label>
                                    <div>'.$detail_transaction["payment_expired"].'</div>
                                </div>
                            </div>
                        </div>
                        
                    </div>
                </div>
            </div>
        </div>
        ');
    }
    ?>

    <!-- Modal Default -->
    <div class="modal fade" tabindex="-1" id="modalDownload">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Download Data Transaksi</h5>
                    <a href="#" class="close" data-dismiss="modal" aria-label="Close">
                        <em class="icon ni ni-cross"></em>
                    </a>
                </div>
                <div class="modal-body">

                    <div class="row">
                        <div class="col-sm-12">
                            <div class="form-group">
                                <label class="form-label" for="default-06">Pilih Jenis Data</label>
                                <div class="form-control-wrap ">
                                    <div class="form-control-select">
                                        <select class="form-control" id="type-data">
                                            <option value="<?= $targetAction.'/1'; ?>">Semua Transaksi Terbayar</option>
                                            <option value="<?= $targetAction.'/2'; ?>">Semua Transaksi Belum Terbayar</option>
                                            <option value="<?= $targetAction.'/3'; ?>">Transaksi DompetJak Terbayar</option>
                                            <option value="<?= $targetAction.'/4'; ?>">Transaksi DompetJak Belum Terbayar</option>
                                            <option value="<?= $targetAction.'/5'; ?>">Transaksi Indomaret Terbayar</option>
                                            <option value="<?= $targetAction.'/6'; ?>">Transaksi Indomaret Belum Terbayar</option>
                                            <option value="<?= $targetAction.'/7'; ?>">Transaksi Alfamart Terbayar</option>
                                            <option value="<?= $targetAction.'/8'; ?>">Transaksi Alfamart Belum Terbayar</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    
                    <div class="row">
                        
                        <div class="col-sm-12">
                            <div class="form-group">
                                <label class="form-label">Mulai Dari Tanggal</label>
                                <div class="form-control-wrap">
                                    <div class="form-icon form-icon-left">
                                        <em class="icon ni ni-calendar"></em>
                                    </div>
                                    <input type="text" class="form-control date-picker" id="date-from" data-date-format="yyyy-mm-dd">
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-12">
                            <div class="form-group">
                                <label class="form-label">Sampai Tanggal</label>
                                <div class="form-control-wrap">
                                    <div class="form-icon form-icon-left">
                                        <em class="icon ni ni-calendar"></em>
                                    </div>
                                    <input type="text" class="form-control date-picker" id="date-to" data-date-format="yyyy-mm-dd">
                                </div>
                            </div>
                        </div>

                    </div>

                </div>
                <div class="modal-footer bg-light">
                    <button class="btn btn-success" type="button"
                    onclick="location.assign(`${$('#type-data').val()}?date_from=${$('#date-from').val()}&date_to=${$('#date-to').val()}`)">Download Data</button>
                </div>
            </div>
        </div>
    </div>

    <!-- JavaScript -->
    <script src="<?= base_url('assets/template/assets/js/bundle.js?ver=1.4.0'); ?>"></script>
    <script src="<?= base_url('assets/template/assets/js/scripts.js?ver=1.4.0'); ?>"></script>
    <script src="<?= base_url('assets/template/assets/js/charts/gd-general.js?ver=1.4.0'); ?>"></script>

    <script>
        // Base URL
        var base_url = "<?= base_url('index.php'); ?>";
        // Load Pagination
        function loadPagination(page) {
            location.assign(base_url + "/dashboard/transaction?page=" + page);
        }

        // Load Detail
        function loadDetail(page, id) {
            location.assign(base_url + "/dashboard/transaction?page=" + page + "&id=" + id);
        }
    </script>

    <?php
    if (isset($data["detail_transaction"])) {
        ?>
        <script>
        $("#modalZoom").modal("show")
        </script>
        <?php
    }
    ?>
</body>

</html>