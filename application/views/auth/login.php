<!DOCTYPE html>
<html lang="zxx" class="js">

<head>
    <meta charset="utf-8">
    <meta name="author" content="JAKET BUS">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="<?= $meta["description"]; ?>">
    <!-- Fav Icon  -->
    <link rel="shortcut icon" href="<?= base_url("assets/template/images/favicon.png"); ?>">
    <!-- Page Title  -->
    <title><?= $meta["title"]; ?></title>
    <!-- StyleSheets  -->
    <link rel="stylesheet" href="<?= base_url("assets/template/assets/css/dashlite.css?ver=1.4.0"); ?>">
    <link id="skin-default" rel="stylesheet" href="<?= base_url("assets/template/assets/css/theme.css?ver=1.4.0"); ?>">
</head>

<body class="nk-body npc-crypto ui-clean pg-auth">
    <!-- app body @s -->
    <div class="nk-app-root">
        <div class="nk-split nk-split-page nk-split-md">
            <div class="nk-split-content nk-block-area nk-block-area-column nk-auth-container">
                <div class="absolute-top-right d-lg-none p-3 p-sm-5">
                    <a href="#" class="toggle btn-white btn btn-icon btn-light" data-target="athPromo"><em class="icon ni ni-info"></em></a>
                </div>
                <div class="nk-block nk-block-middle nk-auth-body">
                    <div class="brand-logo pb-5">
                        <a href="html/general/index.html" class="logo-link">
                            <img class="logo-light logo-img logo-img-lg" src="<?= base_url('assets/template/images/origin_logo.png'); ?>" srcset="<?= base_url('assets/template/images/origin_logo.png 2x'); ?>" alt="logo">
                            <img class="logo-dark logo-img logo-img-lg" src="<?= base_url('assets/template/images/origin_logo.png'); ?>" srcset="<?= base_url('assets/template/images/origin_logo.png 2x'); ?>" alt="logo-dark">
                        </a>
                    </div>
                    <div class="nk-block-head">
                        <div class="nk-block-head-content">
                            <h5 class="nk-block-title">Sign-In</h5>
                            <div class="nk-block-des">
                                <p>Access the DashLite panel using your email and passcode.</p>
                            </div>
                        </div>
                    </div><!-- .nk-block-head -->
                    <form action="<?= base_url('index.php/auth/doLogin'); ?>" method="POST">
                        <div class="form-group">
                            <div class="form-label-group">
                                <label class="form-label" for="default-01">Email</label>
                                <a class="link link-primary link-sm" tabindex="-1" href="#">Need Help?</a>
                            </div>
                            <input type="text" class="form-control form-control-lg" id="email" name="email" placeholder="Enter your email address or username">
                        </div><!-- .foem-group -->
                        <div class="form-group">
                            <div class="form-label-group">
                                <label class="form-label" for="password">Passcode</label>
                                <a class="link link-primary link-sm" tabindex="-1" href="html/general/pages/auths/auth-reset.html">Forgot Code?</a>
                            </div>
                            <div class="form-control-wrap">
                                <a tabindex="-1" href="#" class="form-icon form-icon-right passcode-switch" data-target="password">
                                    <em class="passcode-icon icon-show icon ni ni-eye"></em>
                                    <em class="passcode-icon icon-hide icon ni ni-eye-off"></em>
                                </a>
                                <input type="password" class="form-control form-control-lg" id="password" name="password" placeholder="Enter your passcode">
                            </div>
                        </div><!-- .foem-group -->
                        <div class="form-group">
                            <button class="btn btn-lg btn-primary btn-block" type="submit">Sign in</button>
                        </div>
                    </form><!-- form -->
                </div><!-- .nk-block -->
            </div><!-- .nk-split-content -->
            
            <div class="nk-split-content nk-split-stretch bg-lighter d-flex toggle-break-lg toggle-slide toggle-slide-right" data-content="athPromo" data-toggle-screen="lg" data-toggle-overlay="true">
                <div class="slider-wrap w-100 w-max-550px p-3 p-sm-5 m-auto">
                    <div class="slider-init" data-slick='{"dots":true, "arrows":false}'>
                        <div class="slider-item">
                            <div class="nk-feature nk-feature-center">
                                <div class="nk-feature-img">
                                    <img class="round" src="<?= base_url('assets/template/images/banner.png'); ?>" srcset="<?= base_url('assets/template/images/banner.png'); ?>" alt="">
                                </div>
                            </div>
                        </div>
                    </div><!-- .slider-init -->
                    <div class="slider-dots"></div>
                    <div class="slider-arrows"></div>
                </div><!-- .slider-wrap -->
            </div><!-- .nk-split-content -->
        </div><!-- .nk-split -->
    </div><!-- app body @e -->

    <!-- JavaScript -->
    <script src="<?= base_url('assets/template/assets/js/bundle.js?ver=1.4.0'); ?>"></script>
    <script src="<?= base_url('assets/template/assets/js/scripts.js?ver=1.4.0'); ?>"></script>
</body>

</html>