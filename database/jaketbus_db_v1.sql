-- phpMyAdmin SQL Dump
-- version 4.9.5
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Waktu pembuatan: 10 Okt 2020 pada 11.59
-- Versi server: 10.2.33-MariaDB-cll-lve
-- Versi PHP: 7.3.6

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `jaketbus_db_v1`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `m_experiences`
--

CREATE TABLE `m_experiences` (
  `id` int(10) UNSIGNED NOT NULL,
  `title` varchar(255) NOT NULL,
  `content` text NOT NULL,
  `rating` tinyint(1) NOT NULL,
  `id_m_users` int(10) UNSIGNED NOT NULL,
  `id_m_medias` int(10) UNSIGNED NOT NULL,
  `created_at` varchar(20) DEFAULT NULL,
  `updated_at` varchar(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `m_experiences`
--

INSERT INTO `m_experiences` (`id`, `title`, `content`, `rating`, `id_m_users`, `id_m_medias`, `created_at`, `updated_at`) VALUES
(1, 'Example Experience 1', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.', 4, 1, 4, '2020-08-28 06:35:30', '2020-08-28 06:35:30'),
(2, 'Example Experience 2', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.', 5, 1, 5, '2020-08-28 06:41:20', '2020-08-28 06:41:20');

-- --------------------------------------------------------

--
-- Struktur dari tabel `m_features`
--

CREATE TABLE `m_features` (
  `id` int(10) UNSIGNED NOT NULL,
  `title` varchar(255) NOT NULL,
  `content` text NOT NULL,
  `id_m_users` int(10) UNSIGNED NOT NULL,
  `id_m_medias` int(10) UNSIGNED NOT NULL,
  `created_at` varchar(20) DEFAULT NULL,
  `updated_at` varchar(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `m_features`
--

INSERT INTO `m_features` (`id`, `title`, `content`, `id_m_users`, `id_m_medias`, `created_at`, `updated_at`) VALUES
(1, 'Updates on Flying Domestic in the New Normal', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.', 1, 2, '2020-08-28 06:07:47', '2020-08-28 06:07:47'),
(2, 'Xperience Express', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.', 1, 3, '2020-08-28 06:09:22', '2020-08-28 06:09:22');

-- --------------------------------------------------------

--
-- Struktur dari tabel `m_medias`
--

CREATE TABLE `m_medias` (
  `id` int(10) UNSIGNED NOT NULL,
  `uri` text NOT NULL,
  `label` varchar(255) NOT NULL,
  `created_at` varchar(20) DEFAULT NULL,
  `updated_at` varchar(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `m_medias`
--

INSERT INTO `m_medias` (`id`, `uri`, `label`, `created_at`, `updated_at`) VALUES
(1, 'https://jaketbus.my.id/assets/dist/img/author.jpg', 'Author Default', NULL, NULL),
(2, 'https://jaketbus.my.id/assets/dist/img/20200828060747banner_1.png', '20200828060747banner_1.png', '2020-08-28 06:07:47', '2020-08-28 06:07:47'),
(3, 'https://jaketbus.my.id/assets/dist/img/20200828060922banner_2.jpeg', '20200828060922banner_2.jpeg', '2020-08-28 06:09:22', '2020-08-28 06:09:22'),
(4, 'https://jaketbus.my.id/assets/dist/img/20200828063530thumb_1.jpeg', '20200828063530thumb_1.jpeg', '2020-08-28 06:35:30', '2020-08-28 06:35:30'),
(5, 'https://jaketbus.my.id/assets/dist/img/20200828064120thumb_3.jpeg', '20200828064120thumb_3.jpeg', '2020-08-28 06:41:20', '2020-08-28 06:41:20'),
(6, 'https://jaketbus.my.id/assets/dist/img/202008310637481598855861091_jaket_bus.png', '202008310637481598855861091_jaket_bus.png', '2020-08-31 06:37:48', '2020-08-31 06:37:48'),
(7, 'https://jaketbus.my.id/assets/dist/img/20201010031439IMG_20201010_111434356.jpg', '20201010031439IMG_20201010_111434356.jpg', '2020-10-10 03:14:39', '2020-10-10 03:14:39'),
(8, 'https://jaketbus.my.id/assets/dist/img/20201010032049IMG_20201010_112042703.jpg', '20201010032049IMG_20201010_112042703.jpg', '2020-10-10 03:20:49', '2020-10-10 03:20:49'),
(9, 'https://jaketbus.my.id/assets/dist/img/20201010033034IMG_20201010_113026364.jpg', '20201010033034IMG_20201010_113026364.jpg', '2020-10-10 03:30:34', '2020-10-10 03:30:34'),
(10, 'https://jaketbus.my.id/assets/dist/img/20201010033525IMG_20201010_113518911.jpg', '20201010033525IMG_20201010_113518911.jpg', '2020-10-10 03:35:25', '2020-10-10 03:35:25'),
(11, 'https://jaketbus.my.id/assets/dist/img/20201010033702IMG-20201010-WA0018.jpg', '20201010033702IMG-20201010-WA0018.jpg', '2020-10-10 03:37:02', '2020-10-10 03:37:02'),
(12, 'https://jaketbus.my.id/assets/dist/img/20201010034603IMG-20201010-WA0018.jpg', '20201010034603IMG-20201010-WA0018.jpg', '2020-10-10 03:46:03', '2020-10-10 03:46:03'),
(13, 'https://jaketbus.my.id/assets/dist/img/20201010034634IMG_20201010_114627905.jpg', '20201010034634IMG_20201010_114627905.jpg', '2020-10-10 03:46:34', '2020-10-10 03:46:34'),
(14, 'https://jaketbus.my.id/assets/dist/img/20201010034730IMG-20201010-WA0018.jpg', '20201010034730IMG-20201010-WA0018.jpg', '2020-10-10 03:47:30', '2020-10-10 03:47:30'),
(15, 'https://jaketbus.my.id/assets/dist/img/20201010040243Screenshot_20201010-023501.jpg', '20201010040243Screenshot_20201010-023501.jpg', '2020-10-10 04:02:43', '2020-10-10 04:02:43'),
(16, 'https://jaketbus.my.id/assets/dist/img/20201010040914IMG-20201010-WA0018.jpg', '20201010040914IMG-20201010-WA0018.jpg', '2020-10-10 04:09:14', '2020-10-10 04:09:14'),
(17, 'https://jaketbus.my.id/assets/dist/img/20201010041214Screenshot_20201010-023501.jpg', '20201010041214Screenshot_20201010-023501.jpg', '2020-10-10 04:12:14', '2020-10-10 04:12:14'),
(18, 'https://jaketbus.my.id/assets/dist/img/20201010071614IMG_20201010_113518911.jpg', '20201010071614IMG_20201010_113518911.jpg', '2020-10-10 07:16:14', '2020-10-10 07:16:14'),
(19, 'https://jaketbus.my.id/assets/dist/img/20201010081322IMG_20201010_112042703.jpg', '20201010081322IMG_20201010_112042703.jpg', '2020-10-10 08:13:22', '2020-10-10 08:13:22'),
(20, 'https://jaketbus.my.id/assets/dist/img/20201010084734IMG-20201009-WA0006.jpg', '20201010084734IMG-20201009-WA0006.jpg', '2020-10-10 08:47:34', '2020-10-10 08:47:34'),
(21, 'https://jaketbus.my.id/assets/dist/img/20201010084915IMG-20201009-WA0006.jpg', '20201010084915IMG-20201009-WA0006.jpg', '2020-10-10 08:49:15', '2020-10-10 08:49:15'),
(22, 'https://jaketbus.my.id/assets/dist/img/20201010085254Screenshot_20201010-151750_JAKET BUS.jpg', '20201010085254Screenshot_20201010-151750_JAKET BUS.jpg', '2020-10-10 08:52:54', '2020-10-10 08:52:54'),
(23, 'https://jaketbus.my.id/assets/dist/img/20201010094210Screenshot_20201010-151750_JAKET BUS.jpg', '20201010094210Screenshot_20201010-151750_JAKET BUS.jpg', '2020-10-10 09:42:10', '2020-10-10 09:42:10'),
(24, 'https://jaketbus.my.id/assets/dist/img/20201010094227Screenshot_20201010-151740_JAKET BUS.jpg', '20201010094227Screenshot_20201010-151740_JAKET BUS.jpg', '2020-10-10 09:42:27', '2020-10-10 09:42:27'),
(25, 'https://jaketbus.my.id/assets/dist/img/20201010094746Screenshot_20201010-151750_JAKET BUS.jpg', '20201010094746Screenshot_20201010-151750_JAKET BUS.jpg', '2020-10-10 09:47:46', '2020-10-10 09:47:46'),
(26, 'https://jaketbus.my.id/assets/dist/img/20201010102905IMG_20201010_140152_531.jpg', '20201010102905IMG_20201010_140152_531.jpg', '2020-10-10 10:29:05', '2020-10-10 10:29:05'),
(27, 'https://jaketbus.my.id/assets/dist/img/20201010102959IMG-20201010-WA0043.jpg', '20201010102959IMG-20201010-WA0043.jpg', '2020-10-10 10:29:59', '2020-10-10 10:29:59');

-- --------------------------------------------------------

--
-- Struktur dari tabel `m_notification`
--

CREATE TABLE `m_notification` (
  `id` int(10) UNSIGNED NOT NULL,
  `type` varchar(100) NOT NULL,
  `title` varchar(255) NOT NULL,
  `content` text NOT NULL,
  `id_m_users` int(10) UNSIGNED NOT NULL,
  `created_at` varchar(20) DEFAULT NULL,
  `updated_at` varchar(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `m_notification`
--

INSERT INTO `m_notification` (`id`, `type`, `title`, `content`, `id_m_users`, `created_at`, `updated_at`) VALUES
(1, 'Authentication', 'Success to create user', 'Success to create user with phone number 6282119189690', 1, '2020-08-18 09:46:25', '2020-08-18 09:46:25'),
(2, 'Authentication', 'Success to create user', 'Success to create user with phone number 6289666585006', 2, '2020-08-18 15:06:14', '2020-08-18 15:06:14'),
(3, 'Authentication', 'Success to create user', 'Success to create user with phone number 62895324806198', 3, '2020-08-19 00:07:02', '2020-08-19 00:07:02'),
(4, 'Authentication', 'Success to create user', 'Success to create user with phone number 6282213737535', 4, '2020-08-19 14:05:17', '2020-08-19 14:05:17'),
(5, 'Authentication', 'Success to login user', 'Success to login user with phone number 6282213737535', 4, '2020-08-19 14:07:03', '2020-08-19 14:07:03'),
(6, 'Authentication', 'Success to login user', 'Success to login user with phone number 6282119189690', 1, '2020-08-20 05:01:26', '2020-08-20 05:01:26'),
(7, 'Authentication', 'Success to login user', 'Success to login user with phone number 6282119189690', 1, '2020-08-20 05:02:56', '2020-08-20 05:02:56'),
(8, 'Authentication', 'Success to login user', 'Success to login user with phone number 6282119189690', 1, '2020-08-20 05:04:35', '2020-08-20 05:04:35'),
(9, 'Authentication', 'Success to login user', 'Success to login user with phone number 6282119189690', 1, '2020-08-20 06:01:21', '2020-08-20 06:01:21'),
(10, 'Authentication', 'Success to login user', 'Success to login user with phone number 6282119189690', 1, '2020-08-20 06:01:56', '2020-08-20 06:01:56'),
(11, 'Authentication', 'Success to login user', 'Success to login user with phone number 6282213737535', 4, '2020-08-20 14:00:59', '2020-08-20 14:00:59'),
(12, 'Authentication', 'Success to login user', 'Success to login user with phone number 6282213737535', 4, '2020-08-31 02:09:38', '2020-08-31 02:09:38'),
(13, 'Authentication', 'Success to login user', 'Success to login user with phone number 6282213737535', 4, '2020-08-31 02:27:07', '2020-08-31 02:27:07'),
(14, 'Authentication', 'Success to update profile', 'Success to update profile', 1, '2020-08-31 05:05:32', '2020-08-31 05:05:32'),
(15, 'Authentication', 'Success to login user', 'Success to login user with phone number 6282213737535', 4, '2020-08-31 07:31:50', '2020-08-31 07:31:50'),
(16, 'Authentication', 'Success to update profile', 'Success to update profile', 4, '2020-08-31 07:34:07', '2020-08-31 07:34:07'),
(17, 'Authentication', 'Success to update profile', 'Success to update profile', 4, '2020-08-31 07:39:26', '2020-08-31 07:39:26'),
(18, 'Authentication', 'Success to update profile', 'Success to update profile', 1, '2020-08-31 09:45:44', '2020-08-31 09:45:44'),
(19, 'Authentication', 'Success to login user', 'Success to login user with phone number 6282213737535', 4, '2020-09-03 03:14:11', '2020-09-03 03:14:11'),
(20, 'Authentication', 'Success to create user', 'Success to create user with phone number 6289502100693', 5, '2020-09-04 03:08:25', '2020-09-04 03:08:25'),
(21, 'Authentication', 'Success to login user', 'Success to login user with phone number 6289502100693', 5, '2020-09-04 03:20:43', '2020-09-04 03:20:43'),
(22, 'Authentication', 'Success to login user', 'Success to login user with phone number 62895324806198', 3, '2020-09-04 03:22:21', '2020-09-04 03:22:21'),
(23, 'Authentication', 'Success to update profile', 'Success to update profile', 3, '2020-09-04 03:27:22', '2020-09-04 03:27:22'),
(24, 'Authentication', 'Success to update profile', 'Success to update profile', 3, '2020-09-04 06:47:09', '2020-09-04 06:47:09'),
(25, 'Authentication', 'Success to login user', 'Success to login user with phone number 62895324806198', 3, '2020-09-04 06:54:37', '2020-09-04 06:54:37'),
(26, 'Authentication', 'Success to login user', 'Success to login user with phone number 6282213737535', 4, '2020-09-04 07:26:40', '2020-09-04 07:26:40'),
(27, 'Authentication', 'Success to login user', 'Success to login user with phone number 6282213737535', 4, '2020-09-04 07:32:26', '2020-09-04 07:32:26'),
(28, 'Authentication', 'Success to login user', 'Success to login user with phone number 6282119189690', 1, '2020-09-05 22:24:56', '2020-09-05 22:24:56'),
(29, 'Authentication', 'Success to update profile', 'Success to update profile', 1, '2020-09-05 23:35:51', '2020-09-05 23:35:51'),
(30, 'Authentication', 'Success to create user', 'Success to create user with phone number 6289502100693', 6, '2020-09-07 02:45:02', '2020-09-07 02:45:02'),
(31, 'Authentication', 'Success to login user', 'Success to login user with phone number 62895324806198', 3, '2020-09-07 02:45:18', '2020-09-07 02:45:18'),
(32, 'Authentication', 'Success to login user', 'Success to login user with phone number 62895324806198', 3, '2020-09-10 04:44:36', '2020-09-10 04:44:36'),
(33, 'Authentication', 'Success to login user', 'Success to login user with phone number 62895324806198', 3, '2020-09-10 07:42:42', '2020-09-10 07:42:42'),
(34, 'Authentication', 'Success to login user', 'Success to login user with phone number 6282213737535', 4, '2020-09-17 09:33:57', '2020-09-17 09:33:57'),
(35, 'Authentication', 'Success to login user', 'Success to login user with phone number 6282213737535', 4, '2020-09-19 04:31:26', '2020-09-19 04:31:26'),
(36, 'Authentication', 'Success to login user', 'Success to login user with phone number 6282119189690', 1, '2020-10-06 00:02:33', '2020-10-06 00:02:33'),
(37, 'Authentication', 'Success to create user', 'Success to create user with phone number 6281298270999', 7, '2020-10-06 07:44:45', '2020-10-06 07:44:45'),
(38, 'Authentication', 'Success to create user', 'Success to create user with phone number 6281314630883', 8, '2020-10-07 06:24:49', '2020-10-07 06:24:49'),
(39, 'Authentication', 'Success to login user', 'Success to login user with phone number 6281298270999', 7, '2020-10-08 07:38:14', '2020-10-08 07:38:14'),
(40, 'Authentication', 'Success to create user', 'Success to create user with phone number 6287888882114', 9, '2020-10-08 07:43:15', '2020-10-08 07:43:15'),
(41, 'Authentication', 'Success to create user', 'Success to create user with phone number 6281806994125', 10, '2020-10-08 07:47:29', '2020-10-08 07:47:29'),
(42, 'Authentication', 'Success to create user', 'Success to create user with phone number 6281381153586', 11, '2020-10-08 07:51:21', '2020-10-08 07:51:21'),
(43, 'Authentication', 'Success to login user', 'Success to login user with phone number 6281314630883', 8, '2020-10-08 08:14:15', '2020-10-08 08:14:15'),
(44, 'Authentication', 'Success to create user', 'Success to create user with phone number 6285716461494', 12, '2020-10-08 08:20:07', '2020-10-08 08:20:07'),
(45, 'Authentication', 'Success to create user', 'Success to create user with phone number 6287884514498', 13, '2020-10-08 11:37:24', '2020-10-08 11:37:24'),
(46, 'Authentication', 'Success to create user', 'Success to create user with phone number 6281280642747', 14, '2020-10-08 14:23:02', '2020-10-08 14:23:02'),
(47, 'Authentication', 'Success to login user', 'Success to login user with phone number 6285716461494', 12, '2020-10-09 03:41:52', '2020-10-09 03:41:52'),
(48, 'Authentication', 'Success to login user', 'Success to login user with phone number 6281381153586', 11, '2020-10-09 03:47:28', '2020-10-09 03:47:28'),
(49, 'Authentication', 'Success to login user', 'Success to login user with phone number 6287884514498', 13, '2020-10-09 03:48:28', '2020-10-09 03:48:28'),
(50, 'Authentication', 'Success to create user', 'Success to create user with phone number 6281510744757', 15, '2020-10-09 04:00:05', '2020-10-09 04:00:05'),
(51, 'Authentication', 'Success to create user', 'Success to create user with phone number 6281211238713', 16, '2020-10-09 04:11:36', '2020-10-09 04:11:36'),
(52, 'Authentication', 'Success to create user', 'Success to create user with phone number 6281290388717', 17, '2020-10-09 04:13:18', '2020-10-09 04:13:18'),
(53, 'Authentication', 'Success to create user', 'Success to create user with phone number 6281287779935', 18, '2020-10-09 04:48:16', '2020-10-09 04:48:16'),
(54, 'Authentication', 'Success to create user', 'Success to create user with phone number 62895324999348', 19, '2020-10-09 05:41:05', '2020-10-09 05:41:05'),
(55, 'Authentication', 'Success to login user', 'Success to login user with phone number 6285716461494', 12, '2020-10-09 07:49:10', '2020-10-09 07:49:10'),
(56, 'Authentication', 'Success to create user', 'Success to create user with phone number 6289602686479', 20, '2020-10-09 10:47:05', '2020-10-09 10:47:05'),
(57, 'Authentication', 'Success to update profile', 'Success to update profile', 20, '2020-10-09 10:49:48', '2020-10-09 10:49:48'),
(58, 'Authentication', 'Success to create user', 'Success to create user with phone number 6287784599444', 21, '2020-10-09 10:55:39', '2020-10-09 10:55:39'),
(59, 'Authentication', 'Success to create user', 'Success to create user with phone number 6285211970811', 22, '2020-10-09 10:56:23', '2020-10-09 10:56:23'),
(60, 'Authentication', 'Success to create user', 'Success to create user with phone number 6281316681453', 23, '2020-10-09 10:57:21', '2020-10-09 10:57:21'),
(61, 'Authentication', 'Success to create user', 'Success to create user with phone number 6281218443187', 24, '2020-10-09 13:43:22', '2020-10-09 13:43:22'),
(62, 'Authentication', 'Success to login user', 'Success to login user with phone number 6282213737535', 4, '2020-10-09 14:12:00', '2020-10-09 14:12:00'),
(63, 'Authentication', 'Success to login user', 'Success to login user with phone number 6282119189690', 1, '2020-10-09 17:43:33', '2020-10-09 17:43:33'),
(64, 'Authentication', 'Success to login user', 'Success to login user with phone number 6285716461494', 12, '2020-10-09 23:13:27', '2020-10-09 23:13:27'),
(65, 'Authentication', 'Success to login user', 'Success to login user with phone number 6281218443187', 24, '2020-10-09 23:22:08', '2020-10-09 23:22:08'),
(66, 'Authentication', 'Success to create user', 'Success to create user with phone number 6282111190563', 25, '2020-10-10 01:59:19', '2020-10-10 01:59:19'),
(67, 'Authentication', 'Success to login user', 'Success to login user with phone number 6282213737535', 4, '2020-10-10 02:06:39', '2020-10-10 02:06:39'),
(68, 'Authentication', 'Success to login user', 'Success to login user with phone number 6282213737535', 4, '2020-10-10 02:15:38', '2020-10-10 02:15:38'),
(69, 'Authentication', 'Success to login user', 'Success to login user with phone number 6282119189690', 1, '2020-10-10 03:08:22', '2020-10-10 03:08:22'),
(70, 'Authentication', 'Success to create user', 'Success to create user with phone number 6282112530028', 26, '2020-10-10 04:17:08', '2020-10-10 04:17:08'),
(71, 'Authentication', 'Success to login user', 'Success to login user with phone number 6282213737535', 4, '2020-10-10 06:47:28', '2020-10-10 06:47:28'),
(72, 'Authentication', 'Success to update profile', 'Success to update profile', 1, '2020-10-10 07:11:26', '2020-10-10 07:11:26'),
(73, 'Authentication', 'Success to login user', 'Success to login user with phone number 6282119189690', 1, '2020-10-10 07:15:06', '2020-10-10 07:15:06'),
(74, 'Authentication', 'Success to login user', 'Success to login user with phone number 6282213737535', 4, '2020-10-10 07:31:04', '2020-10-10 07:31:04'),
(75, 'Authentication', 'Success to update profile', 'Success to update profile', 1, '2020-10-10 07:32:31', '2020-10-10 07:32:31'),
(76, 'Authentication', 'Success to create user', 'Success to create user with phone number 6281246625720', 27, '2020-10-10 07:32:40', '2020-10-10 07:32:40'),
(77, 'Authentication', 'Success to create user', 'Success to create user with phone number 6289513526082', 28, '2020-10-10 07:44:27', '2020-10-10 07:44:27'),
(78, 'Authentication', 'Success to create user', 'Success to create user with phone number 6281319249504', 29, '2020-10-10 07:44:45', '2020-10-10 07:44:45'),
(79, 'Authentication', 'Success to create user', 'Success to create user with phone number 6282282539235', 30, '2020-10-10 07:50:41', '2020-10-10 07:50:41'),
(80, 'Authentication', 'Success to create user', 'Success to create user with phone number 6280870947', 31, '2020-10-10 07:58:49', '2020-10-10 07:58:49'),
(81, 'Authentication', 'Success to create user', 'Success to create user with phone number 6287880660607', 32, '2020-10-10 08:00:08', '2020-10-10 08:00:08'),
(82, 'Authentication', 'Success to create user', 'Success to create user with phone number 6281386009376', 33, '2020-10-10 08:00:37', '2020-10-10 08:00:37'),
(83, 'Authentication', 'Success to create user', 'Success to create user with phone number 6281281082095', 34, '2020-10-10 08:03:00', '2020-10-10 08:03:00'),
(84, 'Authentication', 'Success to create user', 'Success to create user with phone number 62878833804884', 35, '2020-10-10 08:04:49', '2020-10-10 08:04:49'),
(85, 'Authentication', 'Success to create user', 'Success to create user with phone number 6281908786469', 36, '2020-10-10 08:04:54', '2020-10-10 08:04:54'),
(86, 'Authentication', 'Success to login user', 'Success to login user with phone number 6280870947', 31, '2020-10-10 08:05:22', '2020-10-10 08:05:22'),
(87, 'Authentication', 'Success to create user', 'Success to create user with phone number 6287871374206', 37, '2020-10-10 08:05:41', '2020-10-10 08:05:41'),
(88, 'Authentication', 'Success to login user', 'Success to login user with phone number 6282282539235', 30, '2020-10-10 08:17:24', '2020-10-10 08:17:24'),
(89, 'Authentication', 'Success to create user', 'Success to create user with phone number 6281287179153', 38, '2020-10-10 08:18:18', '2020-10-10 08:18:18'),
(90, 'Authentication', 'Success to create user', 'Success to create user with phone number 6281218634567', 39, '2020-10-10 08:22:58', '2020-10-10 08:22:58'),
(91, 'Authentication', 'Success to create user', 'Success to create user with phone number 6283898541475', 40, '2020-10-10 09:03:28', '2020-10-10 09:03:28'),
(92, 'Authentication', 'Success to login user', 'Success to login user with phone number 6283898541475', 40, '2020-10-10 09:07:02', '2020-10-10 09:07:02'),
(93, 'Authentication', 'Success to create user', 'Success to create user with phone number 6285697582474', 41, '2020-10-10 09:12:05', '2020-10-10 09:12:05'),
(94, 'Authentication', 'Success to update profile', 'Success to update profile', 1, '2020-10-10 09:48:06', '2020-10-10 09:48:06'),
(95, 'Authentication', 'Success to login user', 'Success to login user with phone number 6282213737535', 4, '2020-10-10 10:28:18', '2020-10-10 10:28:18'),
(96, 'Authentication', 'Success to create user', 'Success to create user with phone number 6285311738907', 42, '2020-10-10 13:52:25', '2020-10-10 13:52:25'),
(97, 'Authentication', 'Success to login user', 'Success to login user with phone number 6285311738907', 42, '2020-10-10 14:06:32', '2020-10-10 14:06:32');

-- --------------------------------------------------------

--
-- Struktur dari tabel `m_pulo_gebang`
--

CREATE TABLE `m_pulo_gebang` (
  `id` int(10) UNSIGNED NOT NULL,
  `username` text NOT NULL,
  `password` varchar(255) NOT NULL,
  `api_token` varchar(255) NOT NULL,
  `created_at` varchar(20) DEFAULT NULL,
  `updated_at` varchar(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `m_pulo_gebang`
--

INSERT INTO `m_pulo_gebang` (`id`, `username`, `password`, `api_token`, `created_at`, `updated_at`) VALUES
(1, 'tron', 's01_)92klMNopq', 'ca1000a3a04f36130fd73d47b362578d', '2020-08-17 17:10:29', '2020-08-17 17:10:29'),
(2, 'tron', 's01_)92klMNopq', '7892c44a302915aeb764e547620550d5', '2020-08-18 11:12:53', '2020-08-18 11:12:53'),
(3, 'tron', '1*7821Ubp@klmbb$', '7892c44a302915aeb764e547620550d5', '2020-10-07 04:38:04', '2020-10-07 04:38:04'),
(4, 'tron', '1*7821Ubp@klmbb$', 'a93b3a5df6c311810abeaf25fa99a0df', '2020-10-07 04:39:54', '2020-10-07 04:39:54');

-- --------------------------------------------------------

--
-- Struktur dari tabel `m_tron_wallet`
--

CREATE TABLE `m_tron_wallet` (
  `id` int(10) UNSIGNED NOT NULL,
  `username` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `api_token` text NOT NULL,
  `created_at` varchar(20) DEFAULT NULL,
  `updated_at` varchar(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `m_tron_wallet`
--

INSERT INTO `m_tron_wallet` (`id`, `username`, `password`, `api_token`, `created_at`, `updated_at`) VALUES
(1, 'jaketbus@tronwallet.id', 'jaketbus', 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6NCwiaWF0IjoxNTk3NjQ4MDMyfQ.OO5QIeKqjv-9UZhjbohJwLU8bE_-zpvbGsJke8Mu6Uc', '2020-08-17 17:11:30', '2020-08-17 17:11:30');

-- --------------------------------------------------------

--
-- Struktur dari tabel `m_users`
--

CREATE TABLE `m_users` (
  `id` int(10) UNSIGNED NOT NULL,
  `first_name` varchar(255) NOT NULL,
  `last_name` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `phone_number` varchar(20) NOT NULL,
  `created_at` varchar(20) DEFAULT NULL,
  `updated_at` varchar(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `m_users`
--

INSERT INTO `m_users` (`id`, `first_name`, `last_name`, `email`, `password`, `phone_number`, `created_at`, `updated_at`) VALUES
(1, 'Andy', 'Maulana', 'goingtoprofandy@gmail.com', '91e48601ee115e22d16077aaa030ff52', '6282119189690', '2020-08-18 09:46:25', '2020-10-10 09:48:06'),
(2, 'Yunan', 'Mubarak Ramadani', 'yunanmr@yahoo.com', '470793e036b9db245ac460dc89b15913', '6289666585006', '2020-08-18 15:06:14', '2020-08-18 15:06:14'),
(3, 'Taufan Bayu A.W', 'Taufan Bayu A.W', 'ph.goru@gmail.com', '8fd75c8763d3d628cd0eba493afc566b', '62895324806198', '2020-08-19 00:07:02', '2020-09-04 06:47:09'),
(4, 'MUTINUL', 'HAKIM', 'hakimceo.btc@gmail.com', 'e10adc3949ba59abbe56e057f20f883e', '6282213737535', '2020-08-19 14:05:17', '2020-08-31 07:39:26'),
(5, 'taufan bayu', 'a. w. ', 'taufan.muse@gmail.com', '8fd75c8763d3d628cd0eba493afc566b', '62895324806198', '2020-09-04 03:08:25', '2020-09-04 03:08:25'),
(6, 'taufan', 'bayu', 'taufan_km@yahoo.co.id', '8fd75c8763d3d628cd0eba493afc566b', '6289502100693', '2020-09-07 02:45:02', '2020-09-07 02:45:02'),
(7, 'Bernard', 'Pasaribu', 'bernadocto1510@gmail.com', 'e10adc3949ba59abbe56e057f20f883e', '6281298270999', '2020-10-06 07:44:45', '2020-10-06 07:44:45'),
(8, 'Sandy', 'Wardhana', 'sandy.wardhana@tron.id', 'f3f2f1df7d4c516da8aedb4f062af045', '6281314630883', '2020-10-07 06:24:49', '2020-10-07 06:24:49'),
(9, 'Junaedi', 'ST', 'junaedi.aldian@gmail.com', 'af0068d71dea47684d6c815211232703', '6287888882114', '2020-10-08 07:43:15', '2020-10-08 07:43:15'),
(10, 'Wahyu', 'Hidayat', 'wandi_180106@yahoo.co.id', 'bfcf45233f5d2a64f71948386dd0ea46', '6281806994125', '2020-10-08 07:47:29', '2020-10-08 07:47:29'),
(11, 'AFIF', 'MUHROJI', 'afifmuhroji@gmail.com', 'c578e3131e0cc407d15f7589d4a7dfc2', '6281381153586', '2020-10-08 07:51:21', '2020-10-08 07:51:21'),
(12, 'Heryanto', 'Heryanto', 'herryanto@tron.id', '89f04359833ce3a29918ef76ec273027', '6285716461494', '2020-10-08 08:20:07', '2020-10-08 08:20:07'),
(13, 'Gansi', 'Maghfirahmi', 'gansimgh@gmail.com', '745e6c98e032a5178dd41eb033489f38', '6287884514498', '2020-10-08 11:37:24', '2020-10-08 11:37:24'),
(14, 'Cahya ', 'berlian', 'muhammadtricahyaberlian@yahoo.com', '8af9a71e2d2880982a1d281b72a05307', '6281280642747', '2020-10-08 14:23:02', '2020-10-08 14:23:02'),
(15, 'M. Sholihin ', 'Lantoni Abdi', 'lantoni.abdi@gmail.com', '1b9cd90e878afd75b425d0de266767f1', '6281510744757', '2020-10-09 04:00:05', '2020-10-09 04:00:05'),
(16, 'Irvan', 'Mustofa', 'irvanmustofa55@gmail.com', 'fa979efb380bccc9a30635e33070951c', '6281211238713', '2020-10-09 04:11:36', '2020-10-09 04:11:36'),
(17, 'saka putra', 'perdana', 'danasaka13@gmail.com', 'a30c280ecb39ec2953d0b6b97d1367d7', '6281290388717', '2020-10-09 04:13:18', '2020-10-09 04:13:18'),
(18, 'Irvan', 'Mustofa', 'irvanmustofa@gumriningsentosa.com', 'fa979efb380bccc9a30635e33070951c', '6281287779935', '2020-10-09 04:48:16', '2020-10-09 04:48:16'),
(19, 'miftach', 'magfira', 'miftehs05@gmail.com', 'e10adc3949ba59abbe56e057f20f883e', '62895324999348', '2020-10-09 05:41:05', '2020-10-09 05:41:05'),
(20, 'eddy', 'purnama siregar', 'eddysiregar35@gmail.com', 'bc378bb1c2978364c61f650307186ede', '6289602686479', '2020-10-09 10:47:05', '2020-10-09 10:49:48'),
(21, 'Siti', 'Nur Nahri', 'riri_nurnahri@yahoo.com', '5ee30e4c39085505e3a676f53b2acec4', '6287784599444', '2020-10-09 10:55:39', '2020-10-09 10:55:39'),
(22, 'Bimo ', 'Fajar Pamungkas', 'bimofajarp@gmail.com', '6832f70541062337827e05c4893b5e6d', '6285211970811', '2020-10-09 10:56:23', '2020-10-09 10:56:23'),
(23, 'nasib', 'Simanjuntak', 'nasibsimanjuntak47@gmail.com', '85c8a821ddbc1ff00f9680b0a8a87c6f', '6281316681453', '2020-10-09 10:57:21', '2020-10-09 10:57:21'),
(24, 'J B', 'Gebang', 'herryantoexpress1@gmail.com', '23b66ce5e638312818b8df16b7f37b1c', '6281218443187', '2020-10-09 13:43:22', '2020-10-09 13:43:22'),
(25, 'Toto Susanto ', 'Sugiarto', 'toto.susanto2672@gmail.com', '8db37207e89387491ae3d97a18ed26e2', '6282111190563', '2020-10-10 01:59:19', '2020-10-10 01:59:19'),
(26, 'gisman', 'sugisman', 'sugisman23@gmail.com', 'e10adc3949ba59abbe56e057f20f883e', '6282112530028', '2020-10-10 04:17:08', '2020-10-10 04:17:08'),
(27, 'sumantri', '. ', 'omansumantri5929@gmail.com', 'd54936249a9599dc45ed899135e583c0', '6281246625720', '2020-10-10 07:32:40', '2020-10-10 07:32:40'),
(28, 'Adi', 'Ramadhan', 'rdhan0148@gmail.com', '9cad41afe0dfba7c71b8e2d20a0e5ade', '6289513526082', '2020-10-10 07:44:27', '2020-10-10 07:44:27'),
(29, 'Ricky ', 'Anadi', 'rickyanadi@gmail.com', 'c2357106936a248bf9a71122a0d454b9', '6281319249504', '2020-10-10 07:44:45', '2020-10-10 07:44:45'),
(30, 'Taufik ', 'Hirawan', 'taufikhirawan2020@gmail.com', '7cc03dd451dbe79b973840a7f7ec29f6', '6282282539235', '2020-10-10 07:50:41', '2020-10-10 07:50:41'),
(31, 'Robert', 'Edward', 'robertbusway@gmail.com', '47a9c0c21951325860228a0c1136c740', '6280870947', '2020-10-10 07:58:49', '2020-10-10 07:58:49'),
(32, 'massdes', 'arouffy', 'arouffy1@gmail.com', '06b3836d6e68c0e4fffbdba9d46d8696', '6287880660607', '2020-10-10 08:00:08', '2020-10-10 08:00:08'),
(33, 'Moh Faisol', 'Moh Faisol', 'mohfaisol1974@gmail.com', 'ef73781effc5774100f87fe2f437a435', '6281386009376', '2020-10-10 08:00:37', '2020-10-10 08:00:37'),
(34, 'Adji', 'Kusambarto', 'akusambarto@yahoo.com', 'df4ad0c9625dbe42114234fd2711eba6', '6281281082095', '2020-10-10 08:03:00', '2020-10-10 08:03:00'),
(35, 'noviesa f', 'Pinem', 'esa_pinem@yahoo.com', 'b2b7062d467b876b3b066a7fb30a937a', '62878833804884', '2020-10-10 08:04:49', '2020-10-10 08:04:49'),
(36, 'Achmad Indra', 'Seftian', 'seftianindra@gmail.com', 'ba96fccd4f8ab3edbc1bb20906b29d45', '6281908786469', '2020-10-10 08:04:54', '2020-10-10 08:04:54'),
(37, 'Taufik', 'Soleh', 'taufikhope@gmail.com', 'e10adc3949ba59abbe56e057f20f883e', '6287871374206', '2020-10-10 08:05:41', '2020-10-10 08:05:41'),
(38, 'chris', 'tianto', 'Christianto.atd@gmail.com', '384ac1de2e769dc0d98745e0a16c4e7b', '6281287179153', '2020-10-10 08:18:18', '2020-10-10 08:18:18'),
(39, 'edy', 'sufaat', 'edysufaat@yahoo.com', 'd5471bba504e65dd4969669c7301cbd1', '6281218634567', '2020-10-10 08:22:58', '2020-10-10 08:22:58'),
(40, 'Indra mardiyanto', 'Indra mardiyanto', 'mardiyantoindra60@gmail.com', '84e24f203dff3a2ce631fcb2fd39af66', '6283898541475', '2020-10-10 09:03:28', '2020-10-10 09:03:28'),
(41, 'Ricky', 'Noviarman', 'rickykalibata15@gmail.com', '48d9e69a7bebf6b54ca47f0c9a07e0f3', '6285697582474', '2020-10-10 09:12:05', '2020-10-10 09:12:05'),
(42, 'M Sadat', 'Kurniawan', 'Fa2sadrin@yahoo.co.id', 'c9472ca5129911524b6e8464b5fa546e', '6285311738907', '2020-10-10 13:52:25', '2020-10-10 13:52:25');

-- --------------------------------------------------------

--
-- Struktur dari tabel `u_user`
--

CREATE TABLE `u_user` (
  `id` int(10) UNSIGNED NOT NULL,
  `id_face` int(10) UNSIGNED NOT NULL,
  `id_m_users` int(10) UNSIGNED NOT NULL,
  `level` tinyint(1) NOT NULL,
  `token` varchar(255) NOT NULL,
  `created_at` varchar(20) DEFAULT NULL,
  `updated_at` varchar(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `u_user`
--

INSERT INTO `u_user` (`id`, `id_face`, `id_m_users`, `level`, `token`, `created_at`, `updated_at`) VALUES
(1, 25, 1, 2, '59596e38c02b9e7a1fd0d6d7e79ccbb2', '2020-08-18 09:46:25', '2020-10-10 09:48:06'),
(2, 1, 2, 1, 'c7b4752188c951c04ed1733980807a41', '2020-08-18 15:06:14', '2020-08-18 15:06:14'),
(3, 1, 3, 1, 'a6fa42b61996f29f3e8c3a1e0d8347fb', '2020-08-19 00:07:02', '2020-09-04 06:47:09'),
(4, 27, 4, 1, 'eb55de9f4e120e13849612226c92d359', '2020-08-19 14:05:17', '2020-10-10 10:29:59'),
(5, 1, 5, 1, 'd2c71a709cd00582d7c3132faca0ed98', '2020-09-04 03:08:25', '2020-09-04 03:08:25'),
(6, 1, 6, 1, 'e94173a41d410e4d7b1641f9aab1d6f8', '2020-09-07 02:45:02', '2020-09-07 02:45:02'),
(7, 1, 7, 1, 'e0edd591f1bc1989fa1130cc0d405785', '2020-10-06 07:44:45', '2020-10-06 07:44:45'),
(8, 1, 8, 1, 'cbca11058db24bc2786e675dffc4bc76', '2020-10-07 06:24:49', '2020-10-07 06:24:49'),
(9, 1, 9, 1, 'd1bcb1404b3c09e1e7a4c8c742d090a7', '2020-10-08 07:43:15', '2020-10-08 07:43:15'),
(10, 1, 10, 1, '26b61efc72eaf66e3bb31ad125f6b136', '2020-10-08 07:47:29', '2020-10-08 07:47:29'),
(11, 1, 11, 1, '2afe0e0ee207d2319d32a1e88b9ae302', '2020-10-08 07:51:21', '2020-10-08 07:51:21'),
(12, 1, 12, 1, '6660c70ff3ed7a35e931fff9dc57f502', '2020-10-08 08:20:07', '2020-10-08 08:20:07'),
(13, 1, 13, 1, 'aa65cef70841610a744869752769b87d', '2020-10-08 11:37:24', '2020-10-08 11:37:24'),
(14, 1, 14, 1, '10904bc40b901307d71d9d67559370f7', '2020-10-08 14:23:02', '2020-10-08 14:23:02'),
(15, 1, 15, 1, '3e675b5e1a9485bb58d4ba9dabd3164b', '2020-10-09 04:00:05', '2020-10-09 04:00:05'),
(16, 1, 16, 1, 'be40d7d55b4d8f6566cd32f4152781f5', '2020-10-09 04:11:36', '2020-10-09 04:11:36'),
(17, 1, 17, 1, '9222c1f7f9698fc32503ad85c741133d', '2020-10-09 04:13:18', '2020-10-09 04:13:18'),
(18, 1, 18, 1, '95613fa6243bb34d8f3aef5135f66bf8', '2020-10-09 04:48:16', '2020-10-09 04:48:16'),
(19, 1, 19, 1, '6296ee22b78dede25603946fdb07a23c', '2020-10-09 05:41:05', '2020-10-09 05:41:05'),
(20, 1, 20, 1, '7306b74d1f8aac569176d16f368343f0', '2020-10-09 10:47:05', '2020-10-09 10:49:48'),
(21, 1, 21, 1, 'c0d55dc015c4ca84622e93456899c067', '2020-10-09 10:55:39', '2020-10-09 10:55:39'),
(22, 1, 22, 1, '9c53496f28bf2296e89627a3ed3bfd23', '2020-10-09 10:56:23', '2020-10-09 10:56:23'),
(23, 1, 23, 1, '3a2c4612d7a19bd5149fb9e7dec0fc9e', '2020-10-09 10:57:21', '2020-10-09 10:57:21'),
(24, 1, 24, 1, '9bd2c2d838b0cd0a228f529bd27ac7f2', '2020-10-09 13:43:22', '2020-10-09 13:43:22'),
(25, 1, 25, 1, 'ed90842f374e5ebde1ea13b172f9e11d', '2020-10-10 01:59:19', '2020-10-10 01:59:19'),
(26, 1, 26, 1, '9bdee044281aacfb8a69da46ea7d7ced', '2020-10-10 04:17:08', '2020-10-10 04:17:08'),
(27, 1, 27, 1, '8cbabccab1f43c0dc16d61659c9da041', '2020-10-10 07:32:40', '2020-10-10 07:32:40'),
(28, 1, 28, 1, '672a4fa1b3234456e9d9a99b7428043b', '2020-10-10 07:44:27', '2020-10-10 07:44:27'),
(29, 1, 29, 1, '0bb1d4f2f1d836dc075f191266ecd7f6', '2020-10-10 07:44:45', '2020-10-10 07:44:45'),
(30, 1, 30, 1, '52aa3ff53d9bf77cd9d054b0e2d3d6e9', '2020-10-10 07:50:41', '2020-10-10 07:50:41'),
(31, 1, 31, 1, '856d053b9b21c6e1a648a221e2b1f7d5', '2020-10-10 07:58:49', '2020-10-10 07:58:49'),
(32, 1, 32, 1, 'e4d036a11d934d862feeb1e3e4a69b0b', '2020-10-10 08:00:08', '2020-10-10 08:00:08'),
(33, 1, 33, 1, 'c6e97bbbf84b1560395a0f6f855edfda', '2020-10-10 08:00:37', '2020-10-10 08:00:37'),
(34, 1, 34, 1, '2b3fa323e5d7df5b8615c285a60d93b1', '2020-10-10 08:03:00', '2020-10-10 08:03:00'),
(35, 1, 35, 1, '12a9c1410b623cd0965d8589ba84dfb7', '2020-10-10 08:04:49', '2020-10-10 08:04:49'),
(36, 1, 36, 1, '2f90d51bc429aa66f835dd2aa8802f2e', '2020-10-10 08:04:54', '2020-10-10 08:04:54'),
(37, 21, 37, 1, '179ca36b34e8eb7715f8dce14a645b05', '2020-10-10 08:05:41', '2020-10-10 08:49:15'),
(38, 1, 38, 1, 'feffccc65223fc251e19bc7a9fa1abb5', '2020-10-10 08:18:18', '2020-10-10 08:18:18'),
(39, 1, 39, 1, '2a94e3bd7da727c3ca647cc8c57e9244', '2020-10-10 08:22:58', '2020-10-10 08:22:58'),
(40, 1, 40, 1, 'aa1943a7addd633c9633524503c44da8', '2020-10-10 09:03:28', '2020-10-10 09:03:28'),
(41, 1, 41, 1, '905afe28779103c864fb80511926fd62', '2020-10-10 09:12:05', '2020-10-10 09:12:05'),
(42, 1, 42, 1, '11ce21673b69bc1a470908e69d1aa5e6', '2020-10-10 13:52:25', '2020-10-10 13:52:25');

-- --------------------------------------------------------

--
-- Struktur dari tabel `u_user_order`
--

CREATE TABLE `u_user_order` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `count_of_passenger` int(11) DEFAULT 1,
  `is_go_back` tinyint(1) DEFAULT 0,
  `status` tinyint(1) DEFAULT 0,
  `id_m_users` int(10) UNSIGNED NOT NULL,
  `created_at` varchar(20) DEFAULT NULL,
  `updated_at` varchar(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `u_user_order`
--

INSERT INTO `u_user_order` (`id`, `count_of_passenger`, `is_go_back`, `status`, `id_m_users`, `created_at`, `updated_at`) VALUES
(1, 1, 0, 0, 1, NULL, NULL),
(2, 1, 0, 0, 1, NULL, NULL),
(3, 1, 0, 0, 1, NULL, NULL),
(4, 1, 0, 0, 1, NULL, NULL),
(5, 1, 0, 0, 1, NULL, NULL),
(6, 1, 0, 0, 1, NULL, NULL),
(7, 1, 0, 0, 4, NULL, NULL),
(8, 1, 0, 0, 4, NULL, NULL),
(9, 1, 0, 0, 4, NULL, NULL),
(10, 1, 0, 0, 4, NULL, NULL),
(11, 1, 0, 0, 3, NULL, NULL),
(12, 1, 0, 0, 3, NULL, NULL),
(13, 1, 0, 0, 3, NULL, NULL),
(14, 1, 0, 0, 3, NULL, NULL),
(15, 1, 0, 0, 3, NULL, NULL),
(16, 2, 0, 0, 4, NULL, NULL),
(17, 1, 0, 0, 4, NULL, NULL),
(18, 1, 0, 0, 4, NULL, NULL),
(19, 1, 0, 0, 3, NULL, NULL),
(20, 1, 0, 0, 3, NULL, NULL),
(21, 1, 0, 0, 3, NULL, NULL),
(22, 1, 0, 0, 3, NULL, NULL),
(23, 1, 0, 0, 3, NULL, NULL),
(24, 1, 0, 0, 3, NULL, NULL),
(25, 1, 0, 0, 3, NULL, NULL),
(26, 1, 0, 0, 3, NULL, NULL),
(27, 1, 0, 0, 3, NULL, NULL),
(28, 1, 0, 0, 3, NULL, NULL),
(29, 1, 0, 0, 4, NULL, NULL),
(30, 1, 0, 0, 4, NULL, NULL),
(31, 1, 0, 0, 1, NULL, NULL),
(32, 1, 0, 0, 1, NULL, NULL),
(33, 1, 0, 0, 1, NULL, NULL),
(34, 1, 0, 0, 1, NULL, NULL),
(35, 1, 0, 0, 4, NULL, NULL),
(36, 1, 0, 0, 4, NULL, NULL),
(37, 1, 0, 0, 4, NULL, NULL),
(38, 1, 0, 0, 4, NULL, NULL),
(39, 1, 0, 0, 4, NULL, NULL),
(40, 1, 0, 0, 4, NULL, NULL),
(41, 1, 0, 0, 7, NULL, NULL),
(42, 1, 0, 0, 7, NULL, NULL),
(43, 1, 0, 0, 7, NULL, NULL),
(44, 1, 0, 0, 1, NULL, NULL),
(45, 1, 0, 0, 7, NULL, NULL),
(46, 1, 0, 0, 7, NULL, NULL),
(47, 1, 0, 0, 7, NULL, NULL),
(48, 1, 0, 0, 8, NULL, NULL),
(49, 1, 0, 0, 8, NULL, NULL),
(50, 1, 0, 0, 8, NULL, NULL),
(51, 1, 0, 0, 8, NULL, NULL),
(52, 1, 0, 0, 8, NULL, NULL),
(53, 1, 0, 0, 8, NULL, NULL),
(54, 1, 0, 0, 8, NULL, NULL),
(55, 1, 0, 0, 8, NULL, NULL),
(56, 1, 0, 0, 7, NULL, NULL),
(57, 1, 0, 0, 7, NULL, NULL),
(58, 1, 0, 0, 7, NULL, NULL),
(59, 1, 0, 0, 8, NULL, NULL),
(60, 1, 0, 0, 11, NULL, NULL),
(61, 1, 0, 0, 9, NULL, NULL),
(62, 1, 0, 0, 8, NULL, NULL),
(63, 1, 0, 0, 10, NULL, NULL),
(64, 1, 0, 0, 8, NULL, NULL),
(65, 1, 0, 0, 8, NULL, NULL),
(66, 1, 0, 0, 13, NULL, NULL),
(67, 1, 0, 0, 13, NULL, NULL),
(68, 1, 0, 0, 13, NULL, NULL),
(69, 1, 0, 0, 14, NULL, NULL),
(70, 1, 0, 0, 14, NULL, NULL),
(71, 1, 0, 0, 12, NULL, NULL),
(72, 1, 0, 0, 12, NULL, NULL),
(73, 1, 0, 0, 12, NULL, NULL),
(74, 1, 0, 0, 12, NULL, NULL),
(75, 1, 0, 0, 12, NULL, NULL),
(76, 1, 0, 0, 12, NULL, NULL),
(77, 1, 0, 0, 7, NULL, NULL),
(78, 1, 0, 0, 12, NULL, NULL),
(79, 1, 0, 0, 7, NULL, NULL),
(80, 1, 0, 0, 7, NULL, NULL),
(81, 1, 0, 0, 7, NULL, NULL),
(82, 1, 0, 0, 15, NULL, NULL),
(83, 1, 0, 0, 15, NULL, NULL),
(84, 1, 0, 0, 15, NULL, NULL),
(85, 1, 0, 0, 15, NULL, NULL),
(86, 1, 0, 0, 15, NULL, NULL),
(87, 1, 0, 0, 15, NULL, NULL),
(88, 1, 0, 0, 16, NULL, NULL),
(89, 1, 0, 0, 13, NULL, NULL),
(90, 1, 0, 0, 13, NULL, NULL),
(91, 1, 0, 0, 13, NULL, NULL),
(92, 1, 0, 0, 19, NULL, NULL),
(93, 1, 0, 0, 19, NULL, NULL),
(94, 1, 0, 0, 19, NULL, NULL),
(95, 1, 0, 0, 1, NULL, NULL),
(96, 1, 0, 0, 18, NULL, NULL),
(97, 1, 0, 0, 15, NULL, NULL),
(98, 1, 0, 0, 15, NULL, NULL),
(99, 1, 0, 0, 15, NULL, NULL),
(100, 1, 0, 0, 15, NULL, NULL),
(101, 1, 0, 0, 15, NULL, NULL),
(102, 1, 0, 0, 15, NULL, NULL),
(103, 1, 0, 0, 15, NULL, NULL),
(104, 1, 0, 0, 15, NULL, NULL),
(105, 1, 0, 0, 13, NULL, NULL),
(106, 1, 0, 0, 20, NULL, NULL),
(107, 1, 0, 0, 20, NULL, NULL),
(108, 1, 0, 0, 20, NULL, NULL),
(109, 1, 0, 0, 20, NULL, NULL),
(110, 1, 0, 0, 20, NULL, NULL),
(111, 1, 0, 0, 21, NULL, NULL),
(112, 1, 0, 0, 23, NULL, NULL),
(113, 1, 0, 0, 24, NULL, NULL),
(114, 1, 0, 0, 24, NULL, NULL),
(115, 1, 0, 0, 24, NULL, NULL),
(116, 1, 0, 0, 7, NULL, NULL),
(117, 1, 0, 0, 7, NULL, NULL),
(118, 1, 0, 0, 7, NULL, NULL),
(119, 1, 0, 0, 8, NULL, NULL),
(120, 1, 0, 0, 4, NULL, NULL),
(121, 1, 0, 0, 8, NULL, NULL),
(122, 2, 0, 0, 8, NULL, NULL),
(123, 1, 0, 0, 12, NULL, NULL),
(124, 1, 0, 0, 25, NULL, NULL),
(125, 2, 0, 0, 12, NULL, NULL),
(126, 1, 0, 0, 12, NULL, NULL),
(127, 1, 0, 0, 12, NULL, NULL),
(128, 1, 0, 0, 7, NULL, NULL),
(129, 1, 0, 0, 24, NULL, NULL),
(130, 1, 0, 0, 7, NULL, NULL),
(131, 1, 0, 0, 24, NULL, NULL),
(132, 1, 0, 0, 22, NULL, NULL),
(133, 1, 0, 0, 11, NULL, NULL),
(134, 1, 0, 0, 22, NULL, NULL),
(135, 1, 0, 0, 22, NULL, NULL),
(136, 1, 0, 0, 26, NULL, NULL),
(137, 1, 0, 0, 11, NULL, NULL),
(138, 1, 0, 0, 11, NULL, NULL),
(139, 1, 0, 0, 10, NULL, NULL),
(140, 1, 0, 0, 23, NULL, NULL),
(141, 1, 0, 0, 23, NULL, NULL),
(142, 1, 0, 0, 23, NULL, NULL),
(143, 1, 0, 0, 23, NULL, NULL),
(144, 1, 0, 0, 8, NULL, NULL),
(145, 2, 0, 0, 4, NULL, NULL),
(146, 1, 0, 0, 22, NULL, NULL),
(147, 1, 0, 0, 13, NULL, NULL),
(148, 1, 0, 0, 22, NULL, NULL),
(149, 1, 0, 0, 22, NULL, NULL),
(150, 1, 0, 0, 13, NULL, NULL),
(151, 2, 0, 0, 22, NULL, NULL),
(152, 1, 0, 0, 13, NULL, NULL),
(153, 1, 0, 0, 22, NULL, NULL),
(154, 1, 0, 0, 24, NULL, NULL),
(155, 1, 0, 0, 12, NULL, NULL),
(156, 1, 0, 0, 24, NULL, NULL),
(157, 1, 0, 0, 4, NULL, NULL),
(158, 1, 0, 0, 4, NULL, NULL),
(159, 1, 0, 0, 4, NULL, NULL),
(160, 1, 0, 0, 4, NULL, NULL),
(161, 1, 0, 0, 24, NULL, NULL),
(162, 1, 0, 0, 7, NULL, NULL),
(163, 1, 0, 0, 22, NULL, NULL),
(164, 1, 0, 0, 7, NULL, NULL),
(165, 1, 0, 0, 29, NULL, NULL),
(166, 2, 0, 0, 29, NULL, NULL),
(167, 1, 0, 0, 35, NULL, NULL),
(168, 1, 0, 0, 35, NULL, NULL),
(169, 1, 0, 0, 35, NULL, NULL),
(170, 1, 0, 0, 35, NULL, NULL),
(171, 1, 0, 0, 32, NULL, NULL),
(172, 1, 0, 0, 35, NULL, NULL),
(173, 1, 0, 0, 35, NULL, NULL),
(174, 1, 0, 0, 35, NULL, NULL),
(175, 1, 0, 0, 35, NULL, NULL),
(176, 1, 0, 0, 33, NULL, NULL),
(177, 1, 0, 0, 35, NULL, NULL),
(178, 1, 0, 0, 34, NULL, NULL),
(179, 1, 0, 0, 31, NULL, NULL),
(180, 1, 0, 0, 34, NULL, NULL),
(181, 1, 0, 0, 34, NULL, NULL),
(182, 1, 0, 0, 34, NULL, NULL),
(183, 1, 0, 0, 31, NULL, NULL),
(184, 1, 0, 0, 38, NULL, NULL),
(185, 1, 0, 0, 34, NULL, NULL),
(186, 1, 0, 0, 38, NULL, NULL),
(187, 1, 0, 0, 34, NULL, NULL),
(188, 1, 0, 0, 39, NULL, NULL),
(189, 1, 0, 0, 39, NULL, NULL),
(190, 1, 0, 0, 39, NULL, NULL),
(191, 1, 0, 0, 39, NULL, NULL),
(192, 1, 0, 0, 34, NULL, NULL),
(193, 1, 0, 0, 39, NULL, NULL),
(194, 1, 0, 0, 11, NULL, NULL),
(195, 1, 0, 0, 32, NULL, NULL),
(196, 1, 0, 0, 40, NULL, NULL),
(197, 1, 0, 0, 40, NULL, NULL),
(198, 1, 0, 0, 42, NULL, NULL),
(199, 1, 0, 0, 42, NULL, NULL),
(200, 1, 0, 0, 42, NULL, NULL),
(201, 1, 0, 0, 42, NULL, NULL),
(202, 1, 0, 0, 42, NULL, NULL),
(203, 1, 0, 0, 42, NULL, NULL),
(204, 1, 0, 0, 42, NULL, NULL);

-- --------------------------------------------------------

--
-- Struktur dari tabel `u_user_order_rd`
--

CREATE TABLE `u_user_order_rd` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `id_u_user_order` bigint(20) UNSIGNED NOT NULL,
  `id_route` varchar(20) NOT NULL,
  `province` varchar(100) NOT NULL,
  `city` varchar(100) NOT NULL,
  `region` varchar(100) NOT NULL,
  `created_at` varchar(20) DEFAULT NULL,
  `updated_at` varchar(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `u_user_order_rd`
--

INSERT INTO `u_user_order_rd` (`id`, `id_u_user_order`, `id_route`, `province`, `city`, `region`, `created_at`, `updated_at`) VALUES
(1, 1, '41', 'Jawa Timur', 'Surabaya', 'Jawa', NULL, NULL),
(2, 2, '41', 'Jawa Timur', 'Surabaya', 'Jawa', NULL, NULL),
(3, 3, '41', 'Jawa Timur', 'Surabaya', 'Jawa', NULL, NULL),
(4, 4, '41', 'Jawa Timur', 'Surabaya', 'Jawa', NULL, NULL),
(5, 5, '41', 'Jawa Timur', 'Surabaya', 'Jawa', NULL, NULL),
(6, 6, '41', 'Jawa Timur', 'Surabaya', 'Jawa', NULL, NULL),
(7, 7, '34', 'Jawa Tengah', 'Semarang', 'Jawa', NULL, NULL),
(8, 8, '41', 'Jawa Timur', 'Surabaya', 'Jawa', NULL, NULL),
(9, 9, '41', 'Jawa Timur', 'Surabaya', 'Jawa', NULL, NULL),
(10, 10, '41', 'Jawa Timur', 'Surabaya', 'Jawa', NULL, NULL),
(11, 11, '418', 'Jawa Tengah', 'Pati', 'Jawa', NULL, NULL),
(12, 12, '418', 'Jawa Tengah', 'Pati', 'Jawa', NULL, NULL),
(13, 13, '2', 'Aceh', 'Langsa', 'Sumatera', NULL, NULL),
(14, 14, '32', 'Jawa Tengah', 'Pekalongan', 'Jawa', NULL, NULL),
(15, 15, '2', 'Aceh', 'Langsa', 'Sumatera', NULL, NULL),
(16, 16, '41', 'Jawa Timur', 'Surabaya', 'Jawa', NULL, NULL),
(17, 17, '34', 'Jawa Tengah', 'Semarang', 'Jawa', NULL, NULL),
(18, 18, '608', 'Jawa Tengah', 'Solo', 'Jawa', NULL, NULL),
(19, 19, '418', 'Jawa Tengah', 'Pati', 'Jawa', NULL, NULL),
(20, 20, '41', 'Jawa Timur', 'Surabaya', 'Jawa', NULL, NULL),
(21, 21, '608', 'Jawa Tengah', 'Solo', 'Jawa', NULL, NULL),
(22, 22, '608', 'Jawa Tengah', 'Solo', 'Jawa', NULL, NULL),
(23, 23, '608', 'Jawa Tengah', 'Solo', 'Jawa', NULL, NULL),
(24, 24, '608', 'Jawa Tengah', 'Solo', 'Jawa', NULL, NULL),
(25, 25, '608', 'Jawa Tengah', 'Solo', 'Jawa', NULL, NULL),
(26, 26, '608', 'Jawa Tengah', 'Solo', 'Jawa', NULL, NULL),
(27, 27, '608', 'Jawa Tengah', 'Solo', 'Jawa', NULL, NULL),
(28, 28, '608', 'Jawa Tengah', 'Solo', 'Jawa', NULL, NULL),
(29, 29, '608', 'Jawa Tengah', 'Solo', 'Jawa', NULL, NULL),
(30, 30, '608', 'Jawa Tengah', 'Solo', 'Jawa', NULL, NULL),
(31, 31, '41', 'Jawa Timur', 'Surabaya', 'Jawa', NULL, NULL),
(32, 32, '41', 'Jawa Timur', 'Surabaya', 'Jawa', NULL, NULL),
(33, 33, '41', 'Jawa Timur', 'Surabaya', 'Jawa', NULL, NULL),
(34, 34, '41', 'Jawa Timur', 'Surabaya', 'Jawa', NULL, NULL),
(35, 35, '41', 'Jawa Timur', 'Surabaya', 'Jawa', NULL, NULL),
(36, 36, '18', 'Jakarta', 'Jakarta Timur', 'Jawa', NULL, NULL),
(37, 37, '41', 'Jawa Timur', 'Surabaya', 'Jawa', NULL, NULL),
(38, 38, '643', 'Banten', 'Merak', 'Jawa', NULL, NULL),
(39, 39, '608', 'Jawa Tengah', 'Solo', 'Jawa', NULL, NULL),
(40, 40, '643', 'Banten', 'Merak', 'Jawa', NULL, NULL),
(41, 41, '643', 'Banten', 'Merak', 'Jawa', NULL, NULL),
(42, 42, '608', 'Jawa Tengah', 'Solo', 'Jawa', NULL, NULL),
(43, 43, '643', 'Banten', 'Merak', 'Jawa', NULL, NULL),
(44, 44, '608', 'Jawa Tengah', 'Solo', 'Jawa', NULL, NULL),
(45, 45, '34', 'Jawa Tengah', 'Semarang', 'Jawa', NULL, NULL),
(46, 46, '608', 'Jawa Tengah', 'Solo', 'Jawa', NULL, NULL),
(47, 47, '41', 'Jawa Timur', 'Surabaya', 'Jawa', NULL, NULL),
(48, 48, '643', 'Banten', 'Merak', 'Jawa', NULL, NULL),
(49, 49, '608', 'Jawa Tengah', 'Solo', 'Jawa', NULL, NULL),
(50, 50, '643', 'Banten', 'Merak', 'Jawa', NULL, NULL),
(51, 51, '643', 'Banten', 'Merak', 'Jawa', NULL, NULL),
(52, 52, '643', 'Banten', 'Merak', 'Jawa', NULL, NULL),
(53, 53, '24', 'Jawa Barat', 'Bogor', 'Jawa', NULL, NULL),
(54, 54, '53', 'Lampung', 'Bandar Lampung', 'Sumatra', NULL, NULL),
(55, 55, '643', 'Banten', 'Merak', 'Jawa', NULL, NULL),
(56, 56, '643', 'Banten', 'Merak', 'Jawa', NULL, NULL),
(57, 57, '643', 'Banten', 'Merak', 'Jawa', NULL, NULL),
(58, 58, '18', 'Jakarta', 'Jakarta Timur', 'Jawa', NULL, NULL),
(59, 59, '18', 'Jakarta', 'Jakarta Timur', 'Jawa', NULL, NULL),
(60, 60, '612', 'Jawa Tengah', 'Bumiayu', 'Jawa', NULL, NULL),
(61, 61, '715', 'Jawa Timur', 'Purabaya', 'Jawa', NULL, NULL),
(62, 62, '643', 'Banten', 'Merak', 'Jawa', NULL, NULL),
(63, 63, '643', 'Banten', 'Merak', 'Jawa', NULL, NULL),
(64, 64, '643', 'Banten', 'Merak', 'Jawa', NULL, NULL),
(65, 65, '643', 'Banten', 'Merak', 'Jawa', NULL, NULL),
(66, 66, '418', 'Jawa Tengah', 'Pati', 'Jawa', NULL, NULL),
(67, 67, '441', 'Jawa Barat', 'Kuningan', 'Jawa', NULL, NULL),
(68, 68, '26', 'Jawa Barat', 'Cirebon', 'Jawa', NULL, NULL),
(69, 69, '7', 'Bali', 'Denpasar', 'Bali dan Nusa Tenggara', NULL, NULL),
(70, 70, '643', 'Banten', 'Merak', 'Jawa', NULL, NULL),
(71, 71, '36', 'Jawa Tengah', 'Tegal', 'Jawa', NULL, NULL),
(72, 72, '643', 'Banten', 'Merak', 'Jawa', NULL, NULL),
(73, 73, '643', 'Banten', 'Merak', 'Jawa', NULL, NULL),
(74, 74, '22', 'Jawa Barat', 'Bandung', 'Jawa', NULL, NULL),
(75, 75, '34', 'Jawa Tengah', 'Semarang', 'Jawa', NULL, NULL),
(76, 76, '608', 'Jawa Tengah', 'Solo', 'Jawa', NULL, NULL),
(77, 77, '643', 'Banten', 'Merak', 'Jawa', NULL, NULL),
(78, 78, '643', 'Banten', 'Merak', 'Jawa', NULL, NULL),
(79, 79, '608', 'Jawa Tengah', 'Solo', 'Jawa', NULL, NULL),
(80, 80, '643', 'Banten', 'Merak', 'Jawa', NULL, NULL),
(81, 81, '643', 'Banten', 'Merak', 'Jawa', NULL, NULL),
(82, 82, '59', 'Nusa Tenggara Barat', 'Bima', 'Bali dan Nusa Tenggara', NULL, NULL),
(83, 83, '59', 'Nusa Tenggara Barat', 'Bima', 'Bali dan Nusa Tenggara', NULL, NULL),
(84, 84, '59', 'Nusa Tenggara Barat', 'Bima', 'Bali dan Nusa Tenggara', NULL, NULL),
(85, 85, '59', 'Nusa Tenggara Barat', 'Bima', 'Bali dan Nusa Tenggara', NULL, NULL),
(86, 86, '34', 'Jawa Tengah', 'Semarang', 'Jawa', NULL, NULL),
(87, 87, '608', 'Jawa Tengah', 'Solo', 'Jawa', NULL, NULL),
(88, 88, '643', 'Banten', 'Merak', 'Jawa', NULL, NULL),
(89, 89, '643', 'Banten', 'Merak', 'Jawa', NULL, NULL),
(90, 90, '643', 'Banten', 'Merak', 'Jawa', NULL, NULL),
(91, 91, '608', 'Jawa Tengah', 'Solo', 'Jawa', NULL, NULL),
(92, 92, '643', 'Banten', 'Merak', 'Jawa', NULL, NULL),
(93, 93, '643', 'Banten', 'Merak', 'Jawa', NULL, NULL),
(94, 94, '643', 'Banten', 'Merak', 'Jawa', NULL, NULL),
(95, 95, '608', 'Jawa Tengah', 'Solo', 'Jawa', NULL, NULL),
(96, 96, '608', 'Jawa Tengah', 'Solo', 'Jawa', NULL, NULL),
(97, 97, '643', 'Banten', 'Merak', 'Jawa', NULL, NULL),
(98, 98, '608', 'Jawa Tengah', 'Solo', 'Jawa', NULL, NULL),
(99, 99, '34', 'Jawa Tengah', 'Semarang', 'Jawa', NULL, NULL),
(100, 100, '608', 'Jawa Tengah', 'Solo', 'Jawa', NULL, NULL),
(101, 101, '608', 'Jawa Tengah', 'Solo', 'Jawa', NULL, NULL),
(102, 102, '643', 'Banten', 'Merak', 'Jawa', NULL, NULL),
(103, 103, '608', 'Jawa Tengah', 'Solo', 'Jawa', NULL, NULL),
(104, 104, '34', 'Jawa Tengah', 'Semarang', 'Jawa', NULL, NULL),
(105, 105, '18', 'Jakarta', 'Jakarta Timur', 'Jawa', NULL, NULL),
(106, 106, '84', 'Sumatera Utara', 'Medan', 'Sumatra', NULL, NULL),
(107, 107, '643', 'Banten', 'Merak', 'Jawa', NULL, NULL),
(108, 108, '34', 'Jawa Tengah', 'Semarang', 'Jawa', NULL, NULL),
(109, 109, '90', 'Yogyakarta', 'Yogyakarta', 'Jawa', NULL, NULL),
(110, 110, '608', 'Jawa Tengah', 'Solo', 'Jawa', NULL, NULL),
(111, 111, '608', 'Jawa Tengah', 'Solo', 'Jawa', NULL, NULL),
(112, 112, '84', 'Sumatera Utara', 'Medan', 'Sumatra', NULL, NULL),
(113, 113, '643', 'Banten', 'Merak', 'Jawa', NULL, NULL),
(114, 114, '608', 'Jawa Tengah', 'Solo', 'Jawa', NULL, NULL),
(115, 115, '34', 'Jawa Tengah', 'Semarang', 'Jawa', NULL, NULL),
(116, 116, '13', 'Bengkulu', 'Bengkulu', 'Sumatera', NULL, NULL),
(117, 117, '608', 'Jawa Tengah', 'Solo', 'Jawa', NULL, NULL),
(118, 118, '34', 'Jawa Tengah', 'Semarang', 'Jawa', NULL, NULL),
(119, 119, '41', 'Jawa Timur', 'Surabaya', 'Jawa', NULL, NULL),
(120, 120, '608', 'Jawa Tengah', 'Solo', 'Jawa', NULL, NULL),
(121, 121, '643', 'Banten', 'Merak', 'Jawa', NULL, NULL),
(122, 122, '643', 'Banten', 'Merak', 'Jawa', NULL, NULL),
(123, 123, '643', 'Banten', 'Merak', 'Jawa', NULL, NULL),
(124, 124, '22', 'Jawa Barat', 'Bandung', 'Jawa', NULL, NULL),
(125, 125, '643', 'Banten', 'Merak', 'Jawa', NULL, NULL),
(126, 126, '34', 'Jawa Tengah', 'Semarang', 'Jawa', NULL, NULL),
(127, 127, '608', 'Jawa Tengah', 'Solo', 'Jawa', NULL, NULL),
(128, 128, '34', 'Jawa Tengah', 'Semarang', 'Jawa', NULL, NULL),
(129, 129, '643', 'Banten', 'Merak', 'Jawa', NULL, NULL),
(130, 130, '608', 'Jawa Tengah', 'Solo', 'Jawa', NULL, NULL),
(131, 131, '643', 'Banten', 'Merak', 'Jawa', NULL, NULL),
(132, 132, '34', 'Jawa Tengah', 'Semarang', 'Jawa', NULL, NULL),
(133, 133, '643', 'Banten', 'Merak', 'Jawa', NULL, NULL),
(134, 134, '1', 'Aceh', 'Banda Aceh', 'Sumatera', NULL, NULL),
(135, 135, '643', 'Banten', 'Merak', 'Jawa', NULL, NULL),
(136, 136, '41', 'Jawa Timur', 'Surabaya', 'Jawa', NULL, NULL),
(137, 137, '34', 'Jawa Tengah', 'Semarang', 'Jawa', NULL, NULL),
(138, 138, '608', 'Jawa Tengah', 'Solo', 'Jawa', NULL, NULL),
(139, 139, '643', 'Banten', 'Merak', 'Jawa', NULL, NULL),
(140, 140, '32', 'Jawa Tengah', 'Pekalongan', 'Jawa', NULL, NULL),
(141, 141, '26', 'Jawa Barat', 'Cirebon', 'Jawa', NULL, NULL),
(142, 142, '608', 'Jawa Tengah', 'Solo', 'Jawa', NULL, NULL),
(143, 143, '608', 'Jawa Tengah', 'Solo', 'Jawa', NULL, NULL),
(144, 144, '643', 'Banten', 'Merak', 'Jawa', NULL, NULL),
(145, 145, '643', 'Banten', 'Merak', 'Jawa', NULL, NULL),
(146, 146, '35', 'Jawa Tengah', 'Surakarta', 'Jawa', NULL, NULL),
(147, 147, '608', 'Jawa Tengah', 'Solo', 'Jawa', NULL, NULL),
(148, 148, '608', 'Jawa Tengah', 'Solo', 'Jawa', NULL, NULL),
(149, 149, '41', 'Jawa Timur', 'Surabaya', 'Jawa', NULL, NULL),
(150, 150, '41', 'Jawa Timur', 'Surabaya', 'Jawa', NULL, NULL),
(151, 151, '41', 'Jawa Timur', 'Surabaya', 'Jawa', NULL, NULL),
(152, 152, '34', 'Jawa Tengah', 'Semarang', 'Jawa', NULL, NULL),
(153, 153, '608', 'Jawa Tengah', 'Solo', 'Jawa', NULL, NULL),
(154, 154, '643', 'Banten', 'Merak', 'Jawa', NULL, NULL),
(155, 155, '643', 'Banten', 'Merak', 'Jawa', NULL, NULL),
(156, 156, '643', 'Banten', 'Merak', 'Jawa', NULL, NULL),
(157, 157, '643', 'Banten', 'Merak', 'Jawa', NULL, NULL),
(158, 158, '643', 'Banten', 'Merak', 'Jawa', NULL, NULL),
(159, 159, '643', 'Banten', 'Merak', 'Jawa', NULL, NULL),
(160, 160, '643', 'Banten', 'Merak', 'Jawa', NULL, NULL),
(161, 161, '643', 'Banten', 'Merak', 'Jawa', NULL, NULL),
(162, 162, '643', 'Banten', 'Merak', 'Jawa', NULL, NULL),
(163, 163, '643', 'Banten', 'Merak', 'Jawa', NULL, NULL),
(164, 164, '643', 'Banten', 'Merak', 'Jawa', NULL, NULL),
(165, 165, '26', 'Jawa Barat', 'Cirebon', 'Jawa', NULL, NULL),
(166, 166, '26', 'Jawa Barat', 'Cirebon', 'Jawa', NULL, NULL),
(167, 167, '94', 'Yogyakarta', 'Sleman', 'Yogyakarta', NULL, NULL),
(168, 168, '94', 'Yogyakarta', 'Sleman', 'Yogyakarta', NULL, NULL),
(169, 169, '94', 'Yogyakarta', 'Sleman', 'Yogyakarta', NULL, NULL),
(170, 170, '36', 'Jawa Tengah', 'Tegal', 'Jawa', NULL, NULL),
(171, 171, '29', 'Jawa Barat', 'Tasikmalaya', 'Jawa', NULL, NULL),
(172, 172, '36', 'Jawa Tengah', 'Tegal', 'Jawa', NULL, NULL),
(173, 173, '36', 'Jawa Tengah', 'Tegal', 'Jawa', NULL, NULL),
(174, 174, '36', 'Jawa Tengah', 'Tegal', 'Jawa', NULL, NULL),
(175, 175, '41', 'Jawa Timur', 'Surabaya', 'Jawa', NULL, NULL),
(176, 176, '41', 'Jawa Timur', 'Surabaya', 'Jawa', NULL, NULL),
(177, 177, '34', 'Jawa Tengah', 'Semarang', 'Jawa', NULL, NULL),
(178, 178, '23', 'Jawa Barat', 'Bekasi', 'Jawa', NULL, NULL),
(179, 179, '90', 'Yogyakarta', 'Yogyakarta', 'Jawa', NULL, NULL),
(180, 180, '41', 'Jawa Timur', 'Surabaya', 'Jawa', NULL, NULL),
(181, 181, '425', 'Jawa Tengah', 'Sragen', 'Jawa', NULL, NULL),
(182, 182, '90', 'Yogyakarta', 'Yogyakarta', 'Jawa', NULL, NULL),
(183, 183, '32', 'Jawa Tengah', 'Pekalongan', 'Jawa', NULL, NULL),
(184, 184, '39', 'Jawa Timur', 'Malang', 'Jawa', NULL, NULL),
(185, 185, '90', 'Yogyakarta', 'Yogyakarta', 'Jawa', NULL, NULL),
(186, 186, '39', 'Jawa Timur', 'Malang', 'Jawa', NULL, NULL),
(187, 187, '41', 'Jawa Timur', 'Surabaya', 'Jawa', NULL, NULL),
(188, 188, '90', 'Yogyakarta', 'Yogyakarta', 'Jawa', NULL, NULL),
(189, 189, '41', 'Jawa Timur', 'Surabaya', 'Jawa', NULL, NULL),
(190, 190, '41', 'Jawa Timur', 'Surabaya', 'Jawa', NULL, NULL),
(191, 191, '41', 'Jawa Timur', 'Surabaya', 'Jawa', NULL, NULL),
(192, 192, '608', 'Jawa Tengah', 'Solo', 'Jawa', NULL, NULL),
(193, 193, '41', 'Jawa Timur', 'Surabaya', 'Jawa', NULL, NULL),
(194, 194, '608', 'Jawa Tengah', 'Solo', 'Jawa', NULL, NULL),
(195, 195, '41', 'Jawa Timur', 'Surabaya', 'Jawa', NULL, NULL),
(196, 196, '398', 'Jawa Timur', 'Sumenep', 'Jawa', NULL, NULL),
(197, 197, '398', 'Jawa Timur', 'Sumenep', 'Jawa', NULL, NULL),
(198, 198, '423', 'Jawa Tengah', 'Rembang', 'Jawa', NULL, NULL),
(199, 199, '41', 'Jawa Timur', 'Surabaya', 'Jawa', NULL, NULL),
(200, 200, '423', 'Jawa Tengah', 'Rembang', 'Jawa', NULL, NULL),
(201, 201, '423', 'Jawa Tengah', 'Rembang', 'Jawa', NULL, NULL),
(202, 202, '690', 'Jawa Tengah', 'Lasem', 'Jawa', NULL, NULL),
(203, 203, '691', 'Jawa Timur', 'Lasem', 'Jawa', NULL, NULL),
(204, 204, '34', 'Jawa Tengah', 'Semarang', 'Jawa', NULL, NULL);

-- --------------------------------------------------------

--
-- Struktur dari tabel `u_user_order_rf`
--

CREATE TABLE `u_user_order_rf` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `id_u_user_order` bigint(20) UNSIGNED NOT NULL,
  `id_route` varchar(20) NOT NULL,
  `province` varchar(100) NOT NULL,
  `city` varchar(100) NOT NULL,
  `region` varchar(100) NOT NULL,
  `created_at` varchar(20) DEFAULT NULL,
  `updated_at` varchar(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `u_user_order_rf`
--

INSERT INTO `u_user_order_rf` (`id`, `id_u_user_order`, `id_route`, `province`, `city`, `region`, `created_at`, `updated_at`) VALUES
(1, 1, '18', 'Jakarta', 'Jakarta Timur', 'Jawa', NULL, NULL),
(2, 2, '18', 'Jakarta', 'Jakarta Timur', 'Jawa', NULL, NULL),
(3, 3, '18', 'Jakarta', 'Jakarta Timur', 'Jawa', NULL, NULL),
(4, 4, '18', 'Jakarta', 'Jakarta Timur', 'Jawa', NULL, NULL),
(5, 5, '18', 'Jakarta', 'Jakarta Timur', 'Jawa', NULL, NULL),
(6, 6, '18', 'Jakarta', 'Jakarta Timur', 'Jawa', NULL, NULL),
(7, 7, '18', 'Jakarta', 'Jakarta Timur', 'Jawa', NULL, NULL),
(8, 8, '18', 'Jakarta', 'Jakarta Timur', 'Jawa', NULL, NULL),
(9, 9, '18', 'Jakarta', 'Jakarta Timur', 'Jawa', NULL, NULL),
(10, 10, '18', 'Jakarta', 'Jakarta Timur', 'Jawa', NULL, NULL),
(11, 11, '434', 'Jawa Barat', 'Cibinong', 'Jawa', NULL, NULL),
(12, 12, '434', 'Jawa Barat', 'Cibinong', 'Jawa', NULL, NULL),
(13, 13, '1', 'Aceh', 'Banda Aceh', 'Sumatera', NULL, NULL),
(14, 14, '11', 'Banten', 'Tangerang Selatan', 'Jawa', NULL, NULL),
(15, 15, '1', 'Aceh', 'Banda Aceh', 'Sumatera', NULL, NULL),
(16, 16, '18', 'Jakarta', 'Jakarta Timur', 'Jawa', NULL, NULL),
(17, 17, '18', 'Jakarta', 'Jakarta Timur', 'Jawa', NULL, NULL),
(18, 18, '18', 'Jakarta', 'Jakarta Timur', 'Jawa', NULL, NULL),
(19, 19, '18', 'Jakarta', 'Jakarta Timur', 'Jawa', NULL, NULL),
(20, 20, '18', 'Jakarta', 'Jakarta Timur', 'Jawa', NULL, NULL),
(21, 21, '18', 'Jakarta', 'Jakarta Timur', 'Jawa', NULL, NULL),
(22, 22, '18', 'Jakarta', 'Jakarta Timur', 'Jawa', NULL, NULL),
(23, 23, '18', 'Jakarta', 'Jakarta Timur', 'Jawa', NULL, NULL),
(24, 24, '18', 'Jakarta', 'Jakarta Timur', 'Jawa', NULL, NULL),
(25, 25, '18', 'Jakarta', 'Jakarta Timur', 'Jawa', NULL, NULL),
(26, 26, '18', 'Jakarta', 'Jakarta Timur', 'Jawa', NULL, NULL),
(27, 27, '18', 'Jakarta', 'Jakarta Timur', 'Jawa', NULL, NULL),
(28, 28, '18', 'Jakarta', 'Jakarta Timur', 'Jawa', NULL, NULL),
(29, 29, '18', 'Jakarta', 'Jakarta Timur', 'Jawa', NULL, NULL),
(30, 30, '18', 'Jakarta', 'Jakarta Timur', 'Jawa', NULL, NULL),
(31, 31, '18', 'Jakarta', 'Jakarta Timur', 'Jawa', NULL, NULL),
(32, 32, '18', 'Jakarta', 'Jakarta Timur', 'Jawa', NULL, NULL),
(33, 33, '18', 'Jakarta', 'Jakarta Timur', 'Jawa', NULL, NULL),
(34, 34, '18', 'Jakarta', 'Jakarta Timur', 'Jawa', NULL, NULL),
(35, 35, '18', 'Jakarta', 'Jakarta Timur', 'Jawa', NULL, NULL),
(36, 36, '41', 'Jawa Timur', 'Surabaya', 'Jawa', NULL, NULL),
(37, 37, '18', 'Jakarta', 'Jakarta Timur', 'Jawa', NULL, NULL),
(38, 38, '18', 'Jakarta', 'Jakarta Timur', 'Jawa', NULL, NULL),
(39, 39, '18', 'Jakarta', 'Jakarta Timur', 'Jawa', NULL, NULL),
(40, 40, '18', 'Jakarta', 'Jakarta Timur', 'Jawa', NULL, NULL),
(41, 41, '18', 'Jakarta', 'Jakarta Timur', 'Jawa', NULL, NULL),
(42, 42, '18', 'Jakarta', 'Jakarta Timur', 'Jawa', NULL, NULL),
(43, 43, '18', 'Jakarta', 'Jakarta Timur', 'Jawa', NULL, NULL),
(44, 44, '18', 'Jakarta', 'Jakarta Timur', 'Jawa', NULL, NULL),
(45, 45, '18', 'Jakarta', 'Jakarta Timur', 'Jawa', NULL, NULL),
(46, 46, '18', 'Jakarta', 'Jakarta Timur', 'Jawa', NULL, NULL),
(47, 47, '18', 'Jakarta', 'Jakarta Timur', 'Jawa', NULL, NULL),
(48, 48, '18', 'Jakarta', 'Jakarta Timur', 'Jawa', NULL, NULL),
(49, 49, '18', 'Jakarta', 'Jakarta Timur', 'Jawa', NULL, NULL),
(50, 50, '18', 'Jakarta', 'Jakarta Timur', 'Jawa', NULL, NULL),
(51, 51, '19', 'Jakarta', 'Jakarta Utara', 'Jawa', NULL, NULL),
(52, 52, '15', 'Jakarta', 'Jakarta Barat', 'Jawa', NULL, NULL),
(53, 53, '15', 'Jakarta', 'Jakarta Barat', 'Jawa', NULL, NULL),
(54, 54, '15', 'Jakarta', 'Jakarta Barat', 'Jawa', NULL, NULL),
(55, 55, '18', 'Jakarta', 'Jakarta Timur', 'Jawa', NULL, NULL),
(56, 56, '18', 'Jakarta', 'Jakarta Timur', 'Jawa', NULL, NULL),
(57, 57, '18', 'Jakarta', 'Jakarta Timur', 'Jawa', NULL, NULL),
(58, 58, '608', 'Jawa Tengah', 'Solo', 'Jawa', NULL, NULL),
(59, 59, '608', 'Jawa Tengah', 'Solo', 'Jawa', NULL, NULL),
(60, 60, '18', 'Jakarta', 'Jakarta Timur', 'Jawa', NULL, NULL),
(61, 61, '18', 'Jakarta', 'Jakarta Timur', 'Jawa', NULL, NULL),
(62, 62, '18', 'Jakarta', 'Jakarta Timur', 'Jawa', NULL, NULL),
(63, 63, '18', 'Jakarta', 'Jakarta Timur', 'Jawa', NULL, NULL),
(64, 64, '18', 'Jakarta', 'Jakarta Timur', 'Jawa', NULL, NULL),
(65, 65, '18', 'Jakarta', 'Jakarta Timur', 'Jawa', NULL, NULL),
(66, 66, '18', 'Jakarta', 'Jakarta Timur', 'Jawa', NULL, NULL),
(67, 67, '18', 'Jakarta', 'Jakarta Timur', 'Jawa', NULL, NULL),
(68, 68, '18', 'Jakarta', 'Jakarta Timur', 'Jawa', NULL, NULL),
(69, 69, '18', 'Jakarta', 'Jakarta Timur', 'Jawa', NULL, NULL),
(70, 70, '18', 'Jakarta', 'Jakarta Timur', 'Jawa', NULL, NULL),
(71, 71, '18', 'Jakarta', 'Jakarta Timur', 'Jawa', NULL, NULL),
(72, 72, '18', 'Jakarta', 'Jakarta Timur', 'Jawa', NULL, NULL),
(73, 73, '18', 'Jakarta', 'Jakarta Timur', 'Jawa', NULL, NULL),
(74, 74, '18', 'Jakarta', 'Jakarta Timur', 'Jawa', NULL, NULL),
(75, 75, '18', 'Jakarta', 'Jakarta Timur', 'Jawa', NULL, NULL),
(76, 76, '18', 'Jakarta', 'Jakarta Timur', 'Jawa', NULL, NULL),
(77, 77, '18', 'Jakarta', 'Jakarta Timur', 'Jawa', NULL, NULL),
(78, 78, '18', 'Jakarta', 'Jakarta Timur', 'Jawa', NULL, NULL),
(79, 79, '18', 'Jakarta', 'Jakarta Timur', 'Jawa', NULL, NULL),
(80, 80, '18', 'Jakarta', 'Jakarta Timur', 'Jawa', NULL, NULL),
(81, 81, '18', 'Jakarta', 'Jakarta Timur', 'Jawa', NULL, NULL),
(82, 82, '18', 'Jakarta', 'Jakarta Timur', 'Jawa', NULL, NULL),
(83, 83, '18', 'Jakarta', 'Jakarta Timur', 'Jawa', NULL, NULL),
(84, 84, '18', 'Jakarta', 'Jakarta Timur', 'Jawa', NULL, NULL),
(85, 85, '18', 'Jakarta', 'Jakarta Timur', 'Jawa', NULL, NULL),
(86, 86, '18', 'Jakarta', 'Jakarta Timur', 'Jawa', NULL, NULL),
(87, 87, '18', 'Jakarta', 'Jakarta Timur', 'Jawa', NULL, NULL),
(88, 88, '18', 'Jakarta', 'Jakarta Timur', 'Jawa', NULL, NULL),
(89, 89, '18', 'Jakarta', 'Jakarta Timur', 'Jawa', NULL, NULL),
(90, 90, '18', 'Jakarta', 'Jakarta Timur', 'Jawa', NULL, NULL),
(91, 91, '18', 'Jakarta', 'Jakarta Timur', 'Jawa', NULL, NULL),
(92, 92, '18', 'Jakarta', 'Jakarta Timur', 'Jawa', NULL, NULL),
(93, 93, '18', 'Jakarta', 'Jakarta Timur', 'Jawa', NULL, NULL),
(94, 94, '18', 'Jakarta', 'Jakarta Timur', 'Jawa', NULL, NULL),
(95, 95, '18', 'Jakarta', 'Jakarta Timur', 'Jawa', NULL, NULL),
(96, 96, '18', 'Jakarta', 'Jakarta Timur', 'Jawa', NULL, NULL),
(97, 97, '18', 'Jakarta', 'Jakarta Timur', 'Jawa', NULL, NULL),
(98, 98, '18', 'Jakarta', 'Jakarta Timur', 'Jawa', NULL, NULL),
(99, 99, '18', 'Jakarta', 'Jakarta Timur', 'Jawa', NULL, NULL),
(100, 100, '18', 'Jakarta', 'Jakarta Timur', 'Jawa', NULL, NULL),
(101, 101, '18', 'Jakarta', 'Jakarta Timur', 'Jawa', NULL, NULL),
(102, 102, '18', 'Jakarta', 'Jakarta Timur', 'Jawa', NULL, NULL),
(103, 103, '18', 'Jakarta', 'Jakarta Timur', 'Jawa', NULL, NULL),
(104, 104, '18', 'Jakarta', 'Jakarta Timur', 'Jawa', NULL, NULL),
(105, 105, '643', 'Banten', 'Merak', 'Jawa', NULL, NULL),
(106, 106, '18', 'Jakarta', 'Jakarta Timur', 'Jawa', NULL, NULL),
(107, 107, '18', 'Jakarta', 'Jakarta Timur', 'Jawa', NULL, NULL),
(108, 108, '18', 'Jakarta', 'Jakarta Timur', 'Jawa', NULL, NULL),
(109, 109, '18', 'Jakarta', 'Jakarta Timur', 'Jawa', NULL, NULL),
(110, 110, '18', 'Jakarta', 'Jakarta Timur', 'Jawa', NULL, NULL),
(111, 111, '18', 'Jakarta', 'Jakarta Timur', 'Jawa', NULL, NULL),
(112, 112, '18', 'Jakarta', 'Jakarta Timur', 'Jawa', NULL, NULL),
(113, 113, '18', 'Jakarta', 'Jakarta Timur', 'Jawa', NULL, NULL),
(114, 114, '18', 'Jakarta', 'Jakarta Timur', 'Jawa', NULL, NULL),
(115, 115, '18', 'Jakarta', 'Jakarta Timur', 'Jawa', NULL, NULL),
(116, 116, '18', 'Jakarta', 'Jakarta Timur', 'Jawa', NULL, NULL),
(117, 117, '18', 'Jakarta', 'Jakarta Timur', 'Jawa', NULL, NULL),
(118, 118, '18', 'Jakarta', 'Jakarta Timur', 'Jawa', NULL, NULL),
(119, 119, '18', 'Jakarta', 'Jakarta Timur', 'Jawa', NULL, NULL),
(120, 120, '18', 'Jakarta', 'Jakarta Timur', 'Jawa', NULL, NULL),
(121, 121, '18', 'Jakarta', 'Jakarta Timur', 'Jawa', NULL, NULL),
(122, 122, '18', 'Jakarta', 'Jakarta Timur', 'Jawa', NULL, NULL),
(123, 123, '18', 'Jakarta', 'Jakarta Timur', 'Jawa', NULL, NULL),
(124, 124, '17', 'Jakarta', 'Jakarta Selatan', 'Jawa', NULL, NULL),
(125, 125, '18', 'Jakarta', 'Jakarta Timur', 'Jawa', NULL, NULL),
(126, 126, '18', 'Jakarta', 'Jakarta Timur', 'Jawa', NULL, NULL),
(127, 127, '18', 'Jakarta', 'Jakarta Timur', 'Jawa', NULL, NULL),
(128, 128, '18', 'Jakarta', 'Jakarta Timur', 'Jawa', NULL, NULL),
(129, 129, '18', 'Jakarta', 'Jakarta Timur', 'Jawa', NULL, NULL),
(130, 130, '18', 'Jakarta', 'Jakarta Timur', 'Jawa', NULL, NULL),
(131, 131, '18', 'Jakarta', 'Jakarta Timur', 'Jawa', NULL, NULL),
(132, 132, '18', 'Jakarta', 'Jakarta Timur', 'Jawa', NULL, NULL),
(133, 133, '18', 'Jakarta', 'Jakarta Timur', 'Jawa', NULL, NULL),
(134, 134, '18', 'Jakarta', 'Jakarta Timur', 'Jawa', NULL, NULL),
(135, 135, '18', 'Jakarta', 'Jakarta Timur', 'Jawa', NULL, NULL),
(136, 136, '18', 'Jakarta', 'Jakarta Timur', 'Jawa', NULL, NULL),
(137, 137, '18', 'Jakarta', 'Jakarta Timur', 'Jawa', NULL, NULL),
(138, 138, '18', 'Jakarta', 'Jakarta Timur', 'Jawa', NULL, NULL),
(139, 139, '18', 'Jakarta', 'Jakarta Timur', 'Jawa', NULL, NULL),
(140, 140, '18', 'Jakarta', 'Jakarta Timur', 'Jawa', NULL, NULL),
(141, 141, '18', 'Jakarta', 'Jakarta Timur', 'Jawa', NULL, NULL),
(142, 142, '18', 'Jakarta', 'Jakarta Timur', 'Jawa', NULL, NULL),
(143, 143, '18', 'Jakarta', 'Jakarta Timur', 'Jawa', NULL, NULL),
(144, 144, '18', 'Jakarta', 'Jakarta Timur', 'Jawa', NULL, NULL),
(145, 145, '18', 'Jakarta', 'Jakarta Timur', 'Jawa', NULL, NULL),
(146, 146, '18', 'Jakarta', 'Jakarta Timur', 'Jawa', NULL, NULL),
(147, 147, '18', 'Jakarta', 'Jakarta Timur', 'Jawa', NULL, NULL),
(148, 148, '18', 'Jakarta', 'Jakarta Timur', 'Jawa', NULL, NULL),
(149, 149, '18', 'Jakarta', 'Jakarta Timur', 'Jawa', NULL, NULL),
(150, 150, '18', 'Jakarta', 'Jakarta Timur', 'Jawa', NULL, NULL),
(151, 151, '18', 'Jakarta', 'Jakarta Timur', 'Jawa', NULL, NULL),
(152, 152, '18', 'Jakarta', 'Jakarta Timur', 'Jawa', NULL, NULL),
(153, 153, '18', 'Jakarta', 'Jakarta Timur', 'Jawa', NULL, NULL),
(154, 154, '18', 'Jakarta', 'Jakarta Timur', 'Jawa', NULL, NULL),
(155, 155, '18', 'Jakarta', 'Jakarta Timur', 'Jawa', NULL, NULL),
(156, 156, '18', 'Jakarta', 'Jakarta Timur', 'Jawa', NULL, NULL),
(157, 157, '18', 'Jakarta', 'Jakarta Timur', 'Jawa', NULL, NULL),
(158, 158, '18', 'Jakarta', 'Jakarta Timur', 'Jawa', NULL, NULL),
(159, 159, '18', 'Jakarta', 'Jakarta Timur', 'Jawa', NULL, NULL),
(160, 160, '18', 'Jakarta', 'Jakarta Timur', 'Jawa', NULL, NULL),
(161, 161, '18', 'Jakarta', 'Jakarta Timur', 'Jawa', NULL, NULL),
(162, 162, '18', 'Jakarta', 'Jakarta Timur', 'Jawa', NULL, NULL),
(163, 163, '18', 'Jakarta', 'Jakarta Timur', 'Jawa', NULL, NULL),
(164, 164, '18', 'Jakarta', 'Jakarta Timur', 'Jawa', NULL, NULL),
(165, 165, '18', 'Jakarta', 'Jakarta Timur', 'Jawa', NULL, NULL),
(166, 166, '18', 'Jakarta', 'Jakarta Timur', 'Jawa', NULL, NULL),
(167, 167, '18', 'Jakarta', 'Jakarta Timur', 'Jawa', NULL, NULL),
(168, 168, '18', 'Jakarta', 'Jakarta Timur', 'Jawa', NULL, NULL),
(169, 169, '18', 'Jakarta', 'Jakarta Timur', 'Jawa', NULL, NULL),
(170, 170, '18', 'Jakarta', 'Jakarta Timur', 'Jawa', NULL, NULL),
(171, 171, '18', 'Jakarta', 'Jakarta Timur', 'Jawa', NULL, NULL),
(172, 172, '18', 'Jakarta', 'Jakarta Timur', 'Jawa', NULL, NULL),
(173, 173, '18', 'Jakarta', 'Jakarta Timur', 'Jawa', NULL, NULL),
(174, 174, '18', 'Jakarta', 'Jakarta Timur', 'Jawa', NULL, NULL),
(175, 175, '18', 'Jakarta', 'Jakarta Timur', 'Jawa', NULL, NULL),
(176, 176, '18', 'Jakarta', 'Jakarta Timur', 'Jawa', NULL, NULL),
(177, 177, '18', 'Jakarta', 'Jakarta Timur', 'Jawa', NULL, NULL),
(178, 178, '10', 'Banten', 'Serang', 'Jawa', NULL, NULL),
(179, 179, '18', 'Jakarta', 'Jakarta Timur', 'Jawa', NULL, NULL),
(180, 180, '18', 'Jakarta', 'Jakarta Timur', 'Jawa', NULL, NULL),
(181, 181, '18', 'Jakarta', 'Jakarta Timur', 'Jawa', NULL, NULL),
(182, 182, '18', 'Jakarta', 'Jakarta Timur', 'Jawa', NULL, NULL),
(183, 183, '18', 'Jakarta', 'Jakarta Timur', 'Jawa', NULL, NULL),
(184, 184, '18', 'Jakarta', 'Jakarta Timur', 'Jawa', NULL, NULL),
(185, 185, '18', 'Jakarta', 'Jakarta Timur', 'Jawa', NULL, NULL),
(186, 186, '18', 'Jakarta', 'Jakarta Timur', 'Jawa', NULL, NULL),
(187, 187, '18', 'Jakarta', 'Jakarta Timur', 'Jawa', NULL, NULL),
(188, 188, '16', 'Jakarta', 'Jakarta Pusat', 'Jawa', NULL, NULL),
(189, 189, '18', 'Jakarta', 'Jakarta Timur', 'Jawa', NULL, NULL),
(190, 190, '18', 'Jakarta', 'Jakarta Timur', 'Jawa', NULL, NULL),
(191, 191, '18', 'Jakarta', 'Jakarta Timur', 'Jawa', NULL, NULL),
(192, 192, '18', 'Jakarta', 'Jakarta Timur', 'Jawa', NULL, NULL),
(193, 193, '18', 'Jakarta', 'Jakarta Timur', 'Jawa', NULL, NULL),
(194, 194, '18', 'Jakarta', 'Jakarta Timur', 'Jawa', NULL, NULL),
(195, 195, '18', 'Jakarta', 'Jakarta Timur', 'Jawa', NULL, NULL),
(196, 196, '18', 'Jakarta', 'Jakarta Timur', 'Jawa', NULL, NULL),
(197, 197, '18', 'Jakarta', 'Jakarta Timur', 'Jawa', NULL, NULL),
(198, 198, '23', 'Jawa Barat', 'Bekasi', 'Jawa', NULL, NULL),
(199, 199, '18', 'Jakarta', 'Jakarta Timur', 'Jawa', NULL, NULL),
(200, 200, '18', 'Jakarta', 'Jakarta Timur', 'Jawa', NULL, NULL),
(201, 201, '18', 'Jakarta', 'Jakarta Timur', 'Jawa', NULL, NULL),
(202, 202, '18', 'Jakarta', 'Jakarta Timur', 'Jawa', NULL, NULL),
(203, 203, '18', 'Jakarta', 'Jakarta Timur', 'Jawa', NULL, NULL),
(204, 204, '18', 'Jakarta', 'Jakarta Timur', 'Jawa', NULL, NULL);

-- --------------------------------------------------------

--
-- Struktur dari tabel `u_user_order_sb`
--

CREATE TABLE `u_user_order_sb` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `id_u_user_order` bigint(20) UNSIGNED NOT NULL,
  `date` varchar(20) NOT NULL,
  `created_at` varchar(20) DEFAULT NULL,
  `updated_at` varchar(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struktur dari tabel `u_user_order_sg`
--

CREATE TABLE `u_user_order_sg` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `id_u_user_order` bigint(20) UNSIGNED NOT NULL,
  `date` varchar(20) NOT NULL,
  `created_at` varchar(20) DEFAULT NULL,
  `updated_at` varchar(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `u_user_order_sg`
--

INSERT INTO `u_user_order_sg` (`id`, `id_u_user_order`, `date`, `created_at`, `updated_at`) VALUES
(1, 1, '2020-08-19 ', NULL, NULL),
(2, 2, '2020-08-19 ', NULL, NULL),
(3, 3, '2020-08-19 ', NULL, NULL),
(4, 4, '2020-08-27 ', NULL, NULL),
(5, 5, '2020-08-27 ', NULL, NULL),
(6, 6, '2020-08-27 ', NULL, NULL),
(7, 7, '2020-08-31 ', NULL, NULL),
(8, 8, '2020-08-31 ', NULL, NULL),
(9, 9, '2020-09-01 ', NULL, NULL),
(10, 10, '2020-08-31 ', NULL, NULL),
(11, 11, '2020-09-25 ', NULL, NULL),
(12, 12, '2020-09-24 ', NULL, NULL),
(13, 13, '2020-09-05 ', NULL, NULL),
(14, 14, '2020-09-05 ', NULL, NULL),
(15, 15, '2020-09-05 ', NULL, NULL),
(16, 16, '2020-09-05 ', NULL, NULL),
(17, 17, '2020-09-04 ', NULL, NULL),
(18, 18, '2020-09-05 ', NULL, NULL),
(19, 19, '2020-09-08 ', NULL, NULL),
(20, 20, '2020-09-08 ', NULL, NULL),
(21, 21, '2020-09-08 ', NULL, NULL),
(22, 22, '2020-09-09 ', NULL, NULL),
(23, 23, '2020-09-08 ', NULL, NULL),
(24, 24, '2020-09-08 ', NULL, NULL),
(25, 25, '2020-09-11 ', NULL, NULL),
(26, 26, '2020-09-11 ', NULL, NULL),
(27, 27, '2020-09-16 ', NULL, NULL),
(28, 28, '2020-09-16 ', NULL, NULL),
(29, 29, '2020-09-18 ', NULL, NULL),
(30, 30, '2020-09-20 ', NULL, NULL),
(31, 31, '2020-10-06 ', NULL, NULL),
(32, 32, '2020-10-06 ', NULL, NULL),
(33, 33, '2020-10-06 ', NULL, NULL),
(34, 34, '2020-10-06 ', NULL, NULL),
(35, 35, '2020-10-07 ', NULL, NULL),
(36, 36, '2020-10-10 ', NULL, NULL),
(37, 37, '2020-10-10 ', NULL, NULL),
(38, 38, '2020-10-07 ', NULL, NULL),
(39, 39, '2020-10-07 ', NULL, NULL),
(40, 40, '2020-10-07 ', NULL, NULL),
(41, 41, '2020-10-07 ', NULL, NULL),
(42, 42, '2020-10-07 ', NULL, NULL),
(43, 43, '2020-10-07 ', NULL, NULL),
(44, 44, '2020-10-07 ', NULL, NULL),
(45, 45, '2020-10-08 ', NULL, NULL),
(46, 46, '2020-10-08 ', NULL, NULL),
(47, 47, '2020-10-08 ', NULL, NULL),
(48, 48, '2020-10-08 ', NULL, NULL),
(49, 49, '2020-10-09 ', NULL, NULL),
(50, 50, '2020-10-09 ', NULL, NULL),
(51, 51, '2020-10-09 ', NULL, NULL),
(52, 52, '2020-10-09 ', NULL, NULL),
(53, 53, '2020-10-09 ', NULL, NULL),
(54, 54, '2020-10-09 ', NULL, NULL),
(55, 55, '2020-10-08 ', NULL, NULL),
(56, 56, '2020-10-08 ', NULL, NULL),
(57, 57, '2020-10-08 ', NULL, NULL),
(58, 58, '2020-10-08 ', NULL, NULL),
(59, 59, '2020-10-08 ', NULL, NULL),
(60, 60, '2020-10-08 ', NULL, NULL),
(61, 61, '2020-10-10 ', NULL, NULL),
(62, 62, '2020-10-08 ', NULL, NULL),
(63, 63, '2020-10-09 ', NULL, NULL),
(64, 64, '2020-10-08 ', NULL, NULL),
(65, 65, '2020-10-08 ', NULL, NULL),
(66, 66, '2020-10-09 ', NULL, NULL),
(67, 67, '2020-10-09 ', NULL, NULL),
(68, 68, '2020-10-09 ', NULL, NULL),
(69, 69, '2020-10-09 ', NULL, NULL),
(70, 70, '2020-10-09 ', NULL, NULL),
(71, 71, '2020-10-09 ', NULL, NULL),
(72, 72, '2020-10-09 ', NULL, NULL),
(73, 73, '2020-10-09 ', NULL, NULL),
(74, 74, '2020-10-09 ', NULL, NULL),
(75, 75, '2020-10-09 ', NULL, NULL),
(76, 76, '2020-10-09 ', NULL, NULL),
(77, 77, '2020-10-09 ', NULL, NULL),
(78, 78, '2020-10-09 ', NULL, NULL),
(79, 79, '2020-10-09 ', NULL, NULL),
(80, 80, '2020-10-09 ', NULL, NULL),
(81, 81, '2020-10-09 ', NULL, NULL),
(82, 82, '2020-10-12 ', NULL, NULL),
(83, 83, '2020-10-09 ', NULL, NULL),
(84, 84, '2020-10-16 ', NULL, NULL),
(85, 85, '2020-10-16 ', NULL, NULL),
(86, 86, '2020-10-09 ', NULL, NULL),
(87, 87, '2020-10-09 ', NULL, NULL),
(88, 88, '2020-10-09 ', NULL, NULL),
(89, 89, '2020-10-09 ', NULL, NULL),
(90, 90, '2020-10-09 ', NULL, NULL),
(91, 91, '2020-10-09 ', NULL, NULL),
(92, 92, '2020-10-09 ', NULL, NULL),
(93, 93, '2020-10-09 ', NULL, NULL),
(94, 94, '2020-10-09 ', NULL, NULL),
(95, 95, '2020-10-09 ', NULL, NULL),
(96, 96, '2020-10-09 ', NULL, NULL),
(97, 97, '2020-10-09 ', NULL, NULL),
(98, 98, '2020-10-09 ', NULL, NULL),
(99, 99, '2020-10-09 ', NULL, NULL),
(100, 100, '2020-10-09 ', NULL, NULL),
(101, 101, '2020-10-09 ', NULL, NULL),
(102, 102, '2020-10-10 ', NULL, NULL),
(103, 103, '2020-10-10 ', NULL, NULL),
(104, 104, '2020-10-10 ', NULL, NULL),
(105, 105, '2020-10-09 ', NULL, NULL),
(106, 106, '2020-10-11 ', NULL, NULL),
(107, 107, '2020-10-11 ', NULL, NULL),
(108, 108, '2020-10-11 ', NULL, NULL),
(109, 109, '2020-10-11 ', NULL, NULL),
(110, 110, '2020-10-11 ', NULL, NULL),
(111, 111, '2020-10-10 ', NULL, NULL),
(112, 112, '2020-10-12 ', NULL, NULL),
(113, 113, '2020-10-12 ', NULL, NULL),
(114, 114, '2020-10-12 ', NULL, NULL),
(115, 115, '2020-10-13 ', NULL, NULL),
(116, 116, '2020-10-10 ', NULL, NULL),
(117, 117, '2020-10-10 ', NULL, NULL),
(118, 118, '2020-10-10 ', NULL, NULL),
(119, 119, '2020-10-10 ', NULL, NULL),
(120, 120, '2020-10-11 ', NULL, NULL),
(121, 121, '2020-10-10 ', NULL, NULL),
(122, 122, '2020-10-10 ', NULL, NULL),
(123, 123, '2020-10-10 ', NULL, NULL),
(124, 124, '2020-10-10 ', NULL, NULL),
(125, 125, '2020-10-10 ', NULL, NULL),
(126, 126, '2020-10-10 ', NULL, NULL),
(127, 127, '2020-10-10 ', NULL, NULL),
(128, 128, '2020-10-10 ', NULL, NULL),
(129, 129, '2020-10-10 ', NULL, NULL),
(130, 130, '2020-10-10 ', NULL, NULL),
(131, 131, '2020-10-10 ', NULL, NULL),
(132, 132, '2020-10-11 ', NULL, NULL),
(133, 133, '2020-10-10 ', NULL, NULL),
(134, 134, '2020-10-11 ', NULL, NULL),
(135, 135, '2020-10-11 ', NULL, NULL),
(136, 136, '2020-10-10 ', NULL, NULL),
(137, 137, '2020-10-10 ', NULL, NULL),
(138, 138, '2020-10-10 ', NULL, NULL),
(139, 139, '2020-10-10 ', NULL, NULL),
(140, 140, '2020-10-12 ', NULL, NULL),
(141, 141, '2020-10-11 ', NULL, NULL),
(142, 142, '2020-10-11 ', NULL, NULL),
(143, 143, '2020-10-11 ', NULL, NULL),
(144, 144, '2020-10-10 ', NULL, NULL),
(145, 145, '2020-10-11 ', NULL, NULL),
(146, 146, '2020-10-11 ', NULL, NULL),
(147, 147, '2020-10-10 ', NULL, NULL),
(148, 148, '2020-10-11 ', NULL, NULL),
(149, 149, '2020-10-11 ', NULL, NULL),
(150, 150, '2020-10-10 ', NULL, NULL),
(151, 151, '2020-10-11 ', NULL, NULL),
(152, 152, '2020-10-10 ', NULL, NULL),
(153, 153, '2020-10-12 ', NULL, NULL),
(154, 154, '2020-10-11 ', NULL, NULL),
(155, 155, '2020-10-12 ', NULL, NULL),
(156, 156, '2020-10-13 ', NULL, NULL),
(157, 157, '2020-10-10 ', NULL, NULL),
(158, 158, '2020-10-11 ', NULL, NULL),
(159, 159, '2020-10-11 ', NULL, NULL),
(160, 160, '2020-10-11 ', NULL, NULL),
(161, 161, '2020-10-13 ', NULL, NULL),
(162, 162, '2020-10-11 ', NULL, NULL),
(163, 163, '2020-10-11 ', NULL, NULL),
(164, 164, '2020-10-10 ', NULL, NULL),
(165, 165, '2020-10-12 ', NULL, NULL),
(166, 166, '2020-10-12 ', NULL, NULL),
(167, 167, '2020-10-22 ', NULL, NULL),
(168, 168, '2020-10-22 ', NULL, NULL),
(169, 169, '2020-10-22 ', NULL, NULL),
(170, 170, '2020-10-22 ', NULL, NULL),
(171, 171, '2020-10-10 ', NULL, NULL),
(172, 172, '2020-10-22 ', NULL, NULL),
(173, 173, '2020-10-30 ', NULL, NULL),
(174, 174, '2020-10-26 ', NULL, NULL),
(175, 175, '2020-10-26 ', NULL, NULL),
(176, 176, '2020-10-23 ', NULL, NULL),
(177, 177, '2020-10-26 ', NULL, NULL),
(178, 178, '2020-10-11 ', NULL, NULL),
(179, 179, '2020-10-11 ', NULL, NULL),
(180, 180, '2020-10-11 ', NULL, NULL),
(181, 181, '2020-10-11 ', NULL, NULL),
(182, 182, '2020-10-11 ', NULL, NULL),
(183, 183, '2020-10-11 ', NULL, NULL),
(184, 184, '2020-10-11 ', NULL, NULL),
(185, 185, '2020-10-11 ', NULL, NULL),
(186, 186, '2020-10-11 ', NULL, NULL),
(187, 187, '2020-10-11 ', NULL, NULL),
(188, 188, '2020-10-14 ', NULL, NULL),
(189, 189, '2020-10-14 ', NULL, NULL),
(190, 190, '2020-10-14 ', NULL, NULL),
(191, 191, '2020-10-14 ', NULL, NULL),
(192, 192, '2020-10-11 ', NULL, NULL),
(193, 193, '2020-10-14 ', NULL, NULL),
(194, 194, '2020-10-10 ', NULL, NULL),
(195, 195, '2020-10-10 ', NULL, NULL),
(196, 196, '2020-10-30 ', NULL, NULL),
(197, 197, '2020-10-30 ', NULL, NULL),
(198, 198, '2020-10-12 ', NULL, NULL),
(199, 199, '2020-10-12 ', NULL, NULL),
(200, 200, '2020-10-12 ', NULL, NULL),
(201, 201, '2020-10-16 ', NULL, NULL),
(202, 202, '2020-10-16 ', NULL, NULL),
(203, 203, '2020-10-16 ', NULL, NULL),
(204, 204, '2020-10-16 ', NULL, NULL);

-- --------------------------------------------------------

--
-- Struktur dari tabel `u_user_order_ticket`
--

CREATE TABLE `u_user_order_ticket` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `id_m_users` int(10) UNSIGNED NOT NULL,
  `ticket_id` varchar(20) NOT NULL,
  `passanger_name` varchar(255) NOT NULL,
  `passanger_gender` varchar(1) DEFAULT 'm',
  `passanger_telp` varchar(20) NOT NULL,
  `pnr` varchar(100) NOT NULL,
  `seat_id` varchar(20) NOT NULL,
  `is_boarding` tinyint(1) DEFAULT 0,
  `date_of_boarding` varchar(20) DEFAULT NULL,
  `date_of_departure` varchar(20) NOT NULL,
  `time_of_departure` varchar(20) NOT NULL,
  `price` bigint(20) DEFAULT 0,
  `id_schedule` varchar(20) NOT NULL,
  `id_of_origin` varchar(20) NOT NULL,
  `destionation_id` varchar(20) NOT NULL,
  `category` varchar(20) NOT NULL,
  `po_name` varchar(100) NOT NULL,
  `destionation` varchar(100) NOT NULL,
  `origin` varchar(100) NOT NULL,
  `seat_number` varchar(20) NOT NULL,
  `created_at` varchar(20) DEFAULT NULL,
  `updated_at` varchar(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `u_user_order_ticket`
--

INSERT INTO `u_user_order_ticket` (`id`, `id_m_users`, `ticket_id`, `passanger_name`, `passanger_gender`, `passanger_telp`, `pnr`, `seat_id`, `is_boarding`, `date_of_boarding`, `date_of_departure`, `time_of_departure`, `price`, `id_schedule`, `id_of_origin`, `destionation_id`, `category`, `po_name`, `destionation`, `origin`, `seat_number`, `created_at`, `updated_at`) VALUES
(1, 1, '93147', 'Andy', '', '6282119189690', 'Andy1597830497972', '325', 0, NULL, '2020-08-19', '20:20:00', 280000, '25', '18', '41', 'VIP', 'HIDUP BARU PUTRA', 'Surabaya', 'Jakarta Timur', '5', '2020-08-19 09:48:20', '2020-08-19 09:48:20'),
(2, 4, '93148', 'Hakim', '', '6282213737535', 'Hakim1598841116779', '331', 0, NULL, '2020-08-31', '20:20:00', 280000, '37', '18', '41', 'VIP', 'HIDUP BARU PUTRA', 'Surabaya', 'Jakarta Timur', '11', '2020-08-31 02:30:01', '2020-08-31 02:30:01'),
(3, 4, '93149', 'Erick', '', '623986532136', 'Erick1599205774705', '448', 0, NULL, '2020-09-05', '20:00:00', 220000, '99', '18', '608', 'Big Top', 'RAYA', 'Solo', 'Jakarta Timur', '12', '2020-09-04 07:47:39', '2020-09-04 07:47:39'),
(4, 3, '93150', 'Taufan Bayu A.W', '', '62895324806198', 'Taufan Bayu A.W15994', '487', 0, NULL, '2020-09-08', '20:00:00', 230000, '137', '18', '608', 'VIP', 'RAYA', 'Solo', 'Jakarta Timur', '7', '2020-09-07 06:04:07', '2020-09-07 06:04:07'),
(5, 1, '93151', 'Andy Maulana', '', '6282119189690', 'Andy Maulana16019467', '56', 0, NULL, '2020-10-06', '09:00:00', 145000, '233', '18', '41', 'Executive', 'HIDUP BARU PUTRA', 'Surabaya', 'Jakarta Timur', '6', '2020-10-06 01:13:22', '2020-10-06 01:13:22'),
(6, 1, '93152', 'Andy Maulana Yusuf', '', '6282119189690', 'Andy Maulana Yusuf16', '57', 0, NULL, '2020-10-06', '09:00:00', 145000, '233', '18', '41', 'Executive', 'HIDUP BARU PUTRA', 'Surabaya', 'Jakarta Timur', '7', '2020-10-06 01:17:32', '2020-10-06 01:17:32'),
(7, 4, '93153', 'Hakim Top', '', '6282213737535', 'Hakim Top16019676963', '56', 0, NULL, '2020-10-07', '09:00:00', 145000, '234', '18', '41', 'Executive', 'HIDUP BARU PUTRA', 'Surabaya', 'Jakarta Timur', '6', '2020-10-06 06:59:27', '2020-10-06 06:59:27'),
(8, 4, '93154', 'Ahmad', '', '6282213737535', 'Ahmad1601968942235', '395', 0, NULL, '2020-10-07', '15:00:00', 60000, '333', '18', '643', 'AC Ekonomi', 'ARIMBI', 'Merak', 'Jakarta Timur', '10', '2020-10-06 07:20:09', '2020-10-06 07:20:09'),
(9, 7, '93155', 'Johanes', '', '6282213737535', 'Johanes1601971642135', '397', 0, NULL, '2020-10-07', '15:00:00', 60000, '333', '18', '643', 'AC Ekonomi', 'ARIMBI', 'Merak', 'Jakarta Timur', '12', '2020-10-06 08:05:09', '2020-10-06 08:05:09'),
(10, 8, '131767', 'sandy wardhana', '', '6281314630883', 'sandy wardhana160214', '492', 0, NULL, '2020-10-09', '12:00:00', 55000, '117', '18', '643', 'Ekonomi', 'ARIMBI', 'Merak', 'Jakarta Timur', '6', '2020-10-08 07:17:50', '2020-10-08 07:17:50'),
(11, 8, '131769', 'sandy wardhana', '', '6281314630883', 'sandy wardhana160214', '492', 0, NULL, '2020-10-08', '16:00:00', 55000, '166', '18', '643', 'Ekonomi', 'ARIMBI', 'Merak', 'Jakarta Timur', '6', '2020-10-08 08:19:42', '2020-10-08 08:19:42'),
(12, 8, '131770', 'sandy', '', '6281314630883', 'sandy1602148033696', '493', 0, NULL, '2020-10-08', '16:00:00', 55000, '166', '18', '643', 'Ekonomi', 'ARIMBI', 'Merak', 'Jakarta Timur', '7', '2020-10-08 09:07:19', '2020-10-08 09:07:19'),
(13, 12, '131784', 'Heryanto', '', '6285716461494', 'Heryanto160221038322', '493', 0, NULL, '2020-10-09', '14:00:00', 55000, '142', '18', '643', 'Ekonomi', 'ARIMBI', 'Merak', 'Jakarta Timur', '7', '2020-10-09 02:26:30', '2020-10-09 02:26:30'),
(14, 7, '131790', 'bernad pasaribu', '', '6281298270999', 'bernad pasaribu16022', '492', 0, NULL, '2020-10-09', '14:00:00', 55000, '142', '18', '643', 'Ekonomi', 'ARIMBI', 'Merak', 'Jakarta Timur', '6', '2020-10-09 03:46:56', '2020-10-09 03:46:56'),
(15, 7, '131791', 'bernad pasaribu', '', '6281298270999', 'bernad pasaribu16022', '495', 0, NULL, '2020-10-09', '12:00:00', 55000, '117', '18', '643', 'Ekonomi', 'ARIMBI', 'Merak', 'Jakarta Timur', '9', '2020-10-09 04:00:34', '2020-10-09 04:00:34'),
(16, 16, '131793', 'sugiono', '', '6281287779935', 'sugiono1602217244143', '495', 0, NULL, '2020-10-09', '14:00:00', 55000, '142', '18', '643', 'Ekonomi', 'ARIMBI', 'Merak', 'Jakarta Timur', '9', '2020-10-09 04:20:54', '2020-10-09 04:20:54'),
(17, 1, '131805', 'Andymul', '', '6282119189690', 'Andymul1602225493971', '537', 0, NULL, '2020-10-09', '16:00:00', 190000, '392', '18', '608', 'AC Ekonomi', 'RAYA', 'Solo', 'Jakarta Timur', '1', '2020-10-09 06:38:21', '2020-10-09 06:38:21'),
(18, 12, '132037', 'Jeryanto', '', '6285716461494', 'Jeryanto1602298219622', '493', 0, NULL, '2020-10-10', '12:00:00', 55000, '118', '18', '643', 'Ekonomi', 'ARIMBI', 'Merak', 'Jakarta Timur', '7', '2020-10-10 02:50:26', '2020-10-10 02:50:26'),
(19, 12, '132041', 'Heryanto', '', '6285716461494', 'Heryanto1602299201666', '497', 0, NULL, '2020-10-10', '12:00:00', 55000, '118', '18', '643', 'Ekonomi', 'ARIMBI', 'Merak', 'Jakarta Timur', '11', '2020-10-10 03:06:51', '2020-10-10 03:06:51'),
(20, 12, '132042', 'Haakim', '', '628121134512', 'Haakim1602299201666', '496', 0, NULL, '2020-10-10', '12:00:00', 55000, '118', '18', '643', 'Ekonomi', 'ARIMBI', 'Merak', 'Jakarta Timur', '10', '2020-10-10 03:06:51', '2020-10-10 03:06:51'),
(21, 4, '132046', 'Rudy', '', '628879454648787', 'Rudy1602306393534', '700', 0, NULL, '2020-10-11', '13:00:00', 55000, '566', '18', '643', 'Ekonomi', 'ASLI PRIMA', 'Merak', 'Jakarta Timur', '6', '2020-10-10 05:04:19', '2020-10-10 05:04:19'),
(22, 4, '132047', 'sandy', '', '62876464849444', 'sandy1602306393534', '701', 0, NULL, '2020-10-11', '13:00:00', 55000, '566', '18', '643', 'Ekonomi', 'ASLI PRIMA', 'Merak', 'Jakarta Timur', '7', '2020-10-10 05:04:19', '2020-10-10 05:04:19'),
(23, 24, '132053', 'test', '', '62895622899908', 'test1602312303958', '497', 0, NULL, '2020-10-11', '12:00:00', 55000, '119', '18', '643', 'Ekonomi', 'ARIMBI', 'Merak', 'Jakarta Timur', '11', '2020-10-10 06:45:12', '2020-10-10 06:45:12'),
(24, 4, '132056', 'Hakim', '', '628454648764879', 'Hakim1602314420236', '496', 0, NULL, '2020-10-11', '14:00:00', 55000, '144', '18', '643', 'Ekonomi', 'ARIMBI', 'Merak', 'Jakarta Timur', '10', '2020-10-10 07:18:05', '2020-10-10 07:18:05'),
(25, 24, '132063', 'Mirza', '', '628176685195', 'Mirza1602314691922', '492', 0, NULL, '2020-10-13', '14:00:00', 55000, '146', '18', '643', 'Ekonomi', 'ARIMBI', 'Merak', 'Jakarta Timur', '6', '2020-10-10 07:25:05', '2020-10-10 07:25:05'),
(26, 7, '132068', 'bernad pasaribu', '', '6281298270999', 'bernad pasaribu1602315369283', '395', 0, NULL, '2020-10-10', '15:00:00', 65000, '193', '18', '643', 'AC Ekonomi', 'ARIMBI', 'Merak', 'Jakarta Timur', '10', '2020-10-10 07:36:16', '2020-10-10 07:36:16');

-- --------------------------------------------------------

--
-- Struktur dari tabel `u_user_order_ticket_f`
--

CREATE TABLE `u_user_order_ticket_f` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `id_u_user_order_ticket` bigint(20) UNSIGNED NOT NULL,
  `facility_name` varchar(100) NOT NULL,
  `facility_icon` text DEFAULT NULL,
  `created_at` varchar(20) DEFAULT NULL,
  `updated_at` varchar(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `u_user_order_ticket_f`
--

INSERT INTO `u_user_order_ticket_f` (`id`, `id_u_user_order_ticket`, `facility_name`, `facility_icon`, `created_at`, `updated_at`) VALUES
(1, 1, 'AC', 'https://pulogebang.niagasolution.com/public/uploads/fasilitas/1597214802.png', NULL, NULL),
(2, 1, 'Kursi Berbaring', 'https://pulogebang.niagasolution.com/public/uploads/fasilitas/kursi.png', NULL, NULL),
(3, 1, 'Toilet', 'https://pulogebang.niagasolution.com/public/uploads/fasilitas/toilet.png', NULL, NULL),
(4, 1, 'Wifi', 'https://pulogebang.niagasolution.com/public/uploads/fasilitas/1578107367.png', NULL, NULL),
(5, 1, 'Listrik', 'https://pulogebang.niagasolution.com/public/uploads/fasilitas/1578107253.png', NULL, NULL),
(6, 1, 'Smoking area', 'https://pulogebang.niagasolution.com/public/uploads/fasilitas/1580379153.png', NULL, NULL),
(7, 1, 'Non AC', 'https://pulogebang.niagasolution.com/public/uploads/fasilitas/1597214836.jpg', NULL, NULL),
(8, 1, 'Servis Makan', 'https://pulogebang.niagasolution.com/public/uploads/fasilitas/1597214908.png', NULL, NULL),
(9, 1, 'Bantal', 'https://pulogebang.niagasolution.com/public/uploads/fasilitas/1597214918.png', NULL, NULL),
(10, 1, 'Selimut', 'https://pulogebang.niagasolution.com/public/uploads/fasilitas/1597214928.png', NULL, NULL),
(11, 2, 'AC', 'https://pulogebang.niagasolution.com/public/uploads/fasilitas/1597214802.png', NULL, NULL),
(12, 2, 'Kursi Berbaring', 'https://pulogebang.niagasolution.com/public/uploads/fasilitas/kursi.png', NULL, NULL),
(13, 2, 'Toilet', 'https://pulogebang.niagasolution.com/public/uploads/fasilitas/toilet.png', NULL, NULL),
(14, 2, 'Wifi', 'https://pulogebang.niagasolution.com/public/uploads/fasilitas/1578107367.png', NULL, NULL),
(15, 2, 'Listrik', 'https://pulogebang.niagasolution.com/public/uploads/fasilitas/1578107253.png', NULL, NULL),
(16, 2, 'Smoking area', 'https://pulogebang.niagasolution.com/public/uploads/fasilitas/1580379153.png', NULL, NULL),
(17, 2, 'Non AC', 'https://pulogebang.niagasolution.com/public/uploads/fasilitas/1597214836.jpg', NULL, NULL),
(18, 2, 'Servis Makan', 'https://pulogebang.niagasolution.com/public/uploads/fasilitas/1597214908.png', NULL, NULL),
(19, 2, 'Bantal', 'https://pulogebang.niagasolution.com/public/uploads/fasilitas/1597214918.png', NULL, NULL),
(20, 2, 'Selimut', 'https://pulogebang.niagasolution.com/public/uploads/fasilitas/1597214928.png', NULL, NULL),
(21, 3, 'AC', 'https://pulogebang.niagasolution.com/public/uploads/fasilitas/1597214802.png', NULL, NULL),
(22, 3, 'Kursi Berbaring', 'https://pulogebang.niagasolution.com/public/uploads/fasilitas/kursi.png', NULL, NULL),
(23, 3, 'Toilet', 'https://pulogebang.niagasolution.com/public/uploads/fasilitas/toilet.png', NULL, NULL),
(24, 3, 'Wifi', 'https://pulogebang.niagasolution.com/public/uploads/fasilitas/1578107367.png', NULL, NULL),
(25, 3, 'Listrik', 'https://pulogebang.niagasolution.com/public/uploads/fasilitas/1578107253.png', NULL, NULL),
(26, 3, 'Smoking area', 'https://pulogebang.niagasolution.com/public/uploads/fasilitas/1580379153.png', NULL, NULL),
(27, 3, 'Non AC', 'https://pulogebang.niagasolution.com/public/uploads/fasilitas/1597214836.jpg', NULL, NULL),
(28, 3, 'Servis Makan', 'https://pulogebang.niagasolution.com/public/uploads/fasilitas/1597214908.png', NULL, NULL),
(29, 3, 'Bantal', 'https://pulogebang.niagasolution.com/public/uploads/fasilitas/1597214918.png', NULL, NULL),
(30, 3, 'Selimut', 'https://pulogebang.niagasolution.com/public/uploads/fasilitas/1597214928.png', NULL, NULL),
(31, 4, 'AC', 'https://pulogebang.niagasolution.com/public/uploads/fasilitas/1597214802.png', NULL, NULL),
(32, 4, 'Kursi Berbaring', 'https://pulogebang.niagasolution.com/public/uploads/fasilitas/kursi.png', NULL, NULL),
(33, 4, 'Toilet', 'https://pulogebang.niagasolution.com/public/uploads/fasilitas/toilet.png', NULL, NULL),
(34, 4, 'Wifi', 'https://pulogebang.niagasolution.com/public/uploads/fasilitas/1578107367.png', NULL, NULL),
(35, 4, 'Listrik', 'https://pulogebang.niagasolution.com/public/uploads/fasilitas/1578107253.png', NULL, NULL),
(36, 4, 'Smoking area', 'https://pulogebang.niagasolution.com/public/uploads/fasilitas/1580379153.png', NULL, NULL),
(37, 4, 'Non AC', 'https://pulogebang.niagasolution.com/public/uploads/fasilitas/1597214836.jpg', NULL, NULL),
(38, 4, 'Servis Makan', 'https://pulogebang.niagasolution.com/public/uploads/fasilitas/1597214908.png', NULL, NULL),
(39, 4, 'Bantal', 'https://pulogebang.niagasolution.com/public/uploads/fasilitas/1597214918.png', NULL, NULL),
(40, 4, 'Selimut', 'https://pulogebang.niagasolution.com/public/uploads/fasilitas/1597214928.png', NULL, NULL),
(41, 5, 'AC', 'https://pulogebang.niagasolution.com/public/uploads/fasilitas/1597214802.png', NULL, NULL),
(42, 5, 'Kursi Berbaring', 'https://pulogebang.niagasolution.com/public/uploads/fasilitas/kursi.png', NULL, NULL),
(43, 5, 'Toilet', 'https://pulogebang.niagasolution.com/public/uploads/fasilitas/toilet.png', NULL, NULL),
(44, 5, 'Wifi', 'https://pulogebang.niagasolution.com/public/uploads/fasilitas/1578107367.png', NULL, NULL),
(45, 5, 'Listrik', 'https://pulogebang.niagasolution.com/public/uploads/fasilitas/1578107253.png', NULL, NULL),
(46, 5, 'Smoking area', 'https://pulogebang.niagasolution.com/public/uploads/fasilitas/1580379153.png', NULL, NULL),
(47, 5, 'Non AC', 'https://pulogebang.niagasolution.com/public/uploads/fasilitas/1597214836.jpg', NULL, NULL),
(48, 5, 'Servis Makan', 'https://pulogebang.niagasolution.com/public/uploads/fasilitas/1597214908.png', NULL, NULL),
(49, 5, 'Bantal', 'https://pulogebang.niagasolution.com/public/uploads/fasilitas/1597214918.png', NULL, NULL),
(50, 5, 'Selimut', 'https://pulogebang.niagasolution.com/public/uploads/fasilitas/1597214928.png', NULL, NULL),
(51, 6, 'AC', 'https://pulogebang.niagasolution.com/public/uploads/fasilitas/1597214802.png', NULL, NULL),
(52, 6, 'Kursi Berbaring', 'https://pulogebang.niagasolution.com/public/uploads/fasilitas/kursi.png', NULL, NULL),
(53, 6, 'Toilet', 'https://pulogebang.niagasolution.com/public/uploads/fasilitas/toilet.png', NULL, NULL),
(54, 6, 'Wifi', 'https://pulogebang.niagasolution.com/public/uploads/fasilitas/1578107367.png', NULL, NULL),
(55, 6, 'Listrik', 'https://pulogebang.niagasolution.com/public/uploads/fasilitas/1578107253.png', NULL, NULL),
(56, 6, 'Smoking area', 'https://pulogebang.niagasolution.com/public/uploads/fasilitas/1580379153.png', NULL, NULL),
(57, 6, 'Non AC', 'https://pulogebang.niagasolution.com/public/uploads/fasilitas/1597214836.jpg', NULL, NULL),
(58, 6, 'Servis Makan', 'https://pulogebang.niagasolution.com/public/uploads/fasilitas/1597214908.png', NULL, NULL),
(59, 6, 'Bantal', 'https://pulogebang.niagasolution.com/public/uploads/fasilitas/1597214918.png', NULL, NULL),
(60, 6, 'Selimut', 'https://pulogebang.niagasolution.com/public/uploads/fasilitas/1597214928.png', NULL, NULL),
(61, 7, 'AC', 'https://pulogebang.niagasolution.com/public/uploads/fasilitas/1597214802.png', NULL, NULL),
(62, 7, 'Kursi Berbaring', 'https://pulogebang.niagasolution.com/public/uploads/fasilitas/kursi.png', NULL, NULL),
(63, 7, 'Toilet', 'https://pulogebang.niagasolution.com/public/uploads/fasilitas/toilet.png', NULL, NULL),
(64, 7, 'Wifi', 'https://pulogebang.niagasolution.com/public/uploads/fasilitas/1578107367.png', NULL, NULL),
(65, 7, 'Listrik', 'https://pulogebang.niagasolution.com/public/uploads/fasilitas/1578107253.png', NULL, NULL),
(66, 7, 'Smoking area', 'https://pulogebang.niagasolution.com/public/uploads/fasilitas/1580379153.png', NULL, NULL),
(67, 7, 'Non AC', 'https://pulogebang.niagasolution.com/public/uploads/fasilitas/1597214836.jpg', NULL, NULL),
(68, 7, 'Servis Makan', 'https://pulogebang.niagasolution.com/public/uploads/fasilitas/1597214908.png', NULL, NULL),
(69, 7, 'Bantal', 'https://pulogebang.niagasolution.com/public/uploads/fasilitas/1597214918.png', NULL, NULL),
(70, 7, 'Selimut', 'https://pulogebang.niagasolution.com/public/uploads/fasilitas/1597214928.png', NULL, NULL),
(71, 8, 'AC', 'https://pulogebang.niagasolution.com/public/uploads/fasilitas/1597214802.png', NULL, NULL),
(72, 8, 'Kursi Berbaring', 'https://pulogebang.niagasolution.com/public/uploads/fasilitas/kursi.png', NULL, NULL),
(73, 8, 'Toilet', 'https://pulogebang.niagasolution.com/public/uploads/fasilitas/toilet.png', NULL, NULL),
(74, 8, 'Wifi', 'https://pulogebang.niagasolution.com/public/uploads/fasilitas/1578107367.png', NULL, NULL),
(75, 8, 'Listrik', 'https://pulogebang.niagasolution.com/public/uploads/fasilitas/1578107253.png', NULL, NULL),
(76, 8, 'Smoking area', 'https://pulogebang.niagasolution.com/public/uploads/fasilitas/1580379153.png', NULL, NULL),
(77, 8, 'Non AC', 'https://pulogebang.niagasolution.com/public/uploads/fasilitas/1597214836.jpg', NULL, NULL),
(78, 8, 'Servis Makan', 'https://pulogebang.niagasolution.com/public/uploads/fasilitas/1597214908.png', NULL, NULL),
(79, 8, 'Bantal', 'https://pulogebang.niagasolution.com/public/uploads/fasilitas/1597214918.png', NULL, NULL),
(80, 8, 'Selimut', 'https://pulogebang.niagasolution.com/public/uploads/fasilitas/1597214928.png', NULL, NULL),
(81, 9, 'AC', 'https://pulogebang.niagasolution.com/public/uploads/fasilitas/1597214802.png', NULL, NULL),
(82, 9, 'Kursi Berbaring', 'https://pulogebang.niagasolution.com/public/uploads/fasilitas/kursi.png', NULL, NULL),
(83, 9, 'Toilet', 'https://pulogebang.niagasolution.com/public/uploads/fasilitas/toilet.png', NULL, NULL),
(84, 9, 'Wifi', 'https://pulogebang.niagasolution.com/public/uploads/fasilitas/1578107367.png', NULL, NULL),
(85, 9, 'Listrik', 'https://pulogebang.niagasolution.com/public/uploads/fasilitas/1578107253.png', NULL, NULL),
(86, 9, 'Smoking area', 'https://pulogebang.niagasolution.com/public/uploads/fasilitas/1580379153.png', NULL, NULL),
(87, 9, 'Non AC', 'https://pulogebang.niagasolution.com/public/uploads/fasilitas/1597214836.jpg', NULL, NULL),
(88, 9, 'Servis Makan', 'https://pulogebang.niagasolution.com/public/uploads/fasilitas/1597214908.png', NULL, NULL),
(89, 9, 'Bantal', 'https://pulogebang.niagasolution.com/public/uploads/fasilitas/1597214918.png', NULL, NULL),
(90, 9, 'Selimut', 'https://pulogebang.niagasolution.com/public/uploads/fasilitas/1597214928.png', NULL, NULL),
(91, 10, 'AC', 'https://pulogebang.jaketbus.id/public/uploads/fasilitas/1582341719.png', NULL, NULL),
(92, 10, 'Kursi Berbaring', 'https://pulogebang.jaketbus.id/public/uploads/fasilitas/kursi.png', NULL, NULL),
(93, 10, 'Toilet', 'https://pulogebang.jaketbus.id/public/uploads/fasilitas/toilet.png', NULL, NULL),
(94, 10, 'Wifi', 'https://pulogebang.jaketbus.id/public/uploads/fasilitas/1578107367.png', NULL, NULL),
(95, 10, 'Listrik', 'https://pulogebang.jaketbus.id/public/uploads/fasilitas/1578107253.png', NULL, NULL),
(96, 10, 'Smoking area', 'https://pulogebang.jaketbus.id/public/uploads/fasilitas/1580379153.png', NULL, NULL),
(97, 10, 'Non AC', 'https://pulogebang.jaketbus.id/public/uploads/fasilitas/1581676374.JPG', NULL, NULL),
(98, 10, 'Servis Makan', 'https://pulogebang.jaketbus.id/public/uploads/fasilitas/1596627640.png', NULL, NULL),
(99, 10, 'Bantal', 'https://pulogebang.jaketbus.id/public/uploads/fasilitas/1596627475.png', NULL, NULL),
(100, 10, 'Selimut', 'https://pulogebang.jaketbus.id/public/uploads/fasilitas/1582341594.png', NULL, NULL),
(101, 11, 'AC', 'https://pulogebang.jaketbus.id/public/uploads/fasilitas/1582341719.png', NULL, NULL),
(102, 11, 'Kursi Berbaring', 'https://pulogebang.jaketbus.id/public/uploads/fasilitas/kursi.png', NULL, NULL),
(103, 11, 'Toilet', 'https://pulogebang.jaketbus.id/public/uploads/fasilitas/toilet.png', NULL, NULL),
(104, 11, 'Wifi', 'https://pulogebang.jaketbus.id/public/uploads/fasilitas/1578107367.png', NULL, NULL),
(105, 11, 'Listrik', 'https://pulogebang.jaketbus.id/public/uploads/fasilitas/1578107253.png', NULL, NULL),
(106, 11, 'Smoking area', 'https://pulogebang.jaketbus.id/public/uploads/fasilitas/1580379153.png', NULL, NULL),
(107, 11, 'Non AC', 'https://pulogebang.jaketbus.id/public/uploads/fasilitas/1581676374.JPG', NULL, NULL),
(108, 11, 'Servis Makan', 'https://pulogebang.jaketbus.id/public/uploads/fasilitas/1596627640.png', NULL, NULL),
(109, 11, 'Bantal', 'https://pulogebang.jaketbus.id/public/uploads/fasilitas/1596627475.png', NULL, NULL),
(110, 11, 'Selimut', 'https://pulogebang.jaketbus.id/public/uploads/fasilitas/1582341594.png', NULL, NULL),
(111, 12, 'AC', 'https://pulogebang.jaketbus.id/public/uploads/fasilitas/1582341719.png', NULL, NULL),
(112, 12, 'Kursi Berbaring', 'https://pulogebang.jaketbus.id/public/uploads/fasilitas/kursi.png', NULL, NULL),
(113, 12, 'Toilet', 'https://pulogebang.jaketbus.id/public/uploads/fasilitas/toilet.png', NULL, NULL),
(114, 12, 'Wifi', 'https://pulogebang.jaketbus.id/public/uploads/fasilitas/1578107367.png', NULL, NULL),
(115, 12, 'Listrik', 'https://pulogebang.jaketbus.id/public/uploads/fasilitas/1578107253.png', NULL, NULL),
(116, 12, 'Smoking area', 'https://pulogebang.jaketbus.id/public/uploads/fasilitas/1580379153.png', NULL, NULL),
(117, 12, 'Non AC', 'https://pulogebang.jaketbus.id/public/uploads/fasilitas/1581676374.JPG', NULL, NULL),
(118, 12, 'Servis Makan', 'https://pulogebang.jaketbus.id/public/uploads/fasilitas/1596627640.png', NULL, NULL),
(119, 12, 'Bantal', 'https://pulogebang.jaketbus.id/public/uploads/fasilitas/1596627475.png', NULL, NULL),
(120, 12, 'Selimut', 'https://pulogebang.jaketbus.id/public/uploads/fasilitas/1582341594.png', NULL, NULL),
(121, 13, 'AC', 'https://pulogebang.jaketbus.id/public/uploads/fasilitas/1582341719.png', NULL, NULL),
(122, 13, 'Kursi Berbaring', 'https://pulogebang.jaketbus.id/public/uploads/fasilitas/kursi.png', NULL, NULL),
(123, 13, 'Toilet', 'https://pulogebang.jaketbus.id/public/uploads/fasilitas/toilet.png', NULL, NULL),
(124, 13, 'Wifi', 'https://pulogebang.jaketbus.id/public/uploads/fasilitas/1578107367.png', NULL, NULL),
(125, 13, 'Listrik', 'https://pulogebang.jaketbus.id/public/uploads/fasilitas/1578107253.png', NULL, NULL),
(126, 13, 'Smoking area', 'https://pulogebang.jaketbus.id/public/uploads/fasilitas/1580379153.png', NULL, NULL),
(127, 13, 'Non AC', 'https://pulogebang.jaketbus.id/public/uploads/fasilitas/1581676374.JPG', NULL, NULL),
(128, 13, 'Servis Makan', 'https://pulogebang.jaketbus.id/public/uploads/fasilitas/1596627640.png', NULL, NULL),
(129, 13, 'Bantal', 'https://pulogebang.jaketbus.id/public/uploads/fasilitas/1596627475.png', NULL, NULL),
(130, 13, 'Selimut', 'https://pulogebang.jaketbus.id/public/uploads/fasilitas/1582341594.png', NULL, NULL),
(131, 14, 'AC', 'https://pulogebang.jaketbus.id/public/uploads/fasilitas/1582341719.png', NULL, NULL),
(132, 14, 'Kursi Berbaring', 'https://pulogebang.jaketbus.id/public/uploads/fasilitas/kursi.png', NULL, NULL),
(133, 14, 'Toilet', 'https://pulogebang.jaketbus.id/public/uploads/fasilitas/toilet.png', NULL, NULL),
(134, 14, 'Wifi', 'https://pulogebang.jaketbus.id/public/uploads/fasilitas/1578107367.png', NULL, NULL),
(135, 14, 'Listrik', 'https://pulogebang.jaketbus.id/public/uploads/fasilitas/1578107253.png', NULL, NULL),
(136, 14, 'Smoking area', 'https://pulogebang.jaketbus.id/public/uploads/fasilitas/1580379153.png', NULL, NULL),
(137, 14, 'Non AC', 'https://pulogebang.jaketbus.id/public/uploads/fasilitas/1581676374.JPG', NULL, NULL),
(138, 14, 'Servis Makan', 'https://pulogebang.jaketbus.id/public/uploads/fasilitas/1596627640.png', NULL, NULL),
(139, 14, 'Bantal', 'https://pulogebang.jaketbus.id/public/uploads/fasilitas/1596627475.png', NULL, NULL),
(140, 14, 'Selimut', 'https://pulogebang.jaketbus.id/public/uploads/fasilitas/1582341594.png', NULL, NULL),
(141, 15, 'AC', 'https://pulogebang.jaketbus.id/public/uploads/fasilitas/1582341719.png', NULL, NULL),
(142, 15, 'Kursi Berbaring', 'https://pulogebang.jaketbus.id/public/uploads/fasilitas/kursi.png', NULL, NULL),
(143, 15, 'Toilet', 'https://pulogebang.jaketbus.id/public/uploads/fasilitas/toilet.png', NULL, NULL),
(144, 15, 'Wifi', 'https://pulogebang.jaketbus.id/public/uploads/fasilitas/1578107367.png', NULL, NULL),
(145, 15, 'Listrik', 'https://pulogebang.jaketbus.id/public/uploads/fasilitas/1578107253.png', NULL, NULL),
(146, 15, 'Smoking area', 'https://pulogebang.jaketbus.id/public/uploads/fasilitas/1580379153.png', NULL, NULL),
(147, 15, 'Non AC', 'https://pulogebang.jaketbus.id/public/uploads/fasilitas/1581676374.JPG', NULL, NULL),
(148, 15, 'Servis Makan', 'https://pulogebang.jaketbus.id/public/uploads/fasilitas/1596627640.png', NULL, NULL),
(149, 15, 'Bantal', 'https://pulogebang.jaketbus.id/public/uploads/fasilitas/1596627475.png', NULL, NULL),
(150, 15, 'Selimut', 'https://pulogebang.jaketbus.id/public/uploads/fasilitas/1582341594.png', NULL, NULL),
(151, 16, 'AC', 'https://pulogebang.jaketbus.id/public/uploads/fasilitas/1582341719.png', NULL, NULL),
(152, 16, 'Kursi Berbaring', 'https://pulogebang.jaketbus.id/public/uploads/fasilitas/kursi.png', NULL, NULL),
(153, 16, 'Toilet', 'https://pulogebang.jaketbus.id/public/uploads/fasilitas/toilet.png', NULL, NULL),
(154, 16, 'Wifi', 'https://pulogebang.jaketbus.id/public/uploads/fasilitas/1578107367.png', NULL, NULL),
(155, 16, 'Listrik', 'https://pulogebang.jaketbus.id/public/uploads/fasilitas/1578107253.png', NULL, NULL),
(156, 16, 'Smoking area', 'https://pulogebang.jaketbus.id/public/uploads/fasilitas/1580379153.png', NULL, NULL),
(157, 16, 'Non AC', 'https://pulogebang.jaketbus.id/public/uploads/fasilitas/1581676374.JPG', NULL, NULL),
(158, 16, 'Servis Makan', 'https://pulogebang.jaketbus.id/public/uploads/fasilitas/1596627640.png', NULL, NULL),
(159, 16, 'Bantal', 'https://pulogebang.jaketbus.id/public/uploads/fasilitas/1596627475.png', NULL, NULL),
(160, 16, 'Selimut', 'https://pulogebang.jaketbus.id/public/uploads/fasilitas/1582341594.png', NULL, NULL),
(161, 17, 'AC', 'https://pulogebang.jaketbus.id/public/uploads/fasilitas/1582341719.png', NULL, NULL),
(162, 17, 'Kursi Berbaring', 'https://pulogebang.jaketbus.id/public/uploads/fasilitas/kursi.png', NULL, NULL),
(163, 17, 'Toilet', 'https://pulogebang.jaketbus.id/public/uploads/fasilitas/toilet.png', NULL, NULL),
(164, 17, 'Wifi', 'https://pulogebang.jaketbus.id/public/uploads/fasilitas/1578107367.png', NULL, NULL),
(165, 17, 'Listrik', 'https://pulogebang.jaketbus.id/public/uploads/fasilitas/1578107253.png', NULL, NULL),
(166, 17, 'Smoking area', 'https://pulogebang.jaketbus.id/public/uploads/fasilitas/1580379153.png', NULL, NULL),
(167, 17, 'Non AC', 'https://pulogebang.jaketbus.id/public/uploads/fasilitas/1581676374.JPG', NULL, NULL),
(168, 17, 'Servis Makan', 'https://pulogebang.jaketbus.id/public/uploads/fasilitas/1596627640.png', NULL, NULL),
(169, 17, 'Bantal', 'https://pulogebang.jaketbus.id/public/uploads/fasilitas/1596627475.png', NULL, NULL),
(170, 17, 'Selimut', 'https://pulogebang.jaketbus.id/public/uploads/fasilitas/1582341594.png', NULL, NULL),
(171, 18, 'AC', 'https://pulogebang.niagasolution.com/public/uploads/fasilitas/1582341719.png', NULL, NULL),
(172, 18, 'Kursi Berbaring', 'https://pulogebang.niagasolution.com/public/uploads/fasilitas/kursi.png', NULL, NULL),
(173, 18, 'Toilet', 'https://pulogebang.niagasolution.com/public/uploads/fasilitas/toilet.png', NULL, NULL),
(174, 18, 'Wifi', 'https://pulogebang.niagasolution.com/public/uploads/fasilitas/1578107367.png', NULL, NULL),
(175, 18, 'Listrik', 'https://pulogebang.niagasolution.com/public/uploads/fasilitas/1578107253.png', NULL, NULL),
(176, 18, 'Smoking area', 'https://pulogebang.niagasolution.com/public/uploads/fasilitas/1580379153.png', NULL, NULL),
(177, 18, 'Non AC', 'https://pulogebang.niagasolution.com/public/uploads/fasilitas/1581676374.JPG', NULL, NULL),
(178, 18, 'Servis Makan', 'https://pulogebang.niagasolution.com/public/uploads/fasilitas/1596627640.png', NULL, NULL),
(179, 18, 'Bantal', 'https://pulogebang.niagasolution.com/public/uploads/fasilitas/1596627475.png', NULL, NULL),
(180, 18, 'Selimut', 'https://pulogebang.niagasolution.com/public/uploads/fasilitas/1582341594.png', NULL, NULL),
(181, 19, 'AC', 'https://pulogebang.niagasolution.com/public/uploads/fasilitas/1582341719.png', NULL, NULL),
(182, 19, 'Kursi Berbaring', 'https://pulogebang.niagasolution.com/public/uploads/fasilitas/kursi.png', NULL, NULL),
(183, 19, 'Toilet', 'https://pulogebang.niagasolution.com/public/uploads/fasilitas/toilet.png', NULL, NULL),
(184, 19, 'Wifi', 'https://pulogebang.niagasolution.com/public/uploads/fasilitas/1578107367.png', NULL, NULL),
(185, 19, 'Listrik', 'https://pulogebang.niagasolution.com/public/uploads/fasilitas/1578107253.png', NULL, NULL),
(186, 19, 'Smoking area', 'https://pulogebang.niagasolution.com/public/uploads/fasilitas/1580379153.png', NULL, NULL),
(187, 19, 'Non AC', 'https://pulogebang.niagasolution.com/public/uploads/fasilitas/1581676374.JPG', NULL, NULL),
(188, 19, 'Servis Makan', 'https://pulogebang.niagasolution.com/public/uploads/fasilitas/1596627640.png', NULL, NULL),
(189, 19, 'Bantal', 'https://pulogebang.niagasolution.com/public/uploads/fasilitas/1596627475.png', NULL, NULL),
(190, 19, 'Selimut', 'https://pulogebang.niagasolution.com/public/uploads/fasilitas/1582341594.png', NULL, NULL),
(191, 20, 'AC', 'https://pulogebang.niagasolution.com/public/uploads/fasilitas/1582341719.png', NULL, NULL),
(192, 20, 'Kursi Berbaring', 'https://pulogebang.niagasolution.com/public/uploads/fasilitas/kursi.png', NULL, NULL),
(193, 20, 'Toilet', 'https://pulogebang.niagasolution.com/public/uploads/fasilitas/toilet.png', NULL, NULL),
(194, 20, 'Wifi', 'https://pulogebang.niagasolution.com/public/uploads/fasilitas/1578107367.png', NULL, NULL),
(195, 20, 'Listrik', 'https://pulogebang.niagasolution.com/public/uploads/fasilitas/1578107253.png', NULL, NULL),
(196, 20, 'Smoking area', 'https://pulogebang.niagasolution.com/public/uploads/fasilitas/1580379153.png', NULL, NULL),
(197, 20, 'Non AC', 'https://pulogebang.niagasolution.com/public/uploads/fasilitas/1581676374.JPG', NULL, NULL),
(198, 20, 'Servis Makan', 'https://pulogebang.niagasolution.com/public/uploads/fasilitas/1596627640.png', NULL, NULL),
(199, 20, 'Bantal', 'https://pulogebang.niagasolution.com/public/uploads/fasilitas/1596627475.png', NULL, NULL),
(200, 20, 'Selimut', 'https://pulogebang.niagasolution.com/public/uploads/fasilitas/1582341594.png', NULL, NULL),
(201, 21, 'AC', 'https://pulogebang.niagasolution.com/public/uploads/fasilitas/1582341719.png', NULL, NULL),
(202, 21, 'Kursi Berbaring', 'https://pulogebang.niagasolution.com/public/uploads/fasilitas/kursi.png', NULL, NULL),
(203, 21, 'Toilet', 'https://pulogebang.niagasolution.com/public/uploads/fasilitas/toilet.png', NULL, NULL),
(204, 21, 'Wifi', 'https://pulogebang.niagasolution.com/public/uploads/fasilitas/1578107367.png', NULL, NULL),
(205, 21, 'Listrik', 'https://pulogebang.niagasolution.com/public/uploads/fasilitas/1578107253.png', NULL, NULL),
(206, 21, 'Smoking area', 'https://pulogebang.niagasolution.com/public/uploads/fasilitas/1580379153.png', NULL, NULL),
(207, 21, 'Non AC', 'https://pulogebang.niagasolution.com/public/uploads/fasilitas/1581676374.JPG', NULL, NULL),
(208, 21, 'Servis Makan', 'https://pulogebang.niagasolution.com/public/uploads/fasilitas/1596627640.png', NULL, NULL),
(209, 21, 'Bantal', 'https://pulogebang.niagasolution.com/public/uploads/fasilitas/1596627475.png', NULL, NULL),
(210, 21, 'Selimut', 'https://pulogebang.niagasolution.com/public/uploads/fasilitas/1582341594.png', NULL, NULL),
(211, 22, 'AC', 'https://pulogebang.niagasolution.com/public/uploads/fasilitas/1582341719.png', NULL, NULL),
(212, 22, 'Kursi Berbaring', 'https://pulogebang.niagasolution.com/public/uploads/fasilitas/kursi.png', NULL, NULL),
(213, 22, 'Toilet', 'https://pulogebang.niagasolution.com/public/uploads/fasilitas/toilet.png', NULL, NULL),
(214, 22, 'Wifi', 'https://pulogebang.niagasolution.com/public/uploads/fasilitas/1578107367.png', NULL, NULL),
(215, 22, 'Listrik', 'https://pulogebang.niagasolution.com/public/uploads/fasilitas/1578107253.png', NULL, NULL),
(216, 22, 'Smoking area', 'https://pulogebang.niagasolution.com/public/uploads/fasilitas/1580379153.png', NULL, NULL),
(217, 22, 'Non AC', 'https://pulogebang.niagasolution.com/public/uploads/fasilitas/1581676374.JPG', NULL, NULL),
(218, 22, 'Servis Makan', 'https://pulogebang.niagasolution.com/public/uploads/fasilitas/1596627640.png', NULL, NULL),
(219, 22, 'Bantal', 'https://pulogebang.niagasolution.com/public/uploads/fasilitas/1596627475.png', NULL, NULL),
(220, 22, 'Selimut', 'https://pulogebang.niagasolution.com/public/uploads/fasilitas/1582341594.png', NULL, NULL),
(221, 23, 'AC', 'https://pulogebang.niagasolution.com/public/uploads/fasilitas/1582341719.png', NULL, NULL),
(222, 23, 'Kursi Berbaring', 'https://pulogebang.niagasolution.com/public/uploads/fasilitas/kursi.png', NULL, NULL),
(223, 23, 'Toilet', 'https://pulogebang.niagasolution.com/public/uploads/fasilitas/toilet.png', NULL, NULL),
(224, 23, 'Wifi', 'https://pulogebang.niagasolution.com/public/uploads/fasilitas/1578107367.png', NULL, NULL),
(225, 23, 'Listrik', 'https://pulogebang.niagasolution.com/public/uploads/fasilitas/1578107253.png', NULL, NULL),
(226, 23, 'Smoking area', 'https://pulogebang.niagasolution.com/public/uploads/fasilitas/1580379153.png', NULL, NULL),
(227, 23, 'Non AC', 'https://pulogebang.niagasolution.com/public/uploads/fasilitas/1581676374.JPG', NULL, NULL),
(228, 23, 'Servis Makan', 'https://pulogebang.niagasolution.com/public/uploads/fasilitas/1596627640.png', NULL, NULL),
(229, 23, 'Bantal', 'https://pulogebang.niagasolution.com/public/uploads/fasilitas/1596627475.png', NULL, NULL),
(230, 23, 'Selimut', 'https://pulogebang.niagasolution.com/public/uploads/fasilitas/1582341594.png', NULL, NULL),
(231, 24, 'AC', 'https://pulogebang.niagasolution.com/public/uploads/fasilitas/1582341719.png', NULL, NULL),
(232, 24, 'Kursi Berbaring', 'https://pulogebang.niagasolution.com/public/uploads/fasilitas/kursi.png', NULL, NULL),
(233, 24, 'Toilet', 'https://pulogebang.niagasolution.com/public/uploads/fasilitas/toilet.png', NULL, NULL),
(234, 24, 'Wifi', 'https://pulogebang.niagasolution.com/public/uploads/fasilitas/1578107367.png', NULL, NULL),
(235, 24, 'Listrik', 'https://pulogebang.niagasolution.com/public/uploads/fasilitas/1578107253.png', NULL, NULL),
(236, 24, 'Smoking area', 'https://pulogebang.niagasolution.com/public/uploads/fasilitas/1580379153.png', NULL, NULL),
(237, 24, 'Non AC', 'https://pulogebang.niagasolution.com/public/uploads/fasilitas/1581676374.JPG', NULL, NULL),
(238, 24, 'Servis Makan', 'https://pulogebang.niagasolution.com/public/uploads/fasilitas/1596627640.png', NULL, NULL),
(239, 24, 'Bantal', 'https://pulogebang.niagasolution.com/public/uploads/fasilitas/1596627475.png', NULL, NULL),
(240, 24, 'Selimut', 'https://pulogebang.niagasolution.com/public/uploads/fasilitas/1582341594.png', NULL, NULL),
(241, 25, 'AC', 'https://pulogebang.niagasolution.com/public/uploads/fasilitas/1582341719.png', NULL, NULL),
(242, 25, 'Kursi Berbaring', 'https://pulogebang.niagasolution.com/public/uploads/fasilitas/kursi.png', NULL, NULL),
(243, 25, 'Toilet', 'https://pulogebang.niagasolution.com/public/uploads/fasilitas/toilet.png', NULL, NULL),
(244, 25, 'Wifi', 'https://pulogebang.niagasolution.com/public/uploads/fasilitas/1578107367.png', NULL, NULL),
(245, 25, 'Listrik', 'https://pulogebang.niagasolution.com/public/uploads/fasilitas/1578107253.png', NULL, NULL),
(246, 25, 'Smoking area', 'https://pulogebang.niagasolution.com/public/uploads/fasilitas/1580379153.png', NULL, NULL),
(247, 25, 'Non AC', 'https://pulogebang.niagasolution.com/public/uploads/fasilitas/1581676374.JPG', NULL, NULL),
(248, 25, 'Servis Makan', 'https://pulogebang.niagasolution.com/public/uploads/fasilitas/1596627640.png', NULL, NULL),
(249, 25, 'Bantal', 'https://pulogebang.niagasolution.com/public/uploads/fasilitas/1596627475.png', NULL, NULL),
(250, 25, 'Selimut', 'https://pulogebang.niagasolution.com/public/uploads/fasilitas/1582341594.png', NULL, NULL),
(251, 26, 'AC', 'https://pulogebang.niagasolution.com/public/uploads/fasilitas/1582341719.png', NULL, NULL),
(252, 26, 'Kursi Berbaring', 'https://pulogebang.niagasolution.com/public/uploads/fasilitas/kursi.png', NULL, NULL),
(253, 26, 'Toilet', 'https://pulogebang.niagasolution.com/public/uploads/fasilitas/toilet.png', NULL, NULL),
(254, 26, 'Wifi', 'https://pulogebang.niagasolution.com/public/uploads/fasilitas/1578107367.png', NULL, NULL),
(255, 26, 'Listrik', 'https://pulogebang.niagasolution.com/public/uploads/fasilitas/1578107253.png', NULL, NULL),
(256, 26, 'Smoking area', 'https://pulogebang.niagasolution.com/public/uploads/fasilitas/1580379153.png', NULL, NULL),
(257, 26, 'Non AC', 'https://pulogebang.niagasolution.com/public/uploads/fasilitas/1581676374.JPG', NULL, NULL),
(258, 26, 'Servis Makan', 'https://pulogebang.niagasolution.com/public/uploads/fasilitas/1596627640.png', NULL, NULL),
(259, 26, 'Bantal', 'https://pulogebang.niagasolution.com/public/uploads/fasilitas/1596627475.png', NULL, NULL),
(260, 26, 'Selimut', 'https://pulogebang.niagasolution.com/public/uploads/fasilitas/1582341594.png', NULL, NULL);

--
-- Indexes for dumped tables
--

--
-- Indeks untuk tabel `m_experiences`
--
ALTER TABLE `m_experiences`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_imu_m_experiences` (`id_m_users`),
  ADD KEY `fk_imm_m_experiences` (`id_m_medias`);

--
-- Indeks untuk tabel `m_features`
--
ALTER TABLE `m_features`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_imu_m_features` (`id_m_users`),
  ADD KEY `fk_imm_m_features` (`id_m_medias`);

--
-- Indeks untuk tabel `m_medias`
--
ALTER TABLE `m_medias`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `m_notification`
--
ALTER TABLE `m_notification`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_imu_m_notification` (`id_m_users`);

--
-- Indeks untuk tabel `m_pulo_gebang`
--
ALTER TABLE `m_pulo_gebang`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `m_tron_wallet`
--
ALTER TABLE `m_tron_wallet`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `m_users`
--
ALTER TABLE `m_users`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `u_user`
--
ALTER TABLE `u_user`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_imu_u_user` (`id_m_users`),
  ADD KEY `fk_if_u_user` (`id_face`);

--
-- Indeks untuk tabel `u_user_order`
--
ALTER TABLE `u_user_order`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_imu_u_user_order` (`id_m_users`);

--
-- Indeks untuk tabel `u_user_order_rd`
--
ALTER TABLE `u_user_order_rd`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_iuuo_u_user_order_rd` (`id_u_user_order`);

--
-- Indeks untuk tabel `u_user_order_rf`
--
ALTER TABLE `u_user_order_rf`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_iuuo_u_user_order_rf` (`id_u_user_order`);

--
-- Indeks untuk tabel `u_user_order_sb`
--
ALTER TABLE `u_user_order_sb`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_iuuo_u_user_order_sb` (`id_u_user_order`);

--
-- Indeks untuk tabel `u_user_order_sg`
--
ALTER TABLE `u_user_order_sg`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_iuuo_u_user_order_sg` (`id_u_user_order`);

--
-- Indeks untuk tabel `u_user_order_ticket`
--
ALTER TABLE `u_user_order_ticket`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_imu_u_user_order_ticket` (`id_m_users`);

--
-- Indeks untuk tabel `u_user_order_ticket_f`
--
ALTER TABLE `u_user_order_ticket_f`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_iusot_u_user_order_ticket_f` (`id_u_user_order_ticket`);

--
-- AUTO_INCREMENT untuk tabel yang dibuang
--

--
-- AUTO_INCREMENT untuk tabel `m_experiences`
--
ALTER TABLE `m_experiences`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT untuk tabel `m_features`
--
ALTER TABLE `m_features`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT untuk tabel `m_medias`
--
ALTER TABLE `m_medias`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=28;

--
-- AUTO_INCREMENT untuk tabel `m_notification`
--
ALTER TABLE `m_notification`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=98;

--
-- AUTO_INCREMENT untuk tabel `m_pulo_gebang`
--
ALTER TABLE `m_pulo_gebang`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT untuk tabel `m_tron_wallet`
--
ALTER TABLE `m_tron_wallet`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT untuk tabel `m_users`
--
ALTER TABLE `m_users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=43;

--
-- AUTO_INCREMENT untuk tabel `u_user`
--
ALTER TABLE `u_user`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=43;

--
-- AUTO_INCREMENT untuk tabel `u_user_order`
--
ALTER TABLE `u_user_order`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=205;

--
-- AUTO_INCREMENT untuk tabel `u_user_order_rd`
--
ALTER TABLE `u_user_order_rd`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=205;

--
-- AUTO_INCREMENT untuk tabel `u_user_order_rf`
--
ALTER TABLE `u_user_order_rf`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=205;

--
-- AUTO_INCREMENT untuk tabel `u_user_order_sb`
--
ALTER TABLE `u_user_order_sb`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT untuk tabel `u_user_order_sg`
--
ALTER TABLE `u_user_order_sg`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=205;

--
-- AUTO_INCREMENT untuk tabel `u_user_order_ticket`
--
ALTER TABLE `u_user_order_ticket`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=27;

--
-- AUTO_INCREMENT untuk tabel `u_user_order_ticket_f`
--
ALTER TABLE `u_user_order_ticket_f`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=261;

--
-- Ketidakleluasaan untuk tabel pelimpahan (Dumped Tables)
--

--
-- Ketidakleluasaan untuk tabel `m_experiences`
--
ALTER TABLE `m_experiences`
  ADD CONSTRAINT `fk_imm_m_experiences` FOREIGN KEY (`id_m_medias`) REFERENCES `m_medias` (`id`) ON DELETE NO ACTION ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_imu_m_experiences` FOREIGN KEY (`id_m_users`) REFERENCES `m_users` (`id`) ON DELETE NO ACTION ON UPDATE CASCADE;

--
-- Ketidakleluasaan untuk tabel `m_features`
--
ALTER TABLE `m_features`
  ADD CONSTRAINT `fk_imm_m_features` FOREIGN KEY (`id_m_medias`) REFERENCES `m_medias` (`id`) ON DELETE NO ACTION ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_imu_m_features` FOREIGN KEY (`id_m_users`) REFERENCES `m_users` (`id`) ON DELETE NO ACTION ON UPDATE CASCADE;

--
-- Ketidakleluasaan untuk tabel `m_notification`
--
ALTER TABLE `m_notification`
  ADD CONSTRAINT `fk_imu_m_notification` FOREIGN KEY (`id_m_users`) REFERENCES `m_users` (`id`) ON DELETE NO ACTION ON UPDATE CASCADE;

--
-- Ketidakleluasaan untuk tabel `u_user`
--
ALTER TABLE `u_user`
  ADD CONSTRAINT `fk_if_u_user` FOREIGN KEY (`id_face`) REFERENCES `m_medias` (`id`) ON DELETE NO ACTION ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_imu_u_user` FOREIGN KEY (`id_m_users`) REFERENCES `m_users` (`id`) ON DELETE NO ACTION ON UPDATE CASCADE;

--
-- Ketidakleluasaan untuk tabel `u_user_order`
--
ALTER TABLE `u_user_order`
  ADD CONSTRAINT `fk_imu_u_user_order` FOREIGN KEY (`id_m_users`) REFERENCES `m_users` (`id`) ON DELETE NO ACTION ON UPDATE CASCADE;

--
-- Ketidakleluasaan untuk tabel `u_user_order_rd`
--
ALTER TABLE `u_user_order_rd`
  ADD CONSTRAINT `fk_iuuo_u_user_order_rd` FOREIGN KEY (`id_u_user_order`) REFERENCES `u_user_order` (`id`) ON DELETE NO ACTION ON UPDATE CASCADE;

--
-- Ketidakleluasaan untuk tabel `u_user_order_rf`
--
ALTER TABLE `u_user_order_rf`
  ADD CONSTRAINT `fk_iuuo_u_user_order_rf` FOREIGN KEY (`id_u_user_order`) REFERENCES `u_user_order` (`id`) ON DELETE NO ACTION ON UPDATE CASCADE;

--
-- Ketidakleluasaan untuk tabel `u_user_order_sb`
--
ALTER TABLE `u_user_order_sb`
  ADD CONSTRAINT `fk_iuuo_u_user_order_sb` FOREIGN KEY (`id_u_user_order`) REFERENCES `u_user_order` (`id`) ON DELETE NO ACTION ON UPDATE CASCADE;

--
-- Ketidakleluasaan untuk tabel `u_user_order_sg`
--
ALTER TABLE `u_user_order_sg`
  ADD CONSTRAINT `fk_iuuo_u_user_order_sg` FOREIGN KEY (`id_u_user_order`) REFERENCES `u_user_order` (`id`) ON DELETE NO ACTION ON UPDATE CASCADE;

--
-- Ketidakleluasaan untuk tabel `u_user_order_ticket`
--
ALTER TABLE `u_user_order_ticket`
  ADD CONSTRAINT `fk_imu_u_user_order_ticket` FOREIGN KEY (`id_m_users`) REFERENCES `m_users` (`id`) ON DELETE NO ACTION ON UPDATE CASCADE;

--
-- Ketidakleluasaan untuk tabel `u_user_order_ticket_f`
--
ALTER TABLE `u_user_order_ticket_f`
  ADD CONSTRAINT `fk_iusot_u_user_order_ticket_f` FOREIGN KEY (`id_u_user_order_ticket`) REFERENCES `u_user_order_ticket` (`id`) ON DELETE NO ACTION ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
